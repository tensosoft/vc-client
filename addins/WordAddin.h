// WordAddin.h : WordAddin 的声明

#pragma once
#include "resource.h"       // 主符号
#include "addins_i.h"
#include <atlstr.h>
#import "C:\Program Files (x86)\Common Files\DESIGNER\MSADDNDR.DLL" raw_interfaces_only, raw_native_types, no_namespace, named_guids, auto_search
#import "C:\Program Files\Microsoft Office\OFFICE15\MSWORD.OLB" raw_interfaces_only,rename_namespace("MSWORD"),rename("ExitWindows","WordExitWindows"),rename("FindText","WordFindText") , named_guids
#import "..\refs\bo.dll" rename_namespace("BO") raw_interfaces_only 
using namespace MSWORD;

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif

extern _ATL_FUNC_INFO OnQuitInfo;
extern _ATL_FUNC_INFO OnDocumentChangeInfo;
extern _ATL_FUNC_INFO OnDocumentOpenInfo;

// WordAddin

class ATL_NO_VTABLE WordAddin :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<WordAddin, &CLSID_WordAddin>,
	public IDispatchImpl<IWordAddin, &IID_IWordAddin, &LIBID_addinsLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IDispatchImpl<_IDTExtensibility2, &__uuidof(_IDTExtensibility2), &LIBID_AddInDesignerObjects, /* wMajor = */ 1>,
	//public IDispatchImpl<ApplicationEvents2, &__uuidof(ApplicationEvents2), &LIBID_Word, /* wMajor = */ 8, /* wMinor = */ 3>,
	public IDispEventSimpleImpl<1,WordAddin,&__uuidof(MSWORD::ApplicationEvents2)>
{
public:
	WordAddin():m_spApp(NULL)
	{
		ZeroMemory(m_szUserName,sizeof(TCHAR)*STRLEN_NORMAL);
	}

	DECLARE_REGISTRY_RESOURCEID(IDR_WORDADDIN)

	BEGIN_SINK_MAP(WordAddin)
	SINK_ENTRY_INFO(/*nID =*/ 1, __uuidof(MSWORD::ApplicationEvents2), /*dispid =*/ 3, OnDocumentChange, &OnDocumentChangeInfo)
	SINK_ENTRY_INFO(/*nID =*/ 1, __uuidof(MSWORD::ApplicationEvents2), /*dispid =*/ 2, OnQuit, &OnQuitInfo)
	SINK_ENTRY_INFO(/*nID =*/ 1, __uuidof(MSWORD::ApplicationEvents2), /*dispid =*/ 4, OnDocumentOpen, &OnDocumentOpenInfo)
	END_SINK_MAP()

	BEGIN_COM_MAP(WordAddin)
		COM_INTERFACE_ENTRY(IWordAddin)
		COM_INTERFACE_ENTRY2(IDispatch, _IDTExtensibility2)
		COM_INTERFACE_ENTRY(_IDTExtensibility2)
	END_COM_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

private:
	CComPtr<MSWORD::_Application> m_spApp;		//Microsoft Word应用程序对象
	TCHAR m_szUserName[STRLEN_NORMAL];

	// _IDTExtensibility2 插件接口实现方法
public:
	STDMETHOD(OnConnection)(LPDISPATCH Application, ext_ConnectMode ConnectMode, LPDISPATCH AddInInst, SAFEARRAY * * custom)
	{
		CComQIPtr<_Application> spApp(Application);
		ATLASSERT(spApp);
		m_spApp = spApp;
		HRESULT hr = DispEventAdvise(m_spApp);
		if(FAILED(hr)) return hr;
		return S_OK;
	}
	STDMETHOD(OnDisconnection)(ext_DisconnectMode RemoveMode, SAFEARRAY * * custom)
	{
		if (! m_spApp) return S_OK;
		DispEventUnadvise(m_spApp);
		m_spApp.Release();
		m_spApp=NULL;
		return S_OK;
	}
	STDMETHOD(OnAddInsUpdate)(SAFEARRAY * * custom)
	{
		return E_NOTIMPL;
	}
	STDMETHOD(OnStartupComplete)(SAFEARRAY * * custom)
	{
		return E_NOTIMPL;
	}
	STDMETHOD(OnBeginShutdown)(SAFEARRAY * * custom)
	{
		return E_NOTIMPL;
	}

	// word应用程序事件处理
public:
	void __stdcall OnDocumentChange(){
		return;
	}
	void __stdcall OnQuit(){
		//设置为原始用户名
		if(m_spApp && _tcslen(this->m_szUserName)>0){
			BSTR bstrUserName=T2BSTR(m_szUserName);
			m_spApp->put_UserName(bstrUserName);
			FREE_SYS_STR(bstrUserName);
		}
		return ;
	}
	void __stdcall OnDocumentOpen(_Document* ptr){
		CComQIPtr<MSWORD::_Document> spDoc(ptr);	//当前打开的word文档
		
		MSWORD::WdDocumentType docType;
		HRESULT hr=spDoc->get_Type(&docType);
		//如果是模版那么不执行后续代码
		if (SUCCEEDED(hr) && docType==WdDocumentType::wdTypeTemplate){
			spDoc->put_Saved(VARIANT_TRUE);
			return ;
		}

		//记录原始用户名
		BSTR bstrUserName=NULL;
		if(m_spApp && SUCCEEDED(m_spApp->get_UserName(&bstrUserName))){
			_tcscpy_s(this->m_szUserName,bstrUserName);
			FREE_SYS_STR(bstrUserName);
		}

		BSTR unid=NULL;
		BSTR name=NULL;
		hr=spDoc->get_Name(&name);
		CComPtr<BO::ILaunchContext> lc;
		CComPtr<IDispatch> pDisp;
		if(SUCCEEDED(hr) && _tcslen(name)>32){
			CAtlString atlStr(name);
			atlStr.Truncate(32);
			CoInitialize(NULL);
			CComPtr<BO::ILaunchContextProvider> lcp;
			hr=CoCreateInstance(__uuidof(BO::LaunchContextProvider),
													NULL,
													CLSCTX_INPROC_SERVER,
													__uuidof(BO::ILaunchContextProvider),
													(void**)&lcp);
			if (FAILED(hr) || !lcp){
				ErrMsg(0,L"无法创建启动上下文提供对象！");
				return;
			}
			unid=atlStr.AllocSysString();
			hr=lcp->GetLaunchContext(unid,VARIANT_FALSE,(IDispatch**)&pDisp);
			FREE_SYS_STR(unid);
			
			//如果非办公系统集成程序打开的文档，那么不做处理退出
			if (FAILED(hr) || !pDisp) return;
			hr=pDisp->QueryInterface(_uuidof(BO::ILaunchContext),(void**)&lc);
			//设置代码模板
			if(lc){
				//设置新用户名
				BSTR bstrNewUserName=NULL;
				if(SUCCEEDED(lc->get_UserName(&bstrNewUserName))){
					if(m_spApp) m_spApp->put_UserName(bstrNewUserName);
					FREE_SYS_STR(bstrNewUserName);
				}
				//绑定代码模板
				BSTR bstrCodeTemplate=NULL;
				hr=lc->get_CodeTemplate(&bstrCodeTemplate);
				if(SUCCEEDED(hr) && PathFileExists(bstrCodeTemplate)){
					spDoc->put_UpdateStylesOnOpen(VARIANT_TRUE);
					VARIANT vCodeTemplate;
					vCodeTemplate.vt= VT_BSTR;
					vCodeTemplate.bstrVal=bstrCodeTemplate;
					spDoc->put_AttachedTemplate(&vCodeTemplate);
					SysFreeString(bstrCodeTemplate);
				}
			}
			CoUninitialize();
		}

		//运行业务功能入口宏
		VARIANT argDoc;
		IDispatch * dispDoc=NULL;

		VariantInit(&argDoc);
		argDoc.vt= VT_ERROR;
		argDoc.scode=DISP_E_PARAMNOTFOUND;
		if(SUCCEEDED(spDoc->QueryInterface(IID_IDispatch,(void**)&dispDoc) && dispDoc)){
			argDoc.vt = VT_DISPATCH;
			argDoc.pdispVal= dispDoc;
		}
		VARIANT covOptional;	//准备Run方法的（可选）参数
		covOptional.vt= VT_ERROR;
		covOptional.scode=DISP_E_PARAMNOTFOUND;
		//执行通用的文档模板中的宏入口函数
		m_spApp->Run(L"EntryMainV2",			//入口函数必须为EntryMainV2。
			&argDoc,			//1
			&covOptional,	//2
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional,
			&covOptional);	//31
		return;
	}
};

OBJECT_ENTRY_AUTO(__uuidof(WordAddin), WordAddin)
