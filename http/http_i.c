

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu Apr 10 11:15:16 2014
 */
/* Compiler settings for http.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IHttpResponse,0xB82D21B3,0x9770,0x4678,0x90,0x8C,0x1B,0x58,0x3C,0xC5,0xA1,0x48);


MIDL_DEFINE_GUID(IID, IID_IHttpRequest,0x009A4A86,0x05EC,0x49B6,0x80,0x00,0x72,0x36,0x51,0x90,0x16,0xD3);


MIDL_DEFINE_GUID(IID, LIBID_httpLib,0x23289BD1,0xCEC3,0x441D,0x8F,0x2F,0x8F,0x58,0x65,0xA3,0x13,0x8E);


MIDL_DEFINE_GUID(CLSID, CLSID_HttpResponse,0x7633F71A,0xE6C7,0x498A,0xAB,0x35,0x26,0xDF,0xBC,0xD9,0xBD,0x59);


MIDL_DEFINE_GUID(CLSID, CLSID_HttpRequest,0xDF4AC3DE,0x4664,0x440E,0xA6,0xCA,0xDE,0x1A,0x5D,0xFB,0x56,0xEE);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



