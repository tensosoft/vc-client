// HttpRequest.cpp : HttpRequest 的实现

#include "stdafx.h"
#include "HttpRequest.h"
#include <stdlib.h>
#include <urlmon.h>
#include <shlwapi.h>
#include <atlstr.h>
#include <AtlConv.h>
#include <DispEx.h>
#pragma comment(lib, "Wininet.lib")
#pragma comment(lib, "Urlmon.lib")
#pragma comment(lib, "Version.lib")

STDMETHODIMP HttpRequest::Send(IHttpResponse** pVal){
	HRESULT hr=S_OK;
	INTERNET_STATUS_CALLBACK iscCallback;
	ATLASSERT(pVal);
	(*pVal)=NULL;

	CComPtr<IHttpResponse> responseDefault=NULL;	//请求响应对象
	//构造默认请求响应。
	responseDefault.p=NULL;
	hr=responseDefault.CoCreateInstance(_T("Discoverx2.HttpResponse"));
	if (FAILED(hr)) {
		ErrorMsgBox(hr,_T("无法初始化返回值：%s"));
		return hr;
	}
	responseDefault->put_ContentLength(0);
	responseDefault->put_ErrorCode(-1);
	responseDefault->put_StatusCode(200);

	if(!this->m_szUrl || _tcslen(this->m_szUrl)==0) {
		WarnMsg(_T("请先指定请求的目标URL地址！"));
		responseDefault.CopyTo(pVal);
		return hr;
	}

	//本地文件，直接拷贝
	if(_tcsstr(this->m_szUrl,L"file:///")!=NULL){
		if(this->m_blSaveResponseToFile && _tcslen(this->m_szResponseFile)>0){
			TCHAR szLocalFile[MAX_PATH]={0};
			_tcscpy_s(szLocalFile,this->m_szUrl+8);
			CAtlString localFile(szLocalFile);
			localFile.Replace(L'|',L':');
			localFile.Replace(L'/',L'\\');
			localFile.Replace(L"%20",L" ");
			CopyFile(localFile.GetString(),this->m_szResponseFile,FALSE);
		}
		CComPtr<IHttpResponse> res;
		res.p=NULL;
		hr=res.CoCreateInstance(_T("Discoverx2.HttpResponse"));
		if (FAILED(hr)) {
			ErrorMsgBox(hr,_T("无法初始化返回值：%s"));
			responseDefault.CopyTo(pVal);
			return S_OK;
		}
		res->put_StatusCode(200);
		res->put_ErrorCode(0);
		res.CopyTo(pVal);
		return S_OK;
	}

	if(!CheckUrlIsSecurity()){responseDefault.CopyTo(pVal); return S_OK;}

	HINTERNET m_hOpen= InternetOpen(USER_AGENT,INTERNET_OPEN_TYPE_PRECONFIG,NULL,NULL,0);
	//设置回调函数
	if (m_hOpen){
		iscCallback=InternetSetStatusCallback(m_hOpen,(INTERNET_STATUS_CALLBACK)INetStatusCallBack);
		if (iscCallback == INTERNET_INVALID_STATUS_CALLBACK){
			ErrorMsgBox(hr,L"无法初始化异步连接请求");
			return hr;
		}
	}else{
		ErrorMsgBox(hr,L"无法初始化连接请求");
		return hr;
	}

	ZeroMemory(&rcContext,sizeof(rcContext));
	rcContext.hOpen=m_hOpen;
	//显示进度
	if (this->m_blShowRequestProgress){
		GetHostProgramVersion(&rcContext.dwHostMajorVersion,NULL,NULL);
		rcContext.hEvent=CreateEvent(NULL,TRUE,FALSE,L"HttpRequestProgressEvent");
		if(rcContext.hEvent==NULL){ ErrMsg(GetLastError(),L"无法创建事件:%s");}

		ZeroMemory(rcContext.szMemo,STRLEN_DEFAULT);
		_tcscpy_s(rcContext.szMemo,_T("请稍候，正在准备连接..."));
		//取对话框上级浏览器窗口句柄
		HWND hwndBrowser=NULL;
		if(m_spWebBrowser2){
			//this->m_spWebBrowser2->get_HWND((SHANDLE_PTR*)&hwndBrowser);
			IServiceProvider* pServiceProvider = NULL;
			if (SUCCEEDED(m_spWebBrowser2->QueryInterface(IID_IServiceProvider,(void**)&pServiceProvider))){
				IOleWindow* pWindow = NULL;
				if (SUCCEEDED(pServiceProvider->QueryService(SID_SShellBrowser, IID_IOleWindow,(void**)&pWindow))){
					pWindow->GetWindow(&hwndBrowser);
					pWindow->Release();
				}
				pServiceProvider->Release();
			}
			rcContext.hWindow=(hwndBrowser?hwndBrowser:HWND_DESKTOP);
		}
		DWORD dialogCreated=ERROR_SUCCESS;
		if(rcContext.dwHostMajorVersion<=8){
			rcContext.hThread=CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE)ProgressDialog,0,0,&(rcContext.dwThreadID));
			if(rcContext.hThread==NULL){
				dialogCreated=GetLastError();
			}
		}else{
			rcContext.hProgressWnd=CreateDialog(_AtlBaseModule.GetModuleInstance(),MAKEINTRESOURCE(IDD_Progress),rcContext.hWindow,(DLGPROC)ProgressProc);
			if(rcContext.hProgressWnd){
				ShowWindow(rcContext.hProgressWnd, SW_SHOW); 
				UpdateWindow(rcContext.hProgressWnd);
				RECT rect;
				GetWindowRect(rcContext.hProgressWnd,&rect);
				::SetWindowPos(rcContext.hProgressWnd,HWND_TOPMOST,rect.left,rect.top,rect.right-rect.left,rect.bottom-rect.top,SWP_SHOWWINDOW);
			}else{
				dialogCreated=GetLastError();
			}
		}
		if(dialogCreated!=ERROR_SUCCESS){
			ErrMsg(dialogCreated,L"无法创建进度显示窗口！");
		}else{
			DWORD dwWaitResult=MsgWaitForMultipleObjects(1,&rcContext.hEvent,TRUE,1000*60,QS_ALLEVENTS|QS_ALLINPUT|QS_ALLPOSTMESSAGE);//WaitForSingleObject(rcContext.hEvent,1000*60);
			switch(dwWaitResult){
			case WAIT_OBJECT_0:
				if(rcContext.hProgressWnd){
					ShowWindow(rcContext.hProgressWnd,SW_SHOW);
					UpdateWindow(rcContext.hProgressWnd);
				}
				break;
			case WAIT_TIMEOUT:
				if(rcContext.hProgressWnd){ShowWindow(rcContext.hProgressWnd,SW_SHOW);UpdateWindow(rcContext.hProgressWnd);}
				break;
			default:
				ErrMsg(GetLastError(),L"等待创建进度窗口时出现异常:%s");
				break;
			}
		}
	}

	//请求方法
	TCHAR szMethod[STRLEN_SMALL]=_T("POST");
	if(this->m_szMethod && _tcslen(this->m_szMethod)>0){
		ZeroMemory(szMethod,sizeof(szMethod));
		_tcscpy_s(szMethod,m_szMethod);
	}

	//变量定义
	CComPtr<IHttpResponse> response=NULL;	//请求响应对象
	DWORD dwError=0;
	DWORD dwFlags=0;
	HINTERNET hConnect=0;			//Internet连接
	HINTERNET hRequest=0;			//Internet请求
	INTERNET_PORT dwPort;

	BOOL postFileFlag=FALSE;	//是否有有效的提交文件。
	HANDLE hPostFile=0;
	DWORD dwPostFileSize=0;

	BOOL sendRequestSucceeded=TRUE;	//发送请求是否成功

	DWORD dwSize=0;

	DWORD dwStatusCode=0;	//http请求响应代码
	DWORD dwContentLength=0;	//http请求响应内容长度
	DWORD dwErrorCode=0;	//http请求响应错误码
	TCHAR szContentType[STRLEN_DEFAULT]={0};	//http请求响应内容类型
	CComBSTR szResText(L"");				//http请求响应返回结果文本
	TCHAR* szHeader=NULL;	//http请求响应头信息

	BOOL getHeanderSucceeded=TRUE;	//获取http请求响应头信息是否成功

	const size_t bufferSize=1024*256;		//缓冲区大小
	char readBuffer[bufferSize]={0};	//每次读取的响应内容
	DWORD dwCalcLength=0;	//通过内容计算出来的ContentLength
	
	BOOL responseFileFlag=FALSE;	//是否有有效的响应内容本地保存文件
	HANDLE hResponseFile=0;	//请求响应内容保存到本地文件的文件句柄

	int requestCount=1;	//请求次数
	
	TCHAR szAccept[] = _T("*/*");
	LPTSTR AcceptTypes[2]={0};
	AcceptTypes[0]=szAccept;
	AcceptTypes[1]=NULL;

	//连接
	dwPort=this->m_usUrlPort;	//端口号
	dwFlags= INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_RELOAD |INTERNET_FLAG_KEEP_CONNECTION;
	if (this->m_blUrlIsSSL) dwFlags |= INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_CN_INVALID | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID;
	
	//打开连接
	if ( !(hConnect = InternetConnect(m_hOpen,this->m_szUrlHost , dwPort, _T(""),  _T(""), INTERNET_SERVICE_HTTP, 0, (DWORD)&rcContext))){
		dwError=GetLastError();
		ErrorMsgBox(dwError,_T("无法连接目标主机：%s"));
		goto clean;
	}
	
reconnect:
	//打开请求
	if ( !(hRequest = HttpOpenRequest(hConnect,this->m_szMethod,this->m_szUrlFile, _T("HTTP/1.1"), _T(""),(LPCTSTR*)AcceptTypes, dwFlags ,(DWORD)&rcContext))){
		dwError=GetLastError();
		ErrorMsgBox(dwError,_T("无法请求目标地址：%s"));
		goto clean;
	}
	
	//有指定提交文件
	if (this->m_szPostFile && _tcslen(this->m_szPostFile)) postFileFlag=PathFileExists(this->m_szPostFile);
	
	//如果有提交的文件
	if(postFileFlag) {
		if ((hPostFile = CreateFile(this->m_szPostFile,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,(HANDLE)NULL)) == (HANDLE)(-1))
		{
			dwError=GetLastError();
			ErrorMsgBox(dwError,_T("要提交的文件不存在或者无法打开：%s"));
			goto clean;
		}
		dwPostFileSize=GetFileSize(hPostFile,NULL);
		rcContext.dwPostFileLength=dwPostFileSize;
		TCHAR szPostFileSize[STRLEN_SMALL]={0};
		_stprintf_s(szPostFileSize,_T("%d"),dwPostFileSize);
		this->AddHeader(T2BSTR(_T("Content-Length")),T2BSTR(szPostFileSize));
	}
	
	//设置请求头
	if (this->m_szHeader && _tcslen(this->m_szHeader)) {
		if (!HttpAddRequestHeaders(hRequest,this->m_szHeader,-1L,HTTP_ADDREQ_FLAG_REPLACE | HTTP_ADDREQ_FLAG_ADD)){
			dwError=GetLastError();
			ErrorMsgBox(dwError,_T("无法设置请求头信息：%s"));
			goto clean;
		}
	}
	
	//处理额外设置（忽略一些错误）
	dwFlags=0;
	dwFlags |= SECURITY_FLAG_IGNORE_UNKNOWN_CA|SECURITY_FLAG_IGNORE_REVOCATION|SECURITY_FLAG_IGNORE_REDIRECT_TO_HTTP|SECURITY_FLAG_IGNORE_REDIRECT_TO_HTTPS|SECURITY_FLAG_IGNORE_CERT_DATE_INVALID|SECURITY_FLAG_IGNORE_CERT_CN_INVALID;
	if (!InternetSetOption (hRequest, INTERNET_OPTION_SECURITY_FLAGS,&dwFlags,sizeof(dwFlags))){
		dwError=GetLastError();
		ErrorMsgBox(dwError,_T("无法设置请求选项：%s"));
		goto clean;
	}
	
	//发送请求
again:
	if(postFileFlag){	//有提交文件内容
		INTERNET_BUFFERS BufferIn = {0};
		DWORD dwBytesWritten;
		BYTE bts[bufferSize]={0};
		DWORD dwBtsRead=0;
		DWORD dwTotalBytes=0;

		BufferIn.dwStructSize = sizeof(INTERNET_BUFFERS);
		BufferIn.Next=NULL;
		BufferIn.dwBufferTotal=dwPostFileSize;

		sendRequestSucceeded=HttpSendRequestEx(hRequest, &BufferIn, NULL, HSR_INITIATE,(DWORD)&rcContext);
		
		//循环发送内容
		while(ReadFile(hPostFile, bts, bufferSize, &dwBtsRead, NULL) && dwBtsRead>0 && dwTotalBytes<=dwPostFileSize){
			if (!InternetWriteFile(hRequest, bts,dwBtsRead, &dwBytesWritten))			{
				dwError=GetLastError();
				ErrorMsgBox(dwError,_T("发送文件内容时发生错误：%s"));
				goto clean;
			}
			dwTotalBytes+=dwBtsRead;
			rcContext.dwPostedLength=dwTotalBytes;
		}
		
		if(!HttpEndRequest(hRequest, NULL, 0, 0))	{
			dwError=GetLastError();
			ErrorMsgBox(dwError,_T("关闭连接时发生错误：%s"));
			goto clean;
		}
	}//if end
	else{	//没有提交文件内容
		sendRequestSucceeded=HttpSendRequest(hRequest, NULL, 0, NULL, 0);
	}	//else end

	//查看是否请求发送成功
	if (!sendRequestSucceeded){
		dwError=GetLastError();
		switch(dwError){
			case ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED:
				if(InternetErrorDlg(GetDesktopWindow(),hRequest,ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED,FLAGS_ERROR_UI_FILTER_FOR_ERRORS|FLAGS_ERROR_UI_FLAGS_GENERATE_DATA|FLAGS_ERROR_UI_FLAGS_CHANGE_OPTIONS,NULL)!=ERROR_SUCCESS){
					requestCount++;
					if (requestCount>2) {
						WarnMsg(_T("证书异常，请联系系统管理员！"));
						goto clean;
					}else goto again;
				}
				break;
			case ERROR_SUCCESS:
				break;
			default:
				ErrorMsgBox(dwError,_T("发送请求错误：%s"));
				goto clean;
		}//switch end
	}//if end

	//取响应状态消息
	dwSize= sizeof(DWORD);
	if (!HttpQueryInfo(hRequest,HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER, &dwStatusCode, &dwSize, NULL)){
		dwError=GetLastError();
		ErrorMsgBox(dwError,_T("获取响应代码错误：%s"));
		goto clean;
	}

	switch(dwStatusCode){
		case HTTP_STATUS_DENIED:	//需要登录
		case HTTP_STATUS_PROXY_AUTH_REQ:
			WarnMsg(_T("请求的地址需要登录，请您先登录后再此发送请求！"));
			goto clean;
		case HTTP_STATUS_REDIRECT:	//重定向
		case HTTP_STATUS_MOVED:
			{
				TCHAR szRedirect[4096]={0};
				DWORD dwRedirect=sizeof(szRedirect);
				if (!HttpQueryInfo(hRequest,HTTP_QUERY_LOCATION,(LPVOID)szRedirect,&dwRedirect,NULL)){
					dwError=GetLastError();
					ErrorMsgBox(hr,_T("获取重定向地址错误：%s"));
					goto clean;
				}
				this->put_Url(szRedirect);
				//先关闭连接和请求，然后重新连接
				if (!InternetCloseHandle(hRequest)){
					dwError=GetLastError();
					ErrorMsgBox(dwError,_T("关闭HTTP请求错误：%s"));
					goto clean;
				}
				goto reconnect;
			}
		case HTTP_STATUS_OK:
		default:
			break;
	}

	//构造请求响应。
	response.p=NULL;
	hr=response.CoCreateInstance(_T("Discoverx2.HttpResponse"));
	if (FAILED(hr)) {
		ErrorMsgBox(hr,_T("创建HttpResponse时发生错误：%s"));
		goto clean;
	}
	response->put_StatusCode(dwStatusCode);
	response->put_ErrorCode(dwError);
	
	//响应内容大小
	dwSize=sizeof(dwContentLength);
	if (HttpQueryInfo(hRequest,HTTP_QUERY_CONTENT_LENGTH | HTTP_QUERY_FLAG_NUMBER ,&dwContentLength,&dwSize,NULL)){
		if (dwContentLength>0) {
			response->put_ContentLength(dwContentLength);
			rcContext.dwContentLength=dwContentLength;
		}
	}

	//响应内容类型 
	dwSize=sizeof(szContentType);
	if (HttpQueryInfo(hRequest,HTTP_QUERY_CONTENT_TYPE ,(LPVOID)szContentType,&dwSize,NULL)){
		if (_tcslen(szContentType)>0) response->put_ContentType(T2BSTR(szContentType));
	}
	
	//所有响应头信息
	szHeader=new TCHAR[STRLEN_8K];
	dwSize=STRLEN_8K*sizeof(TCHAR);
getheader:
	getHeanderSucceeded=HttpQueryInfo(hRequest,HTTP_QUERY_RAW_HEADERS_CRLF ,(LPVOID)szHeader,&dwSize,NULL);
	if(!getHeanderSucceeded){
		dwError=GetLastError();
		if (dwError==ERROR_INSUFFICIENT_BUFFER){	//如果保存头信息长度不够
			SAFE_DEL_ARR(szHeader);
			szHeader=new TCHAR[dwSize+2];
			dwSize+=2;
			dwSize=dwSize*sizeof(TCHAR);
			goto getheader;
		}
		else if (dwError!=ERROR_SUCCESS){
			ErrorMsgBox(dwError,_T("无法获取请求响应头信息：%s"));
			goto clean;
		}
	}
	if(szHeader) response->put_Header(T2BSTR(szHeader));

	//响应内容处理
	if (this->m_blSaveResponseToFile && this->m_szResponseFile && _tcslen(this->m_szResponseFile)>0){	//把响应内容保存到文件
		//创建文件 
		hResponseFile = CreateFile(this->m_szResponseFile,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
		if (hResponseFile == INVALID_HANDLE_VALUE) { 
			dwError=GetLastError();
			ErrorMsgBox(dwError,_T("无法创建本地文件：%s"));
			goto clean;
		}
		responseFileFlag=TRUE;
	}

	//循环读取内容
	do{
		dwSize=0;
		if (!InternetReadFile(hRequest,(LPVOID)readBuffer,sizeof(readBuffer),&dwSize)){
			dwError=GetLastError();
			ErrorMsgBox(dwError,_T("无法读取请求响应内容：%s"));
			goto clean;
		}
		if (dwSize!=0){
			dwCalcLength+=dwSize;
			if (responseFileFlag){	//写文件
				DWORD dwWritten=0;
				if (!WriteFile(hResponseFile,readBuffer,dwSize,&dwWritten,NULL)){
					dwError=GetLastError();
					ErrorMsgBox(dwError,_T("无法写入请求结果内容到本地文件：%s"));
					goto clean;
				}
			}
			else{	//追加到相应文本
				char buffer[bufferSize+2]={0};
				strncpy_s(buffer,readBuffer,dwSize);
				if (szResText) szResText.Append(buffer);
			}
			if (dwCalcLength==dwContentLength) break;
			rcContext.dwReceivedLength=dwCalcLength;
		}//if end
	}while(dwSize!=0);
	
	//把响应内容保存到响应文本
	if (!responseFileFlag){
		response->put_ContentText(szResText.Detach());
	}

	//资源回收
clean:
	//关闭文件
	if(hPostFile) CloseHandle(hPostFile);
	if(hResponseFile) {
		FlushFileBuffers(hResponseFile);
		CloseHandle(hResponseFile);
	}

	//回收http连接和请求句柄
	if (hRequest) InternetCloseHandle(hRequest);
	if (hConnect) InternetCloseHandle(hConnect);

	//关闭状态框
	if (this->m_blShowRequestProgress) EndDialog(rcContext.hProgressWnd,0);

	SAFE_DEL_ARR(szHeader);

	if(response) response.CopyTo(pVal);
	else responseDefault.CopyTo(pVal);
	if (m_hOpen) {InternetCloseHandle(m_hOpen);m_hOpen=NULL;}
	rcContext.hOpen=NULL;
	return S_OK;
}

STDMETHODIMP HttpRequest::get_Url(BSTR* pVal)
{
	ATLASSERT(pVal);
	*pVal=NULL;
	if (_tcslen(m_szUrl)>0) *pVal=T2BSTR(m_szUrl);
	return S_OK;
}

STDMETHODIMP HttpRequest::put_Url(BSTR newVal)
{
	ATLASSERT(newVal);
	if(!PathIsURL(newVal) || IsValidURL(NULL,newVal,0)!=S_OK){
		ErrorMsgBox(0,_T("请求地址不合法！"));
		return S_OK;
	}
	
	DWORD dwSize=STRLEN_1K;
	if (!InternetCanonicalizeUrl(newVal,m_szUrl,&dwSize,ICU_BROWSER_MODE)){
		ErrorMsgBox(GetLastError(),_T("请求地址不合法或者长度太大：%s"));
		ZeroMemory(m_szUrl,sizeof(m_szUrl));
		return S_OK;
	}
	ZeroMemory(m_szUrl,sizeof(m_szUrl));
	_tcscpy_s(m_szUrl,newVal);
	
	ZeroMemory(m_szUrlProtocol,sizeof(m_szUrlProtocol));
	ZeroMemory(m_szUrlHost,sizeof(m_szUrlHost));
	ZeroMemory(m_szUrlFile,sizeof(m_szUrlFile));
	m_usUrlPort=DEFAULT_HTTP_PORT;
	m_blUrlIsSSL=VARIANT_FALSE;

	if (!m_szUrl || _tcslen(m_szUrl)==0) return S_OK;

	//获取url协议部分
	for(size_t i=0;i<sizeof(m_szUrl);i++){
		if (m_szUrl[i]==_T(':') || i>=sizeof(m_szUrlProtocol)) break;
		m_szUrlProtocol[i]=m_szUrl[i];
	}
	
	//是否ssl和ssl默认端口
	if (_tcsicmp(m_szUrlProtocol,_T("https"))==0) {
		m_blUrlIsSSL=VARIANT_TRUE;
		if (m_usUrlPort==DEFAULT_HTTP_PORT) m_usUrlPort=DEFAULT_HTTPS_PORT;
	}

	//获取url端口部分
	TCHAR* szPos1=_tcschr(m_szUrl+_tcslen(m_szUrlProtocol)+1,_T(':'));
	TCHAR* szPos2=_tcschr(m_szUrl+_tcslen(m_szUrlProtocol)+3,_T('/'));
	if (szPos1 && szPos2){
		TCHAR szPort[STRLEN_SMALL]={0};
		_tcsncpy_s(szPort,szPos1+1,szPos2-szPos1-1);
		
		TCHAR *stop;
		m_usUrlPort=(USHORT)_tcstoul(szPort,&stop,0);
	}
	
	//获取url主机部分
	int idx=0;
	for(size_t i=_tcslen(m_szUrlProtocol)+3;i<sizeof(m_szUrl);i++){
		if (m_szUrl[i]==_T(':') || m_szUrl[i]==_T('/')) break;
		m_szUrlHost[idx++]=m_szUrl[i];
	}
	
	//获取url文件部分
	TCHAR* szPos3=_tcschr(m_szUrl+_tcslen(m_szUrlProtocol)+3,_T('/'));
	if(szPos3) _tcscpy_s(this->m_szUrlFile,szPos3);
	else this->m_szUrlFile[0]=_T('/');
	
	return S_OK;
}

STDMETHODIMP HttpRequest::get_SessionKey(BSTR* pVal)
{
	ATLASSERT(pVal);
	*pVal=NULL;
	if (_tcslen(m_szSessionKey)>0) *pVal=T2BSTR(m_szSessionKey);
	return S_OK;
}

STDMETHODIMP HttpRequest::put_SessionKey(BSTR newVal)
{
	ATLASSERT(newVal);
	ZeroMemory(m_szSessionKey,sizeof(m_szSessionKey));
	_tcsncpy_s(m_szSessionKey,newVal,STRLEN_DEFAULT);
	return S_OK;
}

STDMETHODIMP HttpRequest::get_Method(BSTR* pVal)
{
	ATLASSERT(pVal);
	*pVal=NULL;
	if (m_szMethod!=NULL) *pVal=T2BSTR(m_szMethod);
	return S_OK;
}

STDMETHODIMP HttpRequest::put_Method(BSTR newVal)
{
	ATLASSERT(newVal);
	ZeroMemory(m_szMethod,sizeof(m_szMethod));
	_tcsncpy_s(m_szMethod,newVal,STRLEN_SMALL);
	_tcsupr_s(m_szMethod);
	return S_OK;
}

STDMETHODIMP HttpRequest::get_ShowRequestProgress(VARIANT_BOOL* pVal)
{
	ATLASSERT(pVal);
	*pVal=this->m_blShowRequestProgress;
	return S_OK;
}

STDMETHODIMP HttpRequest::put_ShowRequestProgress(VARIANT_BOOL newVal)
{
	this->m_blShowRequestProgress=(newVal?VARIANT_TRUE:VARIANT_FALSE);
	return S_OK;
}

STDMETHODIMP HttpRequest::get_PostFile(BSTR* pVal)
{
	ATLASSERT(pVal);
	*pVal=NULL;
	if (m_szPostFile!=NULL) *pVal=T2BSTR(m_szPostFile);
	return S_OK;
}

STDMETHODIMP HttpRequest::put_PostFile(BSTR newVal)
{
	ATLASSERT(newVal);
	ZeroMemory(m_szPostFile,sizeof(m_szPostFile));
	_tcsncpy_s(m_szPostFile,newVal,MAX_PATH);
	return S_OK;
}

STDMETHODIMP HttpRequest::get_ResponseFile(BSTR* pVal)
{
	ATLASSERT(pVal);
	*pVal=NULL;
	if (m_szResponseFile!=NULL) *pVal=T2BSTR(m_szResponseFile);
	return S_OK;
}

STDMETHODIMP HttpRequest::put_ResponseFile(BSTR newVal)
{
	ATLASSERT(newVal);
	ZeroMemory(m_szResponseFile,sizeof(m_szResponseFile));
	_tcsncpy_s(m_szResponseFile,newVal,MAX_PATH);
	return S_OK;
}

STDMETHODIMP HttpRequest::get_SaveResponseToFile(VARIANT_BOOL* pVal)
{
	ATLASSERT(pVal);
	*pVal=this->m_blSaveResponseToFile;
	return S_OK;
}

STDMETHODIMP HttpRequest::put_SaveResponseToFile(VARIANT_BOOL newVal)
{
	this->m_blSaveResponseToFile=(newVal?VARIANT_TRUE:VARIANT_FALSE);
	return S_OK;
}

STDMETHODIMP HttpRequest::get_UrlProtocol(BSTR* pVal){
	ATLASSERT(pVal);
	*pVal=T2BSTR(this->m_szUrlProtocol);
	return S_OK;
}
STDMETHODIMP HttpRequest::get_UrlPort(USHORT* pVal){
	ATLASSERT(pVal);
	*pVal=this->m_usUrlPort;
	return S_OK;
}
STDMETHODIMP HttpRequest::get_UrlHost(BSTR* pVal){
	ATLASSERT(pVal);
	*pVal=T2BSTR(this->m_szUrlHost);
	return S_OK;
}
STDMETHODIMP HttpRequest::get_UrlFile(BSTR* pVal){
	ATLASSERT(pVal);
	*pVal=T2BSTR(this->m_szUrlFile);
	return S_OK;
}
STDMETHODIMP HttpRequest::get_UrlIsSSL(VARIANT_BOOL* pVal){
	ATLASSERT(pVal);
	*pVal=this->m_blUrlIsSSL;
	return S_OK;
}

BOOL HttpRequest::CheckUrlIsSecurity(){
	//判断权限
	BOOL securityCheckFlag=FALSE;

	TCHAR szUrl[STRLEN_4K]={0};
	_tcscpy_s(szUrl,m_szUrl);
	HRESULT hr=S_OK;
	if(m_spWebBrowser2){
		BSTR bstrLocationUrl=NULL;
		hr=m_spWebBrowser2->get_LocationURL(&bstrLocationUrl);
		if(SUCCEEDED(hr) && bstrLocationUrl){
			TCHAR szHost1[STRLEN_1K]={0};
			TCHAR szHost2[STRLEN_1K]={0};

			URL_COMPONENTS urlc1;
			urlc1.dwStructSize = sizeof(urlc1);
			urlc1.lpszHostName=szHost1;
			urlc1.dwHostNameLength=STRLEN_1K;

			urlc1.dwSchemeLength    = 0;
			urlc1.dwUserNameLength  = 0;
			urlc1.dwPasswordLength  = 0;
			urlc1.dwUrlPathLength   = 0;
			urlc1.dwExtraInfoLength = 0;

			urlc1.lpszScheme     = NULL;
			urlc1.lpszUserName   = NULL;
			urlc1.lpszPassword   = NULL;
			urlc1.lpszUrlPath    = NULL;
			urlc1.lpszExtraInfo  = NULL;

			URL_COMPONENTS urlc2;
			urlc2.dwStructSize = sizeof(urlc2);
			urlc2.lpszHostName=szHost2;
			urlc2.dwHostNameLength=STRLEN_1K;

			urlc2.dwSchemeLength    = 0;
			urlc2.dwUserNameLength  = 0;
			urlc2.dwPasswordLength  = 0;
			urlc2.dwUrlPathLength   = 0;
			urlc2.dwExtraInfoLength = 0;

			urlc2.lpszScheme     = NULL;
			urlc2.lpszUserName   = NULL;
			urlc2.lpszPassword   = NULL;
			urlc2.lpszUrlPath    = NULL;
			urlc2.lpszExtraInfo  = NULL;

			if(!InternetCrackUrl(szUrl,_tcslen(szUrl),0,&urlc1)){
				ErrorMsgBox(GetLastError(),_T("解析请求地址时出现错误1：%s"));
				FREE_SYS_STR(bstrLocationUrl);
				return FALSE;
			}
			TCHAR* szUrl2=OLE2T(bstrLocationUrl);
			if(!InternetCrackUrl(szUrl2,_tcslen(szUrl2),0,&urlc2)){
				ErrorMsgBox(GetLastError(),_T("解析请求地址时出现错误2：%s"));
				FREE_SYS_STR(bstrLocationUrl);
				return FALSE;
			}
			securityCheckFlag=!(urlc1.nScheme!=urlc2.nScheme || urlc1.nPort!=urlc2.nPort || _tcsicmp(urlc1.lpszHostName,urlc2.lpszHostName)!=0);
				
			FREE_SYS_STR(bstrLocationUrl);
		}
	}
	
	if(!securityCheckFlag){
		CComPtr<IInternetSecurityManager> pInetSecMgr;
		hr = CoCreateInstance(CLSID_InternetSecurityManager, NULL, CLSCTX_ALL,IID_IInternetSecurityManager, (void **)&pInetSecMgr);   
		if (SUCCEEDED(hr)){
			DWORD dwZone;
			hr = pInetSecMgr->MapUrlToZone(szUrl, &dwZone, 0);
			if (hr == S_OK){
				if (dwZone <=2)	{
					return TRUE;
				}else{
					ErrorMsgBox(0,_T("错误：请求的地址处于非安全区域！"));
					return FALSE;
				}
			}else{
				ErrorMsgBox(0,_T("错误：无法获取请求地址所属的安全区域！"));
				return FALSE;
			}
		}
	}
	return securityCheckFlag;
}
BOOL HttpRequest::GetErrorMessage(DWORD dwErrCode,TCHAR* szTemplet,TCHAR* szResult,size_t l){
	if(!szResult || l==0) return FALSE;
	size_t len=FALSE;
	LPTSTR msgBuffer = new TCHAR[STRLEN_DEFAULT*sizeof(TCHAR)];
	ZeroMemory(msgBuffer,STRLEN_DEFAULT*sizeof(TCHAR));
	DWORD dwResult = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,
                             NULL,
                             dwErrCode,
                             MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                             msgBuffer,
                             STRLEN_DEFAULT,
                             NULL);
	if (dwResult){
		if(szTemplet && _tcsstr(szTemplet,L"%s")!=NULL){
			_stprintf_s(szResult,l,szTemplet,msgBuffer);
		}else{
			_tcscpy_s(szResult,l,msgBuffer);
		}
		len=_tcslen(msgBuffer);
		SAFE_DEL_ARR(msgBuffer);
	}else{
		_tcscpy_s(szResult,l,L"未知错误！");
	}
	return len;
}
STDMETHODIMP HttpRequest::AddHeader(BSTR szName,BSTR szValue){
	ATLASSERT(szName);
	ATLASSERT(szValue);
	size_t lenName=_tcslen(szName);
	size_t lenValue=_tcslen(szValue);
	if(lenName==0 || lenValue==0) return S_OK;
	CComBSTR str(_T(""));
	if(this->m_szHeader){
		str.Append(this->m_szHeader);
		SAFE_DEL_ARR(this->m_szHeader);
	}
	str.AppendBSTR(szName);
	str.Append(_T(": "));
	str.AppendBSTR(szValue);
	str.Append(_T("\r\n"));
	size_t newLen=str.Length()+1;
	this->m_szHeader=new TCHAR[newLen];
	ZeroMemory(this->m_szHeader,newLen*sizeof(TCHAR));
	_tcscpy_s(this->m_szHeader,newLen,str.Detach());
	return S_OK;
}

STDMETHODIMP HttpRequest::putref_onrequest(IDispatch* newVal){
	if(!newVal) return S_OK;
	//m_pOnrequest = newVal;
	if(m_pStmOnRequest) {CoReleaseMarshalData(m_pStmOnRequest);SAFE_RELEASE(m_pStmOnRequest);}
	HRESULT hr=CreateStreamOnHGlobal(NULL, TRUE, &m_pStmOnRequest);
	if(FAILED(hr)){
		TCHAR errMsg[STRLEN_NORMAL]={0};
		_stprintf_s(errMsg,L"无法设置事件处理程序，错误代码：0x%X",hr);
		ErrMsg(0,errMsg);
	}
	LARGE_INTEGER li = { 0 };
	hr=CoMarshalInterface(m_pStmOnRequest, IID_IDispatch, newVal, MSHCTX_INPROC/*MSHCTX_LOCAL,MSHCTX_INPROC*/, NULL,MSHLFLAGS_TABLESTRONG /*MSHLFLAGS_TABLEWEAK,MSHLFLAGS_TABLESTRONG,MSHLFLAGS_NORMAL*/);
	if(FAILED(hr)) {
		TCHAR errMsg[STRLEN_NORMAL]={0};
		_stprintf_s(errMsg,L"无法设置事件处理程序，错误代码：0x%X",hr);
		ErrMsg(0,errMsg);
	}
	m_pStmOnRequest->Seek(li, STREAM_SEEK_SET, NULL);
	return S_OK;
}
STDMETHODIMP HttpRequest::putref_onerror(IDispatch* newVal){
	if(!newVal) return S_OK;
	//m_pOnerror = newVal;
	if(m_pStmOnRequest) {CoReleaseMarshalData(m_pStmOnError);SAFE_RELEASE(m_pStmOnError);}
	HRESULT hr=CreateStreamOnHGlobal(NULL, TRUE, &m_pStmOnError);
	if(FAILED(hr)){
		TCHAR errMsg[STRLEN_NORMAL]={0};
		_stprintf_s(errMsg,L"无法设置事件处理程序，错误代码：0x%X",hr);
		ErrMsg(0,errMsg);
	}
	LARGE_INTEGER li = { 0 };
	hr=CoMarshalInterface(m_pStmOnError, IID_IDispatch, newVal, MSHCTX_INPROC/*MSHCTX_INPROC*/, NULL,MSHLFLAGS_TABLESTRONG/*MSHLFLAGS_TABLEWEAK,MSHLFLAGS_TABLESTRONG,MSHLFLAGS_NORMAL*/);
	if(FAILED(hr)) {
		TCHAR errMsg[STRLEN_NORMAL]={0};
		_stprintf_s(errMsg,L"无法设置事件处理程序，错误代码：0x%X",hr);
		ErrMsg(0,errMsg);
	}
	m_pStmOnError->Seek(li, STREAM_SEEK_SET, NULL);

	return S_OK;
}
STDMETHODIMP HttpRequest::SendAsync(){
	HRESULT hr=S_OK;
	
	SAFE_DEL_ARR(m_szResponseHeader);
	if(!this->m_szUrl || _tcslen(this->m_szUrl)==0) {
		WarnMsg(L"请先指定请求的目标URL地址！");
		return hr;
	}

	InitMainContext(&mc,this);
	mc.hSession=InternetOpen(USER_AGENT,INTERNET_OPEN_TYPE_PRECONFIG,NULL,NULL,INTERNET_FLAG_ASYNC);
	//设置回调函数
	if (mc.hSession){
		mc.pCallback= InternetSetStatusCallback(mc.hSession,(INTERNET_STATUS_CALLBACK)AsyncCallBack);
		if (mc.pCallback == INTERNET_INVALID_STATUS_CALLBACK){
			FireOnError(GetLastError(),L"无法初始化异步连接请求：%s");
			return hr;
		}
	}else{
		FireOnError(GetLastError(),L"无法初始化连接请求：%s");
		return hr;
	}

	//请求方法
	TCHAR szMethod[STRLEN_SMALL]=_T("POST");
	if(this->m_szMethod && _tcslen(this->m_szMethod)>0){
		ZeroMemory(szMethod,sizeof(szMethod));
		_tcscpy_s(szMethod,m_szMethod);
	}

	//变量定义
	INTERNET_PORT wPort;													//端口号
	DWORD dwFlags=0;
	BOOL postFileFlag=FALSE;											//是否有有效的提交文件。
	DWORD dwPostFileSize=0;
	BOOL sendRequestSucceeded=TRUE;								//发送请求是否成功
	DWORD dwError=0;
	DWORD dwSize=0;

	TCHAR szContentType[STRLEN_DEFAULT]={0};			//http请求响应内容类型
	
	TCHAR szAccept[] = L"*/*";
	LPTSTR AcceptTypes[2]={0};
	AcceptTypes[0]=szAccept;
	AcceptTypes[1]=NULL;

	//连接
	wPort=this->m_usUrlPort;
	dwFlags= INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_RELOAD |INTERNET_FLAG_KEEP_CONNECTION;
	if (this->m_blUrlIsSSL) dwFlags |= INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_CN_INVALID | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID;
	
	//打开连接
	if (!(mc.hConnect=InternetConnect(mc.hSession,this->m_szUrlHost , wPort, NULL,  NULL, INTERNET_SERVICE_HTTP, 0, (DWORD_PTR)&mc))){
		dwError=GetLastError();
		FireOnError(dwError,L"无法连接目标主机：%s");
		goto clean;
	}
	InitRequestContext(&mc,&ac);
	ac.dwState=GET_REQ;
	if(_tcsicmp(this->m_szMethod,L"post")==0) ac.dwState=POST_REQ;

	//打开请求
	if (!(ac.hRequest = HttpOpenRequest(mc.hConnect,this->m_szMethod,this->m_szUrlFile, _T("HTTP/1.1"), _T(""),(LPCTSTR*)AcceptTypes, dwFlags ,(DWORD_PTR)&ac))){
		dwError=GetLastError();
		FireOnError(dwError,L"无法请求目标地址：%s");
		goto clean;
	}

	if(!this->m_blSaveResponseToFile || _tcslen(this->m_szResponseFile)==0){
		ZeroMemory(this->m_szResponseFile,sizeof(TCHAR)*MAX_PATH);
		TCHAR szTempFolder[MAX_PATH]={0};				//临时路径
		DWORD pathLen=GetTempPath(MAX_PATH,szTempFolder);
		if(pathLen>0 && pathLen<=(MAX_PATH-20)){
			if(GetTempFileName(szTempFolder,L"d2ax",0,this->m_szResponseFile)==0){
				FireOnError(GetLastError(),L"无法创建临时文件：%s");
				goto clean;
			}
		}else{
			DWORD dwErr=GetLastError();
			if(dwErr==0) FireOnError(0,L"无法获取临时文件路径！");
			else FireOnError(dwErr,L"无法获取临时文件路径：%s");
			goto clean;
		}
	}
	
	OpenFiles(&ac);
	//有指定提交文件
	if (this->m_szPostFile && _tcslen(this->m_szPostFile)) {
		postFileFlag=PathFileExists(this->m_szPostFile);
		if(postFileFlag && ac.hFile){
			dwPostFileSize = ac.dwPostFileSize;
			ac.dwState=POST_REQ;
		}else{
			ac.dwState=GET_REQ;
		}
	}else{
		ac.dwState=GET_REQ;
	}

	//设置请求头
	if (this->m_szHeader && _tcslen(this->m_szHeader)) {
		if (!HttpAddRequestHeaders(ac.hRequest,this->m_szHeader,-1L,HTTP_ADDREQ_FLAG_REPLACE | HTTP_ADDREQ_FLAG_ADD)){
			dwError=GetLastError();
			FireOnError(dwError,L"无法设置请求头信息：%s");
			goto clean;
		}
	}

	//处理额外设置（忽略一些错误）
	dwFlags=0;
	dwFlags |= SECURITY_FLAG_IGNORE_UNKNOWN_CA|SECURITY_FLAG_IGNORE_REVOCATION|SECURITY_FLAG_IGNORE_REDIRECT_TO_HTTP|SECURITY_FLAG_IGNORE_REDIRECT_TO_HTTPS|SECURITY_FLAG_IGNORE_CERT_DATE_INVALID|SECURITY_FLAG_IGNORE_CERT_CN_INVALID;
	if (!InternetSetOption (ac.hRequest, INTERNET_OPTION_SECURITY_FLAGS,&dwFlags,sizeof(dwFlags))){
		dwError=GetLastError();
		FireOnError(dwError,L"无法设置请求选项：%s");
		goto clean;
	}
	
	//发送请求
//again:
	if(ac.hFile){	//有提交文件内容
		ZeroMemory(&buffersIn,sizeof(INTERNET_BUFFERS));
		buffersIn.dwStructSize = sizeof(INTERNET_BUFFERS);
    buffersIn.lpvBuffer = NULL;
    buffersIn.dwBufferLength = 0;
    buffersIn.dwBufferTotal = dwPostFileSize;
		sendRequestSucceeded=HttpSendRequestEx(ac.hRequest, &buffersIn, NULL, 0,(DWORD_PTR)&ac);
	}else{	//没有提交文件内容
		sendRequestSucceeded=HttpSendRequest(ac.hRequest, NULL, 0, NULL, 0);
	}
	//查看是否请求发送成功
	if (!sendRequestSucceeded && (dwError=GetLastError())!=ERROR_IO_PENDING){
		FireOnError(dwError,L"无法发送请求：%s");
    goto clean;
	}//if end
clean:
	return S_OK;
}
STDMETHODIMP HttpRequest::Abort(){
	ac.blCanceled=TRUE;
	return S_OK;
}
HRESULT HttpRequest::FireOnRequest(DWORD statusCode,TCHAR* statusDesc){
	if(!m_pStmOnRequest) return S_OK;
	HRESULT hr = 0;
	
	DISPPARAMS dispParams;
	VARIANTARG args[3];
	size_t headerLen=(m_szResponseHeader?_tcslen(m_szResponseHeader):0);
	size_t l=headerLen+(statusDesc?_tcslen(statusDesc):0)+(statusCode==DONE&&this->m_szResponseContent?_tcslen(this->m_szResponseContent):0)+STRLEN_1K;
	TCHAR* szJS=new TCHAR[l];
	ZeroMemory(szJS,sizeof(TCHAR)*l);
	if(statusCode==DONE&&this->m_szResponseContent){
		if(this->m_contentType==JSON){
			_stprintf_s(szJS,l,L"{id:'ID%X%X',desc:'%s',header:{%s},contentLength:%u,recievedBytes:%u,postFileLength:%u,sentBytes:%u,\r\nresult:%s\r\n}",this,mc.dwId,(statusDesc?statusDesc:L""),(m_szResponseHeader?m_szResponseHeader:L""),m_dwContentLength,m_dwReceivedBytes,m_dwPostBytes,m_dwSentBytes,(m_szResponseContent?m_szResponseContent:L"null"));
		}else{
			CAtlString atlstr(m_szResponseContent);
			atlstr.Replace(L"'",L"\\'");
			atlstr.Replace(L"\r\n",L"\\r\\n");
			atlstr.Replace(L"\r",L"\\r\\n");
			atlstr.Replace(L"\n",L"\\r\\n");
			_stprintf_s(szJS,l,L"{id:'ID%X%X',desc:'%s',header:{%s},contentLength:%u,recievedBytes:%u,postFileLength:%u,sentBytes:%u,\r\nresult:'%s'\r\n}",this,mc.dwId,(statusDesc?statusDesc:L""),(m_szResponseHeader?m_szResponseHeader:L""),m_dwContentLength,m_dwReceivedBytes,m_dwPostBytes,m_dwSentBytes,(m_szResponseContent?atlstr.GetString():L""));
		}
	}else{
		_stprintf_s(szJS,l,L"{id:'ID%X%X',desc:'%s',header:{%s},contentLength:%u,recievedBytes:%u,postFileLength:%u,sentBytes:%u}",this,mc.dwId,(statusDesc?statusDesc:L""),(m_szResponseHeader?m_szResponseHeader:L""),m_dwContentLength,m_dwReceivedBytes,m_dwPostBytes,m_dwSentBytes);
	}
	
	VariantInit(&args[0]);
	if(szJS && _tcslen(szJS)>0){
		LARGE_INTEGER li = { 0 };
		m_pStmWB->Seek(li, STREAM_SEEK_SET, NULL);
		IDispatch* ps=NULL;
		hr=CoUnmarshalInterface(m_pStmWB, IID_IDispatch,(LPVOID*)&ps);
		if(SUCCEEDED(hr) && ps!=NULL){
			VARIANT ret;
			VariantInit(&ret);

			VARIANTARG params[1];
			VariantInit(&params[0]);
			params[0].vt=VT_BSTR;
			params[0].bstrVal=SysAllocString(szJS);

			if(EvalJSResult(ps,L"_evalCallbackParam",params,1,&ret)){
				if(ret.vt==VT_DISPATCH) VariantCopy(&args[0],&ret);
				//else if(ret.vt==VT_BSTR)TipMsg(ret.bstrVal);
				//else{
				//	TCHAR t[100]={0};
				//	_stprintf_s(t,L"vt=%u",ret.vt);
				//	TipMsg(t);
				//}
			}//else{TipMsg(params[0].bstrVal);}
			ps->Release();
			VariantClear(&params[0]);
			VariantClear(&ret);
		}//else{TipMsg(L"eee");}
	}
	//if(args[0].vt==VT_EMPTY) args[0].vt = VT_NULL;

	VariantInit(&args[1]);
	args[1].vt = VT_INT;
	args[1].intVal=(int)statusCode;

	VariantInit(&args[2]);
	args[2].vt = VT_NULL;

	memset(&dispParams, 0, sizeof(dispParams));
	dispParams.rgvarg = args;
	dispParams.cArgs = sizeof(args)/sizeof(args[0]);
	
	static OLECHAR FAR* szMember = L"call";
	LARGE_INTEGER li = { 0 };
	m_pStmOnRequest->Seek(li, STREAM_SEEK_SET, NULL);
	IDispatch* pGoodForThisThread=NULL;
	CComPtr<IDispatch> pDisp=NULL;
	hr=CoUnmarshalInterface(m_pStmOnRequest, IID_IDispatch,(LPVOID*)&pGoodForThisThread);
	if(FAILED(hr)){
		TCHAR errMsg[STRLEN_NORMAL]={0};
		_stprintf_s(errMsg,L"无法触发请求事件处理程序，错误代码：0x%X",hr);
		ErrMsg(0,errMsg);
		goto clean;
	}
	if(pGoodForThisThread==NULL) goto clean;
	pDisp=pGoodForThisThread;

	DISPID dispId=NULL;
	
	if (!SUCCEEDED(pDisp->GetIDsOfNames(IID_NULL, &szMember, 1, LOCALE_SYSTEM_DEFAULT, &dispId))) {
		if(pGoodForThisThread) pGoodForThisThread->Release();
		goto clean;
	}
	hr= pDisp->Invoke(dispId, IID_NULL, LOCALE_SYSTEM_DEFAULT, DISPATCH_METHOD, &dispParams, NULL,NULL, NULL); 
	if(pGoodForThisThread) pGoodForThisThread->Release();
	//if(FAILED(hr)) {
	//	TCHAR errMsg[STRLEN_NORMAL]={0};
	//	_stprintf_s(errMsg,L"执行请求事件处理程序时出现错误，错误代码：0x%X",hr);
	//	ErrMsg(0,errMsg);
	//	goto clean;
	//}

clean:
	VariantClear(&args[0]);
	VariantClear(&args[1]);
	VariantClear(&args[2]);
	SAFE_DEL_ARR(szJS);
	return S_OK;
}
HRESULT HttpRequest::FireOnError(DWORD errCode,TCHAR* errDesc){
	TCHAR szMsg[STRLEN_1K]={0};
	GetErrorMessage(errCode,errDesc,szMsg,STRLEN_1K);
	if(!m_pStmOnError) return S_OK;

	DISPPARAMS dispParams;
	VARIANTARG args[3];

	HRESULT hr = 0;

	VariantInit(&args[0]);
	if(szMsg && _tcslen(szMsg)>0){
		args[0].vt = VT_BSTR;
		args[0].bstrVal= SysAllocString(szMsg);
	}else if(errDesc && _tcslen(errDesc)>0){
		args[0].vt = VT_BSTR;
		args[0].bstrVal= SysAllocString(errDesc);
	}else{
		args[0].vt = VT_NULL;
	}

	VariantInit(&args[1]);
	args[1].vt = VT_INT;
	args[1].intVal=(int)errCode;

	VariantInit(&args[2]);
	args[2].vt = VT_NULL;

	memset(&dispParams, 0, sizeof(dispParams));
	dispParams.rgvarg = args;
	dispParams.cArgs = sizeof(args)/sizeof(args[0]);
	
	static OLECHAR FAR* szMember = L"call";

	LARGE_INTEGER li = { 0 };
	m_pStmOnError->Seek(li, STREAM_SEEK_SET, NULL);
	CComPtr<IDispatch> pDisp=NULL;
	IDispatch * pGoodForThisThread=NULL;
	hr=CoUnmarshalInterface(m_pStmOnError, IID_IDispatch,(LPVOID*)&pGoodForThisThread);
	if(FAILED(hr)){
		TCHAR errMsg[STRLEN_NORMAL]={0};
		_stprintf_s(errMsg,L"无法触发异常事件处理程序，错误代码：0x%X",hr);
		ErrMsg(0,errMsg);
		goto clean;
	}
	if(pGoodForThisThread==NULL) goto clean;
	pDisp=pGoodForThisThread;
	DISPID dispId=NULL;

	if (!SUCCEEDED(pDisp->GetIDsOfNames(IID_NULL, &szMember, 1, LOCALE_SYSTEM_DEFAULT, &dispId))) {
		if(pGoodForThisThread) pGoodForThisThread->Release();
		goto clean;
	}
	hr= pDisp->Invoke(dispId, IID_NULL, LOCALE_SYSTEM_DEFAULT, DISPATCH_METHOD, &dispParams, NULL,NULL, NULL); 
	if(pGoodForThisThread) pGoodForThisThread->Release();
	//if(FAILED(hr)) {
	//	TCHAR errMsg[STRLEN_NORMAL]={0};
	//	_stprintf_s(errMsg,L"执行异常事件处理程序时出现错误，错误代码：0x%X",hr);
	//	ErrMsg(0,errMsg);
	//	goto clean;
	//}

clean:
	VariantClear(&args[0]);
	VariantClear(&args[1]);
	VariantClear(&args[2]);

	return S_OK;
}
void HttpRequest::ErrorMsgBox(DWORD dwErrorCode,TCHAR* szTemplet){
	ErrMsg(dwErrorCode,szTemplet);
	if (rcContext.hProgressWnd!=0) EndDialog(rcContext.hProgressWnd,0);
}

void CALLBACK INetStatusCallBack(HINTERNET hInternet, DWORD dwContext, DWORD dwInternetStatus,LPVOID lpvStatusInformation, DWORD dwStatusInformationLength){
  REQUEST_CONTEXT *cpContext;
  cpContext = (REQUEST_CONTEXT*)dwContext;
	TCHAR statusTxt[STRLEN_DEFAULT]={0};
	int isSetTxt=1;
  switch (dwInternetStatus)	{
    case INTERNET_STATUS_CLOSING_CONNECTION:
			break;
		case INTERNET_STATUS_CONNECTION_CLOSED:
			break;
		case INTERNET_STATUS_CONNECTING_TO_SERVER:
			break;
    case INTERNET_STATUS_CONNECTED_TO_SERVER:
			break;
    case INTERNET_STATUS_HANDLE_CLOSING:
			if(cpContext->hProgressWnd){
				SetDlgItemText(rcContext.hProgressWnd,IDC_STATIC1,_T("完成"));
				EndDialog(cpContext->hProgressWnd,1);
			}
			break;
    case INTERNET_STATUS_HANDLE_CREATED:
			if(cpContext->hProgressWnd) {SetDlgItemText(rcContext.hProgressWnd,IDC_STATIC1,_T("已连接"));}
			break;
    case INTERNET_STATUS_INTERMEDIATE_RESPONSE:
			break;
    case INTERNET_STATUS_NAME_RESOLVED:
			break;
    case INTERNET_STATUS_RECEIVING_RESPONSE:
			break;
    case INTERNET_STATUS_RESPONSE_RECEIVED:
			{
				ZeroMemory(rcContext.szMemo,STRLEN_DEFAULT*sizeof(TCHAR));
				if (rcContext.dwContentLength>0){
					_stprintf_s(rcContext.szMemo,_T("待接收/已接收[KB]：%u/%u"),rcContext.dwContentLength/1024,rcContext.dwReceivedLength/1024);
				}else{
					_stprintf_s(rcContext.szMemo,_T("已接收[KB]：%u"),rcContext.dwReceivedLength/1024);
				}
				if(cpContext->hProgressWnd) {SetDlgItemText(rcContext.hProgressWnd,IDC_STATIC1,rcContext.szMemo);}
			}
			break;
    case INTERNET_STATUS_REDIRECT:
			break;
    case INTERNET_STATUS_REQUEST_COMPLETE:
      break;
    case INTERNET_STATUS_REQUEST_SENT:
			{
				ZeroMemory(rcContext.szMemo,STRLEN_DEFAULT*sizeof(TCHAR));
				if (rcContext.dwPostFileLength>0){
					_stprintf_s(rcContext.szMemo,_T("待发送/已发送[KB]：%u/%u"),rcContext.dwPostFileLength/1024,rcContext.dwPostedLength/1024);
					if(cpContext->hProgressWnd){SetDlgItemText(rcContext.hProgressWnd,IDC_STATIC1,rcContext.szMemo);}
				}
			}
			break;
    case INTERNET_STATUS_RESOLVING_NAME:
			break;
    case INTERNET_STATUS_SENDING_REQUEST:
			break;
    case INTERNET_STATUS_STATE_CHANGE:
			break;
    default:
			break;
	}
}

BOOL CALLBACK ProgressProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam){
	switch(message){
		case WM_INITDIALOG:
			SetCursor(LoadCursor(NULL,IDC_WAIT));
			hWndGif= GetDlgItem(hWnd,IDC_STATIC2);
			if (hWndGif){
				m_wndBanner.SubclassWindow(hWndGif);
				if (m_wndBanner.Load(MAKEINTRESOURCE(IDR_BUSY),_T("GIF"))){
					m_wndBanner.Draw();
				}
			}
			SetCursor(LoadCursor(NULL,IDC_ARROW));
			SetTimer(hWnd,TIMER_PROCESSDLG,TIMER_PROCESSDLG_PERIOD,NULL);
			if (!SetEvent(rcContext.hEvent)) {ErrMsg(GetLastError(),L"无法触发事件:%s");}
			rcContext.hProgressWnd=hWnd;
			return TRUE;
		case WM_TIMER:
			if(wParam==TIMER_PROCESSDLG && rcContext.hOpen==NULL){EndDialog(hWnd,0);}
			return 0;
		case WM_DESTROY:
			KillTimer(hWnd,TIMER_PROCESSDLG);
			SetCursor(LoadCursor(NULL,IDC_WAIT));
			SetCursor(LoadCursor(NULL,IDC_ARROW));
			if(rcContext.hEvent) {
				CloseHandle(rcContext.hEvent);
				rcContext.hEvent=NULL;
			}
			if(rcContext.hThread){
				CloseHandle(rcContext.hThread);
				rcContext.hThread=NULL;
			}
			rcContext.hProgressWnd=NULL;
			return TRUE;
		default:
			return FALSE;
	}
}
DWORD WINAPI ProgressDialog(LPVOID lpParam){
	//初始化回调信息上下文
	if(!rcContext.hWindow) rcContext.hWindow=HWND_DESKTOP;
	//显示进度窗口
	DialogBox(_AtlBaseModule.GetModuleInstance(),MAKEINTRESOURCE(IDD_Progress), rcContext.hWindow,(DLGPROC)ProgressProc);
	return 0;
}

void CALLBACK AsyncCallBack(HINTERNET hInternet,__in DWORD_PTR dwContext,DWORD dwInternetStatus,__in_bcount(dwStatusInformationLength) LPVOID lpvStatusInformation,DWORD dwStatusInformationLength){
	DWORD dwError=0;
	DWORD dwBytes = 0;
	BOOL bQuit = FALSE;
	MAIN_CONTEXT *mainContext=NULL;
	APP_CONTEXT *appContext=NULL;
	static BOOL contentLengthQuerySuccess=FALSE;
	DWORD *dwStructType = (DWORD*)dwContext;
	HttpRequest *me=NULL;
	if(*dwStructType == STRUCT_TYPE_MAIN_CONTEXT){
		mainContext = (MAIN_CONTEXT*)dwContext;
		me=mainContext->me;
	}else{
		appContext = (APP_CONTEXT*)dwContext;
		me=appContext->me;
	}
	CoInitializeEx(0,2);
	if(appContext!=NULL && appContext->blCanceled){
		if(appContext->blCanceled>TRUE) return;
		me->FireOnRequest(USERCANCELED,L"用户取消");
		SetEvent(appContext->hEvent);
		CleanUp(appContext);
		appContext->blCanceled=2;
		return;
	}
	switch(dwInternetStatus){
	case INTERNET_STATUS_HANDLE_CREATED:
		contentLengthQuerySuccess=FALSE;
		me->FireOnRequest(INITIALIZED,L"连接请求初始化完成");
		break;
	case INTERNET_STATUS_CONNECTING_TO_SERVER:
		me->FireOnRequest(CONNECTING,L"正在连接到服务器");
		break;
	case INTERNET_STATUS_CONNECTED_TO_SERVER:
		me->FireOnRequest(CONNECTED,L"成功连接到服务器");
		break;
	case INTERNET_STATUS_HANDLE_CLOSING:
		//关闭hRequest时触发事件信号
		if(appContext){SetEvent(appContext->hEvent);}
		break;
	case INTERNET_STATUS_CLOSING_CONNECTION:
		me->FireOnRequest(CLOSING,L"正在关闭连接");
		break;
	case INTERNET_STATUS_CONNECTION_CLOSED:
		me->FireOnRequest(CLOSED,L"连接已关闭");
		break;
	case INTERNET_STATUS_INTERMEDIATE_RESPONSE:
		//me->FireOnRequest(dwInternetStatus,L"INTERNET_STATUS_INTERMEDIATE_RESPONSE");
		break;
	case INTERNET_STATUS_RECEIVING_RESPONSE:
		//me->FireOnRequest(dwInternetStatus,L"INTERNET_STATUS_RECEIVING_RESPONSE");
		break;
	case INTERNET_STATUS_RESPONSE_RECEIVED:
		if(lpvStatusInformation && dwStatusInformationLength == sizeof(DWORD)){
			dwBytes = *((LPDWORD)lpvStatusInformation);
		}
		me->m_dwReceivedBytes=dwBytes;
		me->FireOnRequest(RECEIVING,L"接收内容");
		break;
	case INTERNET_STATUS_REDIRECT:
		me->FireOnRequest(dwInternetStatus,(TCHAR*)lpvStatusInformation);
		break;
	case INTERNET_STATUS_REQUEST_COMPLETE:
		//响应内容大小
		if(!contentLengthQuerySuccess && appContext->dwResponseContentLength==0){
			DWORD dwSize=sizeof(DWORD);
			if (HttpQueryInfo(appContext->hRequest,HTTP_QUERY_CONTENT_LENGTH | HTTP_QUERY_FLAG_NUMBER ,&appContext->dwResponseContentLength,&dwSize,NULL)){
				me->m_dwContentLength=appContext->dwResponseContentLength;
				contentLengthQuerySuccess=TRUE;
			}
		}

		//所有响应头信息
		if(me->m_szResponseHeader==NULL || _tcslen(me->m_szResponseHeader)==0){
			TCHAR* h=new TCHAR[STRLEN_8K];
			DWORD dwSize=STRLEN_8K*sizeof(TCHAR);
			ZeroMemory(h,dwSize);
getheader:
			if(!HttpQueryInfo(appContext->hRequest,HTTP_QUERY_RAW_HEADERS,(LPVOID)h,&dwSize,NULL)){
				if(ERROR_INSUFFICIENT_BUFFER==GetLastError()){
					SAFE_DEL_ARR(h);
					h=new TCHAR[dwSize+1];
					dwSize=(dwSize+1)*sizeof(TCHAR);
					ZeroMemory(h,dwSize);
					goto getheader;
				}else{
					SAFE_DEL_ARR(h);
				}
			}else{
				CAtlString str(L"");
				for (LPTSTR pszz = h; *pszz; pszz += _tcslen(pszz) + 1) {
					TCHAR szName[MAX_PATH]={0};
					TCHAR szVal[MAX_PATH]={0};
					if(HttpRequest::ParseHeader(pszz,szName,MAX_PATH,szVal,MAX_PATH)){
						if(str.GetLength()>0) str.Append(L",\r\n");
						if(_tcsicmp(szName,L"Content-Type")==0){
							TCHAR szValL[MAX_PATH]={0};
							_tcscpy_s(szValL,szVal);
							_tcslwr_s(szValL);
							if(_tcsstr(szValL,L"/javascript")!=NULL||_tcsstr(szValL,L"/x-javascript")!=NULL||_tcsstr(szValL,L"/ecmascript")!=NULL||_tcsstr(szVal,L"/jscript")!=NULL){
								me->m_contentType=JSON;
							}else if(_tcsstr(szValL,L"/xml")!=NULL){
								me->m_contentType=XML;
							}
						}
						str.Append(L"'");
						str.Append(szName);
						str.Append(L"'");
						str.Append(L":");
						str.Append(L"'");
						str.Append(szVal);
						str.Append(L"'");
					}
				}
				size_t l=str.GetLength()+1;
				me->m_szResponseHeader=new TCHAR[l];
				ZeroMemory(me->m_szResponseHeader,sizeof(TCHAR)*l);
				_tcscpy_s(me->m_szResponseHeader,l,str);
				SAFE_DEL_ARR(h);
			}
		}

		dwError = ((LPINTERNET_ASYNC_RESULT)lpvStatusInformation)->dwError ;
		if (dwError != ERROR_SUCCESS){
			me->FireOnError(dwError,L"发送请求时出现异常：%s");
			CleanUp(appContext);
		}
		switch (appContext->dwState){
		case POST_REQ:
			if((dwError = DoReadFile(appContext))!=ERROR_SUCCESS && dwError!= ERROR_IO_PENDING){
				me->FireOnError(dwError,L"读取文件时出现异常：%s");
				CleanUp(appContext);
			}
			break;
		case POST_RES:
		case GET_REQ:
			if(!appContext->dwDownloaded ){
				EnterCriticalSection(&appContext->crSection);{
					appContext->bReceiveDone=TRUE;
					if (!appContext->lPendingWrites){bQuit = TRUE;}
				}
				LeaveCriticalSection(&appContext->crSection);
				if(bQuit){SetEvent(appContext->hEvent);CleanUp(appContext);}  
				break;
			}else if(appContext->dwDownloaded !=INVALID_DOWNLOAD_VALUE){
				ZeroMemory(appContext->me->m_writeIO,sizeof(IO_BUF));
				InterlockedIncrement(&appContext->lPendingWrites);
				appContext->me->m_writeIO->aContext = appContext;
				appContext->me->m_writeIO->lpo.Offset=appContext->dwWriteOffset;
				CopyMemory(&appContext->me->m_writeIO->buffer,appContext->pszOutBuffer,appContext->dwDownloaded);
	      
				if(!WriteFile(appContext->hRes,&appContext->me->m_writeIO->buffer,appContext->dwDownloaded,NULL,&appContext->me->m_writeIO->lpo)){
					if((dwError=GetLastError())!= ERROR_IO_PENDING){
						me->FireOnError(dwError,L"写入输出文件时出现异常：%s");
						CleanUp(appContext);
					}
				}
				appContext->dwWriteOffset += appContext->dwDownloaded;
			}else{	//appContext->dwDownloaded ==INVALID_DOWNLOAD_VALUE://开始下载
			}
			DoInternetRead(appContext);
			break;
		}
		break;
	case INTERNET_STATUS_REQUEST_SENT:
		if(lpvStatusInformation && dwStatusInformationLength == sizeof(DWORD)){
			dwBytes = *((LPDWORD)lpvStatusInformation);
		}
		me->m_dwSentBytes=dwBytes;
		me->FireOnRequest(SENDING,L"提交内容");
		break;
	case INTERNET_STATUS_DETECTING_PROXY:
		break;
	case INTERNET_STATUS_RESOLVING_NAME:
		break;
	case INTERNET_STATUS_NAME_RESOLVED:
		break;
	case INTERNET_STATUS_SENDING_REQUEST:
		//me->FireOnRequest(dwInternetStatus,L"INTERNET_STATUS_SENDING_REQUEST");
		break;
	case INTERNET_STATUS_STATE_CHANGE:
		//me->FireOnRequest(dwInternetStatus,L"INTERNET_STATUS_STATE_CHANGE");
		break;
	case INTERNET_STATUS_P3P_HEADER:
		break;
	case INTERNET_STATUS_COOKIE_SENT:
		break;
	case INTERNET_STATUS_COOKIE_RECEIVED:
		break;
	case INTERNET_STATUS_COOKIE_HISTORY:
		break;
	default:
		break;
	}
	CoUninitialize();
}
VOID InitMainContext(__inout MAIN_CONTEXT *pmc,__in HttpRequest* me){
	static DWORD id=0;
	pmc->dwStructType =  STRUCT_TYPE_MAIN_CONTEXT;
	pmc->hSession = NULL;
	pmc->hConnect = NULL;
	pmc->me=me;
	pmc->dwId=id++;
	return;
}

VOID InitRequestContext(__in MAIN_CONTEXT* pmc,__inout APP_CONTEXT *pac){
	pac->dwStructType =  STRUCT_TYPE_APP_CONTEXT;
	pac->mainContext = pmc;
	pac->me=pmc->me;
	pac->hRequest = NULL;
	pac->blCanceled=FALSE;
	pac->dwPostFileSize=0;
	pac->dwResponseContentLength=0;
	pac->dwDownloaded = INVALID_DOWNLOAD_VALUE;
	pac->dwRead = 0;
	pac->dwWritten = 0;
	pac->dwReadOffset = 0;
	pac->dwWriteOffset = 0;
	pac->lPendingWrites = 0;
	pac->bReceiveDone = FALSE;
	pac->hFile = NULL;
	pac->hRes = NULL;
	pac->pszOutBuffer = new char[STRLEN_4K];

	InitializeCriticalSection(&pac->crSection);

	pac->hEvent = CreateEvent(NULL,FALSE,FALSE,L"MAIN_SYNC"); 
	if (!pac->hEvent){
		pmc->me->FireOnError(GetLastError(),L"无法创建事件：%s");
		CleanUp(pac);
	}
	return;
}
DWORD DoReadFile(__in APP_CONTEXT* aContext){
	DWORD dwError = ERROR_SUCCESS;
	ZeroMemory(aContext->me->m_readIO,sizeof(IO_BUF));
	aContext->me->m_readIO->aContext = aContext;
	aContext->me->m_readIO->lpo.Offset = aContext->dwReadOffset;

	if ( !ReadFile(aContext->hFile,&aContext->me->m_readIO->buffer,STRLEN_4K,NULL,&aContext->me->m_readIO->lpo) ){
		if ( (dwError=GetLastError()) == ERROR_HANDLE_EOF ){
			dwError = DoCompleteReadFile(aContext);
		}else if (dwError != ERROR_IO_PENDING ){
			aContext->me->FireOnError(dwError,L"无法读取文件：%s");
			goto Exit;
		}
	}

Exit:
	return dwError;
}

DWORD DoCompleteReadFile(__in APP_CONTEXT* aContext){
	DWORD dwError=ERROR_SUCCESS;
	aContext->dwState = POST_RES;
	if(!HttpEndRequest(aContext->hRequest,NULL,0,0)){
		if ( (dwError = GetLastError()) == ERROR_IO_PENDING){
			//等待下载完成
		}else{
			aContext->me->FireOnError(dwError,L"无法结束请求：%s");
			goto Exit;
		}
	}else{
		DoInternetRead(aContext);
	}
Exit:
	return dwError;
}

VOID DoInternetWrite(__in APP_CONTEXT *aContext){
	DWORD dwError = 0;
	if(InternetWriteFile(aContext->hRequest,aContext->pszOutBuffer,aContext->dwRead,&aContext->dwWritten)){
		if((dwError=DoReadFile(aContext)) != ERROR_SUCCESS&& dwError != ERROR_IO_PENDING){
			aContext->me->FireOnError(dwError,L"无法读取文件：%s");
			CleanUp(aContext);
		}
	}
	if ((dwError=GetLastError()) == ERROR_IO_PENDING){
		//等待上传完成
	}else if ( dwError != ERROR_SUCCESS){
		aContext->me->FireOnError(dwError,L"无法发送文件内容：%s");
		CleanUp(aContext);
	}
}
VOID DoInternetRead(__in APP_CONTEXT *aContext){
	DWORD dwError=0;
	while (InternetReadFile(aContext->hRequest,aContext->pszOutBuffer,STRLEN_4K,&aContext->dwDownloaded)){
		//completed synchronously ; callback won't be issued
		BOOL bQuit = FALSE;
		if (!aContext->dwDownloaded ){
			EnterCriticalSection(&aContext->crSection);{
				aContext->bReceiveDone = TRUE;
				if (!aContext->lPendingWrites){bQuit = TRUE;}
			}
			LeaveCriticalSection(&aContext->crSection);
			if (bQuit){SetEvent(aContext->hEvent);CleanUp(aContext);}
			return;
		}

		ZeroMemory(aContext->me->m_writeIO,sizeof(IO_BUF));
		InterlockedIncrement(&aContext->lPendingWrites);
		aContext->me->m_writeIO->aContext = aContext;
		aContext->me->m_writeIO->lpo.Offset = aContext->dwWriteOffset;
		CopyMemory(&aContext->me->m_writeIO->buffer,aContext->pszOutBuffer,aContext->dwDownloaded); 

		if (!WriteFile(aContext->hRes,&aContext->me->m_writeIO->buffer,aContext->dwDownloaded,NULL,&aContext->me->m_writeIO->lpo)){
			if ( (dwError = GetLastError()) != ERROR_IO_PENDING ){
				aContext->me->FireOnError(dwError,L"无法写入输出文件：%s");
				CleanUp(aContext);
			}
		}
		aContext->dwWriteOffset += aContext->dwDownloaded;
	}

	if ( (dwError=GetLastError()) == ERROR_IO_PENDING){
		//等待下载完成
	}else{
		aContext->me->FireOnError(dwError,L"无法读取输出文件：%s");
		CleanUp(aContext);
	}
}

VOID CALLBACK WriteFileCallBack(DWORD dwErrorCode,DWORD dwNumberOfBytesTransfered,__in LPOVERLAPPED lpOverlapped){
	BOOL bQuit = FALSE;
	APP_CONTEXT *aContext;
	IO_BUF *ioBuf;
	UNREFERENCED_PARAMETER(dwNumberOfBytesTransfered);
	if (dwErrorCode == ERROR_SUCCESS){
		ioBuf = CONTAINING_RECORD(lpOverlapped,IO_BUF,lpo);
		aContext = ioBuf->aContext;

		EnterCriticalSection(&aContext->crSection);
		{
			if (!InterlockedDecrement(&aContext->lPendingWrites)&& aContext->bReceiveDone){bQuit = TRUE;}
		}
		LeaveCriticalSection(&aContext->crSection);

		if (bQuit){SetEvent(aContext->hEvent);CleanUp(aContext);}
	}
	return;
}

VOID CALLBACK ReadFileCallBack(DWORD dwErrorCode,DWORD dwNumberOfBytesTransfered,__in LPOVERLAPPED lpOverlapped){
	LPSTR buffer;
	APP_CONTEXT *aContext;
	IO_BUF *ioBuf;

	if (dwErrorCode == ERROR_SUCCESS||g_pfnRtlNtStatusToDosError(dwErrorCode) == ERROR_HANDLE_EOF){
		ioBuf = CONTAINING_RECORD(lpOverlapped,IO_BUF,lpo);
		aContext = ioBuf->aContext;
		if ( dwErrorCode == ERROR_SUCCESS){
			buffer = ioBuf->buffer;
			aContext->dwReadOffset += dwNumberOfBytesTransfered;
			aContext->dwRead = dwNumberOfBytesTransfered;
			CopyMemory(aContext->pszOutBuffer,buffer,aContext->dwRead);
			DoInternetWrite(aContext);
		}else{ //ERROR_HANDLE_EOF
			DoCompleteReadFile(aContext);
		}
	}
	return;
}
VOID OpenFiles(__inout APP_CONTEXT *aContext){
	if (_tcslen(aContext->me->m_szPostFile)>0){
		aContext->hFile = CreateFile(aContext->me->m_szPostFile,GENERIC_READ,FILE_SHARE_READ, NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,NULL);
		if (!aContext->hFile||aContext->hFile==INVALID_HANDLE_VALUE){
			aContext->me->FireOnError(GetLastError(),L"无法打开发送文件：%s");
			CleanUp(aContext);
		}else{
			aContext->dwPostFileSize=GetFileSize(aContext->hFile,NULL);
			aContext->me->m_dwPostBytes=aContext->dwPostFileSize;
			if(!BindIoCompletionCallback(aContext->hFile,ReadFileCallBack,0)){
				aContext->me->FireOnError(GetLastError(),L"无法绑定发送文件的完成事件：%s");
				CleanUp(aContext);
			}
		}
	}
	if (_tcslen(aContext->me->m_szResponseFile)>0){
		aContext->hRes = CreateFile(aContext->me->m_szResponseFile,GENERIC_WRITE|GENERIC_READ,0,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,NULL); 
		if (!aContext->hRes||aContext->hRes==INVALID_HANDLE_VALUE){
			aContext->me->FireOnError(GetLastError(),L"无法打开输出文件：%s");
			CleanUp(aContext);
		}else{
			if (!BindIoCompletionCallback(aContext->hRes,WriteFileCallBack,0) ){
				aContext->me->FireOnError(GetLastError(),L"无法绑定输出文件的完成事件：%s");
				CleanUp(aContext);   
			}
		}
	}

	aContext->me->m_writeIO =new IO_BUF;
	aContext->me->m_readIO =new IO_BUF;
}
VOID CleanUp(__in APP_CONTEXT* aContext){
	if(aContext->hFile){CloseHandle(aContext->hFile);aContext->hFile=NULL;}
	if(aContext->hRes){
		CloseHandle(aContext->hRes);aContext->hRes=NULL;
		if(!aContext->me->m_blSaveResponseToFile){
			HANDLE hFile= CreateFile(aContext->me->m_szResponseFile,GENERIC_READ,FILE_SHARE_READ, NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
			if (!hFile||hFile==INVALID_HANDLE_VALUE){
				aContext->me->FireOnError(GetLastError(),L"无法打开请求结果内容所在文件：%s");
			}else{
				DWORD l=GetFileSize(hFile,NULL);
				char* responseText=new char[l+1];
				ZeroMemory(responseText,sizeof(char)*(l+1));
				DWORD dwNumOfBytesRead=0;
				if(ReadFile(hFile,(LPVOID)responseText,l,&dwNumOfBytesRead,NULL)==0||dwNumOfBytesRead!=l){
					aContext->me->FireOnError(GetLastError(),L"无法获取请求结果内容：%s");
				}else{
					SAFE_DEL_ARR(aContext->me->m_szResponseContent);
					aContext->me->m_szResponseContent=new TCHAR[l+1];
					ZeroMemory(aContext->me->m_szResponseContent,sizeof(TCHAR)*(l+1));
					USES_CONVERSION;
					TCHAR* szTmp=A2T(responseText);
					_tcscpy_s(aContext->me->m_szResponseContent,l+1,szTmp);
					SAFE_DEL_ARR(responseText);
				}
				CloseHandle(hFile);
			}
			DeleteFile(aContext->me->m_szResponseFile);
			ZeroMemory(aContext->me->m_szResponseFile,sizeof(TCHAR)*MAX_PATH);
		}
	}
	DWORD dwSync = 0;
	if(aContext->hRequest){
		InternetCloseHandle(aContext->hRequest);
		// 等待请求关闭完成
		dwSync = WaitForSingleObject(aContext->hEvent,INFINITE); 
		if(WAIT_ABANDONED == dwSync){
			aContext->me->FireOnRequest(TERMINATED,L"连接被中止");
		}else{
			CoInitializeEx(0,2);
			if(!aContext->blCanceled) aContext->me->FireOnRequest(DONE,L"请求成功完成");
			CoUninitialize();
		}
		aContext->hRequest=NULL;
	}
	if(aContext->mainContext){
		if(aContext->mainContext->hConnect ) {
			//清理回调函数以阻止再接收到事件
			aContext->mainContext->pCallback=InternetSetStatusCallback(aContext->mainContext->hConnect, NULL );
			InternetCloseHandle(aContext->mainContext->hConnect );
			aContext->mainContext->hConnect=NULL;
		}
		if(aContext->mainContext->hSession){ 
			InternetCloseHandle(aContext->mainContext->hSession);
			aContext->mainContext->hSession=NULL;
		}
	}
	if(aContext->hEvent){CloseHandle(aContext->hEvent);aContext->hEvent=NULL;}
	DeleteCriticalSection(&aContext->crSection);
	ZeroMemory(&aContext->crSection,sizeof(aContext->crSection));
	SAFE_DEL_ARR(aContext->pszOutBuffer);
	if(aContext->me){
		SAFE_DEL_PTR(aContext->me->m_readIO);
		SAFE_DEL_PTR(aContext->me->m_writeIO);
	}
}
BOOL HttpRequest::ParseHeader(TCHAR* szRaw,TCHAR* szName,UINT nameLen,TCHAR* szValue,UINT valLen){
	if(!szRaw ||_tcslen(szRaw)==0) return FALSE;
	TCHAR* sz=_tcschr(szRaw,L':');
	if(sz==NULL) return FALSE;
	ZeroMemory(szName,sizeof(TCHAR)*nameLen);
	ZeroMemory(szValue,sizeof(TCHAR)*valLen);
	_tcsncpy_s(szName,nameLen,szRaw,sz-szRaw);
	_tcscpy_s(szValue,valLen,sz+2);
	return TRUE;
}
BOOL HttpRequest::EvalJSResult(IDispatch* pDisp,TCHAR* szJS,VARIANTARG* args,int cArgs,VARIANT* ret){
	if(pDisp==NULL || szJS==NULL ||_tcslen(szJS)==0) return FALSE;

	IHTMLDocument *pDoc=NULL;
	IDispatch* pScript = NULL;
	DISPID idMethod = 0;
	OLECHAR FAR* sMethod = szJS;

	HRESULT hr=pDisp->QueryInterface(IID_IHTMLDocument,(void**)&pDoc);
	if (FAILED(hr)||pDoc==NULL) {
		ErrMsg(hr,L"无法获取文档对象:%s");
		goto end;
	}
	hr=pDoc->get_Script(&pScript);
	if (FAILED(hr)||pScript==NULL){
		ErrMsg(hr,L"无法获取脚本对象:%s");
		goto end;
	}
	//ie8及以下
	hr = pScript->GetIDsOfNames(IID_NULL, &sMethod, 1, LOCALE_SYSTEM_DEFAULT,&idMethod);
	if (SUCCEEDED(hr)) {
		DISPPARAMS dispArgs = {args,NULL, cArgs, 0};
		hr = pScript->Invoke(idMethod, IID_NULL, LOCALE_SYSTEM_DEFAULT, DISPATCH_METHOD,&dispArgs, ret, NULL, NULL);
		//if(FAILED(hr)){
		//	TCHAR t[100]={0};
		//	_stprintf_s(t,L"0x%X",hr);
		//	TipMsg(t);
		//}
	} else {	//ie9及以上
		CComPtr<IDispatchEx> pDispEx;
		hr = pScript->QueryInterface(IID_IDispatchEx, (void**)&pDispEx);
		hr = pDispEx->GetDispID(CComBSTR( szJS ), fdexNameImplicit, &idMethod );
		DISPPARAMS dispArgs = {args,NULL, cArgs, 0};
		hr = pDispEx->InvokeEx(idMethod, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &dispArgs, ret, NULL, NULL);
		//if(FAILED(hr)){
		//	TCHAR t[100]={0};
		//	_stprintf_s(t,L"0x%X",hr);
		//	TipMsg(t);
		//}
	}
end:
	SAFE_RELEASE(pScript);
	SAFE_RELEASE(pDoc);
	return SUCCEEDED(hr)?TRUE:FALSE;
}

HRESULT HttpRequest::GetHostProgramVersion(LPDWORD pdwMajor, LPDWORD pdwMinor, LPDWORD pdwBuild){
	if(pdwMajor) *pdwMajor=0;
	if(pdwMinor) *pdwMinor=0;
	if(pdwBuild) *pdwBuild=0;
	DWORD dwHandle;
	TCHAR szFileName[MAX_PATH]={0};
	GetModuleFileName(NULL,szFileName,MAX_PATH);
	DWORD cchver = GetFileVersionInfoSize(szFileName,&dwHandle);
	if (cchver == 0) return GetLastError();
	TCHAR* pver = new TCHAR[cchver];
	BOOL bret = GetFileVersionInfo(szFileName,dwHandle,cchver,pver);
	if (!bret) return GetLastError();
	UINT uLen;
	void *pbuf;
	VS_FIXEDFILEINFO pvsf;
	bret = VerQueryValue(pver,_T("\\"),&pbuf,&uLen);
	if (!bret) {SAFE_DEL_PTR(pver);return GetLastError();}
	memcpy(&pvsf,pbuf,sizeof(VS_FIXEDFILEINFO));
	if(pdwMajor)*pdwMajor=HIWORD(pvsf.dwProductVersionMS);
	if(pdwMinor)*pdwMinor=LOWORD(pvsf.dwProductVersionMS);
	if(pdwBuild)*pdwBuild=HIWORD(pvsf.dwProductVersionLS);
	SAFE_DEL_PTR(pver);
	return S_OK;
}