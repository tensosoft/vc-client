// HttpRequest.h : HttpRequest 的声明

#pragma once
#include "resource.h"       // 主符号
#include "http_i.h"
#include "PictureExWnd.h"
#include <SHLGUID.h>
#include <wininet.h>
#include <vector>
using namespace std;

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif

#define EVAL_CALLBACK_PARAM_JSBODY L"if(param==undefined || param==null) return null; \
		if(typeof(param)=='object') return param;	\
		if(typeof(param)=='string' && param.length==0) return '';	\
		if(param.charAt(0)!='{')return param;	\
		var result=(new Function('var a=null;try{a='+param+';}catch(e){alert(e);};return a;'))();	\
		return (result==null?param:result);"

#define DEFAULT_HTTP_PORT 80
#define DEFAULT_HTTPS_PORT 443
#define USER_AGENT	_T("TSHTTP/2.0")

#define TIMER_PROCESSDLG 1
#define TIMER_PROCESSDLG_PERIOD 1000

#define GET 1
#define POST 2

#define GET_REQ 1
#define POST_REQ 2
#define POST_RES 3
#define DEFAULT_TIMEOUT 2 * 60 * 1000 //2分钟

#define INVALID_DOWNLOAD_VALUE (DWORD)-1

#define STRUCT_TYPE_MAIN_CONTEXT 1
#define STRUCT_TYPE_APP_CONTEXT 2

enum ENUM_READY_STATE{INITIALIZED,CONNECTING,CONNECTED,SENDING,RECEIVING,DONE,CLOSING,CLOSED,USERCANCELED=-1,TERMINATED=-2};
enum ENUM_CONTENT_TYPE{JSON,XML,HTML,OTHER};

typedef struct _MAIN_CONTEXT{
	DWORD dwStructType;
	HINTERNET hSession;
	HINTERNET hConnect;
	INTERNET_STATUS_CALLBACK pCallback;
	HttpRequest* me;
	DWORD dwId;
} MAIN_CONTEXT;
VOID InitMainContext(__inout MAIN_CONTEXT *pmc,__in HttpRequest* me);

typedef struct _APP_CONTEXT{
	DWORD dwStructType;
	MAIN_CONTEXT* mainContext;
	HINTERNET hRequest;
	HANDLE hEvent;
	HttpRequest* me;
	LPSTR pszOutBuffer;
	BOOL blCanceled;
	DWORD dwPostFileSize;
	DWORD dwResponseContentLength;
	DWORD dwDownloaded ;
	DWORD dwRead;
	DWORD dwWritten;
	DWORD dwReadOffset;
	DWORD dwWriteOffset;
	HANDLE hFile;
	HANDLE hRes;
	DWORD dwState;
	LONG lPendingWrites;
	BOOL bReceiveDone;
	CRITICAL_SECTION crSection;
} APP_CONTEXT;
VOID InitRequestContext(__in MAIN_CONTEXT* pmc,__inout APP_CONTEXT *pac);

typedef struct{
	OVERLAPPED lpo;
	APP_CONTEXT *aContext;
	char buffer[STRLEN_4K];
}IO_BUF;

typedef vector<IStream*> V_STM;
typedef vector<IStream*>::iterator ITRSTM;

DWORD DoReadFile(__in APP_CONTEXT*);
DWORD DoCompleteReadFile(__in APP_CONTEXT* );
VOID CALLBACK WriteFileCallBack(DWORD ,DWORD ,__in LPOVERLAPPED);
VOID CALLBACK ReadFileCallBack(DWORD ,DWORD ,__in LPOVERLAPPED);
VOID DoInternetWrite(__in APP_CONTEXT*);
VOID DoInternetRead(__in APP_CONTEXT*);
VOID CleanUp(__in APP_CONTEXT*);
VOID OpenFiles(__inout APP_CONTEXT*);

typedef ULONG (NTAPI * PRtlNtStatusToDosError)(IN NTSTATUS Status);
PRtlNtStatusToDosError g_pfnRtlNtStatusToDosError = NULL;

void CALLBACK INetStatusCallBack(HINTERNET hInternet, DWORD dwContext, DWORD dwInternetStatus,LPVOID lpvStatusInformation, DWORD dwStatusInformationLength);
VOID CALLBACK AsyncCallBack( HINTERNET,__in DWORD_PTR,DWORD,__in_bcount(dwStatusInformationLength)LPVOID,DWORD dwStatusInformationLength);
BOOL CALLBACK ProgressProc(HWND, UINT, WPARAM, LPARAM);
DWORD WINAPI ProgressDialog(LPVOID lpParam);

typedef struct {
	HWND			hWindow;										//主窗体句柄
	HWND			hProgressWnd;								//进度窗体句柄
	HINTERNET	hOpen;											//InternetOpen打开的句柄
	HANDLE		hThread;										//HTTP请求线程句柄
	HANDLE		hEvent;											//event
	DWORD			dwThreadID;									//HTTP请求线程ID
	DWORD			dwContentLength;						//HTTP响应大小
	DWORD			dwReceivedLength;						//已接收HTTP响应大小
	DWORD			dwPostFileLength;						//发送文件大小
	DWORD			dwPostedLength;							//已发送大小。
	DWORD			dwHostMajorVersion;					//宿主程序主板本。
	TCHAR			szMemo[STRLEN_DEFAULT];			//请求状态提示文本
} REQUEST_CONTEXT;

static REQUEST_CONTEXT rcContext;

CPictureExWnd m_wndBanner;
HWND hWndGif;

// HttpRequest

class ATL_NO_VTABLE HttpRequest :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<HttpRequest, &CLSID_HttpRequest>,
	public IObjectWithSiteImpl<HttpRequest>,
	public IDispatchImpl<IHttpRequest, &IID_IHttpRequest, &LIBID_httpLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IObjectSafetyImpl<HttpRequest,INTERFACESAFE_FOR_UNTRUSTED_CALLER |INTERFACESAFE_FOR_UNTRUSTED_DATA>
{
public:
	HttpRequest(){
		ZeroMemory(m_szUrl,sizeof(m_szUrl));

		ZeroMemory(m_szUrlProtocol,sizeof(m_szUrlProtocol));
		ZeroMemory(m_szUrlHost,sizeof(m_szUrlHost));
		ZeroMemory(m_szUrlFile,sizeof(m_szUrlFile));
		m_usUrlPort=80;
		m_blUrlIsSSL=VARIANT_FALSE;

		ZeroMemory(m_szSessionKey,sizeof(m_szSessionKey));
		ZeroMemory(m_szMethod,sizeof(m_szMethod));
		_tcscpy_s(m_szMethod,_T("POST"));
		ZeroMemory(m_szPostFile,sizeof(m_szPostFile));
		ZeroMemory(m_szResponseFile,sizeof(m_szResponseFile));
		m_blShowRequestProgress=VARIANT_TRUE;
		m_blSaveResponseToFile=VARIANT_FALSE;

		m_contentType=OTHER;
		m_dwContentLength=0;
		m_dwReceivedBytes=0;
		m_dwPostBytes=0;
		m_dwSentBytes=0;
		m_szResponseHeader=NULL;
		m_szResponseContent=NULL;
		m_szHeader=NULL;

		ZeroMemory(&mc,sizeof(MAIN_CONTEXT));
		ZeroMemory(&ac,sizeof(APP_CONTEXT));

		m_spWebBrowser2=NULL;
		
		m_readIO=NULL;
		m_writeIO=NULL;
		m_userTimeout=DEFAULT_TIMEOUT;

		m_pStmOnRequest=NULL;
		m_pStmOnError=NULL;
		m_pStmWB=NULL;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_HTTPREQUEST)


BEGIN_COM_MAP(HttpRequest)
	COM_INTERFACE_ENTRY(IHttpRequest)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IObjectWithSite)
	COM_INTERFACE_ENTRY(IObjectSafety)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct(){
		//m_tid=GetCurrentThreadId();
		HMODULE hModule = GetModuleHandle( L"ntdll.dll" );
		if(hModule){
			g_pfnRtlNtStatusToDosError = (PRtlNtStatusToDosError)GetProcAddress( hModule, "RtlNtStatusToDosError" );
		}
		return S_OK;
	}

	void FinalRelease(){
		CleanUp(&ac);
		SAFE_DEL_ARR(this->m_szHeader);
		SAFE_DEL_ARR(m_szResponseHeader);
		SAFE_DEL_ARR(m_szResponseContent);
		if(m_pStmOnRequest) {CoReleaseMarshalData(m_pStmOnRequest);SAFE_RELEASE(m_pStmOnRequest);}
		if(m_pStmOnError) {CoReleaseMarshalData(m_pStmOnError);SAFE_RELEASE(m_pStmOnError);}
		if(m_pStmWB) {CoReleaseMarshalData(m_pStmWB);SAFE_RELEASE(m_pStmWB);}
		if(m_spWebBrowser2) m_spWebBrowser2.Release();
	}

	STDMETHODIMP HttpRequest::SetSite(IUnknown *pUnkSite){
		//if (!pUnkSite) {
		//	if (this->m_spWebBrowser2) m_spWebBrowser2.Release();
		//	m_spWebBrowser2=NULL;
		//	return S_OK;
		//}
		CComQIPtr<IServiceProvider> spProv(pUnkSite);
		if (spProv){
			CComPtr<IServiceProvider> spProv2;
			HRESULT hr = spProv->QueryService(SID_STopLevelBrowser, IID_IServiceProvider, reinterpret_cast<void **>(&spProv2));
			if (SUCCEEDED(hr) && spProv2){
				hr=spProv2->QueryService(SID_SWebBrowserApp,IID_IWebBrowser2, reinterpret_cast<void **>(&m_spWebBrowser2));
				if(SUCCEEDED(hr) && m_spWebBrowser2 ){
					IDispatch* pDisp=NULL;
					m_spWebBrowser2->get_Document(&pDisp);
					if(pDisp!=NULL){
						IHTMLDocument2* pDoc2=NULL;
						pDisp->QueryInterface(IID_IHTMLDocument2,(void**)&pDoc2);
						if(pDoc2){
							IHTMLWindow2* pWin2=NULL;
							pDoc2->get_parentWindow(&pWin2);
							if(pWin2){
								TCHAR js[STRLEN_1K]={0};
								_stprintf_s(js,L"if(typeof(window._evalCallbackParam)=='undefined'){window._evalCallbackParam=function(param){%s};}",EVAL_CALLBACK_PARAM_JSBODY);
								BSTR bstrJS=SysAllocString(js);
								BSTR bstrLanguage=SysAllocString(L"javascript");
								VARIANT ret;
								pWin2->execScript(bstrJS,bstrLanguage,&ret);
								FREE_SYS_STR(bstrJS);
								FREE_SYS_STR(bstrLanguage);
								pWin2->Release();
							}
							pDoc2->Release();
						}
						if(m_pStmWB) {CoReleaseMarshalData(m_pStmWB);SAFE_RELEASE(m_pStmWB);}
						hr=CreateStreamOnHGlobal(NULL, TRUE, &m_pStmWB);
						if(SUCCEEDED(hr)){
							LARGE_INTEGER li = { 0 };
							hr=CoMarshalInterface(m_pStmWB, IID_IDispatch, pDisp, MSHCTX_INPROC/*MSHCTX_LOCAL,MSHCTX_INPROC*/, NULL,MSHLFLAGS_TABLESTRONG /*MSHLFLAGS_TABLEWEAK,MSHLFLAGS_TABLESTRONG,MSHLFLAGS_NORMAL*/);
							m_pStmWB->Seek(li, STREAM_SEEK_SET, NULL);
						}//if end
						pDisp->Release();
					}
				}//if end
			}//if end
		}
		
		return S_OK;
	}

public:
	STDMETHOD(get_Url)(BSTR* pVal);
	STDMETHOD(put_Url)(BSTR newVal);
	STDMETHOD(get_SessionKey)(BSTR* pVal);
	STDMETHOD(put_SessionKey)(BSTR newVal);
	STDMETHOD(get_Method)(BSTR* pVal);
	STDMETHOD(put_Method)(BSTR newVal);
	STDMETHOD(get_ShowRequestProgress)(VARIANT_BOOL* pVal);
	STDMETHOD(put_ShowRequestProgress)(VARIANT_BOOL newVal);
	STDMETHOD(get_PostFile)(BSTR* pVal);
	STDMETHOD(put_PostFile)(BSTR newVal);
	STDMETHOD(get_SaveResponseToFile)(VARIANT_BOOL* pVal);
	STDMETHOD(put_SaveResponseToFile)(VARIANT_BOOL newVal);
	STDMETHOD(Send)(IHttpResponse** pVal);
	STDMETHOD(get_UrlProtocol)(BSTR* pVal);
	STDMETHOD(get_UrlPort)(USHORT* pVal);
	STDMETHOD(get_UrlHost)(BSTR* pVal);
	STDMETHOD(get_UrlFile)(BSTR* pVal);
	STDMETHOD(get_UrlIsSSL)(VARIANT_BOOL* pVal);
	STDMETHOD(AddHeader)(BSTR szName,BSTR szValue);
	STDMETHOD(get_ResponseFile)(BSTR* pVal);
	STDMETHOD(put_ResponseFile)(BSTR newVal);
	STDMETHOD(putref_onrequest)(IDispatch* newVal);
	STDMETHOD(putref_onerror)(IDispatch* newVal);
	STDMETHOD(SendAsync)();
	STDMETHOD(Abort)();

	TCHAR m_szPostFile[MAX_PATH];
	VARIANT_BOOL m_blSaveResponseToFile;
	TCHAR m_szResponseFile[MAX_PATH];

	ENUM_CONTENT_TYPE m_contentType;
	TCHAR* m_szResponseContent;
	TCHAR* m_szResponseHeader;
	DWORD m_dwContentLength;
	DWORD m_dwReceivedBytes;
	DWORD m_dwPostBytes;
	DWORD m_dwSentBytes;

	IO_BUF *m_readIO;
	IO_BUF *m_writeIO;
	DWORD m_userTimeout;

	MAIN_CONTEXT mc;
  APP_CONTEXT ac;
	INTERNET_BUFFERS buffersIn;

	HRESULT FireOnRequest(DWORD statusCode,TCHAR*statusDesc);
	HRESULT FireOnError(DWORD errCode,TCHAR* errDesc);

	IStream* m_pStmOnRequest;
	IStream* m_pStmOnError;
	IStream* m_pStmWB;
	static BOOL ParseHeader(TCHAR* szRaw,TCHAR* szName,UINT nameLen,TCHAR* szValue,UINT valLen);
private:
	TCHAR m_szUrl[STRLEN_1K];
	TCHAR m_szUrlProtocol[STRLEN_SMALL];
	TCHAR m_szUrlHost[STRLEN_1K];
	TCHAR m_szUrlFile[STRLEN_1K];
	USHORT m_usUrlPort;
	VARIANT_BOOL m_blUrlIsSSL;

	TCHAR* m_szHeader;

	TCHAR m_szSessionKey[STRLEN_DEFAULT];
	TCHAR m_szMethod[STRLEN_SMALL];
	
	VARIANT_BOOL m_blShowRequestProgress;
	
	CComPtr<IWebBrowser2> m_spWebBrowser2;			//宿主浏览器对象

	//CComPtr<IDispatch> m_pOnrequest;					//请求事件处理程序
	//CComPtr<IDispatch> m_pOnerror;						//异常事件处理程序
	
	BOOL GetErrorMessage(DWORD dwErrCode,TCHAR* szTemplet,TCHAR* szResult,size_t l);
	BOOL CheckUrlIsSecurity();
	void ErrorMsgBox(DWORD dwErrorCode,TCHAR* szTemplet);	//获取错误码（dwErrorCode）对应的错误消息，并把消息用（szTemplet）的文本模板输出（如果文本模板部提供，则只输出dwErrorCode对应错误消息）到错误提示对话框。
	BOOL EvalJSResult(IDispatch* pDisp,TCHAR* szJS,VARIANTARG* args,int cArgs,VARIANT* ret);
	HRESULT GetHostProgramVersion(LPDWORD pdwMajor, LPDWORD pdwMinor, LPDWORD pdwBuild);
};

OBJECT_ENTRY_AUTO(__uuidof(HttpRequest), HttpRequest)
