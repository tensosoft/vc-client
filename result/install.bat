@echo off
@REM  ---------------------------------------------------------------------------------
@REM  install.bat 文件
@REM
@REM  本批处理安装或者卸载客户端组件，在Windows 7或Windows Vista下，您必须以管理员权限执行安装过程。
@REM  
@REM  可选参数:
@REM  /u 表示卸载. 否则为安装.
@REM  (C) 2011 腾硕软件
@REM  ----------------------------------------------------------------------------------

if "%1"=="/?" goto HELP
set Path=%windir%\system32\;%Path%
if NOT Exist %windir%\system32\regsvr32.exe GOTO NOREGSVR32

:begininstall

echo.
echo ==========================================================================
echo 开始安装/卸载
echo ==========================================================================
echo.

echo.
echo ==========================================================================
echo 	注册/卸载常用工具组件
echo ==========================================================================
echo.

regsvr32 /s %1 util.dll

echo.
echo ==========================================================================
echo 	注册/卸载http通讯组件
echo ==========================================================================
echo.

regsvr32 /s %1 http.dll

echo.
echo ==========================================================================
echo 	注册/卸载业务组件
echo ==========================================================================
echo.

regsvr32 /s %1 bo.dll

echo.
echo ==========================================================================
echo 	注册/卸载第三方程序集成插件
echo ==========================================================================
echo.

@REM regsvr32 /s %1 addin.dll

echo.
echo ==========================================================================
echo 	安装Word集成插件
echo ==========================================================================
echo.

if Exist "%APPDATA%\Microsoft\Templates\Normal.dotm" copy "templates\discoverx2common.dotm" "%APPDATA%\Microsoft\Word\STARTUP\" /Y
if Exist "%APPDATA%\Microsoft\Templates\Normal.dot" copy "templates\discoverx2common.dot" "%APPDATA%\Microsoft\Word\STARTUP\" /Y

echo.
echo ==========================================================================
echo 	注册/卸载浏览器扩展组件
echo ==========================================================================
echo.

regsvr32 /s %1 bext.dll

echo.
echo ==========================================================================
echo 	注册/卸载可视化流程编辑组件
echo ==========================================================================
echo.

regsvr32 /s %1 vflow.dll

echo.
echo ==========================================================================
echo 	注册/卸载自动更新组件
echo ==========================================================================
echo.

regsvr32 /s %1 updater.dll

if "%1"=="" goto installcontinue1
if "%1"=="/u" echo 	卸载成功！您可以删除此文件及其本文件所在目录下的其它文件。
goto done

:installcontinue1
if Exist "%cd%\tsim_install.bat" CALL tsim_install.bat
if Exist "%cd%\custom_install.bat" CALL custom_install.bat
goto done

:done
echo.
echo ==========================================================================
echo 完成！
echo ==========================================================================
echo.

goto :exit

:HELP
echo 使用方法: Install.bat [/u]
echo.
echo 比如:
echo.
echo    "Install.bat" - 安装
echo    "Install.bat /u" - 卸载
echo    "Install.bat /?" - 打印此帮助信息
echo.
GOTO :exit

:NOREGSVR32
echo 安装错误：找不到Windows 组件注册程序！

:exit
@REM pause
echo on