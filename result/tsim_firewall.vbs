'设置windows内置防火墙以便小助手能够正常使用。
'在Windows 7或Windows Vista下必须以管理员权限执行此程序。
'(C) 2011 腾硕软件
'参考MSDN上的示例修改而成。
Option Explicit
On Error Resume Next
' Set constants
Const NET_FW_PROFILE_DOMAIN = 0
Const NET_FW_PROFILE_STANDARD = 1

' Scope
Const NET_FW_SCOPE_ALL = 0

' IP Version ：当前只支持ANY
Const NET_FW_IP_VERSION_ANY = 2

'错误号
Dim errornum

' 创建防火墙管理COM组件对象
Dim fwMgr
Set fwMgr = CreateObject("HNetCfg.FwMgr")

' 在当前使用的防火墙配置文件中添加应用程序
Dim profile
Set profile = fwMgr.LocalPolicy.CurrentProfile

'创建并初始化应用程序对象
Dim app
Set app = CreateObject("HNetCfg.FwAuthorizedApplication")
'小助手执行文件路径在名为“DISCOVERX2_PROGRAMS_DIR”的环境变量中，因此必须先确保包含了这个环境变量且设置了正确的目录值。
app.ProcessImageFileName = "%DISCOVERX2_PROGRAMS_DIR%\tsim.exe"
app.Name = "腾硕办公小助手"
'使用 Scope 或 RemoteAddresses, 但是不要都选
app.Scope = NET_FW_SCOPE_ALL	'所有端口
'app.RemoteAddresses = "*"
app.IpVersion = NET_FW_IP_VERSION_ANY
app.Enabled = TRUE
'如果只是添加应用程序到防火墙列表中，但是不启用它则注释以下语句
'app.Enabled = FALSE

On Error Resume Next
errornum = 0
profile.AuthorizedApplications.Add app
errornum = Err.Number
if errornum <> 0 then Wscript.Echo("设置小助手防火墙配置时出现异常，代码为：" & errornum)
