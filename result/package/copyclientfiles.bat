set target=Debug
if "%1"=="Release" set target=Release
copy ..\..\addins\%target%\addins.dll client\
copy ..\..\bext\%target%\bext.dll client\
copy ..\..\bo\%target%\bo.dll client\
copy ..\..\http\%target%\http.dll client\
copy ..\..\updater\%target%\updater.dll client\
copy ..\..\updaterapp\%target%\updater.exe client\
copy ..\..\util\%target%\util.dll client\
copy ..\..\vflow\%target%\vflow.dll client\