@echo off
@REM  ---------------------------------------------------------------------------------
@REM  tsim_autorun.bat
@REM
@REM  本批处理安装或者卸载小助手相关功能，包括配置文件安装、自动运行等。
@REM  
@REM  可选参数:
@REM  /u 表示卸载. 否则不提供参数则为安装.
@REM  (C) 2011 腾硕软件
@REM  ----------------------------------------------------------------------------------

if "%1"=="/?" goto HELP
:begin

echo.
echo ==========================================================================
echo 开始安装/卸载
echo ==========================================================================
echo.

if "%1"=="/u" goto uninstall
if Not Exist "%cd%\tsim.exe" goto notsimerr

:tsimini
if Exist "%cd%\tsim.ini" goto copytsimini

:autorun
SET TSIM_AUTORUNKEY=discoverx2tsim
@reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Run" /v %TSIM_AUTORUNKEY% /t REG_SZ /d "%cd%\tsim.exe" /f

if Exist "%cd%\tsim.dll" regsvr32 /s tsim.dll

:setfirewall
if Exist "%DISCOVERX2_PROGRAMS_DIR%\tsim.exe" tsim_firewall.vbs
goto done

:copytsimini
if Not Exist "%DISCOVERX2_DATA_DIR%\tsim.ini" copy "%cd%\tsim.ini" "%DISCOVERX2_DATA_DIR%\tsim.ini" /Y
del "%cd%\tsim.ini" /Q
goto autorun

:uninstall
@reg delete "HKCU\Software\Microsoft\Windows\CurrentVersion\Run" /v %TSIM_AUTORUNKEY% /f

:done
echo.
echo ==========================================================================
echo 完成！
echo ==========================================================================
echo.

goto :exit

:notsimerr
echo 小助手执行文件tsim.exe不存在！
GOTO :exit

:HELP
echo 使用方法: tsim_autorun.bat [/u]
echo.
echo 比如:
echo.
echo    "tsim_autorun.bat" - 安装小助手自动运行
echo    "tsim_autorun.bat /u" - 卸载小助手自动运行
echo    "tsim_autorun.bat /?" - 打印此帮助信息
echo.
GOTO :exit

:exit
