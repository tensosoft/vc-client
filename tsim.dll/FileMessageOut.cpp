#include "StdAfx.h"
#include "FileMessageOut.h"

FileMessageOut::FileMessageOut(void){
	m_initialized=FALSE;
	m_type=FILESHARE;
	m_fileName=NULL;
	m_fileLen=0;
	m_file=NULL;
	m_filemap=NULL;
	m_fn=NULL;
	m_fnLen=0;
	m_packetCount=0;
}

FileMessageOut::FileMessageOut(TCHAR* fileName){
	m_initialized=FALSE;
	m_type=FILESHARE;
	m_fileName=NULL;
	m_fileLen=0;
	m_file=NULL;
	m_filemap=NULL;
	m_fn=NULL;
	m_fnLen=0;
	m_packetCount=0;
	LEN len=_tcslen(fileName);
	if(len>0){
		len++;
		m_fileName=new TCHAR[len];
		memset(m_fileName,0,sizeof(TCHAR)*(len));
		_tcscpy_s(m_fileName,len,fileName);

		TCHAR* tmp=_tcsrchr(m_fileName,L'\\');
		TCHAR fn[MAX_PATH]={0};
		if(tmp!=NULL) _tcscpy_s(fn,++tmp);
		USES_CONVERSION;
		_acp=GetACP();
		LPSTR lpstrFn=T2A(fn);
		m_fnLen=strlen(lpstrFn);
		m_fn=new BYTE[m_fnLen];
		memcpy(m_fn,lpstrFn,m_fnLen);

		m_file=CreateFile(m_fileName,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
		if(m_file==INVALID_HANDLE_VALUE){
			this->m_lastError=GetLastError();
			ErrMsg(m_lastError,L"打开文件时出现错误：%s");
			m_initialized=FALSE;
			return;
		}

		LARGE_INTEGER li={0};
		if(!GetFileSizeEx(m_file,&li)){
			this->m_lastError=GetLastError();
			ErrMsg(m_lastError,L"无法获取文件大小：%s");
			m_initialized=FALSE;
			return;
		}
		if(li.QuadPart==0){
			this->m_lastError=UDF_ERROR_FILE_EMPTY;
			ErrMsg(0,L"要发送的文件内容为空！");
			m_initialized=FALSE;
			return;
		}
		if(li.QuadPart>MAX_FILE_SEND_LENGTH){
			this->m_lastError=UDF_ERROR_FILE_SUPERSCALE;
			ErrMsg(0,L"不能发送大小超过2G的文件！");
			m_initialized=FALSE;
			return;
		}
		m_fileLen=(DWORD)li.QuadPart;

		//m_filemap = CreateFileMapping(m_file,NULL,PAGE_READONLY,0,dwFileLen,NULL);
		//if(m_filemap==NULL){
		//	this->m_lastError=GetLastError();
		//	ErrMsg(m_lastError,L"错误：%s");
		//	m_initialized=FALSE;
		//	return;
		//}

		m_initialized=TRUE;
	}
}

LEN FileMessageOut::getHeaderLen(){
	return sizeof(LEN)/*包序号（从0开始）*/+sizeof(LEN)/*包总序号*/+sizeof(LEN)/*消息类型*/+sizeof(LEN)/*消息id*/+sizeof(LEN)/*文件大小*/+sizeof(LEN)/*文件名长度*/+m_fnLen/*文件名内容*/;
}

LEN FileMessageOut::getPacketCount(){
	if(m_packetCount>0) return m_packetCount;
	LEN headerLen=getHeaderLen();
	LEN dataLen=MAX_PACKET_LENGTH-headerLen;
	LEN div=m_fileLen/dataLen;
	LEN remainder=m_fileLen%dataLen;
	m_packetCount=div+(remainder!=0?1:0);

	return m_packetCount;
}

BYTE* FileMessageOut::getPacket(LEN packetIndex,LEN* pBytesLen){
	*pBytesLen=0;
	LEN packetCount=this->getPacketCount();
	if(packetCount==0 || packetIndex<0 ||packetIndex>=packetCount) return FALSE;
	
	BYTE* bytes=new BYTE[MAX_PACKET_LENGTH];
	memset(bytes,0,sizeof(BYTE)*MAX_PACKET_LENGTH);

	LEN headerLen=getHeaderLen();
	LEN dataLen=MAX_PACKET_LENGTH-headerLen;
	
	//包格式：包序号（从0开始）/包总序号/消息类型/消息id/文件大小/文件名总长度/文件名内容/文件数据内容
	LEN arr[]={packetIndex,packetCount,this->getType(),this->getMessageId(),m_fileLen,m_fnLen};
	for(int i=0;i<sizeof(arr)/sizeof(arr[0]);i++){
		memcpy(bytes+(i*sizeof(arr[i])),&arr[i],sizeof(arr[i]));
	}

	//文件名内容
	memcpy(bytes+sizeof(arr),m_fn,m_fnLen);

	//文件数据内容
	LEN bytesOffset=sizeof(arr)+m_fnLen;
	LEN dataLenToCopy=(packetIndex<(packetCount-1)?dataLen:m_fileLen-(packetIndex*dataLen));

	BYTE* bs=NULL;
	DWORD bytesRead=0;
	if(packetCount==1){	//只有一个包
		bs=new BYTE[m_fileLen];
		if(!ReadFile(m_file,bs,m_fileLen,&bytesRead,NULL) || bytesRead!=m_fileLen){
			this->m_lastError=GetLastError();
			ErrMsg(m_lastError,L"读取文件时出现错误：%s");
			return NULL;
		}
		dataLenToCopy=bytesRead;
	}else{
		bs=new BYTE[dataLenToCopy];
		if(!ReadFile(m_file,bs,dataLenToCopy,&bytesRead,NULL) || bytesRead!=dataLenToCopy){
			this->m_lastError=GetLastError();
			ErrMsg(m_lastError,L"读取文件时出现错误：%s");
			return NULL;
		}
	}
	memcpy(bytes+bytesOffset,bs,dataLenToCopy);
	SAFE_DEL_ARR(bs);

	//LEN bufOffset=0;
	//LEN fileOffsetHigh=0;
	//LEN fileOffsetLow=0;

	//if(packetCount==1){	//只有一个包
	//	dataLenToCopy=m_fileLen;
	//	fileOffsetLow=0;
	//	bufOffset=0;
	//}else{
	//	SYSTEM_INFO si;
	//	GetSystemInfo(&si);
	//	DWORD granularity=si.dwAllocationGranularity;
	//	LEN maxDataOffset=((packetIndex+1)*dataLen);
	//	LEN divided=maxDataOffset/granularity;
	//	LEN remainder=maxDataOffset%granularity;
	//	fileOffsetLow=granularity*divided;
	//	bufOffset=remainder;
	//}
	//
	//BYTE* pBuf = (BYTE*)MapViewOfFile(m_filemap,FILE_MAP_READ,fileOffsetHigh,fileOffsetLow,dataLenToCopy+bufOffset);
	//if(pBuf==NULL){
	//	m_lastError=GetLastError();
	//	return NULL;
	//}
	//memcpy(bytes+bytesOffset,pBuf+bufOffset,dataLenToCopy);

	//UnmapViewOfFile(pBuf);

	if(pBytesLen) *pBytesLen=bytesOffset+dataLenToCopy; 
	
	return bytes;
}

FileMessageOut::~FileMessageOut(void)
{
	SAFE_DEL_ARR(m_fileName);
	SAFE_DEL_ARR(m_fn);
	if(m_filemap) CloseHandle(m_filemap);
	if(m_file) CloseHandle(m_file);
}
