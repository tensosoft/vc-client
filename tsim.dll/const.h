#pragma once
#ifndef _CONST_H_
#define _CONST_H_

#define MAX_PACKET_LENGTH	60000L												//系统默认udp包大小，最大不能超过65535
#define MAX_FILE_SEND_LENGTH 2147483647L							//最大可传输文件
#define DEFAULT_SERVER_PORT 52321											//默认UDP端口
/*消息类型*/
enum MESSAGETYPE {IM/*即时消息*/,FILESHARE/*文件发送接收消息*/,CONTROL/*控制消息*/};

/*控制消息类型*/
//enum CONTROLMESSAGETYPE {UNKNOWN/*未知控制消息类型*/,SHAKEHANDS/*点对点交互握手*/,IMRECEIVED/*即时消息内容接收完成发给发送人的反馈*/,FILESEND/*发送文件请求*/,FILESENDACCEPTED/*同意接收发送的文件*/,FILESENDDENIED/*拒绝接收发送的文件*/,FILERECEIVED/*文件接收成功*/,FILEFAIL/*文件发送或接收失败*/};
enum CONTROLMESSAGETYPE {UNKNOWN/*未知控制消息类型*/,IMRECEIVED/*即时消息内容接收完成发给发送人的反馈*/,FILERECEIVED/*文件接收成功*/,PACKETRECEIVED/*包接收成功*/};

/*发送状态*/
enum SENDSTATUS{SENDING/*发送中*/,SENT/*发送完成*/,RECEIVED/*目标已接收*/,NONE=-1/*无动作*/};

typedef unsigned long LEN;
typedef LEN MSGID;

//socket有关类型定义
typedef short WSFAMILY;
typedef struct in_addr WSADDRESSIPV4;
typedef struct sockaddr_in WSSOCKADDRESS;
typedef unsigned short WSPORT;
typedef WSFAMILY *PWSFAMILY;
typedef WSADDRESSIPV4 *PWSADDRESSIPV4;
typedef WSSOCKADDRESS *PWSSOCKADDRESS;
typedef WSPORT *PWSPORT;

#endif