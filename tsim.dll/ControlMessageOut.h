#pragma once

#ifndef _CONTROLMESSAGEOUT_H_
#define _CONTROLMESSAGEOUT_H_

#include "messageout.h"
class ControlMessageOut :	public MessageOut
{
public:
	ControlMessageOut(CONTROLMESSAGETYPE cmType,LEN targetMessageId);
	virtual ~ControlMessageOut(void);

	virtual LEN getPacketCount();
	virtual BYTE* getPacket(LEN packetIndex,LEN* pBytesLen);
	virtual LEN getHeaderLen();
	virtual LEN getTargetMessageId();
	virtual CONTROLMESSAGETYPE getControlMessageType();
protected:
	ControlMessageOut(void);
private:
	LEN m_targetMessageId;
	CONTROLMESSAGETYPE m_controlMessageType;
};

#endif

