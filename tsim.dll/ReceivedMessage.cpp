// ReceivedMessage.cpp : ReceivedMessage ��ʵ��

#include "stdafx.h"
#include "ReceivedMessage.h"
#include <atlstr.h>

// ReceivedMessage

STDMETHODIMP ReceivedMessage::get_MessageType(ULONG* pVal){
	*pVal=m_mt;
	return S_OK;
}

STDMETHODIMP ReceivedMessage::put_MessageType(ULONG newVal){
	m_mt=newVal;
	return S_OK;
}

STDMETHODIMP ReceivedMessage::get_ID(ULONG* pVal){
	*pVal=m_id;
	return S_OK;
}

STDMETHODIMP ReceivedMessage::put_ID(ULONG newVal){
	m_id=newVal;
	return S_OK;
}

STDMETHODIMP ReceivedMessage::get_Result(BSTR* pVal){
	*pVal=m_result;
	return S_OK;
}

STDMETHODIMP ReceivedMessage::put_Result(BSTR newVal){
	m_result=SysAllocString(newVal);
	return S_OK;
}

STDMETHODIMP ReceivedMessage::get_Source(BSTR* pVal){
	*pVal=m_source;
	return S_OK;
}

STDMETHODIMP ReceivedMessage::put_Source(BSTR newVal){
	m_source=SysAllocString(newVal);
	return S_OK;
}

STDMETHODIMP ReceivedMessage::get_DateTime(BSTR *pVal){
	*pVal=m_dt;
	return S_OK;
}

STDMETHODIMP ReceivedMessage::put_DateTime(BSTR newVal){
	m_dt=SysAllocString(newVal);
	return S_OK;
}

STDMETHODIMP ReceivedMessage::toString(BSTR* ret){
	CAtlString atlStr(L"");
	
	atlStr.Append(m_result);
	atlStr.Replace(L"'",L"\\'");
	atlStr.Replace(L"\r\n",L"\\r\\n");
	atlStr.Replace(L"\r",L"\\r");
	atlStr.Replace(L"\n",L"\\n");
	atlStr.Insert(0,L"{result:'");
	atlStr.Append(L"'");

	atlStr.Append(L",source:'");
	atlStr.Append(m_source);
	atlStr.Append(L"'");

	atlStr.AppendFormat(L",messageType:%u",m_mt);
	atlStr.AppendFormat(L",id:%u",m_id);

	atlStr.Append(L",dateTime:'");
	atlStr.Append(m_dt);
	atlStr.Append(L"'");

	atlStr.Append(L"}");

	*ret=atlStr.AllocSysString();
	return S_OK;
}