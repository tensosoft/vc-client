/*
* 发出去的消息基类。
*	(c) 腾硕软件科技 2010-12-10
*/
#pragma once

#ifndef _MESSAGEOUT_H_
#define _MESSAGEOUT_H_

#include "messagebase.h"
class MessageOut :	public MessageBase
{
public:
	MessageOut(void);
	MessageOut(TCHAR* content,MESSAGETYPE messageType);
	virtual ~MessageOut(void);
	virtual LEN getMessageId();
	virtual LEN getPacketCount()=0;							//返回发送此消息需要的包个数。
	virtual BYTE* getPacket(LEN packetIndex,LEN* pBytesLen)=0;	//填充序号（序号从0开始）指定的包内容并返回之，方法返回时pBytesLen包含了实际的包内容长度（字节），调用者需负责回收返回的内容。
private:
	DWORD m_msgId;
};

#endif