// MessageChannel.h : MessageChannel 的声明

#pragma once
#include "resource.h"       // 主符号
#include "tsim_i.h"
#include "_IMessageChannelEvents_CP.h"
#include "ReceivedMessage.h"
#include "NetChannel.h"

#define ROT_ITEM_NAME L"87A71876F79B440EA5E18AA6A71C8CF8"

typedef pair<DWORD,CComPtr<IDispatch>> CBPair;
typedef map<DWORD,CComPtr<IDispatch>> CBMap;
typedef map<DWORD,CComPtr<IDispatch>>::iterator CBItr;

typedef pair<DWORD,IStream*> CBSPair;
typedef map<DWORD,IStream*> CBSMap;
typedef map<DWORD,IStream*>::iterator CBSItr;

#define WM_IMRECEIVED WM_APP+201
#define WM_SENDSTATUSCHANGED WM_APP+203
#define WM_NETERROR WM_APP+205
#define POST_MESSAGE_TARGET_WINDOW_CLS_NAME	L"TSIMAPP"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif

using namespace ATL;

DWORD WINAPI SenderThreadProc(LPVOID lpParam);

// MessageChannel
class ATL_NO_VTABLE MessageChannel :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<MessageChannel, &CLSID_MessageChannel>,
	public IConnectionPointContainerImpl<MessageChannel>,
	public CProxy_IMessageChannelEvents<MessageChannel>,
	public IObjectWithSiteImpl<MessageChannel>,
	public IDispatchImpl<IMessageChannel, &IID_IMessageChannel, &LIBID_tsimLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IObjectSafetyImpl<MessageChannel,INTERFACESAFE_FOR_UNTRUSTED_CALLER |INTERFACESAFE_FOR_UNTRUSTED_DATA>
{
public:
	MessageChannel():m_currentThreadId(0),m_dwROTRegisterNC(0),m_dwROTRegister(0),m_spWebBrowser2(NULL),m_pmo(NULL),m_spNC(NULL)
	{
		ZeroMemory(m_szMonikerName,sizeof(TCHAR)*STRLEN_SMALL);
		_stprintf_s(m_szMonikerName,L"%X%X%X",GetCurrentProcessId(),GetCurrentThreadId(),this);
	}

DECLARE_REGISTRY_RESOURCEID(IDR_MESSAGECHANNEL)

BEGIN_COM_MAP(MessageChannel)
	COM_INTERFACE_ENTRY(IMessageChannel)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
	COM_INTERFACE_ENTRY(IObjectWithSite)
	COM_INTERFACE_ENTRY(IObjectSafety)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(MessageChannel)
	CONNECTION_POINT_ENTRY(__uuidof(_IMessageChannelEvents))
END_CONNECTION_POINT_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()	{
		m_event=CreateEvent(NULL,FALSE,FALSE,L"sendwaitevent");
		InitializeCriticalSectionAndSpinCount(&criticalSection, 0x80000400); 
		GetNetChannel();
		if(m_spNC){
			BSTR bstr=SysAllocString(m_szMonikerName);
			m_spNC->RegisterMessageChannel(bstr);
			FREE_SYS_STR(bstr);
		}
		
		m_currentThreadId=GetCurrentThreadId();
		
		if (WSAStartup(MAKEWORD(2, 2), &m_wsaData) != 0){
			ReceiveError(WSAGetLastError(),L"");
			return S_OK;
		}
		if ((m_sockSend = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP))==INVALID_SOCKET){
			ReceiveError(WSAGetLastError(),L"");
			return S_OK;
		}
		m_hSenderThread=CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE)SenderThreadProc,(LPVOID)this,0,NULL);
		return S_OK;
	}

	void FinalRelease()	{
		TerminateThread(m_hSenderThread,0);
		CloseHandle(m_hSenderThread);
		CloseHandle(m_event);
		VARIANT_BOOL bl;
		Dispose(&bl);
		
		DeleteCriticalSection(&criticalSection);

		closesocket(m_sockSend);
		WSACleanup();
	}

	STDMETHODIMP MessageChannel::SetSite(IUnknown *pUnkSite){
		if (!pUnkSite) {
			if (this->m_spWebBrowser2) m_spWebBrowser2.Release();
			m_spWebBrowser2=NULL;
			return S_OK;
		}
		CComQIPtr<IServiceProvider> spProv(pUnkSite);
		if (spProv){
			CComPtr<IServiceProvider> spProv2;
			HRESULT hr = spProv->QueryService(SID_STopLevelBrowser, IID_IServiceProvider, reinterpret_cast<void **>(&spProv2));
			if (SUCCEEDED(hr) && spProv2){
				 hr=spProv2->QueryService(SID_SWebBrowserApp,IID_IWebBrowser2, reinterpret_cast<void **>(&m_spWebBrowser2));
			}
		}
		
		return S_OK;
	}
public:
	STDMETHOD(SendMessage)(BSTR msg,BSTR dest,ULONG* ret);
	STDMETHOD(putref_onreceived)(IDispatch* newVal);
	STDMETHOD(putref_onsent)(IDispatch* newVal);
	STDMETHOD(putref_onerr)(IDispatch* newVal);
	STDMETHOD(ReceiveMessage)(BSTR msg,ULONG msgId,BSTR src,BSTR dt,LONG msgType);
	STDMETHOD(ReceiveSendStatus)(LONG statusCode,ULONG msgId,BSTR dest);
	STDMETHOD(ReceiveError)(LONG errCode,BSTR errDesc);
	STDMETHOD(Dispose)(VARIANT_BOOL* ret);
	STDMETHOD(SendFile)(BSTR fn,BSTR dest,ULONG* ret);
	
	WSADATA m_wsaData;											//winsock初始化数据
	int m_sockSend;													//sock句柄（发送用）
	
	V_MSGOUT m_out;													//待发消息队列
	MessageOut* m_pmo;											//待发消息
	HANDLE m_hSenderThread;									//发送线程句柄
	CRITICAL_SECTION criticalSection;				//线程锁定标记
	HANDLE m_event;
private:
	CBMap m_onreceived;											//消息接收到时触发的事件处理程序信息
	CBMap m_onsent;													//消息发送完成时触发的事件处理程序信息
	CBMap m_onerror;												//消息收发异常时触发的事件处理程序信息

	CBSMap m_cbsr;													//消息接收到时触发的事件处理程序信息(跨线程)
	CBSMap m_cbss;													//消息发送完成时触发的事件处理程序信息(跨线程)
	CBSMap m_cbse;													//消息收发异常时触发的事件处理程序信息(跨线程)

	DWORD m_currentThreadId;								//当前线程id
	INetChannel* m_spNC;										//包含的对网络传输组件的引用

	CComPtr<IWebBrowser2> m_spWebBrowser2;

	TCHAR m_szMonikerName[STRLEN_SMALL];
	DWORD m_dwROTRegisterNC;
	DWORD m_dwROTRegister;
	CComPtr<IMoniker> m_spMon;
	CComPtr<IBindCtx> m_spCtx;
	CComPtr<IRunningObjectTable> m_spROT;

	HRESULT fireOnReceived(CComPtr<IReceivedMessage> pDisp);
	HRESULT fireOnSent(LONG statusCode,ULONG msgId,BSTR dest);
	HRESULT fireOnError(LONG errCode,BSTR errDesc);

	HRESULT GetNetChannel();
};

OBJECT_ENTRY_AUTO(__uuidof(MessageChannel), MessageChannel)
