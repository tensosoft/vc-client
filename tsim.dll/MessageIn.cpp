#include "StdAfx.h"
#include "MessageIn.h"

MessageIn::MessageIn(void)
{
	m_messageId=0;
	GetLocalTime(&m_timestamp);
	m_currentIndex=0;
	m_lastIndex=0;
	m_packetCount=0;
	m_receivedPacketCount=0;
}

LEN MessageIn::getMessageId(){
	return m_messageId;
}

BOOL MessageIn::isInitialized(){
	return (m_messageId>0);
}

BOOL MessageIn::isReceived(){
	return (m_receivedPacketCount==m_packetCount);
}

BOOL MessageIn::getTimeStamp(LPSYSTEMTIME lpSysTime){
	if(!lpSysTime) return FALSE;
	(*lpSysTime).wYear=m_timestamp.wYear;		
	(*lpSysTime).wMonth=m_timestamp.wMonth;
	(*lpSysTime).wDayOfWeek=m_timestamp.wDayOfWeek;
	(*lpSysTime).wDay=m_timestamp.wDay;
	(*lpSysTime).wHour=m_timestamp.wHour;
	(*lpSysTime).wMinute=m_timestamp.wMinute;
	(*lpSysTime).wSecond=m_timestamp.wSecond;
	(*lpSysTime).wMilliseconds=m_timestamp.wMilliseconds;
	return TRUE;
}

MessageIn::~MessageIn(void)
{
}