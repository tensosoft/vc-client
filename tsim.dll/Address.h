#pragma once
#ifndef _ADDRESS_H_
#define _ADDRESS_H_
#include "stdafx.h"
#include "const.h"
class Address
{
public:
	Address(void);
	Address(WSSOCKADDRESS& address);
	Address(TCHAR* ipv4,WSPORT port);
	Address(TCHAR* ipv4);
	virtual ~Address(void);

	WSSOCKADDRESS getAddress();
	BOOL getAddressString(TCHAR* addressString,LEN lenOfAddressString);
	WSPORT getPort();
	void setPort(WSPORT family);
	int ToString(TCHAR* result,size_t resultSize);
protected:
	WSSOCKADDRESS m_address; 
};

#endif