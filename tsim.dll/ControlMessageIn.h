#pragma once

#ifndef _CONTROLMESSAGEIN_H_
#define _CONTROLMESSAGEIN_H_

#include "messagein.h"
class ControlMessageIn :	public MessageIn
{
public:
	ControlMessageIn(BYTE* firstPacket,LEN firstPacketBytesLen);
	virtual ~ControlMessageIn(void);
	virtual LEN getHeaderLen();
	virtual BOOL appendPacket(BYTE* bytes,LEN bytesLen);
	virtual LEN getTargetMessageId();
	virtual CONTROLMESSAGETYPE getControlMessageType();
protected:
	ControlMessageIn(void);
private:
	LEN m_targetMessageId;
	CONTROLMESSAGETYPE m_controlMessageType;
};

#endif
