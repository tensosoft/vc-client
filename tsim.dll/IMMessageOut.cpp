#include "StdAfx.h"
#include "IMMessageOut.h"

IMMessageOut::IMMessageOut(void)
{
	m_type=IM;
	m_content=NULL;
	m_byteContent=NULL;
	m_byteContentLen=0;
	m_packetCount=0;
}

IMMessageOut::IMMessageOut(TCHAR* content){
	m_type=IM;
	m_content=NULL;
	m_byteContent=NULL;
	m_byteContentLen=0;
	m_packetCount=0;
	LEN len=_tcslen(content);
	if(len>0){
		m_content=new TCHAR[len+1];
		memset(m_content,0,sizeof(TCHAR)*(len+1));
		_tcscpy_s(m_content,len+1,content);
		USES_CONVERSION;
		_acp=GetACP();
		LPSTR lpstr=T2A(content);
		len=strlen(lpstr);
		m_byteContent=new BYTE[len];
		memcpy(m_byteContent,lpstr,len);
		m_byteContentLen=len;
	}
}

IMMessageOut::~IMMessageOut(void)
{
	SAFE_DEL_PTR(m_content);
	SAFE_DEL_PTR(m_byteContent);
}

LEN IMMessageOut::getHeaderLen(){
	return sizeof(LEN)/*包序号（从0开始）*/+sizeof(LEN)/*包总序号*/+sizeof(LEN)/*消息类型*/+sizeof(LEN)/*消息id*/;
}

LEN IMMessageOut::getPacketCount(){
	if(m_packetCount>0) return m_packetCount;
	LEN headerLen=getHeaderLen();
	LEN dataLen=MAX_PACKET_LENGTH-headerLen;
	for(LEN i=0;i<m_byteContentLen;i+=dataLen){
		m_packetCount++;
	}
	return m_packetCount;
}

BYTE* IMMessageOut::getPacket(LEN packetIndex,LEN* pBytesLen){
	*pBytesLen=0;
	LEN packetCount=this->getPacketCount();
	if(packetCount==0 || packetIndex<0 ||packetIndex>=packetCount) return FALSE;
	
	BYTE* bytes=new BYTE[MAX_PACKET_LENGTH];
	memset(bytes,0,sizeof(BYTE)*MAX_PACKET_LENGTH);

	LEN headerLen=getHeaderLen();
	LEN dataLen=MAX_PACKET_LENGTH-headerLen;
	
	//包格式：包序号（从0开始）/包总序号/消息类型/消息id/即时消息内容
	LEN msgId=this->getMessageId();
	//LEN arr[]={htonl(packetIndex),htonl(packetCount),htonl(this->getType()),htonl(msgId)};
	LEN arr[]={packetIndex,packetCount,this->getType(),msgId};

	//for(int i=0;i<sizeof(arr)/sizeof(arr[0]);i++){}
	memcpy(bytes,&arr[0],sizeof(arr));

	LEN dataLenToCopy=(packetIndex<(packetCount-1)?dataLen:m_byteContentLen-(packetIndex*dataLen));
	memcpy(bytes+sizeof(arr),m_byteContent+(packetIndex*dataLen),dataLenToCopy);

	if(pBytesLen) *pBytesLen=sizeof(arr)+dataLenToCopy;
	
	return bytes;
}