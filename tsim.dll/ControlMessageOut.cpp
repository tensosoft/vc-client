#include "StdAfx.h"
#include "ControlMessageOut.h"

ControlMessageOut::ControlMessageOut(void)
{
	m_type=CONTROL;
	m_targetMessageId=0;
	m_controlMessageType=IMRECEIVED;
}

ControlMessageOut::ControlMessageOut(CONTROLMESSAGETYPE cmType,LEN targetMessageId){
	m_type=CONTROL;
	m_controlMessageType=cmType;
	m_targetMessageId=targetMessageId;
}

ControlMessageOut::~ControlMessageOut(void)
{
}

LEN ControlMessageOut::getHeaderLen(){
	return sizeof(LEN)/*包序号（从0开始）*/+sizeof(LEN)/*包总序号*/+sizeof(LEN)/*消息类型*/;
}

LEN ControlMessageOut::getPacketCount(){
	return 1;
}

BYTE* ControlMessageOut::getPacket(LEN packetIndex,LEN* pBytesLen){
	*pBytesLen=0;
	LEN packetCount=this->getPacketCount();
	if(packetCount==0 || packetIndex<0 ||packetIndex>=packetCount) return NULL;

	BYTE* bytes=new BYTE[MAX_PACKET_LENGTH];
	memset(bytes,0,sizeof(BYTE)*MAX_PACKET_LENGTH);

	LEN headerLen=getHeaderLen();
	LEN dataLen=MAX_PACKET_LENGTH-headerLen;
	
	//包格式：包序号（从0开始）/包总序号/消息类型/控制消息类别/控制消息控制的消息id|别的数据
	//LEN arr[]={htonl(packetIndex),htonl(packetCount),htonl(this->getType()),htonl(this->m_controlMessageType),htonl(this->m_targetMessageId)};
	LEN arr[]={packetIndex,packetCount,this->getType(),this->m_controlMessageType,this->m_targetMessageId};

	memcpy(bytes,&arr[0],sizeof(arr));

	if(pBytesLen) *pBytesLen=sizeof(arr);
	
	return bytes;
}

LEN ControlMessageOut::getTargetMessageId(){
	return this->m_targetMessageId;
}
CONTROLMESSAGETYPE ControlMessageOut::getControlMessageType(){
	return this->m_controlMessageType;
}