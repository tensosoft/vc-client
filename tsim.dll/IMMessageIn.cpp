#include "StdAfx.h"
#include "IMMessageIn.h"

IMMessageIn::IMMessageIn(void)
{
	m_contentLen=0;
}

IMMessageIn::IMMessageIn(BYTE* firstPacket,LEN firstPacketBytesLen){
	m_contentLen=0;
	appendPacket(firstPacket,firstPacketBytesLen);
}

LEN IMMessageIn::getHeaderLen(){
	return sizeof(LEN)/*包序号（从0开始）*/+sizeof(LEN)/*包总序号*/+sizeof(LEN)/*消息类型*/+sizeof(LEN)/*消息id*/;
}

BOOL IMMessageIn::appendPacket(BYTE* bytes,LEN bytesLen){
	LEN headerLen=getHeaderLen();
	if(!bytes || bytesLen==0 || bytesLen<=headerLen){
		m_lastError=UDF_ERROR_FILE_PACKET_CORRUPT;
		return FALSE;
	}

	//头
	LEN headerCnt=headerLen/sizeof(LEN);	//4
	LEN* iarr=new LEN[headerCnt];	//预先解析头信息:包序号、包个数、消息类型、消息id
	memcpy(iarr,bytes,headerLen);

	//包序号、包个数、消息类型、消息id不符则丢弃
	if((m_receivedPacketCount==0&&iarr[0]!=0) || (m_receivedPacketCount>0&&iarr[0]==0) || iarr[1]==0 || (m_packetCount>0 && iarr[1]!=m_packetCount) || iarr[2]!=(LEN)IM || iarr[3]==0 ||(m_messageId!=0 && iarr[3]!=m_messageId)){
		SAFE_DEL_ARR(iarr);
		return TRUE;
	}

	if(m_immap.find(iarr[0])!=m_immap.end()) {SAFE_DEL_ARR(iarr);return TRUE;}		//重复的包也丢弃

	//内容
	LEN contentLen=bytesLen-headerLen;
	m_contentLen+=contentLen;
	BYTE* content=new BYTE[contentLen+1];
	memset(content,0,contentLen+1);
	memcpy(content,bytes+headerLen,contentLen);
	m_immap[iarr[0]]=content;

	//设置状态变量
	m_currentIndex=iarr[0];
	m_packetCount=iarr[1];
	m_type=(MESSAGETYPE)iarr[2];
	m_messageId=iarr[3];
	
	if(m_receivedPacketCount!=0) m_lastIndex=m_currentIndex;
	m_receivedPacketCount++;

	SAFE_DEL_ARR(iarr);
	
	return TRUE;
}

LEN IMMessageIn::getResultLen(){
	LEN len=0;
	getResult(NULL,&len);
	return len;
}

BOOL IMMessageIn::getResult(TCHAR* result,LEN* resultLen){
	if(!resultLen) return FALSE;
	char* bytes=new char[m_contentLen+1];
	ZeroMemory(bytes,m_contentLen+1);
	PIMItr itr;
	int idx=0;
	for ( itr = m_immap.begin(); itr != m_immap.end(); itr++){
		strcat_s(bytes,m_contentLen+1,(char*)itr->second);
		idx++;
	}
	USES_CONVERSION;
	_acp=GetACP();
	TCHAR* tmp=A2T((char*)bytes);
	LEN resultLenCalc=_tcslen(tmp);
	if(*resultLen<=resultLenCalc) {
		*resultLen=resultLenCalc;
		return FALSE;
	}
	if(result) {
		memset(result,0,sizeof(TCHAR)*(*resultLen));
		_tcscpy_s(result,*resultLen,tmp);
	}
	return TRUE;
}

IMMessageIn::~IMMessageIn(void){
	PIMItr itr;
	for ( itr = m_immap.begin(); itr != m_immap.end( ); itr++){
		SAFE_DEL_ARR(itr->second);
	}
	m_immap.clear();
}