#include "StdAfx.h"
#include "FileMessageIn.h"

FileMessageIn::FileMessageIn(void){
	m_filePath=NULL;
	m_fileName=NULL;
	m_fileNameLen=0;
	m_fileLen=0;
	m_file=NULL;
	m_filemap=NULL;
	m_type=FILESHARE;
}

FileMessageIn::FileMessageIn(BYTE* firstPacket,LEN firstPacketBytesLen,const TCHAR* filePath){
	m_filePath=NULL;
	m_fileName=NULL;
	m_fileNameLen=0;
	m_fileLen=0;
	m_file=NULL;
	m_filemap=NULL;
	m_type=FILESHARE;
	LEN len=_tcslen(filePath);
	if(len>0){
		m_filePath=new TCHAR[len+1];
		_tcscpy_s(m_filePath,len+1,filePath);
	}else{
		m_lastError=UDF_ERROR_FILE_TARGETPATH;
		return;
	}

	appendPacket(firstPacket,firstPacketBytesLen);

	//m_filemap = CreateFileMapping(m_file,NULL,PAGE_READWRITE,0,m_fileLen,NULL);
	//if(m_filemap==NULL){
	//	this->m_lastError=GetLastError();
	//	return;
	//}
}

LEN FileMessageIn::getHeaderLen(){
	return sizeof(LEN)/*包序号（从0开始）*/+sizeof(LEN)/*包总序号*/+sizeof(LEN)/*消息类型*/+sizeof(LEN)/*消息id*/+sizeof(LEN)/*文件大小*/+sizeof(LEN)/*文件名长度*/;
}

BOOL FileMessageIn::appendPacket(BYTE* bytes,LEN bytesLen){
	LEN headerLen=getHeaderLen();
	if(!bytes || bytesLen==0 || bytesLen<=headerLen){
		m_lastError=UDF_ERROR_FILE_PACKET_CORRUPT;
		return FALSE;
	}
	//头
	LEN headerCnt=headerLen/sizeof(LEN);	//6
	LEN* iarr=new LEN[headerCnt];	//预先解析头信息:包序号、包个数、消息类型、消息id、文件大小、文件名长度
	memcpy(iarr,bytes,headerLen);

	if((m_receivedPacketCount==0&&iarr[0]!=0) || (m_receivedPacketCount>0&&iarr[0]==0) || iarr[1]==0 || (m_packetCount>0 && iarr[1]!=m_packetCount) || iarr[2]!=(LEN)FILESHARE || iarr[3]==0 ||(m_messageId!=0 && iarr[3]!=m_messageId)){
		SAFE_DEL_ARR(iarr);
		return TRUE;
	}

	//文件长度和文件名长度不匹配则出错
	if(iarr[4]==0 || (m_fileLen>0 && m_fileLen!=iarr[4]) || iarr[5]==0 || (m_fileNameLen>0 && m_fileNameLen!=iarr[5]))	{
		m_lastError=UDF_ERROR_FILE_PACKET_CORRUPT;
		SAFE_DEL_ARR(iarr);
		return FALSE;
	}

	//重复的包也丢弃
	PFMIItr itr=m_map.find(iarr[0]);
	if(itr!=m_map.end() && (itr->second)) {SAFE_DEL_ARR(iarr);return TRUE;}

	//文件名
	if(m_fileName==NULL){
		LEN fnPacketLen=iarr[5];
		BYTE* fnbs=new BYTE[fnPacketLen+1];
		memset(fnbs,0,fnPacketLen+1);
		memcpy(fnbs,bytes+headerLen,fnPacketLen);
		USES_CONVERSION;
		_acp=GetACP();
		TCHAR* tmp=A2T((LPCSTR)fnbs);
		LEN tmpLen=_tcslen(tmp);
		m_fileName=new TCHAR[tmpLen+1];
		_tcscpy_s(m_fileName,tmpLen+1,tmp);

		m_file=INVALID_HANDLE_VALUE;
		TCHAR fn[MAX_PATH]={0};
		if(getFullPath(fn,MAX_PATH)){
			m_file=CreateFile(fn,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
		}
		if(m_file==INVALID_HANDLE_VALUE){
			this->m_lastError=GetLastError();
			ErrMsg(m_lastError,L"创建文件时出现错误：%s");
			return FALSE;
		}
	}

	//文件数据内容
	LEN bytesOffset=headerLen+iarr[5];
	LEN dataLen=bytesLen-bytesOffset;
	LEN dataLenToCopy=dataLen;

	DWORD bytesWritten=0;
	if(!WriteFile(m_file,bytes+bytesOffset,dataLenToCopy,&bytesWritten,NULL) || bytesWritten!=dataLenToCopy){
		m_lastError=GetLastError();
		ErrMsg(m_lastError,L"写入文件时出现错误：%s");
		return FALSE;
	}

	//LEN bufOffset=0;
	//LEN fileOffsetHigh=0;
	//LEN fileOffsetLow=0;

	//SYSTEM_INFO si;
	//GetSystemInfo(&si);
	//DWORD granularity=si.dwAllocationGranularity;
	//if(iarr[1]==1){	//只有一个包
	//	dataLenToCopy=m_fileLen;
	//	fileOffsetLow=0;
	//	bufOffset=0;
	//}else{
	//	LEN maxDataOffset=((iarr[0]+1)*dataLen);
	//	LEN divided=maxDataOffset/granularity;
	//	LEN remainder=maxDataOffset%granularity;
	//	fileOffsetLow=granularity*divided;
	//	bufOffset=remainder;
	//}
	//
	//BYTE* pBuf = (BYTE*)MapViewOfFile(m_filemap,FILE_MAP_WRITE,fileOffsetHigh,fileOffsetLow,dataLenToCopy+bufOffset);
	//if(pBuf==NULL){
	//	m_lastError=GetLastError();
	//	SAFE_DEL_ARR(iarr);
	//	return NULL;
	//}

	//memcpy(pBuf+bufOffset,bytes+bytesOffset,dataLenToCopy);

	//UnmapViewOfFile(pBuf);

	//设置状态变量
	m_currentIndex=iarr[0];
	m_packetCount=iarr[1];
	m_type=(MESSAGETYPE)iarr[2];
	m_messageId=iarr[3];
	m_fileLen=iarr[4];
	m_fileNameLen=iarr[5];

	if(m_receivedPacketCount!=0) m_lastIndex=m_currentIndex;
	m_receivedPacketCount++;
	m_map[iarr[0]]=TRUE;

	SAFE_DEL_ARR(iarr);

	if(isReceived()) {if(m_file) CloseHandle(m_file);}
	return TRUE;
}

FileMessageIn::~FileMessageIn(void)
{
	SAFE_DEL_PTR(m_filePath);
	SAFE_DEL_PTR(m_fileName);
	if(m_filemap) CloseHandle(m_filemap);
	if(m_file) CloseHandle(m_file);
	m_map.clear();
}

BOOL FileMessageIn::getFullPath(TCHAR* path,size_t pathLen){
	if(m_filePath==NULL||_tcslen(m_filePath)==0||m_fileName==NULL||_tcslen(m_fileName)==0) return FALSE;
	ZeroMemory(path,sizeof(TCHAR)*pathLen);
	_tcscpy_s(path,pathLen,m_filePath);
	size_t l=_tcslen(path);
	if(*(path+(l-1))!=L'\\')_tcscat_s(path,pathLen,L"\\");
	_tcscat_s(path,pathLen,m_fileName);
	return TRUE;
}