// NetChannel.h : NetChannel 的声明

#pragma once
#include "resource.h"       // 主符号
#include "tsim_i.h"
#include "Address.h"
#include <vector>
#include <map>
using namespace std;
#include "MessageOut.h"
#include "MessageIn.h"
#include "tsim_i.h"
#include <SHLGUID.h>

#pragma comment( lib, "WS2_32" )

typedef pair<const MSGID, MessageIn*> INMPair;
typedef map<const MSGID, MessageIn*> INMMap;
typedef map<const MSGID, MessageIn*>::iterator INMItr;

typedef vector<MessageOut*> V_MSGOUT;
typedef vector<MessageOut*>::iterator ITROUT;

//typedef vector<MessageIn*> V_MSGIN;
//typedef vector<MessageIn*>::iterator ITRIN;

typedef vector<TCHAR*> V_MESSAGECHANNELID;
typedef vector<TCHAR*>::iterator ITR_MESSAGECHANNELID;

DWORD WINAPI UDPReceiverThreadProc(LPVOID lpParam);

typedef struct {
	HANDLE		hThread;						//接收线程句柄
	DWORD			dwThreadID;					//接收线程ID
} RECEIVE_CONTEXT;

static RECEIVE_CONTEXT rContext;

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif

using namespace ATL;

// NetChannel

class ATL_NO_VTABLE NetChannel :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<NetChannel, &CLSID_NetChannel>,
	//public IObjectWithSiteImpl<NetChannel>,
	public IDispatchImpl<INetChannel, &IID_INetChannel, &LIBID_tsimLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	NetChannel()
	{
		m_lastError=0;
		m_sock=0;
		memset(&m_servAddr,0,sizeof(m_servAddr));
		m_servPort=DEFAULT_SERVER_PORT;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_NETCHANNEL)
//DECLARE_CLASSFACTORY_SINGLETON(NetChannel)

BEGIN_COM_MAP(NetChannel)
	COM_INTERFACE_ENTRY(INetChannel)
	COM_INTERFACE_ENTRY(IDispatch)
	//COM_INTERFACE_ENTRY(IObjectWithSite)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct(){
		NetChannel::me=this;
		WaitAndReceive();
		return S_OK;
	}

	void FinalRelease(){
		TerminateThread(rContext.hThread,0);
		CloseHandle(rContext.hThread);
		NetChannel::me=NULL;
		closesocket(m_sock);
		WSACleanup();
	}

public:
	STDMETHOD (RegisterMessageChannel)(BSTR mcid);
	STDMETHOD (UnregisterMessageChannel)(BSTR mcid);
	STDMETHOD (GetRegisteredMessageChannelCount)(ULONG* ret);
	STDMETHOD (Stop)();
	void RaiseError(DWORD dwErrCode);
	void RaiseReceived(MessageIn* pMsg);
	void RaiseSent(MessageOut* pMsg);
private:
	void WaitAndReceive();
	HRESULT getMessageChannel(BSTR mcid,IMessageChannel** ppmc);

	WSADATA m_wsaData;											//winsock初始化数据
	WSSOCKADDRESS m_servAddr;								//本机（作为服务端的）地址
	WSPORT m_servPort;											//本机（作为服务端的）UDP端口
							
	V_MESSAGECHANNELID m_mcs;								//包含的MessageChannel别名

	CComPtr<IMoniker> m_spMon;
	CComPtr<IBindCtx> m_spCtx;
	CComPtr<IRunningObjectTable> m_spROT;
public:
	int m_sock;															//sock句柄（接收监听用）
	DWORD m_lastError;											//最近操作导致的错误信息
	INMMap	m_in;														//待收消息
	
	static NetChannel* me;									//包含对自身的引用
};

OBJECT_ENTRY_AUTO(__uuidof(NetChannel), NetChannel)
