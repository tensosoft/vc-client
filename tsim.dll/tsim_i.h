

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu Apr 10 11:11:23 2014
 */
/* Compiler settings for tsim.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __tsim_i_h__
#define __tsim_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IReceivedMessage_FWD_DEFINED__
#define __IReceivedMessage_FWD_DEFINED__
typedef interface IReceivedMessage IReceivedMessage;
#endif 	/* __IReceivedMessage_FWD_DEFINED__ */


#ifndef __IMessageChannel_FWD_DEFINED__
#define __IMessageChannel_FWD_DEFINED__
typedef interface IMessageChannel IMessageChannel;
#endif 	/* __IMessageChannel_FWD_DEFINED__ */


#ifndef __INetChannel_FWD_DEFINED__
#define __INetChannel_FWD_DEFINED__
typedef interface INetChannel INetChannel;
#endif 	/* __INetChannel_FWD_DEFINED__ */


#ifndef ___IMessageChannelEvents_FWD_DEFINED__
#define ___IMessageChannelEvents_FWD_DEFINED__
typedef interface _IMessageChannelEvents _IMessageChannelEvents;
#endif 	/* ___IMessageChannelEvents_FWD_DEFINED__ */


#ifndef __MessageChannel_FWD_DEFINED__
#define __MessageChannel_FWD_DEFINED__

#ifdef __cplusplus
typedef class MessageChannel MessageChannel;
#else
typedef struct MessageChannel MessageChannel;
#endif /* __cplusplus */

#endif 	/* __MessageChannel_FWD_DEFINED__ */


#ifndef __ReceivedMessage_FWD_DEFINED__
#define __ReceivedMessage_FWD_DEFINED__

#ifdef __cplusplus
typedef class ReceivedMessage ReceivedMessage;
#else
typedef struct ReceivedMessage ReceivedMessage;
#endif /* __cplusplus */

#endif 	/* __ReceivedMessage_FWD_DEFINED__ */


#ifndef __NetChannel_FWD_DEFINED__
#define __NetChannel_FWD_DEFINED__

#ifdef __cplusplus
typedef class NetChannel NetChannel;
#else
typedef struct NetChannel NetChannel;
#endif /* __cplusplus */

#endif 	/* __NetChannel_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IReceivedMessage_INTERFACE_DEFINED__
#define __IReceivedMessage_INTERFACE_DEFINED__

/* interface IReceivedMessage */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IReceivedMessage;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E56CDDAA-1D1E-48AB-A191-7C703F1D0646")
    IReceivedMessage : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MessageType( 
            /* [retval][out] */ ULONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MessageType( 
            /* [in] */ ULONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Result( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Result( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ID( 
            /* [retval][out] */ ULONG *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ID( 
            /* [in] */ ULONG newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Source( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Source( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DateTime( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DateTime( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE toString( 
            /* [retval][out] */ BSTR *ret) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IReceivedMessageVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IReceivedMessage * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IReceivedMessage * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IReceivedMessage * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IReceivedMessage * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IReceivedMessage * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IReceivedMessage * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IReceivedMessage * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MessageType )( 
            IReceivedMessage * This,
            /* [retval][out] */ ULONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MessageType )( 
            IReceivedMessage * This,
            /* [in] */ ULONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Result )( 
            IReceivedMessage * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Result )( 
            IReceivedMessage * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ID )( 
            IReceivedMessage * This,
            /* [retval][out] */ ULONG *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ID )( 
            IReceivedMessage * This,
            /* [in] */ ULONG newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Source )( 
            IReceivedMessage * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Source )( 
            IReceivedMessage * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DateTime )( 
            IReceivedMessage * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DateTime )( 
            IReceivedMessage * This,
            /* [in] */ BSTR newVal);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *toString )( 
            IReceivedMessage * This,
            /* [retval][out] */ BSTR *ret);
        
        END_INTERFACE
    } IReceivedMessageVtbl;

    interface IReceivedMessage
    {
        CONST_VTBL struct IReceivedMessageVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IReceivedMessage_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IReceivedMessage_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IReceivedMessage_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IReceivedMessage_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IReceivedMessage_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IReceivedMessage_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IReceivedMessage_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IReceivedMessage_get_MessageType(This,pVal)	\
    ( (This)->lpVtbl -> get_MessageType(This,pVal) ) 

#define IReceivedMessage_put_MessageType(This,newVal)	\
    ( (This)->lpVtbl -> put_MessageType(This,newVal) ) 

#define IReceivedMessage_get_Result(This,pVal)	\
    ( (This)->lpVtbl -> get_Result(This,pVal) ) 

#define IReceivedMessage_put_Result(This,newVal)	\
    ( (This)->lpVtbl -> put_Result(This,newVal) ) 

#define IReceivedMessage_get_ID(This,pVal)	\
    ( (This)->lpVtbl -> get_ID(This,pVal) ) 

#define IReceivedMessage_put_ID(This,newVal)	\
    ( (This)->lpVtbl -> put_ID(This,newVal) ) 

#define IReceivedMessage_get_Source(This,pVal)	\
    ( (This)->lpVtbl -> get_Source(This,pVal) ) 

#define IReceivedMessage_put_Source(This,newVal)	\
    ( (This)->lpVtbl -> put_Source(This,newVal) ) 

#define IReceivedMessage_get_DateTime(This,pVal)	\
    ( (This)->lpVtbl -> get_DateTime(This,pVal) ) 

#define IReceivedMessage_put_DateTime(This,newVal)	\
    ( (This)->lpVtbl -> put_DateTime(This,newVal) ) 

#define IReceivedMessage_toString(This,ret)	\
    ( (This)->lpVtbl -> toString(This,ret) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IReceivedMessage_INTERFACE_DEFINED__ */


#ifndef __IMessageChannel_INTERFACE_DEFINED__
#define __IMessageChannel_INTERFACE_DEFINED__

/* interface IMessageChannel */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IMessageChannel;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B21C74B0-BD70-4CF8-A928-01BD9780C542")
    IMessageChannel : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE SendMessage( 
            /* [in] */ BSTR msg,
            /* [in] */ BSTR dest,
            /* [retval][out] */ ULONG *ret) = 0;
        
        virtual /* [helpstring][id][propputref] */ HRESULT STDMETHODCALLTYPE putref_onreceived( 
            /* [in] */ IDispatch *newVal) = 0;
        
        virtual /* [helpstring][id][propputref] */ HRESULT STDMETHODCALLTYPE putref_onsent( 
            /* [in] */ IDispatch *newVal) = 0;
        
        virtual /* [helpstring][id][propputref] */ HRESULT STDMETHODCALLTYPE putref_onerr( 
            /* [in] */ IDispatch *newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ReceiveMessage( 
            /* [in] */ BSTR msg,
            /* [in] */ ULONG msgId,
            /* [in] */ BSTR src,
            /* [in] */ BSTR dt,
            /* [defaultvalue][in] */ LONG msgType = 0) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ReceiveSendStatus( 
            /* [in] */ LONG statusCode,
            /* [in] */ ULONG msgId,
            /* [in] */ BSTR dest) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ReceiveError( 
            /* [in] */ LONG errCode,
            /* [defaultvalue][in] */ BSTR errDesc = L"") = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Dispose( 
            /* [retval][out] */ VARIANT_BOOL *ret) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SendFile( 
            /* [in] */ BSTR fn,
            /* [in] */ BSTR dest,
            /* [retval][out] */ ULONG *ret) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMessageChannelVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMessageChannel * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMessageChannel * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMessageChannel * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMessageChannel * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMessageChannel * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMessageChannel * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMessageChannel * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SendMessage )( 
            IMessageChannel * This,
            /* [in] */ BSTR msg,
            /* [in] */ BSTR dest,
            /* [retval][out] */ ULONG *ret);
        
        /* [helpstring][id][propputref] */ HRESULT ( STDMETHODCALLTYPE *putref_onreceived )( 
            IMessageChannel * This,
            /* [in] */ IDispatch *newVal);
        
        /* [helpstring][id][propputref] */ HRESULT ( STDMETHODCALLTYPE *putref_onsent )( 
            IMessageChannel * This,
            /* [in] */ IDispatch *newVal);
        
        /* [helpstring][id][propputref] */ HRESULT ( STDMETHODCALLTYPE *putref_onerr )( 
            IMessageChannel * This,
            /* [in] */ IDispatch *newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ReceiveMessage )( 
            IMessageChannel * This,
            /* [in] */ BSTR msg,
            /* [in] */ ULONG msgId,
            /* [in] */ BSTR src,
            /* [in] */ BSTR dt,
            /* [defaultvalue][in] */ LONG msgType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ReceiveSendStatus )( 
            IMessageChannel * This,
            /* [in] */ LONG statusCode,
            /* [in] */ ULONG msgId,
            /* [in] */ BSTR dest);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ReceiveError )( 
            IMessageChannel * This,
            /* [in] */ LONG errCode,
            /* [defaultvalue][in] */ BSTR errDesc);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Dispose )( 
            IMessageChannel * This,
            /* [retval][out] */ VARIANT_BOOL *ret);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SendFile )( 
            IMessageChannel * This,
            /* [in] */ BSTR fn,
            /* [in] */ BSTR dest,
            /* [retval][out] */ ULONG *ret);
        
        END_INTERFACE
    } IMessageChannelVtbl;

    interface IMessageChannel
    {
        CONST_VTBL struct IMessageChannelVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMessageChannel_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMessageChannel_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMessageChannel_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMessageChannel_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IMessageChannel_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IMessageChannel_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IMessageChannel_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IMessageChannel_SendMessage(This,msg,dest,ret)	\
    ( (This)->lpVtbl -> SendMessage(This,msg,dest,ret) ) 

#define IMessageChannel_putref_onreceived(This,newVal)	\
    ( (This)->lpVtbl -> putref_onreceived(This,newVal) ) 

#define IMessageChannel_putref_onsent(This,newVal)	\
    ( (This)->lpVtbl -> putref_onsent(This,newVal) ) 

#define IMessageChannel_putref_onerr(This,newVal)	\
    ( (This)->lpVtbl -> putref_onerr(This,newVal) ) 

#define IMessageChannel_ReceiveMessage(This,msg,msgId,src,dt,msgType)	\
    ( (This)->lpVtbl -> ReceiveMessage(This,msg,msgId,src,dt,msgType) ) 

#define IMessageChannel_ReceiveSendStatus(This,statusCode,msgId,dest)	\
    ( (This)->lpVtbl -> ReceiveSendStatus(This,statusCode,msgId,dest) ) 

#define IMessageChannel_ReceiveError(This,errCode,errDesc)	\
    ( (This)->lpVtbl -> ReceiveError(This,errCode,errDesc) ) 

#define IMessageChannel_Dispose(This,ret)	\
    ( (This)->lpVtbl -> Dispose(This,ret) ) 

#define IMessageChannel_SendFile(This,fn,dest,ret)	\
    ( (This)->lpVtbl -> SendFile(This,fn,dest,ret) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMessageChannel_INTERFACE_DEFINED__ */


#ifndef __INetChannel_INTERFACE_DEFINED__
#define __INetChannel_INTERFACE_DEFINED__

/* interface INetChannel */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_INetChannel;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D31FED02-F79E-4A14-BB3C-51A5A6271C4D")
    INetChannel : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE RegisterMessageChannel( 
            /* [in] */ BSTR mcid) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE UnregisterMessageChannel( 
            /* [in] */ BSTR mcid) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetRegisteredMessageChannelCount( 
            /* [retval][out] */ ULONG *ret) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE Stop( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INetChannelVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INetChannel * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INetChannel * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INetChannel * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INetChannel * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INetChannel * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INetChannel * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INetChannel * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RegisterMessageChannel )( 
            INetChannel * This,
            /* [in] */ BSTR mcid);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UnregisterMessageChannel )( 
            INetChannel * This,
            /* [in] */ BSTR mcid);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetRegisteredMessageChannelCount )( 
            INetChannel * This,
            /* [retval][out] */ ULONG *ret);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            INetChannel * This);
        
        END_INTERFACE
    } INetChannelVtbl;

    interface INetChannel
    {
        CONST_VTBL struct INetChannelVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INetChannel_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define INetChannel_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define INetChannel_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define INetChannel_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define INetChannel_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define INetChannel_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INetChannel_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define INetChannel_RegisterMessageChannel(This,mcid)	\
    ( (This)->lpVtbl -> RegisterMessageChannel(This,mcid) ) 

#define INetChannel_UnregisterMessageChannel(This,mcid)	\
    ( (This)->lpVtbl -> UnregisterMessageChannel(This,mcid) ) 

#define INetChannel_GetRegisteredMessageChannelCount(This,ret)	\
    ( (This)->lpVtbl -> GetRegisteredMessageChannelCount(This,ret) ) 

#define INetChannel_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __INetChannel_INTERFACE_DEFINED__ */



#ifndef __tsimLib_LIBRARY_DEFINED__
#define __tsimLib_LIBRARY_DEFINED__

/* library tsimLib */
/* [version][uuid] */ 


EXTERN_C const IID LIBID_tsimLib;

#ifndef ___IMessageChannelEvents_DISPINTERFACE_DEFINED__
#define ___IMessageChannelEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IMessageChannelEvents */
/* [uuid] */ 


EXTERN_C const IID DIID__IMessageChannelEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("77480897-ECF3-4B07-A05A-BF06B33EB4A2")
    _IMessageChannelEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IMessageChannelEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IMessageChannelEvents * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IMessageChannelEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IMessageChannelEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IMessageChannelEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IMessageChannelEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IMessageChannelEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IMessageChannelEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IMessageChannelEventsVtbl;

    interface _IMessageChannelEvents
    {
        CONST_VTBL struct _IMessageChannelEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IMessageChannelEvents_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _IMessageChannelEvents_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _IMessageChannelEvents_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _IMessageChannelEvents_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _IMessageChannelEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _IMessageChannelEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _IMessageChannelEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IMessageChannelEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_MessageChannel;

#ifdef __cplusplus

class DECLSPEC_UUID("28ED71E1-3F59-4C93-A8E2-17FFB1BF007D")
MessageChannel;
#endif

EXTERN_C const CLSID CLSID_ReceivedMessage;

#ifdef __cplusplus

class DECLSPEC_UUID("BA3B75B5-B973-4F2E-9036-CF62AFBE5082")
ReceivedMessage;
#endif

EXTERN_C const CLSID CLSID_NetChannel;

#ifdef __cplusplus

class DECLSPEC_UUID("5C732ECD-3ADF-4140-BA76-684376A716A9")
NetChannel;
#endif
#endif /* __tsimLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


