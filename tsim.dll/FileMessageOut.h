#pragma once

#ifndef _FILEMESSAGEOUT_H_
#define _FILEMESSAGEOUT_H_

#include "messageout.h"
class FileMessageOut :public MessageOut
{
public:
	FileMessageOut(TCHAR* fileName);
	virtual ~FileMessageOut(void);

	virtual LEN getPacketCount();
	virtual BYTE* getPacket(LEN packetIndex,LEN* pBytesLen);
	virtual LEN getHeaderLen();

	BOOL m_initialized;						//是否初始化标记
protected:
	FileMessageOut(void);
private:
	TCHAR* m_fileName;						//原始文件路径和文件名
	LEN m_fileLen;								//文件长度
	HANDLE m_file;								//文件句柄
	HANDLE m_filemap;							//内存映射文件句柄
	BYTE* m_fn;										//纯文件名对应的字节内容
	LEN m_fnLen;									//m_fn的实际长度
	LEN m_packetCount;						//包总个数
};

#endif