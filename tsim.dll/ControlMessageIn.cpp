#include "StdAfx.h"
#include "ControlMessageIn.h"

ControlMessageIn::ControlMessageIn(void)
{
	m_targetMessageId=0;
	m_controlMessageType=UNKNOWN;
}

ControlMessageIn::ControlMessageIn(BYTE* firstPacket,LEN firstPacketBytesLen){
	m_targetMessageId=0;
	m_controlMessageType=UNKNOWN;
	appendPacket(firstPacket,firstPacketBytesLen);
}

LEN ControlMessageIn::getHeaderLen(){
	return sizeof(LEN)/*包序号（从0开始）*/+sizeof(LEN)/*包总序号*/+sizeof(LEN)/*消息类型*/;
}

BOOL ControlMessageIn::appendPacket(BYTE* bytes,LEN bytesLen){
	if(m_receivedPacketCount>0) return TRUE;		//控制消息只能包含一个包
	LEN headerLen=getHeaderLen();
	if(!bytes || bytesLen==0 || bytesLen<=headerLen){
		m_lastError=UDF_ERROR_FILE_PACKET_CORRUPT;
		return FALSE;
	}

	//头+内容
	LEN headerCnt=bytesLen/sizeof(LEN);
	LEN* iarr=new LEN[headerCnt];	//预先解析头信息:包序号、包个数、消息类型、控制消息类型、控制消息对应的目标消息id
	memcpy(iarr,bytes,bytesLen);
	
	//包序号、包个数、消息类型不符则丢弃
	if((m_receivedPacketCount==0&&iarr[0]!=0) || (m_receivedPacketCount>0&&iarr[0]==0) || iarr[1]==0 || (m_packetCount>0 && iarr[1]!=m_packetCount) || iarr[2]!=(LEN)CONTROL){
		SAFE_DEL_ARR(iarr);
		return TRUE;
	}

	if(iarr[1]>1) {SAFE_DEL_ARR(iarr);return TRUE;}		//控制消息只能包含一个包

	//设置状态值
	m_currentIndex=iarr[0];
	m_packetCount=iarr[1];
	m_type=(MESSAGETYPE)iarr[2];
	m_controlMessageType=(CONTROLMESSAGETYPE)iarr[3];
	m_targetMessageId=iarr[4];

	if(m_receivedPacketCount!=0) m_lastIndex=m_currentIndex;
	m_receivedPacketCount++;

	SAFE_DEL_ARR(iarr);
	
	return TRUE;
}

LEN ControlMessageIn::getTargetMessageId(){
	return this->m_targetMessageId;
}
CONTROLMESSAGETYPE ControlMessageIn::getControlMessageType(){
	return this->m_controlMessageType;
}

ControlMessageIn::~ControlMessageIn(void)
{
}
