/*
* 接收到的消息基类。
*	(c) 腾硕软件科技 2010-12-10
*/
#pragma once

#ifndef _MESSAGEIN_H_
#define _MESSAGEIN_H_

#include "messagebase.h"
class MessageIn :	public MessageBase
{
public:
	MessageIn(void);
	virtual ~MessageIn(void);

	virtual BOOL isInitialized();																//返回是否正确初始化（即是否使用第一个包初始化成功）
	virtual BOOL isReceived();																	//返回所有包是否接收完毕。
	virtual LEN getMessageId();																	//返回接收的消息的id
	virtual BOOL getTimeStamp(LPSYSTEMTIME lpSysTime);					//返回消息开始接收的时间戳
	virtual BOOL appendPacket(BYTE* bytes,LEN bytesLen)=0;			//追加包内容到消息
protected:
	LEN m_currentIndex;							//当前包序号
	LEN m_lastIndex;								//上一个包序号
	LEN m_packetCount;							//包总数
	LEN m_receivedPacketCount;			//已接收的包总数
	LEN m_messageId;								//消息ID
	SYSTEMTIME m_timestamp;					//时间
};

#endif