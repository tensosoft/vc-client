/*
* 接收到的即时消息类。
*	(c) 腾硕软件科技 2010-12-10
*/
#pragma once

#ifndef _IMMESSAGEIN_H_
#define _IMMESSAGEIN_H_

#include "messagein.h"
#include <map>
using namespace std;

typedef pair<const LEN,BYTE*> IMPair;
typedef map<const LEN,BYTE*> IMMap;
typedef map<const LEN,BYTE*>::iterator PIMItr;

class IMMessageIn :	public MessageIn{
public:
	IMMessageIn(BYTE* firstPacket,LEN firstPacketBytesLen);
	virtual ~IMMessageIn(void);
	virtual LEN getHeaderLen(void);
	virtual BOOL appendPacket(BYTE* bytes,LEN bytesLen);
	virtual LEN getResultLen(void);															//获取即时消息结果内容长度
	virtual BOOL getResult(TCHAR* result,LEN* resultLen);				//获取即时消息结果内容文本（需在isReceived()返回TRUE之后才能获取到完整内容）
protected:
	IMMessageIn(void);
	IMMap m_immap;
	LEN m_contentLen;
};

#endif