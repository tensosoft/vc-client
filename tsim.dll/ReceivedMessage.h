// ReceivedMessage.h : ReceivedMessage 的声明

#pragma once
#include "resource.h"       // 主符号
#include "tsim_i.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif

using namespace ATL;

// ReceivedMessage

class ATL_NO_VTABLE ReceivedMessage :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<ReceivedMessage, &CLSID_ReceivedMessage>,
	public IObjectWithSiteImpl<ReceivedMessage>,
	public IDispatchImpl<IReceivedMessage, &IID_IReceivedMessage, &LIBID_tsimLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	ReceivedMessage()
	{
		m_result=NULL;
		m_source=NULL;
		m_id=NULL;
		m_mt=NULL;
		m_dt=NULL;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_RECEIVEDMESSAGE)


BEGIN_COM_MAP(ReceivedMessage)
	COM_INTERFACE_ENTRY(IReceivedMessage)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IObjectWithSite)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
		FREE_SYS_STR(m_result);
		FREE_SYS_STR(m_source);
		FREE_SYS_STR(m_dt);
	}

public:
	STDMETHOD(get_MessageType)(ULONG* pVal);
	STDMETHOD(put_MessageType)(ULONG newVal);
	STDMETHOD(get_ID)(ULONG* pVal);
	STDMETHOD(put_ID)(ULONG newVal);
	STDMETHOD(get_Result)(BSTR* pVal);
	STDMETHOD(put_Result)(BSTR newVal);
	STDMETHOD(get_Source)(BSTR* pVal);
	STDMETHOD(put_Source)(BSTR newVal);
	STDMETHOD(get_DateTime)(BSTR *pVal);
	STDMETHOD(put_DateTime)(BSTR newVal);
	STDMETHOD(toString)(BSTR* ret);
private:
	BSTR m_result;
	BSTR m_source;
	ULONG m_id;
	ULONG m_mt;
	BSTR m_dt;
};

OBJECT_ENTRY_AUTO(__uuidof(ReceivedMessage), ReceivedMessage)
