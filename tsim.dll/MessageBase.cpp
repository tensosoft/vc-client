#include "StdAfx.h"
#include "MessageBase.h"

MessageBase::MessageBase(void):
m_lastError(0),
m_type(IM),
m_source(NULL),
m_dest(NULL)
{
}

MessageBase::~MessageBase(void)
{
	SAFE_DEL_PTR(m_source);
	SAFE_DEL_PTR(m_dest);
}

Address* MessageBase::getSourceAddress(){
	return this->m_source;
}
Address* MessageBase::getDestAddress(){
	return this->m_dest;
}
void MessageBase::setSourceAddress(Address source){
	this->m_source=new Address(source.getAddress());
}
void MessageBase::setDestAddress(Address dest){
	this->m_dest=new Address(dest.getAddress());
}

MESSAGETYPE MessageBase::getType(){
	return this->m_type;
}
void MessageBase::setType(MESSAGETYPE type){
	this->m_type=type;
}

DWORD MessageBase::getLastError(){
	return this->m_lastError;
}