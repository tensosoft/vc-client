#pragma once
#ifndef _ERROR_H_
#define _ERROR_H_

#define UDF_ERROR_BASE	70000L

#define UDF_ERROR_INIT 70001L
#define UDF_ERROR_FILE_SUPERSCALE 70002L
#define UDF_ERROR_FILE_EMPTY 70003L
#define UDF_ERROR_FILE_PACKET_CORRUPT  70004L
#define UDF_ERROR_FILE_TARGETPATH  70005L
#define UDF_ERROR_UNKNOWN_MESSAGETYPE 70006L
#define UDF_ERROR_UNKNOWN_CONTROLMESSAGETYPE 70007L
#define UDF_ERROR_PORT_USED 70008L

static TCHAR* UDF_ERRORS[]={
	L"初始化异常",
	L"不能发送或接收超过2GB的文件！",
	L"不能发送空文件！",
	L"数据格式异常或数据在传输过程中被损坏！",
	L"未提供有效的文件路径！",
	L"未知消息类型！",
	L"未知控制消息类型！",
	L"端口已被占用！"
};

#endif