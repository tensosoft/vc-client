// NetChannel.cpp : NetChannel 的实现

#include "stdafx.h"
#include "NetChannel.h"
#include "immessagein.h"
#include "filemessagein.h"
#include "controlmessagein.h"
#include "controlmessageout.h"
#include "Address.h"
// NetChannel

NetChannel* NetChannel::me=NULL;

void NetChannel::WaitAndReceive(){
	if (WSAStartup(MAKEWORD(2, 2), &m_wsaData) != 0){
		m_lastError=WSAGetLastError();
		return;
	}

	if ((m_sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP))==INVALID_SOCKET){
		m_lastError=WSAGetLastError();
		return;
	}

	m_servAddr.sin_family=AF_INET;	
	m_servAddr.sin_addr.s_addr=htonl(INADDR_ANY);	
	m_servAddr.sin_port=htons(m_servPort);

	if (bind(m_sock, (struct sockaddr*) &m_servAddr, sizeof(m_servAddr)) < 0){
		m_lastError=WSAGetLastError();
		if((m_lastError==WSAEADDRINUSE || m_lastError==WSAEINVAL) && m_servPort<65535){
			ErrMsg(0,L"端口“52321”已被占用，您将无法接收消息！");
		}
		return;
	}
	rContext.hThread=CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE)UDPReceiverThreadProc,(LPVOID)this,0,&(rContext.dwThreadID));
}

void NetChannel::RaiseReceived(MessageIn* pMsg){
	Address* add=pMsg->getSourceAddress();
	TCHAR szIP[STRLEN_SMALL]={0};
	add->getAddressString(szIP,STRLEN_SMALL);

	TCHAR szDt[STRLEN_SMALL]={0};
	BSTR bstrDt=NULL;
	SYSTEMTIME st;
	if(pMsg->getTimeStamp(&st)){
		_stprintf_s(szDt,L"%04d-%02d-%02d %02d:%02d:%02d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
		bstrDt=SysAllocString(szDt);
	}

	switch(pMsg->getType()){
	case IM:
		{
			IMMessageIn* imMsg = dynamic_cast<IMMessageIn*>(pMsg);
			LEN resultLen=imMsg->getResultLen()+1;
			TCHAR *result=new TCHAR[resultLen];
			memset(result,0,sizeof(TCHAR)*resultLen);
			if(imMsg->getResult(result,&resultLen)) {
				ITR_MESSAGECHANNELID itr;
				for(itr=m_mcs.begin();itr!=m_mcs.end();itr++){
					IMessageChannel* pmc=NULL;
					BSTR bstr=SysAllocString(*itr);
					getMessageChannel(*itr,&pmc);
					FREE_SYS_STR(bstr);
					if(!pmc) continue;
					pmc->ReceiveMessage(T2BSTR(result),imMsg->getMessageId(),T2BSTR(szIP),bstrDt,IM);
					pmc->Release();
				}
			}
			SAFE_DEL_PTR(result);
		}
		break;
	case FILESHARE:
		{
			FileMessageIn* fileMsg = dynamic_cast<FileMessageIn*>(pMsg);
			TCHAR fn[STRLEN_DEFAULT]={0};
			if(fileMsg->getFullPath(fn,STRLEN_DEFAULT) && PathFileExists(fn)) {
				ITR_MESSAGECHANNELID itr;
				for(itr=m_mcs.begin();itr!=m_mcs.end();itr++){
					IMessageChannel* pmc=NULL;
					BSTR bstr=SysAllocString(*itr);
					getMessageChannel(*itr,&pmc);
					FREE_SYS_STR(bstr);
					if(!pmc) continue;
					pmc->ReceiveMessage(T2BSTR(fn),fileMsg->getMessageId(),T2BSTR(szIP),bstrDt,FILESHARE);
					pmc->Release();
				}
			}
		}
		break;
	case CONTROL:
		{
			ControlMessageIn* ctrlMsg = dynamic_cast<ControlMessageIn*>(pMsg);
			if(!ctrlMsg) break;
			CONTROLMESSAGETYPE cmt=ctrlMsg->getControlMessageType();
			ITR_MESSAGECHANNELID itr;
			for(itr=m_mcs.begin();itr!=m_mcs.end();itr++){
				IMessageChannel* pmc=NULL;
				BSTR bstr=SysAllocString(*itr);
				getMessageChannel(*itr,&pmc);
				FREE_SYS_STR(bstr);
				if(!pmc) continue;
				pmc->ReceiveSendStatus((cmt==FILERECEIVED||cmt==IMRECEIVED)?RECEIVED:cmt,ctrlMsg->getTargetMessageId(),T2BSTR(szIP));
				pmc->Release();
			}
		}
		break;
	}
	FREE_SYS_STR(bstrDt);
}

void NetChannel::RaiseSent(MessageOut* pMsg){
	
}

void NetChannel::RaiseError(DWORD dwErrCode){
	if(m_mcs.empty()) return;
	ITR_MESSAGECHANNELID itr;
	for(itr=m_mcs.begin();itr!=m_mcs.end();itr++){
		IMessageChannel* pmc=NULL;
		BSTR bstr=SysAllocString(*itr);
		getMessageChannel(*itr,&pmc);
		FREE_SYS_STR(bstr);
		if(!pmc) continue;
		pmc->ReceiveError(dwErrCode,NULL);
		pmc->Release();
	}
}

HRESULT NetChannel::getMessageChannel(BSTR mcid,IMessageChannel** ppmc){
	HRESULT hr=0;
	if(!m_spCtx){
		hr=CreateBindCtx(0,&m_spCtx);
		if(FAILED(hr) || !m_spCtx){
			ErrMsg(0,L"无法初始化对象注册上下文！");
			return S_OK;
		}
	}

	if(m_spCtx && !m_spROT){
		hr=m_spCtx->GetRunningObjectTable(&m_spROT);
		if(FAILED(hr) || !m_spROT){
			ErrMsg(0,L"无法初始化对象注册表！");
			return S_OK;
		}
	}
	
	CComPtr<IEnumMoniker> spEnumMon=NULL;
	hr=m_spROT->EnumRunning(&spEnumMon);
	if(hr==E_OUTOFMEMORY || FAILED(hr)){
		ErrMsg(0,L"无法获取对象注册表集合！");
		return S_OK;
	}
	
	TCHAR szKeyStart[MAX_PATH]={0};
	_tcscpy_s(szKeyStart,L"!");
	_tcscat_s(szKeyStart,mcid);
	
	BOOL matched=FALSE;
	IMoniker* spMon=NULL;
	ULONG fetched=0;
	while(spEnumMon->Next(1,&spMon,&fetched)==S_OK){		
		LPOLESTR pDisplayName =(LPOLESTR)HeapAlloc(GetProcessHeap(),0,sizeof(TCHAR)*STRLEN_1K);
		if(!pDisplayName){
			ErrMsg(GetLastError(),L"无法分配内存：%s！");
			return S_OK;
		}
		ZeroMemory(pDisplayName,sizeof(TCHAR)*STRLEN_1K);
		hr=spMon->GetDisplayName(m_spCtx,NULL,&pDisplayName);
		if (SUCCEEDED(hr) && _tcsicmp(pDisplayName,szKeyStart)==0){
			IUnknown* pUnk=NULL;
			hr=m_spROT->GetObject(spMon,&pUnk);
			if(FAILED(hr)){
				ErrMsg(GetLastError(),L"无法获取对象！");
				return S_OK;
			}
			
			hr=pUnk->QueryInterface(IID_IMessageChannel,(void**)ppmc);
			if(FAILED(hr)){
				ErrMsg(GetLastError(),L"无法获取对象！");
				pUnk->Release();
				return S_OK;
			}
			pUnk->Release();
			matched=TRUE;
		}
		if(HeapFree(GetProcessHeap(), 0, pDisplayName)==0){
			ErrMsg(GetLastError(),L"无法释放内存：%s！");
			return S_OK;
		}
		if(matched) break;	//找到匹配则退出
	}
	if(!matched){ErrMsg(0,L"无法获取目标对象！");}
	return S_OK;
}

STDMETHODIMP NetChannel::RegisterMessageChannel(BSTR mcid){
	if(!mcid || _tcslen(mcid)==0) return S_OK;
	TCHAR* szMcId=new TCHAR[STRLEN_SMALL];
	ZeroMemory(szMcId,sizeof(TCHAR)*STRLEN_SMALL);
	_tcscpy_s(szMcId,STRLEN_SMALL,mcid);
	ITR_MESSAGECHANNELID itr;
	for(itr=m_mcs.begin();itr!=m_mcs.end();itr++){
		if(_tcsicmp(szMcId,*itr)==0) {SAFE_DEL_PTR(*itr);m_mcs.erase(itr);break;}
	}
	m_mcs.push_back(szMcId);
	return S_OK;
}

STDMETHODIMP NetChannel::UnregisterMessageChannel(BSTR mcid){
	if(!mcid || _tcslen(mcid)==0) return S_OK;
	ITR_MESSAGECHANNELID itr;
	TCHAR *szMcId=OLE2T(mcid);
	for(itr=m_mcs.begin();itr!=m_mcs.end();itr++){
		if(_tcsicmp(szMcId,*itr)==0) {SAFE_DEL_PTR(*itr);m_mcs.erase(itr);break;}
	}
	return S_OK;
}

STDMETHODIMP NetChannel::GetRegisteredMessageChannelCount(ULONG* ret){
	ATLASSERT(ret);
	*ret=m_mcs.size();
	return S_OK;
}

STDMETHODIMP NetChannel::Stop(){
	TerminateThread(rContext.hThread,0);
	CloseHandle(rContext.hThread);
	NetChannel::me=NULL;
	closesocket(m_sock);
	WSACleanup();
	return S_OK;
}

DWORD WINAPI UDPReceiverThreadProc(LPVOID lpParam){
	NetChannel* p=(NetChannel*)lpParam;
	CoInitializeEx(0,COINIT_MULTITHREADED);//COINIT_MULTITHREADED,COINIT_APARTMENTTHREADED
	int recvMsgSize;												//接收到的包的数据长度
	char recvBuf[MAX_PACKET_LENGTH];				//接收到的包的数据内容缓冲区
	static int receivedCnt=0;
	for (;;){
		WSSOCKADDRESS clntAddr;								//接收到的包对应的发送端地址
		int clntAddrLen=sizeof(clntAddr);			//接收到的包对应的发送端地址长度
		if ((recvMsgSize = recvfrom(p->m_sock, recvBuf, MAX_PACKET_LENGTH, 0,(struct sockaddr *) &clntAddr, &clntAddrLen)) < 0){
			p->m_lastError=WSAGetLastError();
			p->RaiseError(p->m_lastError);
		}
		if((p->m_lastError=WSAGetLastError())== WSAEMSGSIZE || p->m_lastError!=0){
			p->RaiseError(p->m_lastError);
		}
		
		if(recvMsgSize>=16){
			//预先解析头信息:包序号、包个数、消息类型、消息id
			LEN iarr[4]={0,0,0,0};
			memcpy(&iarr[0],recvBuf,16);
			
			//if(iarr[2]==1){
			//receivedCnt++;
			//TCHAR t[100]={0};
			//_stprintf_s(t,L"接收：packetIndex=%u,packetCount=%u,type=%u,msgId=%u,receivedCnt=%u",iarr[0],iarr[1],iarr[2],iarr[3],receivedCnt);
			//TipMsg(t);
			//}

			LEN mt=iarr[2];
			MSGID msgId=iarr[3];
			MessageIn* pMsg=NULL;
			if(iarr[0]==0){	//如果是第一个包
				switch(mt){	//消息类型
				case IM:
					pMsg=new IMMessageIn((BYTE*)recvBuf,recvMsgSize);
					break;
				case FILESHARE:
					pMsg=new FileMessageIn((BYTE*)recvBuf,recvMsgSize,L"e:\\temp\\received\\");	//TODO:文件路径
					break;
				case CONTROL:
					pMsg=new ControlMessageIn((BYTE*)recvBuf,recvMsgSize);
					break;
				}
				if(pMsg){
					Address addSrc(clntAddr);
					pMsg->setSourceAddress(addSrc);
					p->m_in[msgId]=pMsg;
				}
			}else{
				INMItr itr=p->m_in.find(msgId);
				if(itr!=p->m_in.end()){
					pMsg=itr->second;
					pMsg->appendPacket((BYTE*)recvBuf,recvMsgSize);
				}
			}
			//多个包时，要回发包已接收到控制信息以便发送端继续发送下一个包
			if(pMsg && iarr[1]>1 && iarr[0]<(iarr[1]-1) ){	//&& (iarr[0]==0||(iarr[0]%10)==0)
				USES_CONVERSION;
				TCHAR *senderIP=A2T(inet_ntoa(clntAddr.sin_addr));
				Address senderAdd(senderIP);
				WSSOCKADDRESS wsaTarget=senderAdd.getAddress();
				ControlMessageOut cmo(PACKETRECEIVED,msgId);
				DWORD bufLen=0;
				BYTE* buf=cmo.getPacket(0,&bufLen);
				sendto(p->m_sock, (char*)buf,(int)bufLen, 0, (struct sockaddr *)&wsaTarget,sizeof(wsaTarget));
				SAFE_DEL_PTR(buf);
			}

			//接收消息成功则回发一个接收成功的控制消息
			if(pMsg && pMsg->isReceived()){
				p->RaiseReceived(pMsg);
				p->m_in.erase(msgId);
				SAFE_DEL_PTR(pMsg);

				if(mt!=CONTROL){
					USES_CONVERSION;
					TCHAR *senderIP=A2T(inet_ntoa(clntAddr.sin_addr));
					Address senderAdd(senderIP);
					WSSOCKADDRESS wsaTarget=senderAdd.getAddress();
					ControlMessageOut cmo(mt==FILESHARE?FILERECEIVED:IMRECEIVED,msgId);
					DWORD bufLen=0;
					BYTE* buf=cmo.getPacket(0,&bufLen);
					sendto(p->m_sock, (char*)buf,(int)bufLen, 0, (struct sockaddr *)&wsaTarget,sizeof(wsaTarget));
					SAFE_DEL_PTR(buf);
				}//if end
			}
		}//if end
	}//for end
	CoUninitialize();
	return 0;
}