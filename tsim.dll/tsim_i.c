

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu Apr 10 11:11:23 2014
 */
/* Compiler settings for tsim.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IReceivedMessage,0xE56CDDAA,0x1D1E,0x48AB,0xA1,0x91,0x7C,0x70,0x3F,0x1D,0x06,0x46);


MIDL_DEFINE_GUID(IID, IID_IMessageChannel,0xB21C74B0,0xBD70,0x4CF8,0xA9,0x28,0x01,0xBD,0x97,0x80,0xC5,0x42);


MIDL_DEFINE_GUID(IID, IID_INetChannel,0xD31FED02,0xF79E,0x4A14,0xBB,0x3C,0x51,0xA5,0xA6,0x27,0x1C,0x4D);


MIDL_DEFINE_GUID(IID, LIBID_tsimLib,0x616600F0,0x8B51,0x4122,0xA3,0x63,0xBC,0x97,0xCD,0x16,0xB9,0xFE);


MIDL_DEFINE_GUID(IID, DIID__IMessageChannelEvents,0x77480897,0xECF3,0x4B07,0xA0,0x5A,0xBF,0x06,0xB3,0x3E,0xB4,0xA2);


MIDL_DEFINE_GUID(CLSID, CLSID_MessageChannel,0x28ED71E1,0x3F59,0x4C93,0xA8,0xE2,0x17,0xFF,0xB1,0xBF,0x00,0x7D);


MIDL_DEFINE_GUID(CLSID, CLSID_ReceivedMessage,0xBA3B75B5,0xB973,0x4F2E,0x90,0x36,0xCF,0x62,0xAF,0xBE,0x50,0x82);


MIDL_DEFINE_GUID(CLSID, CLSID_NetChannel,0x5C732ECD,0x3ADF,0x4140,0xBA,0x76,0x68,0x43,0x76,0xA7,0x16,0xA9);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



