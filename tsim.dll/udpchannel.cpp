#include "stdafx.h"
#include "udpchannel.h"
#include "immessagein.h"
#include "filemessagein.h"
#include "controlmessagein.h"

#pragma comment( lib, "WS2_32" )

UDPChannel* UDPChannel::me=NULL;
UDPChannel::UDPChannel(void){
	UDPChannel::me=this;
	InitializeCriticalSectionAndSpinCount(&criticalSection, 0x80000400); 
	m_lastError=0;
	m_sock=0;
	memset(&m_servAddr,0,sizeof(m_servAddr));
	m_servPort=DEFAULT_SERVER_PORT;
	memset(&m_clntAddr,0,sizeof(m_clntAddr));
	m_clntAddrLen=0;
	m_recvMsgSize=0;
	memset(m_recvBuf,0,MAX_PACKET_LENGTH);
}

UDPChannel::~UDPChannel(void){
	UDPChannel::me=NULL;
	TerminateThread(rContext.hThread,0);
	//TerminateThread(sContext.hThread,0);
	CloseHandle(rContext.hThread);
	//CloseHandle(sContext.hThread);
	DeleteCriticalSection(&criticalSection);
	closesocket(m_sock);
	//closesocket(m_sockSend);
	WSACleanup();
}

void UDPChannel::WaitAndReceive(){
	if (WSAStartup(MAKEWORD(2, 2), &m_wsaData) != 0){
		m_lastError=WSAGetLastError();
		return;
	}

	//if ((m_sockSend = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP))==INVALID_SOCKET){
	//	m_lastError=WSAGetLastError();
	//	return;
	//}

	if ((m_sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP))==INVALID_SOCKET){
		m_lastError=WSAGetLastError();
		return;
	}

	m_servAddr.sin_family=AF_INET;	
	m_servAddr.sin_addr.s_addr=htonl(INADDR_ANY);	
	m_servAddr.sin_port=htons(m_servPort);

	rebind:
	if (bind(m_sock, (struct sockaddr*) &m_servAddr, sizeof(m_servAddr)) < 0){
		m_lastError=WSAGetLastError();
		if((m_lastError==WSAEADDRINUSE || m_lastError==WSAEINVAL) && m_servPort<65535){
			ErrMsg(0,L"端口已被占用");
			//m_lastError=0;
			//m_servPort++;
			//goto rebind;
		}
		return;
	}
	rContext.hThread=CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE)UDPReceiver,(LPVOID)this,0,&(rContext.dwThreadID));
	//sContext.hThread=CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE)UDPSender,(LPVOID)this,0,&(sContext.dwThreadID));
}

BOOL UDPChannel::Send(MessageOut* msg,Address& add){
	if(!msg) return FALSE;
	msg->setDestAddress(add);
	//EnterCriticalSection(&criticalSection);
	m_out.push_back(msg);
	//LeaveCriticalSection(&criticalSection);
	return TRUE;
}

void UDPChannel::RaiseError(DWORD dwErrCode){
	ITR_PFN_ONERROR itr;
	ITR_MESSAGECHANNEL itrmc;
	for ( itr = m_onerror.begin( ) ; itr != m_onerror.end( ) ; itr++ ){
		for(itrmc=m_mcs.begin();itrmc!=m_mcs.end();itrmc++){
			(*itr)(dwErrCode,*itrmc);
		}
	}
}
void UDPChannel::RaiseReceived(MessageIn* pMsg){
	ITR_PFN_ONRECEIVED itr;
	ITR_MESSAGECHANNEL itrmc;
	for ( itr = m_onreceived.begin( ) ; itr != m_onreceived.end( ) ; itr++ ){
		for(itrmc=m_mcs.begin();itrmc!=m_mcs.end();itrmc++){
			(*itr)(pMsg,*itrmc);
		}
	}
}
void UDPChannel::RaiseSent(MessageOut* pMsg){
	ITR_PFN_ONSENT itr;
	ITR_MESSAGECHANNEL itrmc;
	for ( itr = m_onsent.begin( ) ; itr != m_onsent.end( ) ; itr++ ){
		for(itrmc=m_mcs.begin();itrmc!=m_mcs.end();itrmc++){
			(*itr)(pMsg,*itrmc);
		}
	}
}

void UDPChannel::RegisterErrorCallback(PFN_ONERROR pfn){
	ITR_PFN_ONERROR itr;
	for ( itr = m_onerror.begin( ) ; itr != m_onerror.end( ) ; itr++ ){
		if(pfn==(*itr)) return;
	}
	if(pfn) m_onerror.push_back(pfn);
}

void UDPChannel::RegisterReceivedCallback(PFN_ONRECEIVED pfn){
	ITR_PFN_ONRECEIVED itr;
	for ( itr = m_onreceived.begin( ) ; itr != m_onreceived.end( ) ; itr++ ){
		if(pfn==(*itr)) return;
	}
	if(pfn) m_onreceived.push_back(pfn);
}

void UDPChannel::RegisterSentCallback(PFN_ONSENT pfn){
	ITR_PFN_ONSENT itr;
	for ( itr = m_onsent.begin( ) ; itr != m_onsent.end( ) ; itr++ ){
		if(pfn==(*itr)) return;
	}
	if(pfn) m_onsent.push_back(pfn);
}

void UDPChannel::UnregisterErrorCallback(PFN_ONERROR pfn){
	ITR_PFN_ONERROR itr;
	for ( itr = m_onerror.begin( ) ; itr != m_onerror.end( ) ; itr++ ){
		if(*itr==pfn) m_onerror.erase(itr);
		break;
	}
}

void UDPChannel::UnregisterReceivedCallback(PFN_ONRECEIVED pfn){
	ITR_PFN_ONRECEIVED itr;
	for ( itr = m_onreceived.begin( ) ; itr != m_onreceived.end( ) ; itr++ ){
		if(*itr==pfn) m_onreceived.erase(itr);
		break;
	}
}

void UDPChannel::UnregisterSentCallback(PFN_ONSENT pfn){
	ITR_PFN_ONSENT itr;
	for ( itr = m_onsent.begin( ) ; itr != m_onsent.end( ) ; itr++ ){
		if(*itr==pfn) m_onsent.erase(itr);
		break;
	}
}

void UDPChannel::RegisterMessageChannel(MessageChannel* pMc){
	ITR_MESSAGECHANNEL itrmc;
	for(itrmc=m_mcs.begin();itrmc!=m_mcs.end();itrmc++){
		if((*itrmc)==pMc) return;
	}
	m_mcs.push_back(pMc);
}
void UDPChannel::UnregisterMessageChannel(MessageChannel* pMc){
	ITR_MESSAGECHANNEL itrmc;
	for(itrmc=m_mcs.begin();itrmc!=m_mcs.end();itrmc++){
		if((*itrmc)==pMc) {m_mcs.erase(itrmc);break;}
	}
	if(m_mcs.empty()) delete this;
}

DWORD WINAPI UDPReceiver(LPVOID lpParam){
	UDPChannel* p=(UDPChannel*)lpParam;
	CoInitializeEx(0,2);
	for (;;){
		p->m_clntAddrLen = sizeof(p->m_clntAddr);
		if ((p->m_recvMsgSize = recvfrom(p->m_sock, p->m_recvBuf, MAX_PACKET_LENGTH, 0,(struct sockaddr *) &p->m_clntAddr, &p->m_clntAddrLen)) < 0){
			p->m_lastError=WSAGetLastError();
			p->RaiseError(p->m_lastError);
		}
		if(p->m_recvMsgSize>=16){
			//预先解析头信息:包序号、包个数、消息类型、消息id
			LEN iarr[4]={0,0,0,0};
			LEN nl=0;
			char rev[16]={0};
			for(LEN i=0;i<16;i++){
				rev[i]=p->m_recvBuf[15-i];
			}
			memcpy(&iarr[0],rev,16);

			//TCHAR tmp[100]={0};
			//_stprintf_s(tmp,L"接收：packetIndex=%u,packetCount=%u,type=%u,mid=%u",iarr[3],iarr[2],iarr[1],iarr[0]);
			//TipMsg(tmp);
			//TCHAR msg[STRLEN_4K]={0};
			//size_t msgLen=STRLEN_4K;
			//p->ToHexString((BYTE*)p->m_recvBuf,p->m_recvMsgSize,msg,&msgLen);
			//TipMsg(msg);

			if(iarr[3]==0){	//如果是第一个包
				LEN mt=iarr[1];
				MSGID msgId=iarr[0];
				MessageIn* pMsg=NULL;
				INMItr itr=p->m_in.find(msgId);
				if(itr!=p->m_in.end()){
					pMsg=itr->second;
					pMsg->appendPacket((BYTE*)p->m_recvBuf,p->m_recvMsgSize);
				}else{
					switch(mt){	//消息类型
					case IM:
						pMsg=new IMMessageIn((BYTE*)p->m_recvBuf,p->m_recvMsgSize);
						break;
					case FILESHARE:
						pMsg=new FileMessageIn((BYTE*)p->m_recvBuf,p->m_recvMsgSize,L"e:\\temp\\");	//TODO:文件路径
						break;
					case CONTROL:
						pMsg=new ControlMessageIn((BYTE*)p->m_recvBuf,p->m_recvMsgSize);
						break;
					}
					if(pMsg){
						Address addSrc(p->m_clntAddr);
						pMsg->setSourceAddress(addSrc);
						p->m_in[msgId]=pMsg;
					}
				}
				if(pMsg && pMsg->isReceived()){p->RaiseReceived(pMsg);p->m_in.erase(msgId);SAFE_DEL_PTR(pMsg);}
			}
		}//if end
	}//for end
	CoUninitialize();
	return 0;
}

DWORD WINAPI UDPSender(LPVOID lpParam){
	CoInitializeEx(0,2);
	UDPChannel* p=(UDPChannel*)lpParam;
	for(;;){
		EnterCriticalSection(&p->criticalSection);
		if(p->m_out.size()>0){
			MessageOut* pmo=p->m_out.back();
			Address *padd=pmo->getDestAddress();
			WSSOCKADDRESS wsaTarget=padd->getAddress();
			for(LEN j=0;j<pmo->getPacketCount();j++){
				DWORD bufLen=0;
				BYTE* buf=pmo->getPacket(j,&bufLen);
				if(bufLen<=0) continue;

				//TCHAR tmp[100]={0};
				//_stprintf_s(tmp,L"发送：packetIndex=%u,packetCount=%u,type=%u,mid=%u",j,pmo->getPacketCount(),pmo->getType(),pmo->getMessageId());
				//TipMsg(tmp);

				//TCHAR msg[STRLEN_4K]={0};
				//size_t msgLen=STRLEN_4K;
				//p->ToHexString(buf,bufLen,msg,&msgLen);
				//TipMsg(msg);
					
				//if (sendto(p->m_sockSend, (char*)buf,(int)bufLen, 0, (struct sockaddr *) &wsaTarget,sizeof(wsaTarget)) != p->m_recvMsgSize){
				//	p->m_lastError=WSAGetLastError();
				//	p->RaiseError(p->m_lastError);
				//	SAFE_DEL_PTR(buf);
				//}
				SAFE_DEL_PTR(buf);
			}//for j end
			p->m_out.pop_back();
			p->RaiseSent(pmo);
			SAFE_DEL_PTR(pmo);
		}else Sleep(100);
		LeaveCriticalSection(&p->criticalSection);
	}
	CoUninitialize();
	return 0;
}

BOOL UDPChannel::ToHexString(const unsigned char * bytes,size_t bytesSize,TCHAR* szResult,size_t* resultSize){
	if (!bytes) return FALSE;
	if(resultSize && (*resultSize)<(bytesSize*2+1)) {
		*resultSize=(bytesSize*2+1);
		return FALSE;
	}
	if(szResult){
		SecureZeroMemory(szResult,sizeof(TCHAR)*(*resultSize));
		unsigned char b=0;
		for (int i=0;i<bytesSize;i++) {
			b=bytes[i];
			szResult[i*2]=HEXES[(b & 0xF0) >> 4];
			szResult[i*2+1]=HEXES[(b & 0x0F)];
		}
	}
	if(resultSize) *resultSize=(bytesSize*2+1);
	return TRUE;
}
