#pragma once

#ifndef _FILEMESSAGEIN_H_
#define _FILEMESSAGEIN_H_

#include "messagein.h"
#include <map>
using namespace std;

typedef pair<const LEN,BOOL> FMIPair;
typedef map<const LEN,BOOL> FMIMap;
typedef map<const LEN,BOOL>::iterator PFMIItr;

class FileMessageIn :	public MessageIn
{
public:
	FileMessageIn(BYTE* firstPacket,LEN firstPacketBytesLen,const TCHAR* filePath);
	virtual ~FileMessageIn(void);
	virtual LEN getHeaderLen();
	virtual BOOL appendPacket(BYTE* bytes,LEN bytesLen);
	BOOL getFullPath(TCHAR* path,size_t pathLen);
protected:
	FileMessageIn(void);
private:
	TCHAR* m_filePath;						//接收文件的本地路径和文件名
	TCHAR* m_fileName;						//接收文件的原始文件名
	LEN m_fileNameLen;						//接收文件的原始文件名在包数据中的字节数
	LEN m_fileLen;								//文件长度
	HANDLE m_file;								//文件句柄
	HANDLE m_filemap;							//内存映射文件句柄
	FMIMap m_map;									//检查重复包用的map
};

#endif