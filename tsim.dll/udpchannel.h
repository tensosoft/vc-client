#pragma once
#ifndef _UDPCHANNEL_H_
#define _UDPCHANNEL_H_

#include "Address.h"
#include <vector>
#include <map>
using namespace std;
#include "MessageOut.h"
#include "MessageIn.h"
#include "tsim_i.h"

typedef pair<const MSGID, MessageIn*> INMPair;
typedef map<const MSGID, MessageIn*> INMMap;
typedef map<const MSGID, MessageIn*>::iterator INMItr;

typedef void (*PFN_ONERROR)(DWORD dwErrCode,MessageChannel* unk);
typedef void (*PFN_ONRECEIVED)(MessageIn*,MessageChannel* unk);
typedef void (*PFN_ONSENT)(MessageOut*,MessageChannel* unk);

typedef vector<PFN_ONERROR> V_PFN_ONERROR;
typedef vector<PFN_ONRECEIVED> V_PFN_ONRECEIVED;
typedef vector<PFN_ONSENT> V_PFN_ONSENT;

typedef vector<PFN_ONERROR>::iterator ITR_PFN_ONERROR;
typedef vector<PFN_ONRECEIVED>::iterator ITR_PFN_ONRECEIVED;
typedef vector<PFN_ONSENT>::iterator ITR_PFN_ONSENT;

typedef vector<MessageOut*> V_MSGOUT;
typedef vector<MessageIn*> V_MSGIN;
typedef vector<MessageOut*>::iterator ITROUT;
typedef vector<MessageIn*>::iterator ITRIN;

typedef vector<MessageChannel*> V_MESSAGECHANNEL;
typedef vector<MessageChannel*>::iterator ITR_MESSAGECHANNEL;

DWORD WINAPI UDPReceiver(LPVOID lpParam);
DWORD WINAPI UDPSender(LPVOID lpParam);
const TCHAR HEXES[16]={L'0',L'1',L'2',L'3',L'4',L'5',L'6',L'7',L'8',L'9',L'A',L'B',L'C',L'D',L'E',L'F'};

typedef struct {
	HANDLE		hThread;						//接收线程句柄
	DWORD			dwThreadID;					//接收线程ID
} RECEIVE_CONTEXT;

typedef struct {
	HANDLE		hThread;						//发送线程句柄
	DWORD			dwThreadID;					//发送线程ID
} SEND_CONTEXT;

static RECEIVE_CONTEXT rContext;
static SEND_CONTEXT sContext;

class UDPChannel
{
public:
	UDPChannel(void);
	virtual ~UDPChannel(void);
	virtual void WaitAndReceive();
	virtual BOOL Send(MessageOut* msg,Address& add);
	virtual void RaiseError(DWORD dwErrCode);
	virtual void RaiseReceived(MessageIn* pMsg);
	virtual void RaiseSent(MessageOut* pMsg);
	virtual void RegisterErrorCallback(PFN_ONERROR pfn);
	virtual void RegisterReceivedCallback(PFN_ONRECEIVED pfn);
	virtual void RegisterSentCallback(PFN_ONSENT pfn);
	virtual void UnregisterErrorCallback(PFN_ONERROR pfn);
	virtual void UnregisterReceivedCallback(PFN_ONRECEIVED pfn);
	virtual void UnregisterSentCallback(PFN_ONSENT pfn);
	virtual void RegisterMessageChannel(MessageChannel* pMc);
	virtual void UnregisterMessageChannel(MessageChannel* pMc);
protected:
	WSADATA m_wsaData;											//winsock初始化数据
	WSSOCKADDRESS m_servAddr;								//本机（作为服务端的）地址
	WSPORT m_servPort;											//本机（作为服务端的）UDP端口

	V_PFN_ONERROR m_onerror;								//错误事件处理程序集合
	V_PFN_ONRECEIVED m_onreceived;					//接收成功事件处理程序集合
	V_PFN_ONSENT m_onsent;									//发送成功事件处理程序集合

	V_MESSAGECHANNEL m_mcs;									//包含的MessageChannel对象
public:
	int m_sock;															//sock句柄（接收监听用）
	//int m_sockSend;													//sock句柄（发送用）

	INMMap	m_in;														//待收消息
	V_MSGOUT m_out;													//待发消息

	WSSOCKADDRESS m_clntAddr;								//接收到的包对应的发送端地址
	int m_clntAddrLen;											//接收到的包对应的发送端地址长度
	int m_recvMsgSize;											//接收到的包的数据长度
	char m_recvBuf[MAX_PACKET_LENGTH];			//接收到的包的数据内容缓冲区

	CRITICAL_SECTION criticalSection;				//线程锁定标记
	DWORD m_lastError;											//最近操作导致的错误信息

	static UDPChannel* me;									//包含对自身的引用

	BOOL ToHexString(const unsigned char * bytes,size_t bytesSize,TCHAR* szResult,size_t* resultSize);
};

#endif