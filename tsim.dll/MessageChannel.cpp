// MessageChannel.cpp : MessageChannel 的实现

#include "stdafx.h"
#include "MessageChannel.h"
#include "IMMessageOut.h"
#include "IMMessageIn.h"
#include "FileMessageOut.h"
#include "FileMessageIn.h"

// MessageChannel
STDMETHODIMP MessageChannel::SendMessage(BSTR msg,BSTR dest,ULONG* ret){
	ATLASSERT(ret);
	*ret=0;
	if(!msg ||_tcslen(msg)==0){
		TipMsg(L"没有提供有效消息内容！");
		return S_OK;
	}
	if(!dest ||_tcslen(dest)==0){
		TipMsg(L"没有提供有效目的地址！");
		return S_OK;
	}
	IMMessageOut *pMsg=new IMMessageOut(OLE2T(msg));
	Address destAddress(dest);
	pMsg->setDestAddress(destAddress);
	
	*ret=pMsg->getMessageId();
	if(m_pmo==NULL) m_pmo=pMsg;
	else m_out.push_back(pMsg);
	return S_OK;
}

STDMETHODIMP MessageChannel::SendFile(BSTR fn,BSTR dest,ULONG* ret){
	ATLASSERT(ret);
	*ret=0;
	if(!fn ||_tcslen(fn)==0){
		TipMsg(L"没有提供有效文件名！");
		return S_OK;
	}
	if(!dest ||_tcslen(dest)==0){
		TipMsg(L"没有提供有效目的地址！");
		return S_OK;
	}
	FileMessageOut *pMsg=new FileMessageOut(OLE2T(fn));
	if(!pMsg->m_initialized) return S_OK;
	Address destAddress(dest);
	pMsg->setDestAddress(destAddress);
	
	*ret=pMsg->getMessageId();
	if(m_pmo==NULL) m_pmo=pMsg;
	else m_out.push_back(pMsg);
	return S_OK;
}

STDMETHODIMP MessageChannel::ReceiveMessage(BSTR msg,ULONG msgId,BSTR src,BSTR dt,LONG msgType){
	if(m_spWebBrowser2){
		IServiceProvider* pServiceProvider = NULL;
		if (SUCCEEDED(m_spWebBrowser2->QueryInterface(IID_IServiceProvider,(void**)&pServiceProvider))){
			IOleWindow* pWindow = NULL;
			if (SUCCEEDED(pServiceProvider->QueryService(SID_SShellBrowser,IID_IOleWindow,(void**)&pWindow))){
				HWND hwndBrowser = NULL;
				if (SUCCEEDED(pWindow->GetWindow(&hwndBrowser))){
					HWND hwndRoot=NULL;
					hwndRoot=GetAncestor(hwndBrowser,GA_ROOTOWNER);
					TCHAR szClsName[STRLEN_NORMAL]={0};
					if(hwndRoot && ::GetClassName(hwndRoot,szClsName,STRLEN_NORMAL)>0 && _tcsicmp(szClsName,POST_MESSAGE_TARGET_WINDOW_CLS_NAME)==0){
						size_t l=(_tcslen(msg)+_tcslen(src)+_tcslen(dt)+STRLEN_SMALL)*2;
						int szl=l/2;
						void *content=HeapAlloc(GetProcessHeap(),HEAP_GENERATE_EXCEPTIONS|HEAP_NO_SERIALIZE|HEAP_ZERO_MEMORY,l);
						//msgid
						TCHAR* sz=(TCHAR*)content;
						_stprintf_s(sz,szl,L"%u",msgId);

						//src
						size_t len=(_tcslen(sz)+1);
						szl-=len;
						sz+=(_tcslen(sz)+1);
						_tcscpy_s(sz,szl,src);

						//dt
						len=(_tcslen(sz)+1);
						szl-=len;
						sz+=(_tcslen(sz)+1);
						_tcscpy_s(sz,szl,dt);

						//msg
						len=(_tcslen(sz)+1);
						szl-=len;
						sz+=(_tcslen(sz)+1);
						_tcscpy_s(sz,szl,msg);

						len=(_tcslen(sz)+1);
						szl-=len;
						sz+=(_tcslen(sz)+1);
						ZeroMemory(sz,2);

						PostMessage(hwndRoot,WM_IMRECEIVED,(WPARAM)l,(LPARAM)content);
					}
				}
				pWindow->Release();
			}
			pServiceProvider->Release();
		}
	}
	CComPtr<IReceivedMessage> sp;
	HRESULT hr=sp.CoCreateInstance(L"Discoverx2.ReceivedMessage");
	if(FAILED(hr)){/*TipMsg(L"无法初始化接收到的消息对象！");*/return S_OK;}
	sp->put_Result(msg);
	sp->put_Source(src);
	sp->put_ID(msgId);
	sp->put_MessageType(msgType);
	sp->put_DateTime(dt);
	fireOnReceived(sp);
	return S_OK;
}

STDMETHODIMP MessageChannel::ReceiveSendStatus(LONG statusCode,ULONG msgId,BSTR dest){
	if(m_spWebBrowser2){
		IServiceProvider* pServiceProvider = NULL;
		if (SUCCEEDED(m_spWebBrowser2->QueryInterface(IID_IServiceProvider,(void**)&pServiceProvider))){
			IOleWindow* pWindow = NULL;
			if (SUCCEEDED(pServiceProvider->QueryService(SID_SShellBrowser,IID_IOleWindow,(void**)&pWindow))){
				HWND hwndBrowser = NULL;
				if (SUCCEEDED(pWindow->GetWindow(&hwndBrowser))){
					HWND hwndRoot=NULL;
					hwndRoot=GetAncestor(hwndBrowser,GA_ROOTOWNER);
					TCHAR szClsName[STRLEN_NORMAL]={0};
					if(hwndRoot && ::GetClassName(hwndRoot,szClsName,STRLEN_NORMAL)>0 && _tcsicmp(szClsName,POST_MESSAGE_TARGET_WINDOW_CLS_NAME)==0){
						size_t l=(_tcslen(dest)+STRLEN_SMALL+1)*2;
						int szl=l/2;
						void *content=HeapAlloc(GetProcessHeap(),HEAP_GENERATE_EXCEPTIONS|HEAP_NO_SERIALIZE|HEAP_ZERO_MEMORY,l);
						
						//msgid
						TCHAR* sz=(TCHAR*)content;
						_stprintf_s(sz,szl,L"%u",msgId);

						//statusCode
						size_t len=(_tcslen(sz)+1);
						szl-=len;
						sz+=(_tcslen(sz)+1);
						_stprintf_s(sz,szl,L"%u",statusCode);

						//ip
						len=(_tcslen(sz)+1);
						szl-=len;
						sz+=(_tcslen(sz)+1);
						_tcscpy_s(sz,szl,dest);

						len=(_tcslen(sz)+1);
						szl-=len;
						sz+=(_tcslen(sz)+1);
						ZeroMemory(sz,2);
						PostMessage(hwndRoot,WM_SENDSTATUSCHANGED,(WPARAM)l,(LPARAM)content);
					}
				}
				pWindow->Release();
			}
			pServiceProvider->Release();
		}
	}
	if(statusCode==SENT||statusCode==RECEIVED) {
		fireOnSent(statusCode,msgId,dest);
	}else if(statusCode==PACKETRECEIVED) {SetEvent(m_event);}
	return S_OK;
}

STDMETHODIMP MessageChannel::ReceiveError(LONG errCode,BSTR errDesc){
	if(m_spWebBrowser2){
		IServiceProvider* pServiceProvider = NULL;
		if (SUCCEEDED(m_spWebBrowser2->QueryInterface(IID_IServiceProvider,(void**)&pServiceProvider))){
			IOleWindow* pWindow = NULL;
			if (SUCCEEDED(pServiceProvider->QueryService(SID_SShellBrowser,IID_IOleWindow,(void**)&pWindow))){
				HWND hwndBrowser = NULL;
				if (SUCCEEDED(pWindow->GetWindow(&hwndBrowser))){
					HWND hwndRoot=NULL;
					hwndRoot=GetAncestor(hwndBrowser,GA_ROOTOWNER);
					TCHAR szClsName[STRLEN_NORMAL]={0};
					if(hwndRoot && ::GetClassName(hwndRoot,szClsName,STRLEN_NORMAL)>0 && _tcsicmp(szClsName,POST_MESSAGE_TARGET_WINDOW_CLS_NAME)==0){
						PostMessage(hwndRoot,WM_NETERROR,(WPARAM)errCode,(LPARAM)errDesc);
					}
				}
				pWindow->Release();
			}
			pServiceProvider->Release();
		}
	}
	fireOnError(errCode,errDesc);
	return S_OK;
}

STDMETHODIMP MessageChannel::putref_onreceived(IDispatch* newVal){
	CComPtr<IDispatch> dispOfCallOnReceived = newVal;
	m_onreceived[m_currentThreadId]=dispOfCallOnReceived;

	CBSItr cbsItr=m_cbsr.find(m_currentThreadId);
	if(cbsItr!=m_cbsr.end() && cbsItr->second){CoReleaseMarshalData(cbsItr->second);SAFE_RELEASE(cbsItr->second);}

	IStream *pStm=NULL;
	HRESULT hr=CreateStreamOnHGlobal(NULL, TRUE, &pStm);
	if(FAILED(hr)){
		TCHAR errMsg[STRLEN_NORMAL]={0};
		_stprintf_s(errMsg,L"无法设置事件处理程序，错误代码：0x%X",hr);
		ErrMsg(0,errMsg);
	}
	LARGE_INTEGER li = { 0 };
	hr=CoMarshalInterface(pStm, IID_IDispatch, newVal, MSHCTX_LOCAL/*MSHCTX_INPROC*/, NULL,MSHLFLAGS_TABLEWEAK /*MSHLFLAGS_TABLESTRONG,MSHLFLAGS_NORMAL*/);
	if(FAILED(hr)) {
		TCHAR errMsg[STRLEN_NORMAL]={0};
		_stprintf_s(errMsg,L"无法设置事件处理程序，错误代码：0x%X",hr);
		ErrMsg(0,errMsg);
	}
	pStm->Seek(li, STREAM_SEEK_SET, NULL);
	m_cbsr[m_currentThreadId]=pStm;
	return S_OK;
}

HRESULT MessageChannel::fireOnReceived(CComPtr<IReceivedMessage> pDisp){
	if(m_onreceived.empty()) return S_OK;

	DISPPARAMS dispParams;
	VARIANTARG args[2];

	HRESULT hRes = 0;

	IDispatch* pDispRef=NULL;
	if(pDisp){
		hRes=pDisp->QueryInterface(IID_IDispatch, (void**)&pDispRef);
		if(hRes==S_OK && pDispRef){
			VariantInit(&args[0]);
			args[0].vt = VT_DISPATCH;
			args[0].pdispVal= pDisp;
		}
	}
	if(!pDispRef){
		VariantInit(&args[0]);
		args[0].vt=VT_NULL;//VT_BSTR;
	}

	IDispatch* pDispMe=NULL;
	//hRes = this->QueryInterface(IID_IDispatch, (void**)&pDispMe);
	VariantInit(&args[1]);
	//args[1].vt = VT_DISPATCH;
	//args[1].pdispVal= pDispMe;
	args[1].vt = VT_NULL;

	memset(&dispParams, 0, sizeof(dispParams));
	dispParams.rgvarg = args;
	dispParams.cArgs = sizeof(args)/sizeof(args[0]);
	
	OLECHAR FAR* szMember = L"call";
	CBItr itr;
	CBSItr cbsItr;
	HRESULT hr;
	for ( itr = m_onreceived.begin(); itr != m_onreceived.end( ); itr++ ){
		if(m_currentThreadId!=itr->first) continue;
		CComPtr<IDispatch> dispOfCallOnReceived=NULL;	//itr->second;
		DISPID dispIdOfCallOnReceived=NULL;

		cbsItr=m_cbsr.find(m_currentThreadId);
		if(cbsItr==m_cbsr.end()) continue;
		LARGE_INTEGER li = { 0 };
		cbsItr->second -> Seek(li, STREAM_SEEK_SET, NULL);
		IDispatch * pGoodForThisThread=NULL;
		hr=CoUnmarshalInterface(cbsItr->second, IID_IDispatch,(LPVOID*)&pGoodForThisThread);
		if(FAILED(hr)){
			TCHAR errMsg[STRLEN_NORMAL]={0};
			_stprintf_s(errMsg,L"无法触发消息接收事件处理程序，错误代码：0x%X",hr);
			ErrMsg(0,errMsg);
			continue;
		}
		if(pGoodForThisThread==NULL) continue;
		dispOfCallOnReceived=pGoodForThisThread;

		if (!SUCCEEDED(dispOfCallOnReceived->GetIDsOfNames(IID_NULL, &szMember, 1, LOCALE_SYSTEM_DEFAULT, &dispIdOfCallOnReceived))) {pGoodForThisThread->Release();continue;}
		hr= dispOfCallOnReceived->Invoke(dispIdOfCallOnReceived, IID_NULL, LOCALE_SYSTEM_DEFAULT, DISPATCH_METHOD, &dispParams, NULL,NULL, NULL); 
		pGoodForThisThread->Release();
		//if(FAILED(hr)) {
		//	TCHAR errMsg[STRLEN_NORMAL]={0};
		//	_stprintf_s(errMsg,L"执行消息接收事件处理程序时出现错误，错误代码：0x%X",hr);
		//	ErrMsg(0,errMsg);
		//	continue;
		//}
	}

	VariantClear(&args[0]);
	VariantClear(&args[1]);
	SAFE_RELEASE(pDispMe);

	return S_OK;
}

STDMETHODIMP MessageChannel::putref_onsent(IDispatch* newVal){
	CComPtr<IDispatch> dispOfCallOnSent = newVal;
	m_onsent[m_currentThreadId]=dispOfCallOnSent;

	CBSItr cbsItr=m_cbss.find(m_currentThreadId);
	if(cbsItr!=m_cbss.end() && cbsItr->second){CoReleaseMarshalData(cbsItr->second);SAFE_RELEASE(cbsItr->second);}

	IStream *pStm=NULL;
	HRESULT hr=CreateStreamOnHGlobal(NULL, TRUE, &pStm);
	if(FAILED(hr)){
		TCHAR errMsg[STRLEN_NORMAL]={0};
		_stprintf_s(errMsg,L"无法设置事件处理程序，错误代码：0x%X",hr);
		ErrMsg(0,errMsg);
	}
	LARGE_INTEGER li = { 0 };
	hr=CoMarshalInterface(pStm, IID_IDispatch, newVal, MSHCTX_INPROC/*MSHCTX_LOCAL,MSHCTX_INPROC*/, NULL,MSHLFLAGS_TABLEWEAK /*MSHLFLAGS_TABLESTRONG,MSHLFLAGS_NORMAL*/);
	if(FAILED(hr)) {
		TCHAR errMsg[STRLEN_NORMAL]={0};
		_stprintf_s(errMsg,L"无法设置事件处理程序，错误代码：0x%X",hr);
		ErrMsg(0,errMsg);
	}
	pStm->Seek(li, STREAM_SEEK_SET, NULL);
	m_cbss[m_currentThreadId]=pStm;

	return S_OK;
}

HRESULT MessageChannel::fireOnSent(LONG statusCode,ULONG msgId,BSTR dest){
	if(m_onsent.empty()) return S_OK;

	DISPPARAMS dispParams;
	VARIANTARG args[4];

	HRESULT hRes = 0;

	VariantInit(&args[0]);
	args[0].vt = VT_BSTR;
	args[0].bstrVal=SysAllocString(dest);
	
	VariantInit(&args[1]);
	args[1].vt = VT_UI4;
	args[1].ulVal=msgId;

	VariantInit(&args[2]);
	args[2].vt = VT_I4;
	args[2].lVal=statusCode;

	IDispatch* pDispMe=NULL;
	//hRes = this->QueryInterface(IID_IDispatch, (void**)&pDispMe);
	VariantInit(&args[3]);
	//args[3].vt = VT_DISPATCH;
	//args[3].pdispVal= pDispMe;
	args[3].vt = VT_NULL;
	
	memset(&dispParams, 0, sizeof(dispParams));
	dispParams.rgvarg = args;
	dispParams.cArgs = sizeof(args)/sizeof(args[0]);

	OLECHAR FAR* szMember = L"call";
	CBItr itr;
	CBSItr cbsItr;
	HRESULT hr;
	for ( itr = m_onsent.begin(); itr != m_onsent.end( ); itr++ ){
		if(m_currentThreadId!=itr->first) continue;
		CComPtr<IDispatch> dispOfCallOnSent=NULL;	//itr->second;
		DISPID dispIdOfCallOnSent=NULL;

		cbsItr=m_cbss.find(m_currentThreadId);
		if(cbsItr==m_cbss.end()) continue;
		LARGE_INTEGER li = { 0 };
		cbsItr->second -> Seek(li, STREAM_SEEK_SET, NULL);
		IDispatch * pGoodForThisThread=NULL;
		hr=CoUnmarshalInterface(cbsItr->second, IID_IDispatch,(LPVOID*)&pGoodForThisThread);
		if(FAILED(hr)){
			TCHAR errMsg[STRLEN_NORMAL]={0};
			_stprintf_s(errMsg,L"无法触发消息发送事件处理程序，错误代码：0x%X",hr);
			ErrMsg(0,errMsg);
			continue;
		}
		if(pGoodForThisThread==NULL) continue;
		dispOfCallOnSent=pGoodForThisThread;

		if (!SUCCEEDED(dispOfCallOnSent->GetIDsOfNames(IID_NULL, &szMember, 1, LOCALE_SYSTEM_DEFAULT, &dispIdOfCallOnSent))) {continue;}
		hr= dispOfCallOnSent->Invoke(dispIdOfCallOnSent, IID_NULL, LOCALE_SYSTEM_DEFAULT, DISPATCH_METHOD, &dispParams, NULL,NULL, NULL); 
		//if(FAILED(hr)) {
		//	TCHAR errMsg[STRLEN_NORMAL]={0};
		//	_stprintf_s(errMsg,L"执行消息发送事件处理程序时出现错误，错误代码：0x%X",hr);
		//	ErrMsg(0,errMsg);
		//	continue;
		//}
	}

	VariantClear(&args[0]);
	VariantClear(&args[1]);
	VariantClear(&args[2]);
	VariantClear(&args[3]);

	SAFE_RELEASE(pDispMe);

	return S_OK;
}

STDMETHODIMP MessageChannel::putref_onerr(IDispatch* newVal){
	CComPtr<IDispatch> dispOfCallOnError = newVal;
	m_onsent[m_currentThreadId]=dispOfCallOnError;

	CBSItr cbsItr=m_cbse.find(m_currentThreadId);
	if(cbsItr!=m_cbse.end() && cbsItr->second){CoReleaseMarshalData(cbsItr->second);SAFE_RELEASE(cbsItr->second);}

	IStream *pStm=NULL;
	HRESULT hr=CreateStreamOnHGlobal(NULL, TRUE, &pStm);
	if(FAILED(hr)){
		TCHAR errMsg[STRLEN_NORMAL]={0};
		_stprintf_s(errMsg,L"无法设置事件处理程序，错误代码：0x%X",hr);
		ErrMsg(0,errMsg);
	}
	LARGE_INTEGER li = { 0 };
	hr=CoMarshalInterface(pStm, IID_IDispatch, newVal, MSHCTX_LOCAL/*MSHCTX_INPROC*/, NULL,MSHLFLAGS_TABLEWEAK /*MSHLFLAGS_TABLESTRONG,MSHLFLAGS_NORMAL*/);
	if(FAILED(hr)) {
		TCHAR errMsg[STRLEN_NORMAL]={0};
		_stprintf_s(errMsg,L"无法设置事件处理程序，错误代码：0x%X",hr);
		ErrMsg(0,errMsg);
	}
	pStm->Seek(li, STREAM_SEEK_SET, NULL);
	m_cbse[m_currentThreadId]=pStm;

	return S_OK;
}

HRESULT MessageChannel::fireOnError(LONG errCode,BSTR errDesc){
	if(m_onerror.empty()) return S_OK;

	DISPPARAMS dispParams;
	VARIANTARG args[3];

	HRESULT hRes = 0;

	VariantInit(&args[0]);
	args[0].vt = VT_BSTR;
	args[0].bstrVal=(errDesc?SysAllocString(errDesc):SysAllocString(L""));

	VariantInit(&args[1]);
	args[1].vt = VT_I4;
	args[1].lVal=errCode;

	IDispatch* pDispMe=NULL;
	//hRes = this->QueryInterface(IID_IDispatch, (void**)&pDispMe);
	VariantInit(&args[2]);
	//args[2].vt = VT_DISPATCH;
	//args[2].pdispVal= pDispMe;
	args[2].vt = VT_NULL;

	memset(&dispParams, 0, sizeof(dispParams));
	dispParams.rgvarg = args;
	dispParams.cArgs = sizeof(args)/sizeof(args[0]);

	OLECHAR FAR* szMember = L"call";
	CBItr itr;
	CBSItr cbsItr;
	HRESULT hr;
	for ( itr = m_onsent.begin(); itr != m_onsent.end( ); itr++ ){
		if(m_currentThreadId!=itr->first) continue;
		CComPtr<IDispatch> dispOfCallOnError=NULL;	//itr->second;
		DISPID dispIdOfCallOnError=NULL;

		cbsItr=m_cbse.find(m_currentThreadId);
		if(cbsItr==m_cbse.end()) continue;
		LARGE_INTEGER li = { 0 };
		cbsItr->second -> Seek(li, STREAM_SEEK_SET, NULL);
		IDispatch * pGoodForThisThread=NULL;
		hr=CoUnmarshalInterface(cbsItr->second, IID_IDispatch,(LPVOID*)&pGoodForThisThread);
		if(FAILED(hr)){
			TCHAR errMsg[STRLEN_NORMAL]={0};
			_stprintf_s(errMsg,L"无法触发错误事件处理程序，错误代码：0x%X",hr);
			ErrMsg(0,errMsg);
			continue;
		}
		if(pGoodForThisThread==NULL) continue;
		dispOfCallOnError=pGoodForThisThread;

		if (!SUCCEEDED(dispOfCallOnError->GetIDsOfNames(IID_NULL, &szMember, 1, LOCALE_SYSTEM_DEFAULT, &dispIdOfCallOnError))) {continue;}
		hr= dispOfCallOnError->Invoke(dispIdOfCallOnError, IID_NULL, LOCALE_SYSTEM_DEFAULT, DISPATCH_METHOD, &dispParams, NULL,NULL, NULL); 
		//if(FAILED(hr)) {
		//	TCHAR errMsg[STRLEN_NORMAL]={0};
		//	_stprintf_s(errMsg,L"执行错误事件处理程序时出现错误，错误代码：0x%X",hr);
		//	ErrMsg(0,errMsg);
		//	continue;
		//}
	}

	VariantClear(&args[0]);
	VariantClear(&args[1]);
	VariantClear(&args[2]);

	SAFE_RELEASE(pDispMe);

	return S_OK;
}

STDMETHODIMP MessageChannel::Dispose(VARIANT_BOOL* ret){
	*ret=VARIANT_FALSE;

	CBSItr cbsItr;
	for (cbsItr=m_cbsr.begin();cbsItr!=m_cbsr.end();cbsItr++){
		if(cbsItr->second) {CoReleaseMarshalData(cbsItr->second);SAFE_RELEASE(cbsItr->second);}
	}
	m_cbsr.clear();
	for (cbsItr=m_cbss.begin();cbsItr!=m_cbss.end();cbsItr++){
		if(cbsItr->second) {CoReleaseMarshalData(cbsItr->second);SAFE_RELEASE(cbsItr->second);}
	}
	m_cbss.clear();
	for (cbsItr=m_cbse.begin();cbsItr!=m_cbse.end();cbsItr++){
		if(cbsItr->second) {CoReleaseMarshalData(cbsItr->second);SAFE_RELEASE(cbsItr->second);}
	}
	m_cbse.clear();

	if(m_spNC){
		BSTR bstr=SysAllocString(m_szMonikerName);
		m_spNC->UnregisterMessageChannel(bstr);
		ULONG mcc=0;
		if(m_dwROTRegisterNC && m_spNC->GetRegisteredMessageChannelCount(&mcc)==S_OK && mcc==0){
			m_spROT->Revoke(m_dwROTRegisterNC);
			m_spNC->Stop();
			m_dwROTRegisterNC=0;
		}
		FREE_SYS_STR(bstr);
		SAFE_RELEASE(m_spNC);
	}

	if(m_dwROTRegister>0 && m_spROT){
		HRESULT hr=m_spROT->Revoke(m_dwROTRegister);
		if(FAILED(hr)){
			ErrMsg(0,L"无法卸载传输对象！"); 
			return S_OK;
		}
		m_dwROTRegister=0;
	}

	*ret=VARIANT_TRUE;
	return S_OK;
}

HRESULT MessageChannel::GetNetChannel(){
	m_spNC=NULL;
	HRESULT hr=0;
	if(!m_spCtx){
		hr=CreateBindCtx(0,&m_spCtx);
		if(FAILED(hr) || !m_spCtx){
			ErrMsg(0,L"无法初始化对象注册上下文！");
			return S_OK;
		}
	}

	if(m_spCtx && !m_spROT){
		hr=m_spCtx->GetRunningObjectTable(&m_spROT);
		if(FAILED(hr) || !m_spROT){
			ErrMsg(0,L"无法初始化对象注册表！");
			return S_OK;
		}
	}
	
	CComPtr<IEnumMoniker> spEnumMon=NULL;
	hr=m_spROT->EnumRunning(&spEnumMon);
	if(hr==E_OUTOFMEMORY || FAILED(hr)){
		ErrMsg(0,L"无法获取对象注册表集合！");
		return S_OK;
	}
	
	TCHAR szKeyStart[MAX_PATH]={0};
	_tcscpy_s(szKeyStart,L"!");
	_tcscat_s(szKeyStart,ROT_ITEM_NAME);
	
	BOOL matched=FALSE;
	IMoniker* spMon=NULL;
	ULONG fetched=0;
	while(spEnumMon->Next(1,&spMon,&fetched)==S_OK){		
		LPOLESTR pDisplayName =(LPOLESTR)HeapAlloc(GetProcessHeap(),0,sizeof(TCHAR)*STRLEN_1K);
		if(!pDisplayName){
			ErrMsg(GetLastError(),L"无法分配内存：%s！");
			return S_OK;
		}
		ZeroMemory(pDisplayName,sizeof(TCHAR)*STRLEN_1K);
		hr=spMon->GetDisplayName(m_spCtx,NULL,&pDisplayName);
		if (SUCCEEDED(hr) && _tcsicmp(pDisplayName,szKeyStart)==0){
			IUnknown* pUnk=NULL;
			hr=m_spROT->GetObject(spMon,&pUnk);
			if(FAILED(hr)){
				ErrMsg(GetLastError(),L"无法获取对象！");
				return S_OK;
			}
			
			hr=pUnk->QueryInterface(IID_INetChannel,(void**)&m_spNC);	//IDispatch
			if(FAILED(hr)){
				ErrMsg(GetLastError(),L"无法获取传输对象！");
				pUnk->Release();
				return S_OK;
			}
			pUnk->Release();
			matched=TRUE;
		}
		if(HeapFree(GetProcessHeap(), 0, pDisplayName)==0){
			ErrMsg(GetLastError(),L"无法释放内存：%s！");
			return S_OK;
		}
		if(matched) break;	//找到匹配则退出
	}
		
	// 没找到已经注册的，则根据选项决定是否新建一个并返回。
	if(!matched){
		hr=CoCreateInstance(__uuidof(NetChannel),NULL, CLSCTX_INPROC_SERVER,__uuidof(INetChannel), (void**)&m_spNC);
		if(FAILED(hr)){
			ErrMsg(GetLastError(),L"无法创建传输对象！");
			return S_OK;
		}
		
		hr=CreateItemMoniker(L"!",ROT_ITEM_NAME,&m_spMon);
		if(FAILED(hr) || !m_spMon){
			ErrMsg(0,L"无法创建传输对象别名！");
			return S_OK;
		}

		//IUnknown* pvUnk=NULL;
		//hr=m_spNC->QueryInterface(IID_IUnknown,(void**)&pvUnk);
		//if(FAILED(hr) || !pvUnk){
		//	ErrMsg(0,L"无法访问对象！");
		//	return S_OK;
		//}
		this->m_dwROTRegisterNC=0;
		hr=m_spROT->Register(ROTFLAGS_REGISTRATIONKEEPSALIVE,m_spNC,m_spMon,&this->m_dwROTRegisterNC);
		if(FAILED(hr)){
			ErrMsg(0,L"无法注册传输对象！"); 
			//pvUnk->Release();
			return S_OK;
		}
		//pvUnk->Release();
	}
	m_spMon=NULL;
	hr=CreateItemMoniker(L"!",m_szMonikerName,&m_spMon);
	if(FAILED(hr) || !m_spMon){
		ErrMsg(0,L"无法创建当前对象别名！");
		return S_OK;
	}

	IUnknown* pvUnk=NULL;
	hr=this->QueryInterface(IID_IUnknown,(void**)&pvUnk);
	if(FAILED(hr) || !pvUnk){
		ErrMsg(0,L"无法访问当前对象！");
		return S_OK;
	}
	this->m_dwROTRegister=0;
	hr=m_spROT->Register(ROTFLAGS_REGISTRATIONKEEPSALIVE,pvUnk,m_spMon,&this->m_dwROTRegister);
	if(FAILED(hr)){
		ErrMsg(0,L"无法注册当前对象！"); 
		pvUnk->Release();
		return S_OK;
	}
	pvUnk->Release();

	return S_OK;
}

DWORD WINAPI SenderThreadProc(LPVOID lpParam){
	CoInitializeEx(0,2);
	MessageChannel* p=(MessageChannel*)lpParam;
	int sentbytes=0;
	for(;;){
		if(p->m_pmo){
			//MessageOut* pmo=p->m_pmo;
			Address *padd=p->m_pmo->getDestAddress();
			WSSOCKADDRESS wsaTarget=padd->getAddress();
			LEN packetCount=p->m_pmo->getPacketCount();
			for(LEN j=0;j<packetCount;j++){
				DWORD bufLen=0;
				BYTE* buf=p->m_pmo->getPacket(j,&bufLen);
				if(bufLen<=0) continue;
sentto:
				//fd_set fdset; 
				//FD_ZERO(&fdset);
				//FD_SET(p->m_sockSend,&fdset); 
				//int s=select(0,NULL,&fdset,NULL,NULL);
				//while(s<=0){
				//	s=select(0,NULL,&fdset,NULL,NULL);
				//}
				if ((sentbytes=sendto(p->m_sockSend, (char*)buf,(int)bufLen, 0, (struct sockaddr *) &wsaTarget,sizeof(wsaTarget)))!=bufLen||sentbytes==SOCKET_ERROR){
					DWORD dwErr=WSAGetLastError();
					p->ReceiveError(dwErr,L"");
					SAFE_DEL_PTR(buf);
				}
				SAFE_DEL_PTR(buf);

				if(packetCount>1 && j<(packetCount-1)){	//发送多个数据包且非最后一个数据包时等待接收确认 && (j==0||(j%10)==0)
					DWORD dw=WaitForSingleObject(p->m_event,10000);
					switch (dw){
					case WAIT_OBJECT_0:
						break;
					case WAIT_ABANDONED: 
						break;
					case WAIT_TIMEOUT:
						goto sentto;
						break;
					}//switch end
				}//if end
			}//for j end
			//TCHAR t[100]={0};
			//_stprintf_s(t,L"发送：packetCount=%u",packetCount);
			//TipMsg(t);
			TCHAR szIP[STRLEN_SMALL]={0};
			padd->getAddressString(szIP,STRLEN_SMALL);
			BSTR bstrDest=SysAllocString(szIP);
			p->ReceiveSendStatus(SENT,p->m_pmo->getMessageId(),bstrDest);
			FREE_SYS_STR(bstrDest);

			EnterCriticalSection(&p->criticalSection);
			ITROUT itr=p->m_out.begin();
			if(itr!=p->m_out.end() && (*itr)==p->m_pmo) {
				itr=p->m_out.erase(itr);
			}
			SAFE_DEL_PTR(p->m_pmo);
			for(itr=p->m_out.begin();itr!=p->m_out.end();itr++){
				p->m_pmo=(*itr);
				if(p->m_pmo) break;
			}
			LeaveCriticalSection(&p->criticalSection);
		}else Sleep(100);
	}//for end
	CoUninitialize();
	return 0;
}
