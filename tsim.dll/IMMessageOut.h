#pragma once

#ifndef _IMMESSAGEOUT_H_
#define _IMMESSAGEOUT_H_

#include "messageout.h"
class IMMessageOut :	public MessageOut
{
public:
	IMMessageOut(TCHAR* content);
	virtual ~IMMessageOut(void);

	virtual LEN getPacketCount();
	virtual BYTE* getPacket(LEN packetIndex,LEN* pBytesLen);
	virtual LEN getHeaderLen();
protected:
	IMMessageOut(void);
	TCHAR* m_content;
	BYTE* m_byteContent;
	LEN m_byteContentLen;
	LEN m_packetCount;
};

#endif
