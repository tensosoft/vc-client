/*
* 所有收到或发送的消息的基类。
*	(c) 腾硕软件科技 2010-12-09
*/
#pragma once
#ifndef _MESSAGEBASE_H_
#define _MESSAGEBASE_H_

#include "Address.h"
class MessageBase
{
public:
	MessageBase(void);
	virtual ~MessageBase(void);

	Address* getSourceAddress();
	void setSourceAddress(Address source);
	Address* getDestAddress();
	void setDestAddress(Address dest);
	MESSAGETYPE getType();
	void setType(MESSAGETYPE type);
	DWORD getLastError();
	virtual LEN getMessageId()=0;														//返回消息ID对应的数字
	virtual LEN getHeaderLen()=0;														//返回消息头长度（字节）
protected:
	Address* m_source;
	Address* m_dest;
	MESSAGETYPE m_type;
	DWORD m_lastError;
};

#endif
