// IEBHO.cpp : IEBHO 的实现

#include "stdafx.h"
#include "IEBHO.h"
#include <regex>
using namespace std::tr1; 

// IEBHO
BOOL InitDjslMap(void){
	if(!m_djslmapFlag){
		HUSKEY hKey=NULL;
		if(SHRegOpenUSKey(L"Software\\Discoverx2",KEY_READ,NULL,&hKey,FALSE)==ERROR_SUCCESS){
			DWORD dwValueCount=0;
			DWORD dwMaxValueNameLen=0;
			TCHAR* szPattern=NULL;
			if(SHRegQueryInfoUSKey(hKey,0,0,&dwValueCount,&dwMaxValueNameLen,SHREGENUM_HKCU)==ERROR_SUCCESS){
				for(DWORD i=0;i<dwValueCount;i++){
					SAFE_DEL_ARR(szPattern);
					szPattern=new TCHAR[dwMaxValueNameLen+2];
					ZeroMemory(szPattern,sizeof(TCHAR)*(dwMaxValueNameLen+2));
					DWORD dwPatternLen=dwMaxValueNameLen+2;
					DWORD dwDataType=REG_SZ;
					TCHAR szData[STRLEN_1K]={0};
					DWORD dwDataLen=sizeof(TCHAR)*STRLEN_1K;
					ZeroMemory(szData,dwDataLen);
					LSTATUS ls=SHRegEnumUSValue(hKey,i,szPattern,&dwPatternLen,&dwDataType,(LPVOID)(&szData[0]),&dwDataLen,SHREGENUM_HKCU);
					if(ls!=ERROR_SUCCESS) continue;
					if(!szPattern || _tcslen(szPattern)==0) continue;
					if(!szData || _tcslen(szData)==0) continue;
					CComBSTR key(szPattern);
					CComBSTR val(szData);
					m_djslmap.insert(DynamicJSLoaderInfoPair(key,val));
				}
			}
			SAFE_DEL_ARR(szPattern);
			SHRegCloseUSKey(hKey);
		}
	}else{
		return m_djslmapFlag;
	}
	m_djslmapFlag=TRUE;
	return m_djslmapFlag;
}

STDMETHODIMP IEBHO::SetSite(IUnknown* pUnkSite){
	if (pUnkSite != NULL){
		HRESULT hr = pUnkSite->QueryInterface(IID_IWebBrowser2, (void **)&m_spWebBrowser);
		if (SUCCEEDED(hr)){
			// 注册以支持来自DWebBrowserEvents2的事件
			hr = DispEventAdvise(m_spWebBrowser);
			if (SUCCEEDED(hr)){
				m_fAdvised = TRUE;
			}
		}
	}else{
		// 解除来自DWebBrowserEvents2的事件注册
		if (m_fAdvised){
			DispEventUnadvise(m_spWebBrowser);
			m_fAdvised = FALSE;
		}
		m_spWebBrowser.Release();
		m_spWebBrowser=NULL; 
	}
	ROT_UnRegister();
	HRESULT ret=IObjectWithSiteImpl<IEBHO>::SetSite(pUnkSite);
	ROT_Register();
	return ret;
}

void STDMETHODCALLTYPE IEBHO::OnDocumentComplete(IDispatch *pDisp, VARIANT *pvarURL){
	IHTMLDocument2	*htmlDoc2=NULL;
	HRESULT hr=m_spWebBrowser->get_Document((IDispatch**)&htmlDoc2);
	if (FAILED(hr)) goto end;
	IHTMLWindow2* win=NULL;
	hr=htmlDoc2->get_parentWindow(&win);
	if (FAILED(hr)) goto end;
	BSTR bstrLanguage=SysAllocString(L"javascript");
	VARIANT ret;

	IHTMLLocation *loc=NULL;
	win->get_location(&loc);
	BSTR bstrHref=NULL;
	if(loc)loc->get_href(&bstrHref);
	if(bstrHref && m_djslmap.size()>0){
		DynamicJSLoaderInfoItr itr;
		for (itr=m_djslmap.begin();itr!=m_djslmap.end();itr++){
			TCHAR* szPattern=OLE2T(itr->first.m_str);
			TCHAR* szData=OLE2T(itr->second.m_str);
			wregex rgx(szPattern,std::regex_constants::icase);
			if(szData && _tcslen(szData)>0 && regex_match(bstrHref,rgx)){
				TCHAR strImport[STRLEN_1K]={0};
				_stprintf_s(strImport,L"try{var djsflag=true;if(window.djsarr){for(var i=0;i<window.djsarr.length;i++)if(window.djsarr[i]=='%s') {djsflag=false;break;}}if(djsflag){if(!window.djsarr){window.djsarr=new Array();}window.djsarr.push('%s');var script = document.createElement('script');script.type = 'text/javascript';script.src = '%s';script.charset='gbk';var hs=document.getElementsByTagName('head');if(hs!=null && hs.length>0) {hs[0].appendChild(script);}else{document.body.appendChild(script);}}}catch(e){}",szData,szData,szData);
				BSTR jsImport=SysAllocString(strImport);
				hr=win->execScript(jsImport,bstrLanguage,&ret);
				FREE_SYS_STR(jsImport);
			}
		}
	}
	/*
	HUSKEY hKey=NULL;
	if(bstrHref && SHRegOpenUSKey(L"Software\\Discoverx2",KEY_READ,NULL,&hKey,FALSE)==ERROR_SUCCESS){
		DWORD dwValueCount=0;
		DWORD dwMaxValueNameLen=0;
		TCHAR* szPattern=NULL;
		if(SHRegQueryInfoUSKey(hKey,0,0,&dwValueCount,&dwMaxValueNameLen,SHREGENUM_HKCU)==ERROR_SUCCESS){
			for(DWORD i=0;i<dwValueCount;i++){
				SAFE_DEL_ARR(szPattern);
				szPattern=new TCHAR[dwMaxValueNameLen+2];
				ZeroMemory(szPattern,sizeof(TCHAR)*(dwMaxValueNameLen+2));
				DWORD dwPatternLen=dwMaxValueNameLen+2;
				DWORD dwDataType=REG_SZ;
				TCHAR szData[STRLEN_1K]={0};
				DWORD dwDataLen=sizeof(TCHAR)*STRLEN_1K;
				ZeroMemory(szData,dwDataLen);
				LSTATUS ls=SHRegEnumUSValue(hKey,i,szPattern,&dwPatternLen,&dwDataType,(LPVOID)(&szData[0]),&dwDataLen,SHREGENUM_HKCU);
				if(ls!=ERROR_SUCCESS) {continue;}	
				if(!szPattern || _tcslen(szPattern)==0) continue;
				wregex rgx(szPattern,regex_constants::icase);
				if(szData && _tcslen(szData)>0 && regex_match(bstrHref,rgx)){
					TCHAR strImport[STRLEN_1K]={0};
					_stprintf_s(strImport,L"try{var djsflag=true;if(window.djsarr){for(var i=0;i<window.djsarr.length;i++)if(window.djsarr[i]=='%s') {djsflag=false;break;}}if(djsflag){if(!window.djsarr){window.djsarr=new Array();}window.djsarr.push('%s');var script = document.createElement('script');script.type = 'text/javascript';script.src = '%s';script.charset='gbk';var hs=document.getElementsByTagName('head');if(hs!=null && hs.length>0) {hs[0].appendChild(script);}else{document.body.appendChild(script);}}}catch(e){}",szData,szData,szData);
					BSTR jsImport=SysAllocString(strImport);
					hr=win->execScript(jsImport,bstrLanguage,&ret);
					FREE_SYS_STR(jsImport);
				}
			}
		}
		SAFE_DEL_ARR(szPattern);
	}
	*/

	BSTR jsFunc=SysAllocString(L"if(window.webBrowser && typeof(window.webBrowser.onDocumentComplete)=='function') window.webBrowser.onDocumentComplete();");
	hr=win->execScript(jsFunc,bstrLanguage,&ret);

	FREE_SYS_STR(bstrLanguage);
	FREE_SYS_STR(jsFunc);
	
end:
	//if(hKey) SHRegCloseUSKey(hKey);
	SAFE_RELEASE(loc);
	SAFE_RELEASE(win);
	SAFE_RELEASE(htmlDoc2);
}

void STDMETHODCALLTYPE IEBHO::OnQuit(){
	if(m_spWebBrowser && !m_fClosed){
		IHTMLDocument2	*htmlDoc2=NULL;
		HRESULT hr=m_spWebBrowser->get_Document((IDispatch**)&htmlDoc2);
		if (FAILED(hr)) goto end;
		IHTMLWindow2* win=NULL;
		hr=htmlDoc2->get_parentWindow(&win);
		if (FAILED(hr)) goto end;
		BSTR bstrLanguage=SysAllocString(L"javascript");
		VARIANT ret;
		BSTR jsFunc=SysAllocString(L"if(window.webBrowser && typeof(window.webBrowser.onQuit)=='function') window.webBrowser.onQuit();");
		hr=win->execScript(jsFunc,bstrLanguage,&ret);
		FREE_SYS_STR(bstrLanguage);
	end:
		SAFE_RELEASE(win);
		SAFE_RELEASE(htmlDoc2);
	}
}

void STDMETHODCALLTYPE IEBHO::OnWindowClosing(VARIANT_BOOL IsChildWindow,VARIANT_BOOL *Cancel){
	if(m_spWebBrowser){
		m_fClosed=TRUE;
		IHTMLDocument2	*htmlDoc2=NULL;
		HRESULT hr=m_spWebBrowser->get_Document((IDispatch**)&htmlDoc2);
		if (FAILED(hr)) goto end;
		IHTMLWindow2* win=NULL;
		hr=htmlDoc2->get_parentWindow(&win);
		if (FAILED(hr)) goto end;
		BSTR bstrLanguage=SysAllocString(L"javascript");
		VARIANT ret;
		BSTR jsFunc=SysAllocString(L"if(window.webBrowser && typeof(window.webBrowser.onWindowClosing)=='function') window.webBrowser.onWindowClosing();");
		hr=win->execScript(jsFunc,bstrLanguage,&ret);
		FREE_SYS_STR(bstrLanguage);
	end:
		SAFE_RELEASE(win);
		SAFE_RELEASE(htmlDoc2);
	}
}
void IEBHO::SetROTID(DWORD dwValue){m_dwROTID = dwValue;}
void IEBHO::ROT_Register(){
	CComPtr<IRunningObjectTable> pTable;
	if(GetRunningObjectTable(0, &pTable) == S_OK){
		CComBSTR bstrItemName;
		CComPtr<IMoniker> pMoniker;
		TCHAR szBuffer[STRLEN_1K]={0};
		_stprintf_s(szBuffer, L"DISCOVERX2_IEBHO@%d", m_spUnkSite);
		bstrItemName = szBuffer;
		if(CreateItemMoniker(L"!", bstrItemName, &pMoniker) == S_OK){
			DWORD dwRegister;
			IUnknown* pvUnk=NULL;
			if(this->QueryInterface(IID_IUnknown,(void**)&pvUnk)==S_OK){
				if(pTable->Register(1, pvUnk, pMoniker, &dwRegister) == S_OK){
					SetROTID(dwRegister);
					ATLTRACE(L"IEBHO: the moniker %s has been registered successfully\n", szBuffer);
					//TipMsg(L"0");
				}else{
					ATLTRACE(L"IEBHO: failed to register moniker % s in the ROT\n", szBuffer);
					//TipMsg(L"1");
				}
			}else{
				ATLTRACE(L"IEBHO: failed to get IUnknown Pointer");
				//TipMsg(L"2");
			}
			pvUnk->Release();
		}else{
			ATLTRACE(L"IEBHO: failed to create moniker %s\n", szBuffer);
			//TipMsg(L"3");
		}
	}
	else{
		ATLTRACE(L"IEBHO: Could not retrieve the ROT pointer\n");
		//TipMsg(L"4");
	}
}

void IEBHO::ROT_UnRegister(){
	CComPtr<IRunningObjectTable> pTable;
	if(GetRunningObjectTable(0, &pTable) == S_OK){
		HRESULT hRes = pTable->Revoke(m_dwROTID);
		ATLTRACE(L"IEBHO: ID = %d, result code %x\n", m_dwROTID, hRes);
	}
	SetROTID(0);
}

//void IEBHO::EnumIE(){
//	int cnt=0;
//	IRunningObjectTable* pTable;
//	if(!FAILED(GetRunningObjectTable(0, &pTable))){
//		IEnumMoniker* pEnum;
//		if(!FAILED(pTable->EnumRunning(&pEnum))){
//			IUnknown* pUnknown;
//			IMoniker* pCurMoniker = NULL;
//			LPMALLOC pMalloc;
//			while (pEnum->Next(1, &pCurMoniker, NULL) == S_OK){
//				if(!FAILED(pTable->GetObject(pCurMoniker, &pUnknown))){
//					IObjectWithSite* pSite;
//					if(!FAILED(pUnknown->QueryInterface(IID_IObjectWithSite, (void**)&pSite))){
//						IDispatch* pDispatch;
//						if(pSite->GetSite(IID_IDispatch, (void**)&pDispatch) == S_OK){
//							CComPtr<IWebBrowser2>  spWebBrowser;
//							if(pDispatch->QueryInterface(IID_IWebBrowser2,(void**)&spWebBrowser)==S_OK){
//								BSTR bstrUrl=NULL;
//								spWebBrowser->get_LocationURL(&bstrUrl);
//								TipMsg(bstrUrl);
//								FREE_SYS_STR(bstrUrl);
//							}
//							cnt++;
//							pDispatch->Release();
//						}
//						pSite->Release();
//					}
//					pUnknown->Release();
//				}
//				pCurMoniker->Release();
//			}
//			pEnum->Release();
//		}
//		pTable->Release();
//	}
//	TCHAR szTip[100]={0};
//	_stprintf_s(szTip,L"count:%d",cnt);
//	TipMsg(szTip);
//}