// IEBHO.h : IEBHO 的声明

#pragma once
#include "stdafx.h"
#include "resource.h"       // 主符号
#include "bext_i.h"
#include <shlguid.h>
#include <exdispid.h>
#include <mshtml.h>
#include <map>
using namespace std;

typedef std::map<CComBSTR,CComBSTR> DynamicJSLoaderInfoMap;
typedef std::pair<CComBSTR,CComBSTR> DynamicJSLoaderInfoPair;
typedef std::map<CComBSTR,CComBSTR>::iterator	DynamicJSLoaderInfoItr;

static DynamicJSLoaderInfoMap m_djslmap;
static BOOL m_djslmapFlag;
static BOOL InitDjslMap(void);

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif

// IEBHO

class ATL_NO_VTABLE IEBHO :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<IEBHO, &CLSID_IEBHO>,
	public IObjectWithSiteImpl<IEBHO>,
	public IDispatchImpl<IIEBHO, &IID_IIEBHO, &LIBID_bextLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IDispEventImpl<1, IEBHO, &DIID_DWebBrowserEvents2, &LIBID_SHDocVw, 1, 1>
{
public:
	IEBHO()
	{
		m_fClosed=FALSE;
		InitDjslMap();
	}

DECLARE_REGISTRY_RESOURCEID(IDR_IEBHO)

DECLARE_NOT_AGGREGATABLE(IEBHO)

BEGIN_COM_MAP(IEBHO)
	COM_INTERFACE_ENTRY(IIEBHO)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IObjectWithSite)
END_COM_MAP()

BEGIN_SINK_MAP(IEBHO)
	SINK_ENTRY_EX(1, DIID_DWebBrowserEvents2, DISPID_DOCUMENTCOMPLETE, OnDocumentComplete)
	SINK_ENTRY_EX(1, DIID_DWebBrowserEvents2, DISPID_ONQUIT, OnQuit)
	SINK_ENTRY_EX(1, DIID_DWebBrowserEvents2, DISPID_WINDOWCLOSING, OnWindowClosing)
END_SINK_MAP()

	//事件处理程序定义
	void STDMETHODCALLTYPE OnDocumentComplete(IDispatch *pDisp, VARIANT *pvarURL); 
	void STDMETHODCALLTYPE OnQuit(); 
	void STDMETHODCALLTYPE OnWindowClosing(VARIANT_BOOL IsChildWindow,VARIANT_BOOL *Cancel);

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
	STDMETHOD(SetSite)(IUnknown *pUnkSite);
	// DWebBrowserEvents2
	void SetROTID(DWORD dwValue);
private:
	CComPtr<IWebBrowser2>  m_spWebBrowser;
	BOOL m_fClosed;
	BOOL m_fAdvised; 
	DWORD m_dwROTID;
	void ROT_UnRegister();
	void ROT_Register();
};

OBJECT_ENTRY_AUTO(__uuidof(IEBHO), IEBHO)
