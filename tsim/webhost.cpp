//webhost.cpp

///////////////////////////////////////////////////////////////////////////////
#include "webhost.h"
#include <tchar.h>
#include <atlbase.h>
#include <atlconv.h>

// IUnknown 成员
HRESULT STDMETHODCALLTYPE CWBEvent::QueryInterface(REFIID riid,void ** ppvObject)
{
	if(riid == IID_IUnknown)
		*ppvObject = this;
	else if ( riid==IID_IDispatch)
		*ppvObject = (void *) this;
	else if (riid==DIID_DWebBrowserEvents2)
		*ppvObject = (void *) this;
	else
	{
		*ppvObject = NULL;
		return E_NOINTERFACE;
	}
	return S_OK;
}

ULONG STDMETHODCALLTYPE CWBEvent::AddRef(void)
{
	return 1;
}

ULONG STDMETHODCALLTYPE CWBEvent::Release(void)
{
	return 1;
}

//IDispatch
HRESULT STDMETHODCALLTYPE CWBEvent::GetTypeInfoCount(UINT *pctinfo) {
	return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CWBEvent::GetTypeInfo(UINT iTInfo,LCID lcid,ITypeInfo **ppTInfo) {
	return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CWBEvent::GetIDsOfNames(REFIID riid,LPOLESTR *rgszNames,UINT cNames, LCID lcid,DISPID *rgDispId) {
	return S_OK;
}

HRESULT STDMETHODCALLTYPE CWBEvent::Invoke(DISPID  dispIdMember, REFIID  riid, LCID  lcid, WORD  wFlags, DISPPARAMS*  pDispParams, VARIANT*  pVarResult, EXCEPINFO*  pExcepInfo, UINT*  puArgErr) {
	switch (dispIdMember) {
		case DISPID_BEFORENAVIGATE2:
			// call BeforeNavigate
			// (parameters are on stack, thus in reverse order)
			BeforeNavigate2( pDispParams->rgvarg[6].pdispVal,    // pDisp
					pDispParams->rgvarg[5].pvarVal,     // url
					pDispParams->rgvarg[4].pvarVal,     // Flags
					pDispParams->rgvarg[3].pvarVal,     // TargetFrameName
					pDispParams->rgvarg[2].pvarVal,     // PostData
					pDispParams->rgvarg[1].pvarVal,     // Headers
					pDispParams->rgvarg[0].pboolVal);   // Cancel
			break;
		case DISPID_CLIENTTOHOSTWINDOW:
			ClientToHostWindow(pDispParams->rgvarg[1].plVal, pDispParams->rgvarg[0].plVal);
			break;
		case DISPID_COMMANDSTATECHANGE:
			CommandStateChange(pDispParams->rgvarg[1].lVal, pDispParams->rgvarg[0].boolVal);
			break;
		case DISPID_DOCUMENTCOMPLETE:
			DocumentComplete(pDispParams->rgvarg[1].pdispVal, pDispParams->rgvarg[0].pvarVal);
			break;
		case DISPID_DOWNLOADBEGIN:
			DownloadBegin();
			break;
		case DISPID_DOWNLOADCOMPLETE:
			DownloadComplete();
			break;
		case DISPID_FILEDOWNLOAD:
			FileDownload(pDispParams->rgvarg[1].pboolVal, pDispParams->rgvarg[0].pboolVal);
			break;
		case DISPID_NAVIGATECOMPLETE2:
			NavigateComplete2(pDispParams->rgvarg[1].pdispVal, pDispParams->rgvarg[0].pvarVal);
			break;
		case DISPID_NAVIGATEERROR:
			NavigateError(pDispParams->rgvarg[4].pdispVal, pDispParams->rgvarg[3].pvarVal, pDispParams->rgvarg[2].pvarVal, pDispParams->rgvarg[1].pvarVal, pDispParams->rgvarg[0].pboolVal);
			break;
		case DISPID_NEWWINDOW2:
			NewWindow2(pDispParams->rgvarg[1].ppdispVal, pDispParams->rgvarg[0].pboolVal);
			break;
		case DISPID_ONFULLSCREEN:
			OnFullScreen(pDispParams->rgvarg[0].boolVal);
			break;
		case DISPID_ONMENUBAR:
			OnMenuBar(pDispParams->rgvarg[0].boolVal);
			break;
		case DISPID_ONQUIT:
			OnQuit();
			break;
		case DISPID_ONSTATUSBAR:
			OnStatusBar(pDispParams->rgvarg[0].boolVal);
			break;
		case DISPID_ONTHEATERMODE:
			OnTheaterMode(pDispParams->rgvarg[0].boolVal);
			break;
		case DISPID_ONTOOLBAR:
			OnToolBar(pDispParams->rgvarg[0].boolVal);
			break;
		case DISPID_ONVISIBLE:
			OnVisible(pDispParams->rgvarg[0].boolVal);
			break;
		case DISPID_PRINTTEMPLATEINSTANTIATION:
			PrintTemplateInstantiation(pDispParams->rgvarg[0].pdispVal);
			break;
		case DISPID_PRINTTEMPLATETEARDOWN:
			PrintTemplateTeardown(pDispParams->rgvarg[0].pdispVal);
			break;
		case DISPID_PRIVACYIMPACTEDSTATECHANGE:
			PrivacyImpactedStateChange(pDispParams->rgvarg[0].boolVal);
			break;
		case DISPID_PROGRESSCHANGE:
			ProgressChange(pDispParams->rgvarg[1].lVal, pDispParams->rgvarg[0].lVal);
			break;
		case DISPID_PROPERTYCHANGE:
			PropertyChange(pDispParams->rgvarg[0].bstrVal);
			break;
		case DISPID_SETSECURELOCKICON:
			SetSecureLockIcon(pDispParams->rgvarg[0].lVal);
			break;
		case DISPID_STATUSTEXTCHANGE:
			StatusTextChange(pDispParams->rgvarg[0].bstrVal);
			break;
		case DISPID_TITLECHANGE:
			TitleChange(pDispParams->rgvarg[0].bstrVal);
			break;
		case DISPID_WINDOWCLOSING:
			WindowClosing(pDispParams->rgvarg[1].boolVal, pDispParams->rgvarg[0].pboolVal);
			break;
		case DISPID_WINDOWSETHEIGHT:
			WindowSetHeight(pDispParams->rgvarg[0].lVal);
			break;
		case DISPID_WINDOWSETLEFT:
			WindowSetLeft(pDispParams->rgvarg[0].lVal);
			break;
		case DISPID_WINDOWSETRESIZABLE:
			WindowSetResizable(pDispParams->rgvarg[0].boolVal);
			break;
		case DISPID_WINDOWSETTOP:
			WindowSetTop(pDispParams->rgvarg[0].lVal);
			break;
		case DISPID_WINDOWSETWIDTH:
			WindowSetWidth(pDispParams->rgvarg[0].lVal);
			break;
		default:
			return DISP_E_MEMBERNOTFOUND;
	} //end switch

	return S_OK;
}

//DWebBrowserEvents2
void CWBEvent::DocumentComplete(IDispatch *pDisp,VARIANT *URL){}
void CWBEvent::BeforeNavigate2( IDispatch *pDisp, VARIANT *&url, VARIANT *&Flags, VARIANT *&TargetFrameName, VARIANT *&PostData, VARIANT *&Headers, VARIANT_BOOL *&Cancel){}
void CWBEvent::ClientToHostWindow( long *&CX, long *&CY){}
void CWBEvent::CommandStateChange( long Command, VARIANT_BOOL Enable){}
void CWBEvent::DownloadBegin(VOID){}
void CWBEvent::DownloadComplete(VOID){}
void CWBEvent::FileDownload( VARIANT_BOOL *&ActiveDocument, VARIANT_BOOL *&Cancel){}
void CWBEvent::NavigateComplete2( IDispatch *pDisp, VARIANT *URL){}
void CWBEvent::NavigateError( IDispatch *pDisp, VARIANT *URL, VARIANT *TargetFrameName, VARIANT *StatusCode, VARIANT_BOOL *&Cancel){}
void CWBEvent::NewWindow2( IDispatch **&ppDisp, VARIANT_BOOL *&Cancel){}
void CWBEvent::NewWindow3( IDispatch **&ppDisp, VARIANT_BOOL *&Cancel, DWORD dwFlags, BSTR bstrUrlContext, BSTR bstrUrl){}
void CWBEvent::OnFullScreen( VARIANT_BOOL FullScreen){}
void CWBEvent::OnMenuBar( VARIANT_BOOL MenuBar){}
void CWBEvent::OnQuit(VOID){}
void CWBEvent::OnStatusBar( VARIANT_BOOL StatusBar){}
void CWBEvent::OnTheaterMode( VARIANT_BOOL TheaterMode){}
void CWBEvent::OnToolBar( VARIANT_BOOL ToolBar){}
void CWBEvent::OnVisible( VARIANT_BOOL Visible){}
void CWBEvent::PrintTemplateInstantiation( IDispatch *pDisp){}
void CWBEvent::PrintTemplateTeardown( IDispatch *pDisp){}
void CWBEvent::PrivacyImpactedStateChange(VARIANT_BOOL PrivacyImpacted){}
void CWBEvent::ProgressChange( long Progress, long ProgressMax){}
void CWBEvent::PropertyChange( BSTR szProperty){}
void CWBEvent::SetSecureLockIcon( LONG SecureLockIcon){}
void CWBEvent::StatusTextChange( BSTR Text){}
void CWBEvent::TitleChange( BSTR Text){}
void CWBEvent::WindowClosing( VARIANT_BOOL IsChildWindow, VARIANT_BOOL *&Cancel){}
void CWBEvent::WindowSetHeight( long Height){}
void CWBEvent::WindowSetLeft( long Left){}
void CWBEvent::WindowSetResizable( VARIANT_BOOL Resizable){}
void CWBEvent::WindowSetTop( long Top){}
void CWBEvent::WindowSetWidth( long Width){}

// IUnknown 
STDMETHODIMP CNullStorage::QueryInterface(REFIID riid,void ** ppvObject)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP_(ULONG) CNullStorage::AddRef(void)
{
  return 1;
}

STDMETHODIMP_(ULONG) CNullStorage::Release(void)
{
  return 1;
}


// IStorage
STDMETHODIMP CNullStorage::CreateStream(const WCHAR * pwcsName,DWORD grfMode,DWORD reserved1,DWORD reserved2,IStream ** ppstm)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::OpenStream(const WCHAR * pwcsName,void * reserved1,DWORD grfMode,DWORD reserved2,IStream ** ppstm)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::CreateStorage(const WCHAR * pwcsName,DWORD grfMode,DWORD reserved1,DWORD reserved2,IStorage ** ppstg)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::OpenStorage(const WCHAR * pwcsName,IStorage * pstgPriority,DWORD grfMode,SNB snbExclude,DWORD reserved,IStorage ** ppstg)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::CopyTo(DWORD ciidExclude,IID const * rgiidExclude,SNB snbExclude,IStorage * pstgDest)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::MoveElementTo(const OLECHAR * pwcsName,IStorage * pstgDest,const OLECHAR* pwcsNewName,DWORD grfFlags)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::Commit(DWORD grfCommitFlags)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::Revert(void)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::EnumElements(DWORD reserved1,void * reserved2,DWORD reserved3,IEnumSTATSTG ** ppenum)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::DestroyElement(const OLECHAR * pwcsName)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::RenameElement(const WCHAR * pwcsOldName,const WCHAR * pwcsNewName)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::SetElementTimes(const WCHAR * pwcsName,FILETIME const * pctime,FILETIME const * patime,FILETIME const * pmtime)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::SetClass(REFCLSID clsid)
{
  return S_OK;
}

STDMETHODIMP CNullStorage::SetStateBits(DWORD grfStateBits,DWORD grfMask)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CNullStorage::Stat(STATSTG * pstatstg,DWORD grfStatFlag)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CMySite::QueryInterface(REFIID riid,void ** ppvObject)
{
  if(riid == IID_IUnknown || riid == IID_IOleClientSite)
    *ppvObject = (IOleClientSite*)this;
  else if(riid == IID_IOleInPlaceSite) // || riid == IID_IOleInPlaceSiteEx || riid == IID_IOleInPlaceSiteWindowless)
    *ppvObject = (IOleInPlaceSite*)this;
	else if (riid==IID_IDocHostUIHandler)
		*ppvObject=(IDocHostUIHandler*)this;
	else if (riid==IID_IDocHostShowUI)
		*ppvObject=(IDocHostShowUI*)this;
  else
  {
    *ppvObject = NULL;
    return E_NOINTERFACE;
  }

  return S_OK;
}

STDMETHODIMP_(ULONG) CMySite::AddRef(void)
{
  return 1;
}

STDMETHODIMP_(ULONG) CMySite::Release(void)
{
  return 1;
}

// IOleClientSite

STDMETHODIMP CMySite::SaveObject()
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CMySite::GetMoniker(DWORD dwAssign,DWORD dwWhichMoniker,IMoniker ** ppmk)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CMySite::GetContainer(LPOLECONTAINER FAR* ppContainer)
{
  //简单对象不支持容器。
  *ppContainer = NULL;
  return E_NOINTERFACE;
}

STDMETHODIMP CMySite::ShowObject()
{
  return S_OK;//NOERROR;
}

STDMETHODIMP CMySite::OnShowWindow(BOOL fShow)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CMySite::RequestNewObjectLayout()
{
  NOTIMPLEMENTED;
}

// IOleWindow

STDMETHODIMP CMySite::GetWindow(HWND FAR* lphwnd)
{
	*lphwnd = host->hwnd;
  return S_OK;
}

STDMETHODIMP CMySite::ContextSensitiveHelp(BOOL fEnterMode)
{
  NOTIMPLEMENTED;
}

// IOleInPlaceSite
STDMETHODIMP CMySite::CanInPlaceActivate()
{
  return S_OK;
}

STDMETHODIMP CMySite::OnInPlaceActivate()
{
  return S_OK;
}

STDMETHODIMP CMySite::OnUIActivate()
{
  return S_OK;
}

STDMETHODIMP CMySite::GetWindowContext(LPOLEINPLACEFRAME FAR* ppFrame,LPOLEINPLACEUIWINDOW FAR* ppDoc,LPRECT prcPosRect,LPRECT prcClipRect,LPOLEINPLACEFRAMEINFO lpFrameInfo)
{
  *ppFrame = &host->frame;
  *ppDoc = NULL;
  GetClientRect(host->hwnd,prcPosRect);
  GetClientRect(host->hwnd,prcClipRect);

  lpFrameInfo->fMDIApp = FALSE;
	lpFrameInfo->hwndFrame = host->hwnd;
  lpFrameInfo->haccel = NULL;
  lpFrameInfo->cAccelEntries = 0;

  return S_OK;
}

STDMETHODIMP CMySite::Scroll(SIZE scrollExtent)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CMySite::OnUIDeactivate(BOOL fUndoable)
{
  return S_OK;
}

STDMETHODIMP CMySite::OnInPlaceDeactivate()
{
  return S_OK;
}

STDMETHODIMP CMySite::DiscardUndoState()
{
   return S_OK;	//NOTIMPLEMENTED
}

STDMETHODIMP CMySite::DeactivateAndUndo()
{
   return S_OK;	//NOTIMPLEMENTED
}

STDMETHODIMP CMySite::OnPosRectChange(LPCRECT lprcPosRect)
{
  IOleObject			*browserObject;
	IOleInPlaceObject	*inplace;

	browserObject = this->host->mpWebObject;
	HRESULT hr=browserObject->QueryInterface(IID_IOleInPlaceObject, (void**)&inplace);
	if (SUCCEEDED(hr) && inplace)
	{
		inplace->SetObjectRects(lprcPosRect, lprcPosRect);
	}
	return S_OK;
}

//IDocHostUIHandler
STDMETHODIMP CMySite::ShowContextMenu( DWORD dwID, POINT __RPC_FAR *ppt, IUnknown __RPC_FAR *pcmdTarget, IDispatch __RPC_FAR *pdispReserved){
	if (dwID==CONTEXT_MENU_TEXTSELECT) return S_FALSE;
	else return S_OK;

	return S_OK;
}
STDMETHODIMP CMySite::GetHostInfo( DOCHOSTUIINFO __RPC_FAR *pInfo){
	pInfo->dwFlags = DOCHOSTUIFLAG_NO3DBORDER|DOCHOSTUIFLAG_OPENNEWWIN;
	pInfo->dwDoubleClick = DOCHOSTUIDBLCLK_DEFAULT;
	return S_OK;
}
STDMETHODIMP CMySite::ShowUI(DWORD dwID, IOleInPlaceActiveObject __RPC_FAR *pActiveObject, IOleCommandTarget __RPC_FAR *pCommandTarget, IOleInPlaceFrame __RPC_FAR *pFrame, IOleInPlaceUIWindow __RPC_FAR *pDoc){
	return S_FALSE;
}
STDMETHODIMP CMySite::HideUI(void){
	return S_OK;
}
STDMETHODIMP CMySite::UpdateUI(void){
	return S_OK;
}
STDMETHODIMP CMySite::EnableModeless( BOOL fEnable){
	return S_OK;
}
STDMETHODIMP CMySite::OnDocWindowActivate( BOOL fEnable){
	return S_OK;
}
STDMETHODIMP CMySite::OnFrameWindowActivate( BOOL fEnable){
	return S_OK;
}
STDMETHODIMP CMySite::ResizeBorder( LPCRECT prcBorder, IOleInPlaceUIWindow __RPC_FAR *pUIWindow, BOOL fRameWindow){
	return S_OK;
}
STDMETHODIMP CMySite::TranslateAccelerator( LPMSG lpMsg, const GUID __RPC_FAR *pguidCmdGroup, DWORD nCmdID){
	return S_FALSE;
}
STDMETHODIMP CMySite::GetOptionKeyPath( LPOLESTR __RPC_FAR *pchKey, DWORD dw){
	return S_FALSE;
}
STDMETHODIMP CMySite::GetDropTarget( IDropTarget __RPC_FAR *pDropTarget, IDropTarget __RPC_FAR *__RPC_FAR *ppDropTarget){
	return S_FALSE;
}
STDMETHODIMP CMySite::GetExternal( IDispatch __RPC_FAR *__RPC_FAR *ppDispatch){
	ppDispatch = NULL;
	return S_OK;
}
STDMETHODIMP CMySite::TranslateUrl( DWORD dwTranslate, OLECHAR __RPC_FAR *pchURLIn, OLECHAR __RPC_FAR *__RPC_FAR *ppchURLOut){
	return S_FALSE;
}
STDMETHODIMP CMySite::FilterDataObject( IDataObject __RPC_FAR *pDO, IDataObject __RPC_FAR *__RPC_FAR *ppDORet){
	//ppDORet=NULL;
	return S_FALSE;
}

//IDocHostShowUI
STDMETHODIMP CMySite::ShowMessage(HWND hwnd, LPOLESTR lpstrText,LPOLESTR lpstrCaption,DWORD dwType,LPOLESTR lpstrHelpFile, DWORD dwHelpContext,LRESULT *plResult){
	//USES_CONVERSION;
	//TCHAR pBuffer[64];
	//
	//#define IDS_MESSAGE_BOX_TITLE		   2213
	//HINSTANCE hinstSHDOCLC = LoadLibrary(TEXT("SHDOCLC.DLL"));
	//if (hinstSHDOCLC == NULL){return S_FALSE;}
	//LoadString(hinstSHDOCLC, IDS_MESSAGE_BOX_TITLE, pBuffer, 64);
	//if (_tcscmp(OLE2T(lpstrCaption), pBuffer) == 0) lpstrCaption = L"小助手";

	*plResult = MessageBox(hwnd,OLE2T(lpstrText), L"小助手", dwType);

	//FreeLibrary(hinstSHDOCLC);

	return S_OK;
}
STDMETHODIMP CMySite::ShowHelp(HWND hwnd,LPOLESTR pszHelpFile,UINT uCommand,DWORD dwData,POINT ptMouse,IDispatch *pDispatchObjectHit){
	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////
//
// CMyFrame
//

// IUnknown
STDMETHODIMP CMyFrame::QueryInterface(REFIID riid,void ** ppvObject)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP_(ULONG) CMyFrame::AddRef(void)
{
  return 1;
}

STDMETHODIMP_(ULONG) CMyFrame::Release(void)
{
  return 1;
}

// IOleWindow
STDMETHODIMP CMyFrame::GetWindow(HWND FAR* lphwnd)
{
  *lphwnd = this->host->hwnd;
  return S_OK;
//  NOTIMPLEMENTED;
}

STDMETHODIMP CMyFrame::ContextSensitiveHelp(BOOL fEnterMode)
{
  NOTIMPLEMENTED;
}

// IOleInPlaceUIWindow
STDMETHODIMP CMyFrame::GetBorder(LPRECT lprectBorder)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CMyFrame::RequestBorderSpace(LPCBORDERWIDTHS pborderwidths)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CMyFrame::SetBorderSpace(LPCBORDERWIDTHS pborderwidths)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CMyFrame::SetActiveObject(IOleInPlaceActiveObject *pActiveObject,LPCOLESTR pszObjName)
{
  return S_OK;
}

// IOleInPlaceFrame
STDMETHODIMP CMyFrame::InsertMenus(HMENU hmenuShared,LPOLEMENUGROUPWIDTHS lpMenuWidths)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CMyFrame::SetMenu(HMENU hmenuShared,HOLEMENU holemenu,HWND hwndActiveObject)
{
  return S_OK;
}

STDMETHODIMP CMyFrame::RemoveMenus(HMENU hmenuShared)
{
  NOTIMPLEMENTED;
}

STDMETHODIMP CMyFrame::SetStatusText(LPCOLESTR pszStatusText)
{
  return S_OK;
}

STDMETHODIMP CMyFrame::EnableModeless(BOOL fEnable)
{
  return S_OK;
}

STDMETHODIMP CMyFrame::TranslateAccelerator(LPMSG lpmsg,WORD wID)
{
  return S_OK;
}

//WebHostBase
WebHostBase::WebHostBase()
{
	this->mpWebObject=NULL;
	m_hParentWnd=NULL;
	hwnd=NULL;
	mcRef=1;
  site.host = this;
  frame.host = this;
	m_dwCookie=0;
}

WebHostBase::~WebHostBase()
{
}

HWND WebHostBase::operator =(WebHostBase* rhs)
{
  return hwnd;
}

ULONG WebHostBase::AddRef()
{
  return mcRef++;
}

ULONG WebHostBase::Release()
{
  if(--mcRef) return mcRef;
  delete this;
  return 0;
}
void WebHostBase::Resize(){
	if (!mpWebObject) return;

	RECT rect;
	GetClientRect(hwnd,&rect);
	
  IWebBrowser2* iBrowser;
  mpWebObject->QueryInterface(IID_IWebBrowser2,(void**)&iBrowser);

	iBrowser->put_Left(0);
	iBrowser->put_Top(0);
	iBrowser->put_Width(rect.right);
	iBrowser->put_Height(rect.bottom);

  iBrowser->Release();
}

void WebHostBase::CreateEmbeddedWebControl(TCHAR* url)
{
	OleInitialize(NULL);
  
	OleCreate(CLSID_WebBrowser,IID_IOleObject,OLERENDER_DRAW,0,&site,&storage,(void**)&mpWebObject);

  mpWebObject->SetHostNames(L"tsim",NULL);
	mpWebObject->SetClientSite(&site);

  OleSetContainedObject(mpWebObject,TRUE);

	RECT rect;
	GetClientRect(hwnd,&rect);
	mpWebObject->DoVerb(OLEIVERB_SHOW,NULL,&site,-1,hwnd,&rect);

  IWebBrowser2* iBrowser;
  mpWebObject->QueryInterface(IID_IWebBrowser2,(void**)&iBrowser);

  VARIANT vURL;
  vURL.vt = VT_BSTR;
	vURL.bstrVal = SysAllocString(url);
  VARIANT ve1, ve2, ve3, ve4;
  ve1.vt = VT_EMPTY;
  ve2.vt = VT_EMPTY;
  ve3.vt = VT_EMPTY;
  ve4.vt = VT_EMPTY;

	iBrowser->put_Left(0);
	iBrowser->put_Top(0);
	iBrowser->put_Width(rect.right);
	iBrowser->put_Height(rect.bottom);

  iBrowser->Navigate2(&vURL, &ve1, &ve2, &ve3, &ve4);

	//IOleCommandTarget*	m_pCmdTarg;
	//iBrowser->QueryInterface(IID_IOleCommandTarget, (void**)&m_pCmdTarg);

	VariantClear(&vURL);

  iBrowser->Release();
}

void WebHostBase::EventAdivse(CWBEvent* evt){
	if (!mpWebObject || !evt) return;
	IWebBrowser2* iBrowser;
  HRESULT hr=mpWebObject->QueryInterface(IID_IWebBrowser2,(void**)&iBrowser);
	if (FAILED(hr)) return;
	m_dwCookie=0;
	hr = AtlAdvise(iBrowser, evt, DIID_DWebBrowserEvents2, &m_dwCookie);
	if (FAILED(hr)) MessageBox(0,L"无法设置事件！",L"错误",MB_OK|MB_ICONERROR);
	iBrowser->Release();
}

void WebHostBase::EventUnadivse(){
	if (!mpWebObject) return;
	IWebBrowser2* iBrowser;
  HRESULT hr=mpWebObject->QueryInterface(IID_IWebBrowser2,(void**)&iBrowser);
	if (FAILED(hr)) return;
	hr = AtlUnadvise(iBrowser,DIID_DWebBrowserEvents2,m_dwCookie);
	iBrowser->Release();
}

HRESULT WebHostBase::CallJavaScript(BSTR jsFunc){
	if (!mpWebObject) return S_OK;
	IWebBrowser2* iBrowser=NULL;
  HRESULT hr=mpWebObject->QueryInterface(IID_IWebBrowser2,(void**)&iBrowser);
	if (FAILED(hr)) return S_OK;
	IHTMLDocument2	*htmlDoc2=NULL;
	hr=iBrowser->get_Document((IDispatch**)&htmlDoc2);
	if (FAILED(hr)) goto end;
	IHTMLWindow2* win=NULL;
	hr=htmlDoc2->get_parentWindow(&win);
	if (FAILED(hr)) goto end;
	BSTR bstrLanguage=SysAllocString(L"javascript");
	VARIANT innerRet;
	size_t l=_tcslen(jsFunc)+MAX_PATH;
	TCHAR* szJS=new TCHAR[l];
	ZeroMemory(szJS,sizeof(TCHAR)*l);
	_stprintf_s(szJS, l, L"var _f_=null;try{\r\n _f_=function(){%s;};_f_();\r\n}catch(e){alert('出现运行时错误，请检查您的系统的IE浏览器及其设置是否正常。\\r\\n请尝试完全退出系统并重新登录，如果问题无法解决请联系提供商。\\r\\n错误消息:\\r\\n'+e+'。\\r\\n错误代码:\\r\\n'+_f_);}", jsFunc);
	BSTR bstrJS=SysAllocString(szJS);
	hr=win->execScript(szJS,bstrLanguage,&innerRet);
	SysFreeString(bstrLanguage);
	SysFreeString(bstrJS);
	delete[] szJS;
end:
	if (win) win->Release();
	if (htmlDoc2) htmlDoc2->Release();
	if (iBrowser) iBrowser->Release();
	return hr;
}

void WebHostBase::NavigateTo(TCHAR* url){
	if (url==NULL || _tcslen(url)<=0 || !mpWebObject) return;
	IWebBrowser2* iBrowser=NULL;
  HRESULT hr=mpWebObject->QueryInterface(IID_IWebBrowser2,(void**)&iBrowser);
	if (FAILED(hr) || !iBrowser) return ;

	VARIANT vURL;
  vURL.vt = VT_BSTR;
	vURL.bstrVal = SysAllocString(url);
  VARIANT ve1, ve2, ve3, ve4;
  ve1.vt = VT_EMPTY;
  ve2.vt = VT_EMPTY;
  ve3.vt = VT_EMPTY;
  ve4.vt = VT_EMPTY;

  iBrowser->Navigate2(&vURL, &ve1, &ve2, &ve3, &ve4);

	VariantClear(&vURL);
  iBrowser->Release();
}

void WebHostBase::UnCreateEmbeddedWebControl(void){
	if (!mpWebObject) return;
	EventUnadivse();
	IWebBrowser2* iBrowser=NULL;
  HRESULT hr=mpWebObject->QueryInterface(IID_IWebBrowser2,(void**)&iBrowser);
	if (SUCCEEDED(hr) && iBrowser) {
		iBrowser->Stop();
		iBrowser->Quit();
		iBrowser->Release();
		iBrowser=NULL;
	}
  mpWebObject->Close(OLECLOSE_NOSAVE);
	mpWebObject->Release();
	mpWebObject=NULL;
	m_hParentWnd=NULL;
	hwnd=NULL;
	OleUninitialize();
}