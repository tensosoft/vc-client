#ifndef _OPTIONS_H_
#define _OPTIONS_H_

//缺省刷新内容的时间间隔，单位秒
#define DEFAULT_REFRESH_INTERVAL 60
#define WIN_SIZE_SECTION_NAME	L"WindowsSize"

//站点信息
typedef struct tagSiteInfo{
	TCHAR Title[MAX_PATH];																				//站点标题
	TCHAR Url[STRLEN_1K];																					//站点url
	TCHAR UrlExt[STRLEN_SMALL];																		//站点url文件的后缀，如.jsp或.aspx
}SiteInfo;

//刷新间隔信息
typedef struct tagRefreshInfo{
	TCHAR Title[MAX_PATH];																				//消息的标题
	TCHAR UNID[STRLEN_SMALL];																			//消息唯一UNID
	int	RefershSeconds;																						//刷新消息间隔时间，默认以为90为基数，加30递增
}RefreshInfo;

class AppOptions{
private:
	AppOptions();																									//私有构造器
	HRESULT Init();																								//从配置文件中装载配置信息并初始化配置对象实例
	AppOptions(AppOptions const&){};
	AppOptions& operator=(AppOptions const&){};
	static AppOptions* opt;																				//唯一实例
	BOOL GetIniFile(TCHAR* filePath,size_t size);									//获取工作目录
public:
	~AppOptions();																								//析构函数

	static SiteInfo* pSiteInfoArr;																//站点信息数组指针
	static DWORD dwSiteCount;																			//本机记录的所有站点个数
	static TCHAR szCurrentSiteUrl[STRLEN_1K];											//当前站点url
	static TCHAR szCurrentSiteUrlExt[STRLEN_SMALL];								//当前站点url文件的扩展名
	static AppOptions* GetInstance();															//获取配置对象唯一实例
	static AppOptions* Reload();																	//重新加载

	HRESULT Save();																								//保存
	HRESULT SaveSubscribedServices(TCHAR* szSubscribedServices);	//保存订阅的服务/通知消息。
	HRESULT SaveMainWinSize(int w,int h);													//保存窗口大小

	BOOL Initialized;																							//是否初始化
	BOOL AutoRun;																									//是否自动运行
	BOOL AutoLogon;																								//是否自动登录

	TCHAR Title[MAX_PATH];																				//产品名称
	TCHAR Belong[MAX_PATH];																				//使用单位
	TCHAR Provider[MAX_PATH];																			//提供商
	TCHAR UserName[MAX_PATH];																			//登录用户名
	TCHAR Password[MAX_PATH];																			//登录用户密码
	TCHAR LocalMessagePath[MAX_PATH];															//保存本地消息的路径,缺省为“我的文档\我的办公助手\消息”
	TCHAR LocalFilePath[MAX_PATH];																//保存本地接收文件的路径,缺省为“我的文档\我的办公助手\文件”

	TCHAR DisplayUserName[MAX_PATH];															//保存用户对话时显示的用户名，如果不提供则使用用户通用名。
	TCHAR DisplayAvatar[MAX_PATH];																//保存用户的头像，如果不提供则使用缺省头像，保存格式为内部头像资源的ID或者磁盘上的图片文件名。
	int DefaultLoginPresence;																			//缺省登录后的在线状态，保存数字
	TCHAR SendMsgKey[STRLEN_SMALL];																//发送消息的快捷键，支持CTRL+ENTER（默认）或ENTER
	TCHAR SubscribedServices[STRLEN_4K];													//订阅的服务/通知消息。
	TCHAR BaseUrl[STRLEN_1K];																			//服务器URL基址
	int MainWinWidth;																							//窗口宽度
	int MainWinHeight;																						//窗口高度
};

HRESULT ShowOptionsDialog(HWND hWndParent, BSTR bstrTitle);			//显示选项对话框
void ErrorMsg(LPTSTR lpszErrDesc,LPTSTR lpszTitle,DWORD dw);		//输出错误信息
void RegisterProtocol();																				//注册URL处理程序
void RegisterAutoRun(BOOL isAutoRun);														//设置自动运行的注册表项

#endif //_OPTIONS_H_