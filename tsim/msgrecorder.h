#ifndef _MSGRECORDER_H_
#define _MSGRECORDER_H_

class MsgRecorder;

#define CACHE_SIZE 4096
//#define XML_HEAD "<?xml version=\"1.0\" encoding=\"gbk\"?>\r\n<result>\r\n"
//#define XML_TAIL "</result>"

class MsgRecorder{
public:
	MsgRecorder(TCHAR* pdir);
	~MsgRecorder();
	BOOL RecordMsg(TCHAR* lid,TCHAR* szMsg,TCHAR* szCN,TCHAR* szDT,TCHAR* szStyle);
	BOOL RecordError(TCHAR* lid,TCHAR* errDesc);
	BOOL RecordInfo(TCHAR* lid,TCHAR* info);
	TCHAR m_pdir[MAX_PATH];
	TCHAR m_fn[MAX_PATH];
	TCHAR *m_oldContent;
private:
	BOOL CheckFileHandle(BOOL alertFlag);
	BOOL Append(TCHAR* result);

	//BOOL m_headAppended;
	BOOL m_hasContent;
	TCHAR m_cached[CACHE_SIZE];
	HANDLE m_file;
};

#endif	//_MSGRECORDER_H_