#include "stdafx.h"
#include "infonotify.h"
#include "options.h"

#define IDT_HIDDEN		0
#define IDT_APPEARING		1
#define IDT_WAITING		2
#define IDT_DISAPPEARING	3

#define TASKBAR_ON_TOP		1
#define TASKBAR_ON_LEFT		2
#define TASKBAR_ON_RIGHT	3
#define TASKBAR_ON_BOTTOM	4

InfoNotify::InfoNotify(void){
	m_hWnd = 0;
	m_hParentWnd=0;
  ZeroMemory(m_szUNID,sizeof(TCHAR)*STRLEN_SMALL);
	m_notifierType=0;

	m_hNormalFont=0;                    
	m_hSelectedFont=0;                  
	m_crNormalTextColor=RGB(133,146,181);         
	m_crSelectedTextColor=RGB(10,36,106);            
	m_hCursor=0;                        
	                                  
	m_hSkinRegion=0;							      
	SetRect(&m_rcText,0,0,0,0);
	m_nSkinWidth=0;							        
	m_nSkinHeight=0;							      
	m_biSkinBackground=0;               
	                                  
	ZeroMemory(m_strCaption,STRLEN_1K*sizeof(TCHAR));

	m_bMouseIsOver=FALSE;                   
	m_nAnimStatus=IDT_HIDDEN;                    
	                                  
	m_dwTimeToShow=0;                   
	m_dwTimeToLive=0;                   
	m_dwTimeToHide=0;                   
	m_dwDelayBetweenShowEvents=0;       
	m_dwDelayBetweenHideEvents=0;       
	m_nStartPosX=0;                     
	m_nStartPosY=0;                     
	m_nCurrentPosX=0;                   
	m_nCurrentPosY=0;                   
	m_nTaskbarPlacement=0;              
	m_nIncrement=2;
	m_tabIndex=-1;
}

InfoNotify::~InfoNotify(void){
}

InfoNotify* InfoNotify::Create(HWND hWndParent)
{
	HWND hwnd=CreateWindowEx(WS_EX_TOPMOST|WS_EX_TOOLWINDOW,IN_CLASS,IN_TITLE,WS_POPUP,0,0,0,0,NULL,NULL,GetModuleHandle(NULL),NULL);
	if (hwnd) {
		InfoNotify * notifier=(InfoNotify *)GetWindowLongPtr(hwnd, GWLP_USERDATA);
		notifier->m_hParentWnd=hWndParent;
		return notifier;
	}
	else return NULL;
}

LRESULT CALLBACK InfoNotify::WindowProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam){
	InfoNotify* me=NULL;
	LRESULT  lr = 0;
	if ( uMsg == WM_CREATE ){
		// 创建本类的实例
		me = new InfoNotify;
		me->m_hWnd = hWnd;
		//me->m_hParentWnd=GetParent(hWnd);
		me->OnCreate(NULL);
		// 保存类实例以供后续使用
		SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)me);
		return lr;
	}
	// 从Windows的用户数据中提取本对象的实例
	me = (InfoNotify *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	POINT p;
	switch( uMsg ){
		case WM_ERASEBKGND :
			return me->OnEraseBkgnd((HDC)wParam);
		case WM_DESTROY:
			me->OnDestroy();
			delete me;
			break;
		case WM_PAINT:
			me->OnPaint();
			break;
		case WM_MOUSEMOVE:
			p.x= GET_X_LPARAM(lParam); 
			p.y = GET_Y_LPARAM(lParam); 
			me->OnMouseMove(wParam,p);
			break;
		case WM_MOUSEHOVER:
			return me->OnMouseHover(wParam,lParam);
		case WM_MOUSELEAVE:
			return me->OnMouseLeave(wParam,lParam);
		case WM_LBUTTONUP:
			p.x= GET_X_LPARAM(lParam); 
			p.y = GET_Y_LPARAM(lParam); 
			me->OnLButtonUp(wParam,p);
			break;
		case WM_SETCURSOR:
			return me->OnSetCursor(me->m_hParentWnd,LOWORD(lParam),HIWORD(lParam));
		case WM_TIMER:
			me->OnTimer((UINT)wParam);
			break;
		default:
			lr = DefWindowProc( hWnd, uMsg, wParam, lParam);
	}
	return lr;
}

int InfoNotify::OnCreate(LPCREATESTRUCT lpCreateStruct){
	m_hCursor = ::LoadCursor(NULL,  MAKEINTRESOURCE(32649));
	return 0;
}

void InfoNotify::OnDestroy(){
}

void InfoNotify::OnMouseMove(UINT nFlags, POINT point){
	TRACKMOUSEEVENT t_MouseEvent;
	t_MouseEvent.cbSize      = sizeof(TRACKMOUSEEVENT);
	t_MouseEvent.dwFlags     = TME_LEAVE | TME_HOVER;
	t_MouseEvent.hwndTrack   = m_hWnd;
	t_MouseEvent.dwHoverTime = 1;

	::_TrackMouseEvent(&t_MouseEvent);
}

void InfoNotify::OnLButtonUp(UINT nFlags, POINT point){
	if(
		(point.x>(this->m_nSkinWidth-25)) 
		&& (point.x<(this->m_nSkinWidth-2))
		&& (point.y>2)
		&& (point.y<25)
	){
		DestroyWindow(m_hWnd);
		return;
	}
	SendMessage(m_hParentWnd,WM_TASKBARNOTIFIERCLICKED,(WPARAM)m_notifierType,(LPARAM)this);	//MAKELPARAM(point.x,point.y)
	DestroyWindow(m_hWnd);
}

LRESULT InfoNotify::OnMouseHover(WPARAM w, LPARAM l){
	if (m_bMouseIsOver==FALSE){
		m_bMouseIsOver=TRUE;
		RedrawWindow();
	}
	return 0;
}

LRESULT InfoNotify::OnMouseLeave(WPARAM w, LPARAM l){
	if (m_bMouseIsOver==TRUE){
		m_bMouseIsOver=FALSE;
		RedrawWindow();
	}
	return 0;
}

BOOL InfoNotify::OnSetCursor(HWND hWndParent, UINT nHitTest, UINT message){
	if (nHitTest == HTCLIENT)	{
		::SetCursor(m_hCursor);
		return TRUE;
	}
	return DefWindowProc(hWndParent, nHitTest, (WPARAM)m_hWnd,message);
}

BOOL InfoNotify::OnEraseBkgnd(HDC hDC){
	HDC memDC;
	HBITMAP oldBitmap=NULL;
	BITMAP bm;

	memDC=CreateCompatibleDC(hDC);
	GetObject(m_biSkinBackground, sizeof(bm), &bm);

	oldBitmap=(HBITMAP)SelectObject(memDC,m_biSkinBackground);

	BitBlt(hDC,0,0,bm.bmWidth,bm.bmHeight,memDC,0,0,SRCCOPY);
	SelectObject(memDC,oldBitmap);
	DeleteDC(memDC);
	return TRUE;
}

void InfoNotify::OnPaint(){
	PAINTSTRUCT m_ps;
	HDC dc;
	dc=BeginPaint(m_hWnd, &m_ps);

	HFONT oldFont=NULL;
	TCHAR szBuffer[STRLEN_1K]={0};	//TCHAR *szBuffer;

	SetBkMode(dc,TRANSPARENT);
	if (m_bMouseIsOver){
		::SetTextColor(dc,m_crSelectedTextColor);
		oldFont=(HFONT)SelectObject(dc,m_hSelectedFont);
	}else{
		::SetTextColor(dc,m_crNormalTextColor);
		oldFont=(HFONT)SelectObject(dc,m_hNormalFont);
	}
	_tcscpy_s(szBuffer,m_strCaption);

	DrawText(dc,szBuffer,-1,&m_rcText,DT_LEFT | DT_VCENTER | DT_WORDBREAK | DT_END_ELLIPSIS);
	
	::SetTextColor(dc,m_crNormalTextColor);
	SelectObject(dc,m_hNormalFont);
	AppOptions *opt=AppOptions::GetInstance();
	TextOut(dc,24,6,opt->Title,_tcslen(opt->Title));

	SelectObject(dc,oldFont);
	EndPaint(m_hWnd,&m_ps);
}

void InfoNotify::OnTimer(UINT nIDEvent){
	switch (nIDEvent)
	{
		case IDT_APPEARING:
			m_nAnimStatus=IDT_APPEARING;
			switch(m_nTaskbarPlacement)
			{
				case TASKBAR_ON_BOTTOM:
					if (m_nCurrentPosY>(m_nStartPosY-m_nSkinHeight)) m_nCurrentPosY-=m_nIncrement;
					else
					{
						KillTimer(m_hWnd,IDT_APPEARING);
						SetTimer(m_hWnd,IDT_WAITING,m_dwTimeToLive,NULL);
						m_nAnimStatus=IDT_WAITING;
					}
					break;
				case TASKBAR_ON_TOP:
					if ((m_nCurrentPosY-m_nStartPosY)<m_nSkinHeight) m_nCurrentPosY+=m_nIncrement;
					else
					{
						KillTimer(m_hWnd,IDT_APPEARING);
						SetTimer(m_hWnd,IDT_WAITING,m_dwTimeToLive,NULL);
						m_nAnimStatus=IDT_WAITING;
					}
					break;
				case TASKBAR_ON_LEFT:
					if ((m_nCurrentPosX-m_nStartPosX)<m_nSkinWidth) m_nCurrentPosX+=m_nIncrement;
					else
					{
						KillTimer(m_hWnd,IDT_APPEARING);
						SetTimer(m_hWnd,IDT_WAITING,m_dwTimeToLive,NULL);
						m_nAnimStatus=IDT_WAITING;
					}
					break;
				case TASKBAR_ON_RIGHT:
					if (m_nCurrentPosX>(m_nStartPosX-m_nSkinWidth)) m_nCurrentPosX-=m_nIncrement;
					else
					{
						KillTimer(m_hWnd,IDT_APPEARING);
						SetTimer(m_hWnd,IDT_WAITING,m_dwTimeToLive,NULL);
						m_nAnimStatus=IDT_WAITING;
					}
					break;
			}
			SetWindowPos(m_hWnd,NULL,m_nCurrentPosX,m_nCurrentPosY,m_nSkinWidth,m_nSkinHeight,SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOACTIVATE);
			//SetWindowPos(m_hWnd,HWND_TOPMOST,m_nCurrentPosX,m_nCurrentPosY,m_nSkinWidth,m_nSkinHeight,SWP_NOOWNERZORDER | SWP_NOACTIVATE);
			break;
		case IDT_WAITING:
			KillTimer(m_hWnd,IDT_WAITING);
			SetTimer(m_hWnd,IDT_DISAPPEARING,m_dwDelayBetweenHideEvents,NULL);
			break;
		case IDT_DISAPPEARING:
			m_nAnimStatus=IDT_DISAPPEARING;
			switch(m_nTaskbarPlacement)
			{
				case TASKBAR_ON_BOTTOM:
					if (m_nCurrentPosY<m_nStartPosY) m_nCurrentPosY+=m_nIncrement;
					else
					{
						KillTimer(m_hWnd,IDT_DISAPPEARING);
						Hide();
					}
					break;
				case TASKBAR_ON_TOP:
					if (m_nCurrentPosY>m_nStartPosY) m_nCurrentPosY-=m_nIncrement;
					else
					{
						KillTimer(m_hWnd,IDT_DISAPPEARING);
						Hide();
					}
					break;
				case TASKBAR_ON_LEFT:
					if (m_nCurrentPosX>m_nStartPosX) m_nCurrentPosX-=m_nIncrement;
					else
					{
						KillTimer(m_hWnd,IDT_DISAPPEARING);
						Hide();
					}
					break;
				case TASKBAR_ON_RIGHT:
					if (m_nCurrentPosX<m_nStartPosX) m_nCurrentPosX+=m_nIncrement;
					else
					{
						KillTimer(m_hWnd,IDT_DISAPPEARING);
						Hide();
					}
					break;
			}
			SetWindowPos(m_hWnd,NULL,m_nCurrentPosX,m_nCurrentPosY,m_nSkinWidth,m_nSkinHeight,SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOACTIVATE);
			//SetWindowPos(m_hWnd,HWND_TOPMOST,m_nCurrentPosX,m_nCurrentPosY,m_nSkinWidth,m_nSkinHeight,SWP_NOOWNERZORDER | SWP_NOACTIVATE);
			//RedrawWindow();
			break;
	}
}

int InfoNotify::RedrawWindow(){
	return ::RedrawWindow(this->m_hWnd,NULL,NULL,RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE);
}

void InfoNotify::SetTextFont(LPCTSTR szFont,int nSize,int nNormalStyle,int nSelectedStyle){
	LOGFONT lf;
	DeleteObject(m_hNormalFont);

	LOGFONT logFont;
	memset(&logFont, 0, sizeof(LOGFONT));
	logFont.lfCharSet = DEFAULT_CHARSET;
	logFont.lfHeight = nSize;
	lstrcpyn(logFont.lfFaceName, szFont, sizeof(logFont.lfFaceName)/sizeof(logFont.lfFaceName[0]));
	HDC hDC = GetDC(NULL);
	POINT pt;
	pt.y = GetDeviceCaps(hDC, LOGPIXELSY) * logFont.lfHeight;
	pt.y /= 720;    // 72 points/inch, 10 decipoints/point
	pt.x = 0;
	DPtoLP(hDC, &pt, 1);
	POINT ptOrg = { 0, 0 };
	DPtoLP(hDC, &ptOrg, 1);
	logFont.lfHeight = -abs(pt.y - ptOrg.y);
	this->m_hNormalFont=CreateFontIndirect(&logFont);
	ReleaseDC(NULL, hDC);
	GetObject(this->m_hNormalFont, sizeof(LOGFONT), &lf);

	//设置未选中的条目字体
	if (nNormalStyle & TN_TEXT_BOLD)
		lf.lfWeight = FW_BOLD;
	else
		lf.lfWeight = FW_NORMAL;
	
	if (nNormalStyle & TN_TEXT_ITALIC)
		lf.lfItalic=TRUE;
	else
		lf.lfItalic=FALSE;
	
	if (nNormalStyle & TN_TEXT_UNDERLINE)
		lf.lfUnderline=TRUE;
	else
		lf.lfUnderline=FALSE;

	DeleteObject(m_hNormalFont);
	m_hNormalFont=CreateFontIndirect(&lf);
	
	//设置选中条目的字体
	if (nSelectedStyle & TN_TEXT_BOLD)
		lf.lfWeight = FW_BOLD;
	else
		lf.lfWeight = FW_NORMAL;
	
	if (nSelectedStyle & TN_TEXT_ITALIC)
		lf.lfItalic=TRUE;
	else
		lf.lfItalic=FALSE;
	
	if (nSelectedStyle & TN_TEXT_UNDERLINE)
		lf.lfUnderline=TRUE;
	else
		lf.lfUnderline=FALSE;

	DeleteObject(this->m_hSelectedFont);
	this->m_hSelectedFont=CreateFontIndirect(&lf);
}

void InfoNotify::SetTextColor(COLORREF crNormalTextColor,COLORREF crSelectedTextColor){
	this->m_crNormalTextColor=crNormalTextColor;
	this->m_crSelectedTextColor=crSelectedTextColor;
	RedrawWindow();
}

void InfoNotify::SetTextRect(RECT rcText){
	m_rcText=rcText;
}

BOOL InfoNotify::SetSkin(UINT nBitmapID,short red,short green,short blue){
	BITMAP bm;HBITMAP
	
	DeleteObject(m_biSkinBackground);

	if ((m_biSkinBackground=LoadBitmap(GetModuleHandle(NULL),MAKEINTRESOURCE(nBitmapID)))==NULL) return FALSE;
	GetObject(m_biSkinBackground, sizeof(bm), &bm);
	m_nSkinWidth=bm.bmWidth;
	m_nSkinHeight=bm.bmHeight;

	SetRect(&m_rcText,0,0,bm.bmWidth,bm.bmHeight);
	if (red!=-1 && green!=-1 && blue!=-1)
	{
		// No need to delete the HRGN,  SetWindowRgn() owns it after being called
		m_hSkinRegion=GenerateRegion((HBITMAP)m_biSkinBackground,(BYTE) red,(BYTE) green,(BYTE) blue);
		SetWindowRgn(m_hWnd,m_hSkinRegion, true);
	}

	return TRUE;
}

HRGN InfoNotify::GenerateRegion(HBITMAP hBitmap, BYTE red, BYTE green, BYTE blue){
	WORD wBmpWidth,wBmpHeight;
	HRGN hRgn, hTmpRgn;

	// 24bit pixels from the bitmap
	BYTE *pPixels = Get24BitPixels(hBitmap, &wBmpWidth, &wBmpHeight);
	if (!pPixels) return NULL;

	// create our working region
	hRgn = CreateRectRgn(0,0,wBmpWidth,wBmpHeight);
	if (!hRgn) { delete pPixels; return NULL; }

	DWORD p=0;
	for (WORD y=0; y<wBmpHeight; y++)
	{
		for (WORD x=0; x<wBmpWidth; x++)
		{
			BYTE jRed   = pPixels[p+2];
			BYTE jGreen = pPixels[p+1];
			BYTE jBlue  = pPixels[p+0];

			if (jRed==red && jGreen==green && jBlue==blue)
			{
				// remove transparent color from region
				hTmpRgn = CreateRectRgn(x,y,x+1,y+1);
				CombineRgn(hRgn, hRgn, hTmpRgn, RGN_XOR);
				DeleteObject(hTmpRgn);
			}

			// next pixel
			p+=3;
		}
	}

	// release pixels
	delete pPixels;

	// return the region
	return hRgn;
}

BYTE* InfoNotify::Get24BitPixels(HBITMAP pBitmap, WORD *pwWidth, WORD *pwHeight){
	BITMAP bmpBmp;
	LPBITMAPINFO pbmiInfo;
	BITMAPINFO bmiInfo;
	WORD wBmpWidth, wBmpHeight;

	GetObject(pBitmap, sizeof(bmpBmp),&bmpBmp);
	pbmiInfo   = (LPBITMAPINFO)&bmpBmp;

	wBmpWidth  = (WORD)pbmiInfo->bmiHeader.biWidth;
	wBmpWidth -= (wBmpWidth%4);
	wBmpHeight = (WORD)pbmiInfo->bmiHeader.biHeight;

	*pwWidth  = wBmpWidth;
	*pwHeight = wBmpHeight;
	
	BYTE *pPixels = new BYTE[wBmpWidth*wBmpHeight*3];
	if (!pPixels) return NULL;

	HDC hDC =::GetWindowDC(NULL);

	bmiInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmiInfo.bmiHeader.biWidth = wBmpWidth;
	bmiInfo.bmiHeader.biHeight = -wBmpHeight;
	bmiInfo.bmiHeader.biPlanes = 1;
	bmiInfo.bmiHeader.biBitCount = 24;
	bmiInfo.bmiHeader.biCompression = BI_RGB;
	bmiInfo.bmiHeader.biSizeImage = wBmpWidth*wBmpHeight*3;
	bmiInfo.bmiHeader.biXPelsPerMeter = 0;
	bmiInfo.bmiHeader.biYPelsPerMeter = 0;
	bmiInfo.bmiHeader.biClrUsed = 0;
	bmiInfo.bmiHeader.biClrImportant = 0;

	// get pixels from the original bitmap converted to 24bits
	int iRes = GetDIBits(hDC,pBitmap,0,wBmpHeight,(LPVOID)pPixels,&bmiInfo,DIB_RGB_COLORS);

	// release the device context
	::ReleaseDC(NULL,hDC);

	// if failed, cancel the operation.
	if (!iRes){
		delete pPixels;
		return NULL;
	};

	// return the pixel array
	return pPixels;
}

void InfoNotify::Show(LPCTSTR szCaption,DWORD dwTimeToShow,DWORD dwTimeToLive,DWORD dwTimeToHide,int nIncrement){
	unsigned int nDesktopHeight;
	unsigned int nDesktopWidth;
	unsigned int nScreenWidth;
	unsigned int nScreenHeight;
	RECT rcDesktop;

	_tcscpy_s(m_strCaption,szCaption);

	m_dwTimeToShow=dwTimeToShow;
	m_dwTimeToLive=dwTimeToLive;
	m_dwTimeToHide=dwTimeToHide;

	::SystemParametersInfo(SPI_GETWORKAREA,0,&rcDesktop,0);
	nDesktopWidth=rcDesktop.right-rcDesktop.left;
	nDesktopHeight=rcDesktop.bottom-rcDesktop.top;
	nScreenWidth=::GetSystemMetrics(SM_CXSCREEN);
	nScreenHeight=::GetSystemMetrics(SM_CYSCREEN);

	BOOL bTaskbarOnRight=nDesktopWidth<nScreenWidth && rcDesktop.left==0;
	BOOL bTaskbarOnLeft=nDesktopWidth<nScreenWidth && rcDesktop.left!=0;
	BOOL bTaskBarOnTop=nDesktopHeight<nScreenHeight && rcDesktop.top!=0;
	BOOL bTaskbarOnBottom=nDesktopHeight<nScreenHeight && rcDesktop.top==0;
	
	DWORD dwStyle = GetWindowLong(m_hWnd, GWL_STYLE);
  dwStyle &= ~(WS_CAPTION|WS_SIZEBOX);
  SetWindowLong(m_hWnd, GWL_STYLE, dwStyle);

	switch (m_nAnimStatus)
	{
		case IDT_HIDDEN:
			ShowWindow(m_hWnd,SW_SHOWNA);	//SW_SHOWNOACTIVATE
			if (bTaskbarOnRight)
			{
				m_dwDelayBetweenShowEvents=m_dwTimeToShow/(m_nSkinWidth/m_nIncrement);
				m_dwDelayBetweenHideEvents=m_dwTimeToHide/(m_nSkinWidth/m_nIncrement);
				m_nStartPosX=rcDesktop.right;
				m_nStartPosY=rcDesktop.bottom-m_nSkinHeight;
				m_nTaskbarPlacement=TASKBAR_ON_RIGHT;
			}
			else if (bTaskbarOnLeft)
			{
				m_dwDelayBetweenShowEvents=m_dwTimeToShow/(m_nSkinWidth/m_nIncrement);
				m_dwDelayBetweenHideEvents=m_dwTimeToHide/(m_nSkinWidth/m_nIncrement);
				m_nStartPosX=rcDesktop.left-m_nSkinWidth;
				m_nStartPosY=rcDesktop.bottom-m_nSkinHeight;
				m_nTaskbarPlacement=TASKBAR_ON_LEFT;
			}
			else if (bTaskBarOnTop)
			{
				m_dwDelayBetweenShowEvents=m_dwTimeToShow/(m_nSkinHeight/m_nIncrement);
				m_dwDelayBetweenHideEvents=m_dwTimeToHide/(m_nSkinHeight/m_nIncrement);
				m_nStartPosX=rcDesktop.right-m_nSkinWidth;
				m_nStartPosY=rcDesktop.top-m_nSkinHeight;
				m_nTaskbarPlacement=TASKBAR_ON_TOP;
			}
			else //if (bTaskbarOnBottom)
			{
				// Taskbar is on the bottom or Invisible
				m_dwDelayBetweenShowEvents=m_dwTimeToShow/(m_nSkinHeight/m_nIncrement);
				m_dwDelayBetweenHideEvents=m_dwTimeToHide/(m_nSkinHeight/m_nIncrement);
				m_nStartPosX=rcDesktop.right-m_nSkinWidth;
				m_nStartPosY=rcDesktop.bottom;
				m_nTaskbarPlacement=TASKBAR_ON_BOTTOM;
			}

			m_nCurrentPosX=m_nStartPosX;
			m_nCurrentPosY=m_nStartPosY;
	
			SetTimer(m_hWnd,IDT_APPEARING,m_dwDelayBetweenShowEvents,NULL);
			break;
		case IDT_WAITING:
			RedrawWindow();
			KillTimer(m_hWnd,IDT_WAITING);
			SetTimer(m_hWnd,IDT_WAITING,m_dwTimeToLive,NULL);
			break;
		case IDT_APPEARING:
			RedrawWindow();
			break;
		case IDT_DISAPPEARING:
			KillTimer(m_hWnd,IDT_DISAPPEARING);
			SetTimer(m_hWnd,IDT_WAITING,m_dwTimeToLive,NULL);
			if (bTaskbarOnRight)
				m_nCurrentPosX=rcDesktop.right-m_nSkinWidth;
			else if (bTaskbarOnLeft)
				m_nCurrentPosX=rcDesktop.left;
			else if (bTaskBarOnTop)
				m_nCurrentPosY=rcDesktop.top;
			else //if (bTaskbarOnBottom)
				m_nCurrentPosY=rcDesktop.bottom-m_nSkinHeight;
			
			SetWindowPos(m_hWnd,NULL,m_nCurrentPosX,m_nCurrentPosY,m_nSkinWidth,m_nSkinHeight,SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOACTIVATE);
			//SetWindowPos(m_hWnd,HWND_TOPMOST,m_nCurrentPosX,m_nCurrentPosY,m_nSkinWidth,m_nSkinHeight,SWP_NOOWNERZORDER | SWP_NOACTIVATE);
			RedrawWindow();
			break;
	}
}

void InfoNotify::Hide(){
	switch (m_nAnimStatus)
	{
		case IDT_APPEARING:
			KillTimer(m_hWnd,IDT_APPEARING);
			break;
		case IDT_WAITING:
			KillTimer(m_hWnd,IDT_WAITING);
			break;
		case IDT_DISAPPEARING:
			KillTimer(m_hWnd,IDT_DISAPPEARING);
			break;
	}
	MoveWindow(m_hWnd,0,0,0,0,TRUE);
	ShowWindow(m_hWnd,SW_HIDE);
	m_nAnimStatus=IDT_HIDDEN;
	DestroyWindow(m_hWnd);
}

HRESULT InfoNotify::RegisterClass(){
	//注册窗口类
	WNDCLASSEX wc;
	ATOM atom;

	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	wc.cbSize				 = sizeof(wc);
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = (WNDPROC)InfoNotify::WindowProc;
	wc.hInstance     = GetModuleHandle(NULL);
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
	wc.lpszClassName = IN_CLASS;
	atom = ::RegisterClassEx( &wc );
	if (!atom ) return E_FAIL;
	return S_OK;
}