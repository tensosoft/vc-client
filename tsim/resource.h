//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by tsim.rc
//
#define IDR_MENU_APP                    103
#define IDI_SMALL                       108
#define IDI_TSIM                        109
#define IDD_ABOUT                       112
#define IDD_OPTIONS                     113
#define IDR_ACCELERATORMSG              119
#define IDB_WHITEMASK                   121
#define IDB_FONT                        122
#define IDB_CLOSE                       123
#define IDB_SMILE                       124
#define IDR_MENU_IM                     125
#define IDB_FILETRANS                   130
#define IDR_GIF1                        130
#define IDC_ABOUTTITLE                  1015
#define IDC_ABOUTDESC                   1016
#define IDC_GROUP                       1017
#define IDC_GPRAUTO                     1018
#define IDC_AUTORUN                     1020
#define IDC_GRPUSER                     1021
#define IDC_USERNAME                    1022
#define IDC_PASSWORD                    1023
#define IDC_EDITUSERNAME                1024
#define IDC_EDITPASSWORD                1025
#define IDC_GRPLOCAL                    1026
#define IDC_LOCALMSG                    1027
#define IDC_LOCALMSGPATH                1028
#define IDC_BTNBROWSEMSG                1029
#define IDC_LOCALFILE                   1030
#define IDC_LOCALFILEPATH               1031
#define IDC_BTNBROWSEFILE               1032
#define IDC_GRPURL                      1033
#define IDC_IMURL                       1034
#define IDC_IMBASEURL                   1035
#define IDC_GPRINFO                     1037
#define IDC_AUTOLOGIN                   1043
#define IDC_ABOUTICON                   1118
#define IDC_STATUSBAR                   2000
#define IDC_BUDDYLIST                   2001
#define IDC_IM_DISPLAY                  2002
#define IDC_IM_EDIT                     2003
#define IDC_IM_SEND                     2004
#define IDC_MIC_VOL                     2005
#define IDC_MIC_MUTE                    2006
#define IDC_SPK_VOL                     2007
#define IDC_SPK_MUTE                    2008
#define IDC_TREEVIEW                    2009
#define IDC_LISTVIEW                    2010
#define IDC_HEADERSTATIC                2011
#define IDC_TAB                         2012
#define IDC_HEADERAVATAR                2013
#define IDB_DEFAULT_OLUSER_BUSY         3003
#define IDB_DEFAULT_OLUSER_OUT          3004
#define IDB_DEFAULT_USERGROUP           3005
#define IDB_DEFAULT_OLUSER              3006
#define IDB_SKIN_MSN                    3007
#define IDB_DOCUMENT                    3008
#define IDB_DEFAULT_OFFLUSER            3009
//#define IDC_IM_TOOLBAR                  4000
//#define IDC_IM_TBBTN_FONT               4001
//#define IDC_IM_TBBTN_ICON               4002
//#define IDC_IM_TBBTN_FILE               4003
//#define IDC_IM_TBBTN_HISTORY            4004
//#define IDC_IM_TBBTN_CPIC               4005
//#define IDC_IM_TBBTN_CLOSE              4020
//#define IDB_EMOTICON_BASE               5000
//#define IDB_EMOTICON_01                 5001
//#define IDB_EMOTICON_02                 5002
//#define IDB_EMOTICON_03                 5003
//#define IDB_EMOTICON_04                 5004
//#define IDB_EMOTICON_05                 5005
//#define IDB_EMOTICON_06                 5006
//#define IDB_EMOTICON_07                 5007
//#define IDB_EMOTICON_08                 5008
//#define IDB_EMOTICON_09                 5009
//#define IDB_EMOTICON_10                 5010
//#define IDB_EMOTICON_11                 5011
//#define IDB_EMOTICON_12                 5012
//#define IDB_EMOTICON_13                 5013
//#define IDB_EMOTICON_14                 5014
//#define IDB_EMOTICON_15                 5015
//#define IDB_EMOTICON_16                 5016
//#define IDB_EMOTICON_17                 5017
//#define IDB_EMOTICON_18                 5018
//#define IDB_EMOTICON_19                 5019
//#define IDB_EMOTICON_20                 5020
//#define IDB_EMOTICON_21                 5021
//#define IDB_EMOTICON_22                 5022
//#define IDB_EMOTICON_23                 5023
//#define IDB_EMOTICON_24                 5024
#define WAV_INFONOTIFICATION            5025
#define WAV_MSGNOTIFICATION             5926
#define ID_FILE_LOGON                   40001
#define ID_FILE_LOGOFF                  40002
#define ID_FILE_EXIT                    40003
#define ID_FILE_PRESENCESTATUS_OFFLINE  40011
#define ID_FILE_PRESENCESTATUS_ONLINE   40012
#define ID_FILE_PRESENCESTATUS_AWAY     40013
#define ID_FILE_PRESENCESTATUS_IDLE     40014
#define ID_FILE_PRESENCESTATUS_BUSY     40015
#define ID_FILE_PRESENCESTATUS_BERIGHTBACK 40016
#define ID_FILE_PRESENCESTATUS_ONTHEPHONE 40017
#define ID_FILE_PRESENCESTATUS_OUTTOLUNCH 40018
#define ID_HELP_CONTENT                 40020
#define ID_HELP_ABOUT                   40021
#define ID_TOOLS_OPTIONS                40024
#define ID_TOOLS_SUBSCRIBE              40025
#define ID_ACCELERATORSENDMSG           40026
#define ID_FILE_HOMEPAGE                40027
#define ID_FILEOPEN                     40032
#define ID_FILEREFRESH                  40033

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         40036
#define _APS_NEXT_CONTROL_VALUE         1046
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
