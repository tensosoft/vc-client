//options.cpp
#include "stdafx.h"
#include "options.h"
#include "cryptutil.h"

//可订阅信息控件基础编号
#define IDC_SNUT_BASE 5000
#define IDC_SNRS_BASE 6000

AppOptions* AppOptions::opt=NULL;
DWORD AppOptions::dwSiteCount=0;
SiteInfo* AppOptions::pSiteInfoArr=NULL;
TCHAR AppOptions::szCurrentSiteUrl[STRLEN_1K]={0};
TCHAR AppOptions::szCurrentSiteUrlExt[STRLEN_SMALL]={0};

AppOptions::AppOptions(){Init();}
AppOptions::~AppOptions(){}
HRESULT AppOptions::Init(){
	ZeroMemory(this->Title,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(this->Belong,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(this->Provider,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(this->UserName,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(this->Password,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(this->SubscribedServices,sizeof(TCHAR)*STRLEN_4K);

	HUSKEY hKey;
	TCHAR szKey[STRLEN_1K]={0};
	_stprintf_s(szKey,L"Software\\Discoverx2\\%s",szCurrentSiteUrl);
	DWORD dwValueType=REG_SZ;
	DWORD dwSize=sizeof(TCHAR)*MAX_PATH;
	if(SHRegOpenUSKey(szKey,KEY_READ,NULL,&hKey,FALSE)==ERROR_SUCCESS){
		dwSize=sizeof(TCHAR)*MAX_PATH;
		SHRegQueryUSValue(hKey,L"title",&dwValueType,this->Title,&dwSize,FALSE,NULL,0);
		dwSize=sizeof(TCHAR)*MAX_PATH;
		SHRegQueryUSValue(hKey,L"uid",&dwValueType,this->UserName,&dwSize,FALSE,NULL,0);
		dwSize=sizeof(TCHAR)*MAX_PATH;
		SHRegQueryUSValue(hKey,L"pwd",&dwValueType,this->Password,&dwSize,FALSE,NULL,0);
		SHRegCloseUSKey(hKey);
	}

	TCHAR* appName=szCurrentSiteUrl;
	TCHAR fn[MAX_PATH]={0};
	int ret=GetIniFile(fn,MAX_PATH);
	if(!ret){
		ErrMsg(GetLastError(),L"无法获取配置文件！");
		return E_FAIL;
	}
	int inited=GetPrivateProfileInt(appName,_T("Initialized"),0,fn);
	if (inited>0) this->Initialized=true;
	else this->Initialized=false;
	TCHAR defaultLMP[MAX_PATH]={0};
	TCHAR defaultLFP[MAX_PATH]={0};
	TCHAR mydocPath[MAX_PATH]={0};
	if (SHGetFolderPath(NULL,CSIDL_PERSONAL,NULL,0,mydocPath)==S_OK){
		_stprintf_s(defaultLMP,_T("%s\\discoverx2\\historychats"),mydocPath);
		_stprintf_s(defaultLFP,_T("%s\\discoverx2\\historyfiles"),mydocPath);
	}
	GetPrivateProfileString(appName,_T("LocalMessagePath"),defaultLMP,this->LocalMessagePath,MAX_PATH,fn);
	GetPrivateProfileString(appName,_T("LocalFilePath"),defaultLFP,this->LocalFilePath,MAX_PATH,fn);
	SHCreateDirectoryEx(NULL,this->LocalMessagePath,NULL);
	SHCreateDirectoryEx(NULL,this->LocalFilePath,NULL);
	int iAL=GetPrivateProfileInt(appName,_T("AutoLogon"),1,fn);
	if (iAL>0) this->AutoLogon=true;
	else this->AutoLogon=false;
	int iAR=GetPrivateProfileInt(appName,_T("AutoRun"),1,fn);
	if (iAR>0) this->AutoRun=true;
	else this->AutoRun=false;
	int iLP=GetPrivateProfileInt(appName,_T("DefaultLoginPresence"),1,fn);
	if (iLP>=0) this->DefaultLoginPresence=iLP;
	else this->DefaultLoginPresence=1;
	GetPrivateProfileString(appName,_T("DisplayUserName"),NULL,this->DisplayUserName,MAX_PATH,fn);
	GetPrivateProfileString(appName,_T("DisplayAvatar"),NULL,this->DisplayAvatar,MAX_PATH,fn);
	GetPrivateProfileString(appName,_T("SendMsgKey"),L"CTRL+ENTER",this->SendMsgKey,STRLEN_SMALL,fn);
	GetPrivateProfileString(appName,_T("BaseUrl"),L"",this->BaseUrl,STRLEN_1K,fn);
	GetPrivateProfileString(appName,_T("SubscribedServices"),L"",this->SubscribedServices,STRLEN_4K,fn);

	int w=GetPrivateProfileInt(WIN_SIZE_SECTION_NAME,_T("MainWinWidth"),0,fn);
	this->MainWinWidth=w;
	int h=GetPrivateProfileInt(WIN_SIZE_SECTION_NAME,_T("MainWinHeight"),0,fn);
	this->MainWinHeight=h;
	return S_OK;
}
AppOptions* AppOptions::GetInstance(){
	HANDLE hMutex = CreateMutex(NULL,FALSE,_T("{664f16b2-6d47-435b-b3c9-4f361bddbf2a}"));
	if (GetLastError() == ERROR_ALREADY_EXISTS) {WaitForSingleObject(hMutex,INFINITE);}
	if (AppOptions::opt==NULL) {
		AppOptions::opt=new AppOptions();
	}
	CloseHandle(hMutex);
	ReleaseMutex(hMutex);
	return AppOptions::opt;
}
BOOL AppOptions::GetIniFile(TCHAR* filePath,size_t size){
	TCHAR szBuffer[MAX_PATH]={0};
	ZeroMemory(szBuffer,sizeof(TCHAR)*MAX_PATH);
	HRESULT hResult=SHGetFolderPath(NULL, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, NULL, 0, szBuffer);
	if (!SUCCEEDED(hResult)) return FALSE;
	_tcscat_s(szBuffer,WORK_FOLDER);	
	CreateDirectory(szBuffer,NULL);
	_tcscat_s(szBuffer,L"\\tsim.ini");
	if(!filePath || size<=_tcslen(szBuffer)) return FALSE;
	_tcscpy_s(filePath,size,szBuffer);
	return TRUE;
}
HRESULT AppOptions::Save(){
	TCHAR* appName=szCurrentSiteUrl;
	TCHAR fn[MAX_PATH]={0};
	int ret=GetIniFile(fn,MAX_PATH);
	if (ret){
		if (this->Initialized) WritePrivateProfileString(appName,_T("Initialized"),_T("1"),fn);
		else WritePrivateProfileString(appName,_T("Initialized"),_T("0"),fn);
		WritePrivateProfileString(appName,_T("LocalMessagePath"),this->LocalMessagePath,fn);
		WritePrivateProfileString(appName,_T("LocalFilePath"),this->LocalFilePath,fn);
		if (this->AutoLogon) WritePrivateProfileString(appName,_T("AutoLogon"),_T("1"),fn);
		else WritePrivateProfileString(appName,_T("AutoLogon"),_T("0"),fn);
		if (this->AutoRun) WritePrivateProfileString(appName,_T("AutoRun"),_T("1"),fn);
		else WritePrivateProfileString(appName,_T("AutoRun"),_T("0"),fn);

		WritePrivateProfileString(appName,_T("Title"),this->Title,fn);
		WritePrivateProfileString(appName,_T("Provider"),this->Provider,fn);
		WritePrivateProfileString(appName,_T("Belong"),this->Belong,fn);
		WritePrivateProfileString(appName,_T("DisplayUserName"),this->DisplayUserName,fn);
		WritePrivateProfileString(appName,_T("DisplayAvatar"),this->DisplayAvatar,fn);
		TCHAR szDLP[MAX_PATH]={0};
		_stprintf_s(szDLP,_T("%d"),this->DefaultLoginPresence);
		WritePrivateProfileString(appName,_T("DefaultLoginPresence"),szDLP,fn);
		WritePrivateProfileString(appName,_T("SendMsgKey"),this->SendMsgKey,fn);
		return S_OK;
	}
	else ErrorMsg(_T("无法保存配置项，发生了如下错误：%s(%d)"),_T("错误"),GetLastError());
	return E_FAIL;
}
HRESULT AppOptions::SaveSubscribedServices(TCHAR* szSubscribedServices){
	if(!szSubscribedServices) return E_FAIL;
	TCHAR* appName=szCurrentSiteUrl;
	TCHAR fn[MAX_PATH]={0};
	int ret=GetIniFile(fn,MAX_PATH);
	if (ret){
		if (this->Initialized) WritePrivateProfileString(appName,_T("Initialized"),_T("1"),fn);
		else WritePrivateProfileString(appName,_T("Initialized"),_T("0"),fn);
		_tcscpy_s(this->SubscribedServices,szSubscribedServices);
		WritePrivateProfileString(appName,_T("SubscribedServices"),this->SubscribedServices,fn);
		return S_OK;
	}
	else ErrorMsg(_T("无法保存订阅信息，发生了如下错误：%s(%d)"),_T("错误"),GetLastError());
	return E_FAIL;
}
HRESULT AppOptions::SaveMainWinSize(int w,int h){
	if(w==0 || h==0) return E_FAIL;
	TCHAR* appName=szCurrentSiteUrl;
	TCHAR fn[MAX_PATH]={0};
	int ret=GetIniFile(fn,MAX_PATH);
	if (ret){
		this->MainWinWidth=w;
		this->MainWinHeight=h;
		TCHAR szW[MAX_PATH]={0};
		TCHAR szH[MAX_PATH]={0};
		_stprintf_s(szW,_T("%d"),w);
		_stprintf_s(szH,_T("%d"),h);
		WritePrivateProfileString(WIN_SIZE_SECTION_NAME,_T("MainWinWidth"),szW,fn);
		WritePrivateProfileString(WIN_SIZE_SECTION_NAME,_T("MainWinHeight"),szH,fn);
		return S_OK;
	}
	else return E_FAIL;
}

INT_PTR CALLBACK OptionsDialogProc(HWND hwndDlg,UINT uMsg,WPARAM wParam,LPARAM lParam){	
	switch (uMsg) 
	{ 
	case WM_INITDIALOG:
	{
		AppOptions* opt=AppOptions::GetInstance();
		HWND hwndInfoGroup=GetDlgItem(hwndDlg,IDC_GPRINFO);
		SetDlgItemText(hwndDlg,IDC_EDITUSERNAME,opt->UserName);
		size_t size=MAX_PATH;
		TCHAR szPwdPlainText[MAX_PATH];
		Decrypt(opt->Password,szPwdPlainText,&size);
		SetDlgItemText(hwndDlg,IDC_EDITPASSWORD,szPwdPlainText);
		SetDlgItemText(hwndDlg,IDC_LOCALMSGPATH,opt->LocalMessagePath);
		SetDlgItemText(hwndDlg,IDC_LOCALFILEPATH,opt->LocalFilePath);
		if (opt->AutoLogon) SendDlgItemMessage(hwndDlg,IDC_AUTOLOGIN,BM_SETCHECK,BST_CHECKED,NULL);
		else SendDlgItemMessage(hwndDlg,IDC_AUTOLOGIN,BM_SETCHECK,BST_UNCHECKED,NULL);
		if (opt->AutoRun) SendDlgItemMessage(hwndDlg,IDC_AUTORUN,BM_SETCHECK,BST_CHECKED,NULL);
		else SendDlgItemMessage(hwndDlg,IDC_AUTORUN,BM_SETCHECK,BST_UNCHECKED,NULL);
		SetDlgItemText(hwndDlg,IDC_IMBASEURL,AppOptions::szCurrentSiteUrl);
		EnableWindow(GetDlgItem(hwndDlg,IDC_IMBASEURL),FALSE);

		return TRUE;
	}
	case WM_COMMAND: 
		switch (LOWORD(wParam)) 
		{ 
		case IDOK: 
		{
			AppOptions* opt=AppOptions::GetInstance();
			//自动登录
			opt->AutoLogon=(IsDlgButtonChecked (hwndDlg,IDC_AUTOLOGIN)==BST_CHECKED);
			//自动运行
			opt->AutoRun=(IsDlgButtonChecked (hwndDlg,IDC_AUTORUN)==BST_CHECKED);
			RegisterAutoRun(opt->AutoRun);
			//账号名
			SecureZeroMemory(opt->UserName,MAX_PATH*sizeof(TCHAR));
			GetDlgItemText(hwndDlg,IDC_EDITUSERNAME,opt->UserName,MAX_PATH);
			//密码
			TCHAR szPwdPlainText[MAX_PATH]={0};
			GetDlgItemText(hwndDlg,IDC_EDITPASSWORD,szPwdPlainText,MAX_PATH);
			TCHAR szPwdEncrypted[MAX_PATH]={0};
			size_t szPwdEncryptedLen=MAX_PATH;
			Encrypt(szPwdPlainText,szPwdEncrypted,&szPwdEncryptedLen);
			SecureZeroMemory(opt->Password,MAX_PATH*sizeof(TCHAR));
			_tcscpy_s(opt->Password,szPwdEncrypted);
			//本地消息保存路径
			SecureZeroMemory(opt->LocalMessagePath,MAX_PATH*sizeof(TCHAR));
			GetDlgItemText(hwndDlg,IDC_LOCALMSGPATH,opt->LocalMessagePath,MAX_PATH);
			//接收文件保存路径
			SecureZeroMemory(opt->LocalFilePath,MAX_PATH*sizeof(TCHAR));
			GetDlgItemText(hwndDlg,IDC_LOCALFILEPATH,opt->LocalFilePath,MAX_PATH);
			//初始化完成标记
			if (!opt->Initialized) opt->Initialized=TRUE;
			//保存
			opt->Save();
		}
		case IDCANCEL: 
			EndDialog(hwndDlg, wParam); 
			return TRUE; 
		case IDC_BTNBROWSEFILE:
		case IDC_BTNBROWSEMSG:
		{
			BROWSEINFO bi = { 0 };
			bi.lpszTitle = _T("选择目标路径");
			bi.ulFlags=BIF_RETURNONLYFSDIRS;
			bi.hwndOwner=hwndDlg;
			LPITEMIDLIST pidl = SHBrowseForFolder ( &bi );
			if (pidl!=0){
				TCHAR path[MAX_PATH]={0};
				// 获取路径名称
				if (SHGetPathFromIDList(pidl,path)) {
					if (LOWORD(wParam)==IDC_BTNBROWSEFILE) SetDlgItemText(hwndDlg,IDC_LOCALFILEPATH,path);
					if (LOWORD(wParam)==IDC_BTNBROWSEMSG) SetDlgItemText(hwndDlg,IDC_LOCALMSGPATH,path);
				}
				// 释放内存
				IMalloc * imalloc = 0;
				if ( SUCCEEDED( SHGetMalloc ( &imalloc ))){
					imalloc->Free(pidl);
					imalloc->Release();
				}
			} //if (pidl!=0) end
			return TRUE;
		}//case IDC_BTNBROWSEMSG: end
		}//switch (LOWORD(wParam)) end
	} //switch (uMsg) end
	return FALSE; 
}
HRESULT ShowOptionsDialog(HWND hWndParent, BSTR bstrTitle){
	int iRes;
	iRes = DialogBoxParam(GetModuleHandle(NULL),MAKEINTRESOURCE(IDD_OPTIONS),hWndParent,(DLGPROC)OptionsDialogProc,NULL);
	if (iRes == IDOK) return S_OK;
	return E_FAIL;
}
void ErrorMsg(LPTSTR lpszErrDesc,LPTSTR lpszTitle,DWORD dw) 
{ 
	ErrMsg(dw,lpszErrDesc);
}
void RegisterProtocol(){
	HKEY hKey;
	DWORD dwDisposition;
	const TCHAR keyName[]=L"tsim";
	const TCHAR keyNameDI[]=L"tsim\\DefaultIcon";
	const TCHAR keyNameSOC[]=L"tsim\\shell\\open\\command";
	//如果已经注册，则退出
	RegOpenKeyEx(HKEY_CLASSES_ROOT,keyName,NULL,KEY_READ,&hKey);
	if (hKey) {
		RegCloseKey(hKey);
		return;
	}

	if (ERROR_SUCCESS==RegCreateKeyEx(HKEY_CLASSES_ROOT,keyName,0,_T(""),REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,0,&hKey,&dwDisposition)){ 
		TCHAR url[]=_T("URL:腾硕办公助手URL协议");
		if (ERROR_SUCCESS!=RegSetValueEx(hKey, _T(""),0,REG_SZ,(BYTE*)url,_tcslen(url)*sizeof(TCHAR)+2)) ErrorMsg(_T("无法注册应用程序URL协议，发生了如下错误：%s(%d)，系统可能无法正常运行！"),_T("错误"),0);
		TCHAR dummy[]=_T("");
		if (ERROR_SUCCESS!=RegSetValueEx(hKey, _T("URL Protocol"),0,REG_SZ,(BYTE*)dummy,_tcslen(dummy)*sizeof(TCHAR)+2)) ErrorMsg(_T("无法注册应用程序URL协议，发生了如下错误：%s(%d)，系统可能无法正常运行！"),_T("错误"),0);
	}
	else ErrorMsg(_T("无法注册应用程序URL协议，发生了如下错误：%s(%d)，系统可能无法正常运行！"),_T("错误"),0);
	RegCloseKey(hKey);

	TCHAR exeFN[MAX_PATH]={0};
	GetModuleFileName(NULL, exeFN, MAX_PATH);

	if (ERROR_SUCCESS==RegCreateKeyEx(HKEY_CLASSES_ROOT,keyNameDI,0,_T(""),REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,0,&hKey,&dwDisposition)){
		if (ERROR_SUCCESS!=RegSetValueEx(hKey, _T(""),0,REG_SZ,(BYTE*)exeFN,_tcslen(exeFN)*sizeof(TCHAR)+2)) ErrorMsg(_T("无法注册应用程序URL协议，发生了如下错误：%s(%d)，系统可能无法正常运行！"),_T("错误"),0);
	}
	else ErrorMsg(_T("无法注册应用程序URL协议，发生了如下错误：%s(%d)，系统可能无法正常运行！"),_T("错误"),0);
	RegCloseKey(hKey);

	if (ERROR_SUCCESS==RegCreateKeyEx(HKEY_CLASSES_ROOT,keyNameSOC,0,_T(""),REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,0,&hKey,&dwDisposition)){
		TCHAR exeParam[STRLEN_1K]={0};
		_stprintf_s(exeParam,_T("%s %s"),exeFN,_T("%1"));
		if (ERROR_SUCCESS!=RegSetValueEx(hKey, _T(""),0,REG_SZ,(BYTE*)exeParam,_tcslen(exeParam)*sizeof(TCHAR)+2)) ErrorMsg(_T("无法注册应用程序URL协议，发生了如下错误：%s(%d)，系统可能无法正常运行！"),_T("错误"),0);
	}
	else ErrorMsg(_T("无法注册应用程序URL协议，发生了如下错误：%s(%d)，系统可能无法正常运行！"),_T("错误"),0);
	RegCloseKey(hKey);
}
void RegisterAutoRun(BOOL isAutoRun){
	HKEY hKey;
	const TCHAR keyName[]=L"discoverx2tsim";
	if (RegOpenKeyEx(HKEY_CURRENT_USER,_T("Software\\Microsoft\\Windows\\CurrentVersion\\Run"),0,KEY_SET_VALUE,&hKey )==ERROR_SUCCESS){
		if (isAutoRun){
			TCHAR exeFN[MAX_PATH]={0};
			if (GetModuleFileName(NULL,exeFN,MAX_PATH)>0){
				if (ERROR_SUCCESS!=RegSetValueEx(hKey,keyName,0,REG_SZ,(BYTE*)exeFN,_tcslen(exeFN)*sizeof(TCHAR)+2)) ErrorMsg(_T("无法设置自动运行选项，发生了如下错误：%s(%d)"),_T("错误"),0);
			}
		}	else {
			RegDeleteValue(hKey,keyName);
		}
		RegCloseKey(hKey);
	}
	else ErrorMsg(_T("无法设置自动运行选项，发生了如下错误：%s(%d)"),_T("错误"),0);
}

AppOptions* AppOptions::Reload(){
	AppOptions::opt=new AppOptions();
	return AppOptions::opt;
}