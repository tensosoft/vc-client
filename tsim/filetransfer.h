// 文件传输相关定义的头文件
#ifndef _FILETRANSFER_H_
#define _FILETRANSFER_H_

#pragma once

#include "mainwin.h"

//文件传输成功完毕消息
#define WM_FT_COMPLETE	WM_APP+300
//文件传输完毕但是出错消息
#define WM_FT_ERROR	WM_APP+301
//文件传输进度变化消息
#define WM_FT_PROGRESS	WM_APP+302
//发送文件数据消息
#define WM_FT_SENDFDATA	WM_APP+303
//默认使用的发送文件TCP端口
#define DEFAULT_PORT 52321
//最大可传输文件
#define MAX_FILE_SEND_LENGTH 2147483647L
//传输时一次读取的文件内容
#define DEFAULT_NUM_OF_BYTES_TO_READ	1024*512
//发送和接收文件的线程上下文信息（线程过程参数）
typedef struct tagFileTransferContext{
	DWORD m_id;																	//文件唯一数字id
	BOOL m_isSender;														//是否文件的发送人（TRUE为发送人，FALSE为接收人）
	DWORD m_fileSize;														//文件大小
	DWORD	m_receivedBytes;											//已经接收的字节数
	DWORD	m_sentBytes;													//已经发送的字节数
	DWORD m_tid;																//发送或接收线程的ID
	HANDLE m_thread;														//发送或接收线程的句柄
	HWND	m_hwnd;																//发送或接收所发生的会话窗口的句柄
	TCHAR m_fn[MAX_PATH];												//原始文件名
	TCHAR m_fp[MAX_PATH];												//要发送或接收的目标文件名和路径。
	TCHAR m_src[MAX_PATH];											//发送文件的计算机的ip
	TCHAR m_dest[MAX_PATH];											//接收文件的计算机的ip
	SOCKET m_socket;														//发送文件内容所使用的SOCKET
	BOOL m_canceled;														//发送/接收是否被取消标记
	USHORT m_port;															//发送/接收文件使用的服务端口
}FTContext,*PFTContext;

//文件发送监听器
DWORD WINAPI SendFileListener(LPVOID lpParam);
//通过win socket发送文件内容
DWORD WINAPI SendFileContent(LPVOID lpParam);
//通过win socket接收文件内容
DWORD WINAPI RecvFileContent(LPVOID lpParam);

#endif //_FILETRANSFER_H_