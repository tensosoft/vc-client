//加解密文本的定义
#ifndef __CRYPTUTIL_H__
#define __CRYPTUTIL_H__
#pragma once

const int IN_BUFFER_SIZE    = 1024;
// OUT_BUFFER_SIZE is 8 bytes larger than IN_BUFFER_SIZE
// When CALG_RC2 algorithm is used, encrypted data 
// will be 8 bytes larger than IN_BUFFER_SIZE
const int OUT_BUFFER_SIZE   = IN_BUFFER_SIZE + 8; // extra padding

const TCHAR HEXES[16]={L'0',L'1',L'2',L'3',L'4',L'5',L'6',L'7',L'8',L'9',L'A',L'B',L'C',L'D',L'E',L'F'};
static BYTE KEY_IV[] = { 0xEF, 0x34, 0xCD, 0x78, 0x90, 0xAB, 0x56, 0x12 };
//加密原始字符串并以base64编码方式输出，加密密码默认为：11235813
BOOL Encrypt(const TCHAR* szRaw,TCHAR* szOut,size_t* iOutSize,TCHAR* szKey=L"11235813");
//解密以base64方式编码的加过密的字符串并输出原始字符串，解密密码默认为：11235813
BOOL Decrypt(TCHAR* szRaw,TCHAR* szOut,size_t* iOutSize,TCHAR* szKey=L"11235813");
BOOL GetSystemDriveSerialNumber(TCHAR* szSerialNumber,UINT iSize);
//把utf8指定的utf-8格式字符串转换为unicode字符串，结果保存在dst中，dst的大小由dstSize指定（如果dstSize为0，则返回需要的dst大小）。
BOOL ToUTF16(IN const char * utf8 ,wchar_t * dst,size_t dstSize);
//把utf16指定的unicode字符串转换为utf8字符串，结果保存在dst中，dst的大小由dstSize指定（如果dstSize为0，则返回需要的dst大小）。
BOOL FromUTF16( IN const wchar_t * utf16,char* dst,size_t dstSize);
BOOL ToHexString(const unsigned char * bytes,size_t bytesSize,TCHAR* szResult,size_t* resultSize);
BOOL FromHexString(const TCHAR* szHexString,unsigned char * bytes,size_t* bytesSize);
#endif // __CRYPTUTIL_H__