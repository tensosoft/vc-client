// 文件传输实现
#include "stdafx.h"
#include "filetransfer.h"
#include "chatwin.h"

#pragma comment( lib, "WS2_32" )

#define SD_BOTH 0x02
#define TF_USE_DEFAULT_WORKER 0x00

DWORD WINAPI SendFileListener(LPVOID lpParam){
	DWORD hr=S_OK;
	PFSLContext ctx=(PFSLContext)lpParam;
	if (!ctx) {
		ErrMsg(0,L"由于缺乏必要启动参数而导致无法启动文件发送网络监听程序！");
		return E_POINTER;
	}
	// 初始化套接字
	WSADATA wsaData;
	int iResult = WSAStartup( MAKEWORD(2,2), &wsaData );
	if (iResult!= NO_ERROR ) {
		ErrMsg(iResult,L"无法初始化网络：%s");
		return iResult;
	}
	// 创建套接字
	SOCKET sendFileListenerSocket=INVALID_SOCKET;
	sendFileListenerSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP );
	if (sendFileListenerSocket == INVALID_SOCKET ) {
		hr=WSAGetLastError();
		ErrMsg(iResult,L"无法创建网络：%s");
		WSACleanup();
		return hr;
	}
	u_long ul=1;
	ioctlsocket(sendFileListenerSocket, FIONBIO, (u_long FAR *)&ul);
	ctx->socket=sendFileListenerSocket;

	// 绑定套接字
	sockaddr_in service;
	service.sin_family = AF_INET;
	if(_tcslen(ctx->address)==0 || _tcschr(ctx->address,L';')!=NULL){
		service.sin_addr.s_addr=htonl(INADDR_ANY);
	}else{
		service.sin_addr.s_addr = inet_addr(CT2A(ctx->address).m_psz);
	}
	service.sin_port = htons(ctx->port==0?DEFAULT_PORT:ctx->port);
	if(bind(sendFileListenerSocket, (SOCKADDR*) &service, sizeof(service) ) == SOCKET_ERROR ) {
		hr=WSAGetLastError();
		if(hr==0) hr=E_FAIL;
		ErrMsg(hr,L"无法绑定网络：%s");
		goto end;
	}

	// 监听套接字
	if(listen(sendFileListenerSocket, SOMAXCONN) == SOCKET_ERROR ){
		hr=WSAGetLastError();
		if(hr==0) hr=E_FAIL;
		ErrMsg(hr,L"无法监听网络：%s");
		goto end;
	}

	// 等待接收连接
	while (ctx->waiting) {
		SOCKET acceptSocket=INVALID_SOCKET;
		//while (ctx->waiting && (acceptSocket==SOCKET_ERROR || acceptSocket==INVALID_SOCKET)) {
		//	acceptSocket=accept(sendFileListenerSocket, NULL, NULL);
		//}
		acceptSocket=accept(sendFileListenerSocket, NULL, NULL);
		//接收文件id
		DWORD dwWait=0;
		int bytesRecv=0;
		while(ctx->waiting && !(acceptSocket==SOCKET_ERROR || acceptSocket==INVALID_SOCKET)){
			DWORD dwFid=0;
recv:
			bytesRecv=recv(acceptSocket,(char*)&dwFid,sizeof(DWORD),0);
			if(bytesRecv==SOCKET_ERROR && WSAGetLastError()==WSAEWOULDBLOCK) goto recv;
			if(bytesRecv==sizeof(DWORD)){
				if(dwFid>0){
					CWItr itr;
					ChatWin* pcw=NULL;
					for(itr=ctx->pmw->m_chatwininfo.begin();itr!=ctx->pmw->m_chatwininfo.end();itr++){
						if(itr->second==NULL ||itr->second->hWnd==NULL) continue;
						pcw=(ChatWin*)GetWindowLongPtr(itr->second->hWnd, GWLP_USERDATA);
						if(pcw==NULL||pcw->m_ftc.empty()) continue;
						ITRFTCONTEXT itrftc;
						for(itrftc=pcw->m_ftc.begin();itrftc!=pcw->m_ftc.end();itrftc++){
							if((*itrftc) && (*itrftc)->m_id==dwFid){
								PFTContext pftc=(*itrftc);
								pftc->m_socket=acceptSocket;
								PostMessage(pftc->m_hwnd,WM_FT_SENDFDATA,(WPARAM)0,(LPARAM)pftc);
								break;
							}
						}
					}
				}
				break;
			}
			if(dwWait<10000){
				dwWait+=100;
				Sleep(100);
				continue;
			}else{
				break;
			}
		}//while end
		Sleep(100);
	}	//while end
	hr=S_OK;
end:
	if(sendFileListenerSocket!=SOCKET_ERROR && sendFileListenerSocket!=INVALID_SOCKET){
		shutdown(sendFileListenerSocket, SD_BOTH);
		closesocket(sendFileListenerSocket);
	}
	WSACleanup();

	ctx->socket=INVALID_SOCKET;
	ctx->thread=NULL;
	ctx->tid=0;

	return hr;
}
DWORD WINAPI SendFileContent(LPVOID lpParam){
	DWORD hr=S_OK;
	HANDLE hFile=INVALID_HANDLE_VALUE;
	
	PFTContext ctx=(PFTContext)lpParam;
	if(ctx==NULL||ctx->m_socket==INVALID_SOCKET||ctx->m_socket==SOCKET_ERROR) return 0;
	// 发送和接收数据
	hFile=CreateFile(ctx->m_fp,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN,NULL);
	if (hFile == INVALID_HANDLE_VALUE) { 
		hr=GetLastError();
		if(hr==0) hr=E_FAIL;
		goto end;
	}
	ctx->m_sentBytes=0;
	//发送文件内容
	DWORD dwNumOfBytesToRead=DEFAULT_NUM_OF_BYTES_TO_READ;
	DWORD dwNumOfBytesRead=0;
	char buf[DEFAULT_NUM_OF_BYTES_TO_READ]={0};
	int sentBytes=0;
	while(ReadFile(hFile,buf,dwNumOfBytesToRead,&dwNumOfBytesRead,NULL) && dwNumOfBytesRead>0){
		if(ctx->m_canceled){hr=ERROR_CANCELLED;goto end;}
resend:
		if((sentBytes=send(ctx->m_socket,buf,(int)dwNumOfBytesRead,0))==SOCKET_ERROR){
			hr=WSAGetLastError();
			if(hr==WSAEWOULDBLOCK){Sleep(62); goto resend;}
			goto end;
		}
		if(sentBytes!=(int)dwNumOfBytesRead){
			hr=E_FAIL;
			goto end;
		}
		ctx->m_sentBytes+=sentBytes;
		SendMessage(ctx->m_hwnd,WM_FT_PROGRESS,0,(LPARAM)ctx);
	}//while end
	hr=S_OK;
	hr=GetLastError();
	if(hr==0) hr=WSAGetLastError();
	if(hr!=S_OK) goto end;
	hr=S_OK;
end:
	if(ctx->m_socket!=SOCKET_ERROR && ctx->m_socket!=INVALID_SOCKET){
		shutdown(ctx->m_socket, SD_BOTH);
		closesocket(ctx->m_socket);
		ctx->m_socket=INVALID_SOCKET;
	}

	if(hr!=S_OK)SendMessage(ctx->m_hwnd,WM_FT_ERROR,(WPARAM)hr,(LPARAM)ctx);
	else SendMessage(ctx->m_hwnd,WM_FT_COMPLETE,0,(LPARAM)ctx);
	if(hFile!=INVALID_HANDLE_VALUE&& hFile!=NULL) CloseHandle(hFile);

	ctx->m_socket=INVALID_SOCKET;
	ctx->m_tid=0;
	ctx->m_thread=NULL;

	return 0;
}
DWORD WINAPI RecvFileContent(LPVOID lpParam){
	DWORD hr=S_OK;
	HANDLE hFile=INVALID_HANDLE_VALUE;
	PFTContext ctx=(PFTContext)lpParam;
	if (!ctx || _tcslen(ctx->m_fp)<=0) return E_POINTER;
	// 初始化套接字
	WSADATA wsaData;
	int iResult = WSAStartup( MAKEWORD(2,2), &wsaData );
	if ( iResult != NO_ERROR ){
		SendMessage(ctx->m_hwnd,WM_FT_ERROR,(WPARAM)iResult,(LPARAM)ctx);
		return iResult;
	}

	// 创建套接字
	SOCKET clientSocket;
	clientSocket = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
	if (clientSocket == INVALID_SOCKET ) {
		hr=WSAGetLastError();
		SendMessage(ctx->m_hwnd,WM_FT_ERROR,(WPARAM)hr,(LPARAM)ctx);
		WSACleanup();
		return hr;
	}
	//u_long ul=1;
	//ioctlsocket(clientSocket, FIONBIO, (u_long FAR *)&ul);
	ctx->m_socket=clientSocket;

	// 连接到服务器
	sockaddr_in clientService;
	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = inet_addr(CT2A(ctx->m_src).m_psz);
	clientService.sin_port = htons(ctx->m_port==0?DEFAULT_PORT:ctx->m_port);
	DWORD dwWait=0;
connect:
	if (connect(clientSocket, (SOCKADDR*) &clientService, sizeof(clientService) ) == SOCKET_ERROR) {
		if(dwWait<10000){
			dwWait+=100;
			Sleep(100);
			goto connect;
		}else{
			hr=WSAGetLastError();
			goto end;
		}
	}

	//发送文件id
	int sentBytes=send(clientSocket,(const char*)&ctx->m_id,sizeof(ctx->m_id),0);
	if(sentBytes==SOCKET_ERROR){
		hr=WSAGetLastError();
		goto end;
	}
	if(sentBytes!=sizeof(ctx->m_id)){
		hr=E_FAIL;
		goto end;
	}

	//创建文件
	hFile = CreateFile(ctx->m_fp,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN,NULL);
	if (hFile == INVALID_HANDLE_VALUE){ 
		hr=GetLastError();
		if(hr==0) hr=E_FAIL;
		goto end;
	}

	// 接收数据
	ctx->m_receivedBytes=0;

	int bytesRecv = SOCKET_ERROR;
	char recvbuf[STRLEN_1K] ={0};
	DWORD numOfBytesWritten=0;

	while((bytesRecv = recv(clientSocket, recvbuf, STRLEN_1K, 0))!=SOCKET_ERROR) {
		if(ctx->m_canceled){hr=ERROR_CANCELLED;goto end;}
		if (bytesRecv == 0 ){break;}	//数据获取完毕	//连接关闭条件：|| (bytesRecv == SOCKET_ERROR && hr==WSAECONNRESET)

		ctx->m_receivedBytes+=bytesRecv;
		SendMessage(ctx->m_hwnd,WM_FT_PROGRESS,0,(LPARAM)ctx);

		if (!WriteFile(hFile,recvbuf,bytesRecv,&numOfBytesWritten,NULL) || bytesRecv!=numOfBytesWritten){	//写入文件
			hr=GetLastError();
			if(hr==0) hr=E_FAIL;
			goto end;
		}
		if(ctx->m_receivedBytes==ctx->m_fileSize) break;
	}
	hr=S_OK;
	hr=GetLastError();
	if(hr==0) hr=WSAGetLastError();
	if(hr!=S_OK) goto end;

	if (ctx->m_receivedBytes!=ctx->m_fileSize) {
		hr=E_FAIL;
		CloseHandle(hFile);
		DeleteFile(ctx->m_fp);
		hFile=NULL;
		goto end;
	}
	hr=S_OK;
end:
	if(hFile!=INVALID_HANDLE_VALUE&& hFile!=NULL) CloseHandle(hFile);
	if(clientSocket!=SOCKET_ERROR && clientSocket!=INVALID_SOCKET){
		shutdown(clientSocket, SD_BOTH);
		closesocket(clientSocket);
	}
	WSACleanup();

	if(hr!=S_OK)SendMessage(ctx->m_hwnd,WM_FT_ERROR,(WPARAM)hr,(LPARAM)ctx);
	else SendMessage(ctx->m_hwnd,WM_FT_COMPLETE,0,(LPARAM)ctx);

	ctx->m_socket=INVALID_SOCKET;
	ctx->m_tid=0;
	ctx->m_thread=NULL;

	return hr;
}