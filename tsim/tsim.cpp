// tsim.cpp : 程序入口点

#include "stdafx.h"
#include "infonotify.h"
#include "mainwin.h"
#include "chatwin.h"
#include "options.h"

extern NOTIFYICONDATA	niData;	//通知栏消息

//窗口枚举函数
BOOL CALLBACK searcher(HWND hWnd, LPARAM lParam){
	TCHAR clsName[STRLEN_DEFAULT]={0};
	int res=GetClassName(hWnd,clsName,STRLEN_DEFAULT);
	if (_tcsicmp(clsName,APP_CLASS)==0){
		if (!IsWindowVisible(hWnd)) ShowWindow(hWnd, SW_SHOW);
		if (IsIconic(hWnd)) ShowWindow(hWnd,SW_RESTORE);
		SetForegroundWindow(hWnd);
		return false;
	}
  return TRUE;	//继续枚举
}
//初始化任务栏图标
BOOL InitTaskbarNotifier(HWND hWnd){
	ZeroMemory(&niData,sizeof(NOTIFYICONDATA));
	niData.cbSize = sizeof(NOTIFYICONDATA);
	
	// ID
	niData.uID = TRAYICONID;

	// 标志
	niData.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP | NIF_GUID;

	// 图标
	niData.hIcon = (HICON)LoadImage(GetModuleHandle(NULL),MAKEINTRESOURCE(IDI_SMALL),IMAGE_ICON, GetSystemMetrics(SM_CXSMICON),GetSystemMetrics(SM_CYSMICON),LR_DEFAULTCOLOR);

	//窗口和消息
	niData.hWnd = hWnd;
  niData.uCallbackMessage = SWM_TRAYMSG;

	//消息提示
	AppOptions* appOpts=AppOptions::GetInstance();
	TCHAR TBTip[MAX_PATH]={0};
	_stprintf_s(TBTip,_T("%s - %s"),appOpts->Title,_T("已登录"));
  lstrcpyn(niData.szTip, TBTip, sizeof(niData.szTip)/sizeof(TCHAR));

	Shell_NotifyIcon(NIM_ADD,&niData);

	//释放图标资源
	if(niData.hIcon && DestroyIcon(niData.hIcon))	niData.hIcon = NULL;
	return TRUE;
}
// 主入口函数
int APIENTRY WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow){
	TCHAR* cmdLine=GetCommandLine();

	//读取所有可用站点
	HUSKEY hKey;
	AppOptions::pSiteInfoArr=NULL;
	ZeroMemory(AppOptions::szCurrentSiteUrl,sizeof(TCHAR)*STRLEN_1K);
	if(SHRegOpenUSKey(L"Software\\Discoverx2",KEY_READ,NULL,&hKey,FALSE)==ERROR_SUCCESS){
		DWORD dwValidCount=0;
		DWORD dwSubKeyCount=0;
		DWORD dwMaxSubKeyLen=0;
		if(SHRegQueryInfoUSKey(hKey,&dwSubKeyCount,&dwMaxSubKeyLen,0,0,SHREGENUM_HKCU)==ERROR_SUCCESS){
			AppOptions::pSiteInfoArr=new SiteInfo[dwSubKeyCount];
			SiteInfo* pSi=NULL;
			DWORD szUrlLen=0;
			DWORD szDt=REG_SZ;
			DWORD szTitleLen=0;
			DWORD szUrlExtLen=0;
			for(DWORD i=0;i<dwSubKeyCount;i++){
				pSi=&AppOptions::pSiteInfoArr[i];
				ZeroMemory(pSi,sizeof(SiteInfo));
				ZeroMemory(pSi->Url,sizeof(TCHAR)*STRLEN_1K);
				szUrlLen=STRLEN_1K;
				ZeroMemory(pSi->Title,sizeof(TCHAR)*MAX_PATH);
				ZeroMemory(pSi->UrlExt,sizeof(TCHAR)*STRLEN_SMALL);
				if(SHRegEnumUSKey(hKey,i,pSi->Url,&szUrlLen,SHREGENUM_HKCU)!=ERROR_SUCCESS) continue;
				if(_tcsstr(pSi->Url,L"www.tensosoft.com")!=NULL||_tcsstr(pSi->Url,L"demo.tensosoft.cn")!=NULL) {
					ZeroMemory(pSi->Url,sizeof(TCHAR)*STRLEN_1K);
					continue;
				}

				TCHAR szKey[STRLEN_1K]={0};
				_stprintf_s(szKey,L"Software\\Discoverx2\\%s",pSi->Url);
				
				szDt=REG_SZ;
				szTitleLen=sizeof(TCHAR)*MAX_PATH;
				SHRegGetUSValue(szKey,L"title",&szDt,pSi->Title,&szTitleLen,FALSE,NULL,0);

				szDt=REG_SZ;
				szUrlExtLen=sizeof(TCHAR)*STRLEN_SMALL;
				SHRegGetUSValue(szKey,L"ext",&szDt,pSi->UrlExt,&szUrlExtLen,FALSE,NULL,0);
				
				dwValidCount++;
			}
			if(dwValidCount==1 && AppOptions::pSiteInfoArr) {
				for(DWORD i=0;i<dwSubKeyCount;i++){
					pSi=&AppOptions::pSiteInfoArr[i];
					if(_tcslen(pSi->Url)==0) continue;
					_tcscpy_s(AppOptions::szCurrentSiteUrl,pSi->Url);
					_tcscpy_s(AppOptions::szCurrentSiteUrlExt,pSi->UrlExt);
				}
			}else{
				TCHAR fn[MAX_PATH]={0};
				int len=GetModuleFileName(NULL,fn,MAX_PATH);
				if (len>0){
					_stprintf_s(AppOptions::szCurrentSiteUrl,L"res://%s/IDH_SELECT_SITES",fn);
					_tcscpy_s(AppOptions::szCurrentSiteUrlExt,L".htm");
				}
			}
		}
		AppOptions::dwSiteCount=dwSubKeyCount;
		SHRegCloseUSKey(hKey);
	}

	//初始安装标记
	if (_tcsstr(cmdLine,L" -i")!=NULL || _tcsstr(cmdLine,L" -I")!=NULL) {
		AppOptions* appOpts=AppOptions::GetInstance();
		//注册自动运行
		RegisterAutoRun(appOpts->AutoRun);
		//注册自定义URL处理程序
		RegisterProtocol();
		return 0;	//安装完毕时，直接退出
	}

	//创建互斥对象
	HANDLE hMutex = CreateMutex(NULL, FALSE,_T("{6d71599b-57c1-473e-81be-0c8e0d9f5e18}"));
	if (GetLastError() == ERROR_ALREADY_EXISTS){
		EnumWindows(searcher,NULL);
		return 0;
	}

	// 初始化COM支持
	HRESULT hr = CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);
	if (FAILED(hr)) return 0;

	// 初始化通用Windows控件
	InitCommonControls();

	// 装载富文本支持
	//HMODULE hRichEdit;
	//hRichEdit = LoadLibraryW(L"riched20.dll");
	//if (hRichEdit == NULL) return 0;

	// 注册所有窗口的窗口类
	MainWin::RegisterClass();
	InfoNotify::RegisterClass();
	ChatWin::RegisterClass();
	
	// 创建主窗口
	int cx = GetSystemMetrics(SM_CXSCREEN);
	int cy = GetSystemMetrics(SM_CYSCREEN);
	AppOptions* appOpts=AppOptions::GetInstance();
	int w=(appOpts->MainWinWidth==0?MAINWIN_WIDTH:appOpts->MainWinWidth);
	int h=(appOpts->MainWinHeight==0?MAINWIN_HEIGHT:appOpts->MainWinHeight);
	HWND hWnd = CreateWindowExW(0,APP_CLASS,APP_TITLE,WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,(cx-w)-8, 50,w, h,NULL,NULL,hInstance,NULL);
	MainWin *pmw=(MainWin *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	if ( !hWnd ) return 0;
	SetWindowText(hWnd,L"办公小助手");
	
	// 显示主窗口
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	//显示任务栏通知图标
	InitTaskbarNotifier(hWnd);

	//如果没有设置，则显示设置对话框
	//if (!appOpts->Initialized) ShowOptionsDialog(hWnd,_T("选项"));

	// 进入消息循环
	BOOL bRet;
	MSG msg;
	while( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0){ 
		if (bRet == -1) return 0;
		else{
			if(msg.message==WM_KEYDOWN||msg.message==WM_KEYUP){
				pmw->ProcessKeystrokes(NULL,&msg);
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	// 释放富文本控件资源
	//FreeLibrary(hRichEdit);

	//删除站点信息
	SAFE_DEL_ARR(AppOptions::pSiteInfoArr);

	// 关闭COM支持
	CoUninitialize();

	return msg.wParam;
}