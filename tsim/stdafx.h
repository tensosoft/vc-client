// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#include "targetver.h"

#ifndef STRICT
#define STRICT
#endif

#include "../commonfiles/discoverx2const.h"

#define WIN32_LEAN_AND_MEAN             // 从 Windows 头中排除极少使用的资料

// Windows API头文件:
#include <windows.h>
#include <shellapi.h>
#include <commctrl.h>
#include <shlwapi.h>
#include <shlobj.h>
//ATL头文件:
#include <atlbase.h>
#include <atlconv.h>
#include <atlwin.h>
#include <atlcomcli.h>
using namespace ATL;
// C 运行时头文件:
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <vector>
using namespace std;

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 某些 CString 构造函数将是显式的

// 主窗口常数定义
#define APP_CLASS L"TSIMAPP"							//应用程序类名
#define APP_TITLE L"腾硕办公助手"
#define MAINWIN_WIDTH  350
#define MAINWIN_HEIGHT 600

//任务栏图标常数定义
#define TRAYICONID	1000									//任务栏通知图标ID
#define SWM_TRAYMSG	WM_APP+1000						//任务栏通知图标发给主窗口的消息
#define SWM_SHOW	SWM_TRAYMSG + 1					//显示窗口
#define SWM_EXIT	SWM_TRAYMSG + 2					//关闭窗口
#define SWM_HOMEPAGE	SWM_TRAYMSG + 3			//打开门户主页
#define SWM_LOGON	SWM_TRAYMSG + 4					//登录
#define SWM_LOGOFF	SWM_TRAYMSG + 5				//注销
#define SWM_SEP	SWM_TRAYMSG + 15					//分隔符

#include "resource.h"