#pragma once

#ifndef _INFONOTIFY_H_
#define _INFONOTIFY_H_

#define WM_TASKBARNOTIFIERCLICKED	WM_APP+2000
#define TN_TEXT_NORMAL			0x0000
#define TN_TEXT_BOLD			0x0001
#define TN_TEXT_ITALIC			0x0002
#define TN_TEXT_UNDERLINE		0x0004

//消息提醒窗口定义
#define IN_TITLE  L"消息提醒"
#define IN_CLASS  L"TSINFONOTIFY"

class InfoNotify{
public:
	InfoNotify(void);
	~InfoNotify(void);

	// 注册窗口类
	static HRESULT RegisterClass();    
	// 窗口过程
	static LRESULT CALLBACK WindowProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam);
	//创建
	static InfoNotify* Create(HWND hWndParent);
	void Show(LPCTSTR szCaption,DWORD dwTimeToShow=500,DWORD dwTimeToLive=3000,DWORD dwTimeToHide=500,int nIncrement=1);
	void Hide();
	BOOL SetSkin(UINT nBitmapID,short red=-1,short green=-1,short blue=-1);
	BOOL SetSkin(LPCTSTR szFileName,short red=-1,short green=-1,short blue=-1);
	void SetTextFont(LPCTSTR szFont,int nSize,int nNormalStyle,int nSelectedStyle);
	void SetTextColor(COLORREF crNormalTextColor,COLORREF crSelectedTextColor);
	void SetTextRect(RECT rcText);
	int RedrawWindow();
	HWND						m_hWnd;
	HWND						m_hParentWnd;
	TCHAR						m_szUNID[STRLEN_SMALL];
	int							m_notifierType;

	HFONT						m_hNormalFont;
	HFONT						m_hSelectedFont;
	COLORREF				m_crNormalTextColor;
	COLORREF				m_crSelectedTextColor;
	HCURSOR 				m_hCursor;

	HRGN						m_hSkinRegion;							
	RECT						m_rcText;							
	int 						m_nSkinWidth;							
	int 						m_nSkinHeight;							
	HBITMAP 				m_biSkinBackground;

	TCHAR 					m_strCaption[STRLEN_1K];
	BOOL						m_bMouseIsOver;
	int 						m_nAnimStatus;

	DWORD 					m_dwTimeToShow;
	DWORD 					m_dwTimeToLive;
	DWORD 					m_dwTimeToHide;
	DWORD 					m_dwDelayBetweenShowEvents;
	DWORD 					m_dwDelayBetweenHideEvents;
	int 						m_nStartPosX;
	int 						m_nStartPosY;
	int 						m_nCurrentPosX;
	int 						m_nCurrentPosY;
	int 						m_nTaskbarPlacement;
	int 						m_nIncrement;
	int							m_tabIndex;
protected:
	BYTE* Get24BitPixels(HBITMAP pBitmap, WORD *pwWidth, WORD *pwHeight);
	HRGN GenerateRegion(HBITMAP hBitmap, BYTE red, BYTE green, BYTE blue);
public:
	int OnCreate(LPCREATESTRUCT lpCreateStruct);
	void OnDestroy();
	BOOL OnSetCursor(HWND hWndParent, UINT nHitTest, UINT message);
	void OnMouseMove(UINT nFlags, POINT point);
	void OnLButtonUp(UINT nFlags, POINT point);
	LRESULT OnMouseHover(WPARAM w, LPARAM l);
	LRESULT OnMouseLeave(WPARAM w, LPARAM l);
	BOOL OnEraseBkgnd(HDC hDC);
	void OnPaint();
	void OnTimer(UINT nIDEvent);
};

#endif