// rtcwin.h
#include "stdafx.h"
#include "mainwin.h"
#include "chatwin.h"
#include "options.h"
#include "cryptutil.h"
#include "msgrecorder.h"
#include <Sensapi.h>
#include <UrlMon.h>
#include <WinInet.h>

#pragma comment(lib,"Winmm")
#pragma comment(lib,"Ws2_32")
//检查网络是否正常的定时器ID
#define IDT_CHECKNETCONNECT		1
//获取通过网页发送的通知的定时器ID
#define IDT_CHECKNOTIFICATION		3
#define CHECKNETCONNECT_PERIOD 60000
#define CHECKNOTIFICATION_PERIOD 30000
#define INVALID_COLOR	0xFFFFFFFE

NOTIFYICONDATA	niData;	//通知栏消息

/*********** MainWin 成员 ************/
MainWin::MainWin(){
	m_blIsLogon=FALSE;
	m_blCanConnect=FALSE;
	m_presence=ID_FILE_PRESENCESTATUS_OFFLINE;
	m_webBrowserEvent.m_pMainWin=this;
	ZeroMemory(m_szSystemName,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(m_szBelong,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(m_szProvider,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(&m_userInfo,sizeof(UserInfo));
	ZeroMemory(m_curErrMsg,sizeof(TCHAR)*STRLEN_1K);
	ZeroMemory(m_requestip,sizeof(m_requestip));
	ZeroMemory(m_clientip,sizeof(m_clientip));
	ZeroMemory(m_serverip,sizeof(m_serverip));
	
	pVoice=NULL;
	pfslc=NULL;
}
MainWin::~MainWin(){
}

HRESULT MainWin::RegisterClass(){
	// 注册主窗口的窗口类
	WNDCLASSEX wc;
	ATOM atom;

	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	wc.cbSize				 = sizeof(wc);
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = (WNDPROC)MainWin::WindowProc;
	wc.hInstance     = GetModuleHandle(NULL);
	wc.hIcon         = LoadIcon(wc.hInstance,MAKEINTRESOURCE(IDI_TSIM));
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
	wc.lpszMenuName  = MAKEINTRESOURCE(IDR_MENU_APP);
	wc.lpszClassName = APP_CLASS;
	wc.hIconSm			 = LoadIcon(wc.hInstance,MAKEINTRESOURCE(IDI_SMALL));

	atom = ::RegisterClassEx( &wc );
	if ( !atom )
	{
		// 注册失败
		return E_FAIL;
	}

	return S_OK;
}
LRESULT CALLBACK MainWin::WindowProc(HWND hWnd, UINT uMsg,WPARAM wParam,LPARAM lParam){
	MainWin * me = NULL;
	LRESULT  lr = 0;
	if ( uMsg == WM_CREATE ){
		// 创建本类的实例
		me = new MainWin;
		me->m_hWnd=hWnd;
		// 保存类实例以供后续使用
		SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)me);
		me->OnCreate(uMsg,wParam,lParam);
	}else{
		// 从Windows的用户数据中提取本对象的实例
		me = (MainWin *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
		//消息处理
		switch( uMsg ){
		case SWM_TRAYMSG:	//通知栏消息
			switch(lParam)
			{
			case WM_LBUTTONDBLCLK:
				ShowWindow(hWnd, SW_RESTORE);
				break;
			case WM_LBUTTONDOWN:
			case WM_RBUTTONDOWN:
			case WM_CONTEXTMENU:
				me->ShowNotifyIconContextMenu();
				break;
			}
			break;
		case WM_DESTROY:
			me->OnDestroy(uMsg,wParam,lParam);
			delete me;					// 删除MainWin对象实例
			PostQuitMessage(0); // 发送退出消息
			break;
		case WM_CLOSE:
			me->OnClose(uMsg,wParam,lParam);
			break;
		case WM_SIZE:
			me->OnSize(uMsg,wParam,lParam);
			break;
		case WM_COMMAND:
			me->OnCommand(uMsg,wParam,lParam);
			break;
		case WM_NOTIFY:
			me->OnNotify(uMsg,wParam,lParam);
			break;
		case WM_ACTIVATEAPP:
			me->OnActiveApp(uMsg,wParam,lParam);
			break;
		case WM_GETMINMAXINFO:
			me->OnGetMinMaxInfo(uMsg,wParam,lParam);
			break;
		case WM_TIMER:
			me->OnTimer(uMsg,wParam,lParam);
			break;
		case WM_TASKBARNOTIFIERCLICKED:
			{
				InfoNotify *pin=(InfoNotify*)lParam;
				if(!pin) return 0;
				TCHAR* szUNID=pin->m_szUNID;
				//TCHAR *szUNID=(TCHAR*)lParam;
				NOTIFIERTYPE nt=(NOTIFIERTYPE)wParam;
				if(_tcslen(szUNID)>0 && nt==DOCUMENTLINK){
					TCHAR szCredential[STRLEN_1K]={0};
					me->BuildLoginCredential(szCredential,STRLEN_1K);
					TCHAR szJS[STRLEN_NORMAL]={0};
					_stprintf_s(szJS,L"tsimActions.opendoc('%s','%s');",szCredential,szUNID);
					BSTR bstrJS=T2BSTR(szJS);
					me->m_webBrowser.CallJavaScript(bstrJS);
					FREE_SYS_STR(bstrJS);
				}else{
					BringWindowToTop(hWnd);
					ShowWindow(hWnd, SW_RESTORE);
					MessageBox(hWnd,pin->m_strCaption,L"小助手",MB_OK|MB_ICONINFORMATION);
				}
				TCHAR szJS[MAX_PATH]={0};
				_stprintf_s(szJS,L"notification.setRead('%s');",szUNID);
				BSTR bstrJS=T2BSTR(szJS);
				me->m_webBrowser.CallJavaScript(bstrJS);
				FREE_SYS_STR(bstrJS);
			}
			return 0;
		case WM_NMRECEIVED:
			me->OnNetMsgReceived(uMsg,wParam,lParam);
			return 0;
		case WM_SENDSTATUSCHANGED:
			me->OnSendStatusChanged(uMsg,wParam,lParam);
			return 0;
		case WM_NETERROR:
			me->OnNetError(uMsg,wParam,lParam);
			return 0;
		case WM_SENDIM:
			me->OnSendIM(uMsg,wParam,lParam);
			return 0;
		default:
			lr = DefWindowProc( hWnd, uMsg, wParam, lParam );
		}	//switch end
	}//else end
	return lr;
}
LRESULT MainWin::OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam){
	WORD wID=LOWORD(wParam);
	switch(wID)
	{
	case SWM_LOGON:	
	case ID_FILE_LOGON:	//登录
		{
		BSTR bstrJS=T2BSTR(L"doLogin();");
		this->m_webBrowser.CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
		}
		break;
	case SWM_LOGOFF:
	case ID_FILE_LOGOFF:	//注销
		{
		BSTR bstrJS=T2BSTR(L"tsimActions.logout();");
		this->m_webBrowser.CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
		}
		break;
	case ID_FILE_EXIT:	//关闭
		PostMessage(m_hWnd, WM_CLOSE, 0, 0);
		break;
	case ID_FILEREFRESH:	//刷新
		{
		BSTR bstrJS=T2BSTR(L"tsimActions.reload();");
		this->m_webBrowser.CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
		}
		break;
	case ID_FILE_PRESENCESTATUS_OFFLINE:				//设置状态：离线
	case ID_FILE_PRESENCESTATUS_ONLINE:					//设置状态：在线
	case ID_FILE_PRESENCESTATUS_AWAY:						//设置状态：离开
	case ID_FILE_PRESENCESTATUS_IDLE:						//设置状态：空闲
	case ID_FILE_PRESENCESTATUS_BUSY:						//设置状态：忙碌
	case ID_FILE_PRESENCESTATUS_BERIGHTBACK:		//设置状态：马上回来
	case ID_FILE_PRESENCESTATUS_ONTHEPHONE:			//设置状态：电话中
	case ID_FILE_PRESENCESTATUS_OUTTOLUNCH:			//设置状态：外出就餐
		{
		TCHAR szSetPresenceJS[STRLEN_1K]={0};
		_stprintf_s(szSetPresenceJS,L"tsimActions.setPresence(%d);",wID);
		BSTR bstrSetPresenceJS=T2BSTR(szSetPresenceJS);
		this->m_webBrowser.CallJavaScript(bstrSetPresenceJS);
		FREE_SYS_STR(bstrSetPresenceJS)
		m_presence=wID;
		SetMenuState();
		}
		break;
	case SWM_SHOW:	//任务栏通知图标菜单：显示主窗口
		if (!IsWindowVisible(m_hWnd)) ShowWindow(m_hWnd, SW_SHOW);
		if (IsIconic(m_hWnd)) ShowWindow(m_hWnd,SW_RESTORE);
		SetForegroundWindow(m_hWnd);
		break;
	case SWM_EXIT:	//任务栏通知图标菜单：退出
		{
			if(!CheckSessionBeforeClose()) break;
			//注销
			if(m_blIsLogon){
				AppOptions* opt=AppOptions::GetInstance();
				if (MessageBox(m_hWnd,L"退出后将无法使用小助手的所有功能，是否确认要退出？",opt->Title,MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)==IDNO) break;
				BSTR bstrJS=T2BSTR(L"tsimActions.logout();");
				this->m_webBrowser.CallJavaScript(bstrJS);
				FREE_SYS_STR(bstrJS);
			}
			m_presence=ID_FILE_PRESENCESTATUS_OFFLINE;
			DestroyWindow(m_hWnd);
		}
		break;
	case ID_FILE_HOMEPAGE:
	case SWM_HOMEPAGE:			//打开门户首页
		{
		TCHAR szHomeJS[STRLEN_1K]={0};
		TCHAR szCredential[STRLEN_1K]={0};
		BuildLoginCredential(szCredential,STRLEN_1K);
		_stprintf_s(szHomeJS,L"tsimActions.home('%s');",szCredential);
		BSTR bstrHomeJS=T2BSTR(szHomeJS);
		this->m_webBrowser.CallJavaScript(bstrHomeJS);
		FREE_SYS_STR(bstrHomeJS);
		}
		break;
	case ID_FILEOPEN:				//打开：发送即时消息
		{
		BSTR bstrJS=T2BSTR(L"tsimActions.openimsession();");
		this->m_webBrowser.CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
		}
		break;
	case ID_HELP_CONTENT:		//打开帮助首页
		{
		BSTR bstrJS=T2BSTR(L"tsimActions.help();");
		this->m_webBrowser.CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
		}
		break;
	case ID_HELP_ABOUT:			//帮助：关于
		DialogBox(GetModuleHandle(NULL), (LPCTSTR)IDD_ABOUT, m_hWnd, (DLGPROC)About);
		break;
	case ID_TOOLS_OPTIONS:	//选项
		ShowOptionsDialog(m_hWnd,_T("选项"));
		break;
	case ID_TOOLS_SUBSCRIBE:	//订阅
		{
		BSTR bstrJS=T2BSTR(L"tsimActions.subscribe();");
		this->m_webBrowser.CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
		}
		break;
	}
	return 0;
}
LRESULT MainWin::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam){
	CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);

	//创建状态栏控件窗口
	m_hWndStatusbar = CreateStatusWindow(WS_CHILD | WS_VISIBLE,NULL,m_hWnd,IDC_STATUSBAR);
	if (!m_hWndStatusbar) return -1;

	RECT rc,rcStatusbar;
	GetClientRect(m_hWnd,&rc);
	GetClientRect(m_hWndStatusbar,&rcStatusbar);

	//创建WebBrowser控件容器窗口
	if (m_hWndStatic>0) DeleteWindow(m_hWndStatic);
	m_hWndStatic= CreateWindowEx(0,L"STATIC",L"", WS_CHILD | WS_VISIBLE,0, 0,(rc.right-rc.left),(rc.bottom-rc.top-(rcStatusbar.bottom-rcStatusbar.top)) ,m_hWnd, (HMENU)IDC_HEADERSTATIC, GetModuleHandle(NULL), NULL); 
	if (!m_hWndStatic) return -1;

	//设置菜单状态
	this->SetMenuState();
	HMENU hMenu = GetMenu(this->m_hWnd);
	HMENU hFileMenu = GetSubMenu(hMenu, 0);
	RemoveMenu(hFileMenu, 7, MF_BYPOSITION);
	RemoveMenu(hFileMenu, 7, MF_BYPOSITION);
	
	//设置自定义User-Agent
	TCHAR szUserAgent[STRLEN_NORMAL]={0};
	TCHAR szVer[STRLEN_SMALL]={0};
	GetVersionString(szVer,STRLEN_SMALL);
	int major,minor;
	GetIEVersion(&major,&minor);
	_stprintf_s(szUserAgent,L"TSIM/%s (compatible; MSIE %d.%d)",szVer,major,minor);
	USES_CONVERSION;
	char* userAgent=OLE2A(szUserAgent);
	UrlMkSetSessionOption(URLMON_OPTION_USERAGENT,userAgent,strlen(userAgent),0);

	//语音朗读对象
	if(CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&pVoice)!=S_OK){
		pVoice=NULL;
	}

	//ip地址
	WORD wVersionRequested;
	WSADATA wsaData;
	char Name[MAX_PATH]={0};
	PHOSTENT HostInfo;
	wVersionRequested = MAKEWORD(2, 2);
	if (WSAStartup( wVersionRequested, &wsaData ) == 0 ){
		if( gethostname ( Name, sizeof(Name)) == 0){
			if((HostInfo = gethostbyname(Name)) != NULL){
				int nCount = 0;
				while(HostInfo->h_addr_list[nCount]){
					char *ipaddr = inet_ntoa(*(struct in_addr *)HostInfo->h_addr_list[nCount]);
					USES_CONVERSION;
					if(nCount==0) _tcscpy_s(m_userInfo.ip,A2T(ipaddr));
					else {_tcscat_s(m_userInfo.ip,L";");_tcscat_s(m_userInfo.ip,A2T(ipaddr));}
					++nCount;
				}
			}
		}
		WSACleanup();
	}

	//文件发送监听线程
	pfslc=new FSLContext;
	ZeroMemory(pfslc,sizeof(FSLContext));
	pfslc->port=DEFAULT_PORT;
	pfslc->waiting=TRUE;
	pfslc->pmw=this;
	pfslc->thread=CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE)SendFileListener,(LPVOID)pfslc,0,&(pfslc->tid));

	//定时获取网页通知内容
	SetTimer(m_hWnd,IDT_CHECKNOTIFICATION,CHECKNOTIFICATION_PERIOD,NULL);
	return 0;
}
LRESULT MainWin::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam){
	KillTimer(m_hWnd,IDT_CHECKNETCONNECT);
	KillTimer(m_hWnd,IDT_CHECKNOTIFICATION);
	SAFE_RELEASE(pVoice);

	//解除ie嵌入
	this->m_webBrowser.UnCreateEmbeddedWebControl();

	CWItr itr=m_chatwininfo.begin();
	while(itr!=m_chatwininfo.end()){
		if(itr->second) {
			ChatWinInfo *pcwi=(ChatWinInfo*)(itr->second);
			if(pcwi && pcwi->hWnd) {DeleteWindow(pcwi->hWnd);}
		}
		itr++;
	}
	m_chatwininfo.clear();
	
	if(pfslc)pfslc->waiting=FALSE;
	//等待发送文件监听线程关闭
	if(pfslc->thread!=NULL){
		DWORD dwWaitRet=WaitForSingleObject(pfslc->thread,5000);
		switch(dwWaitRet){
		case WAIT_OBJECT_0:
			break;
		case WAIT_TIMEOUT:
		default:
			TerminateThread(pfslc->thread,0);
			break;
		}
	}
	SAFE_DEL_PTR(pfslc);

	//删除任务栏图标
	niData.uFlags = 0;
	Shell_NotifyIcon(NIM_DELETE,&niData);

	CoUninitialize();

	return 0;
}
LRESULT MainWin::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam){
	if(m_blIsLogon){
		ShowWindow(m_hWnd,SW_HIDE);
	}else{
		DestroyWindow(m_hWnd);
	}
	return 0;
}
LRESULT MainWin::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam){
	SetStatusText(this->m_blIsLogon?L"已登录":L"未登录");
	RECT rc,rcStatusbar;

	GetClientRect(m_hWnd, &rc);

	// 设置状态栏大小
	SendMessage(m_hWndStatusbar, uMsg, wParam, lParam);
	GetClientRect(m_hWndStatusbar, &rcStatusbar);
	
	int winh=(int)(short)HIWORD(lParam);
	//设置容器控件大小
	int staticw=(int)(short)LOWORD(lParam);
	int statich=(rc.bottom-rc.top-(rcStatusbar.bottom-rcStatusbar.top));
	SetWindowPos(m_hWndStatic,HWND_TOP,0,0,staticw,statich,SWP_SHOWWINDOW);

	//嵌入浏览器对象（WebBrowser)
	if (!m_webBrowser.mpWebObject){
		m_webBrowser.hwnd=m_hWndStatic;
		m_webBrowser.m_hParentWnd=m_hWnd;
	
		TCHAR szUrl[STRLEN_1K]={0};
		if(_tcslen(AppOptions::szCurrentSiteUrl)>0 && !_tcsstr(AppOptions::szCurrentSiteUrl,L"res://")){
			_stprintf_s(szUrl,L"http://%s/tsim/login%s",AppOptions::szCurrentSiteUrl,AppOptions::szCurrentSiteUrlExt);
			m_blCanConnect=CheckUrlConnectable(szUrl);
			//定时检查网络是否可用
			SetTimer(m_hWnd,IDT_CHECKNETCONNECT,CHECKNETCONNECT_PERIOD,NULL);
		}else{
			_stprintf_s(szUrl,L"%s",AppOptions::szCurrentSiteUrl);
			m_blCanConnect=TRUE;
		}
		if (this->m_blCanConnect){
			this->m_webBrowser.CreateEmbeddedWebControl(szUrl);
		}else {
			ShowNetErrPage(NULL);
		}

		m_webBrowserEvent.host=&m_webBrowser;
		m_webBrowser.EventAdivse(&m_webBrowserEvent);
	}else {
		m_webBrowser.Resize();
	}
	AppOptions* appOpts=AppOptions::GetInstance();
	appOpts->SaveMainWinSize(staticw,winh);

	return 0;
}
LRESULT MainWin::OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam){
	return 0;
}
LRESULT MainWin::OnActiveApp(UINT uMsg, WPARAM wParam, LPARAM lParam){
	return 0;
}
LRESULT MainWin::OnGetMinMaxInfo(UINT uMsg, WPARAM wParam, LPARAM lParam){
	LPMINMAXINFO p=(LPMINMAXINFO)lParam;

	p->ptMinTrackSize.x = MAINWIN_WIDTH;
	p->ptMinTrackSize.y = MAINWIN_HEIGHT;

	return 0;
}
void MainWin::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam){
	switch((UINT)wParam){
	case IDT_CHECKNETCONNECT:
		{
			TCHAR szUrl[STRLEN_1K]={0};
			if(_tcslen(AppOptions::szCurrentSiteUrl)>0 && !_tcsstr(AppOptions::szCurrentSiteUrl,L"res://")){
				_stprintf_s(szUrl,L"http://%s/tsim/login%s",AppOptions::szCurrentSiteUrl,AppOptions::szCurrentSiteUrlExt);
				m_blCanConnect=CheckUrlConnectable(szUrl);
			}
			if (!this->m_blCanConnect) ShowNetErrPage(NULL);
		}
		break;
	case IDT_CHECKNOTIFICATION:
		if(!this->m_blIsLogon) break;
		{
			IWebBrowser2* pBrowser=NULL;
			IHTMLDocument* pDoc=NULL;
			IDispatch* pScript = NULL;
			DISPID idMethod = 0;
			OLECHAR FAR* sMethod = L"checkNotifier";
			DISPPARAMS dispArgs = {NULL,NULL, 0, 0};
			VARIANT ret;
			VariantInit(&ret);
			HRESULT hr=m_webBrowser.mpWebObject->QueryInterface(IID_IWebBrowser2,(void**)&pBrowser);
			if (FAILED(hr)) break;
			hr=pBrowser->get_Document((IDispatch**)&pDoc);
			if (FAILED(hr)||pDoc==NULL) goto end;
			hr=pDoc->get_Script(&pScript);
			if (FAILED(hr)||pScript==NULL)goto end;
			hr = pScript->GetIDsOfNames(IID_NULL, &sMethod, 1, LOCALE_SYSTEM_DEFAULT,&idMethod);
			if (FAILED(hr))goto end;
			hr = pScript->Invoke(idMethod, IID_NULL, LOCALE_SYSTEM_DEFAULT, DISPATCH_METHOD,&dispArgs, &ret, NULL, NULL);
			int l=0; 
			if(ret.vt==VT_BSTR && (l=_tcslen(ret.bstrVal))>0){
				TCHAR *sz1=new TCHAR[l+2];
				TCHAR *sz2=NULL;
				TCHAR* tok=NULL;
				const TCHAR* sep=L"`|~";
				memset(sz1,0,sizeof(TCHAR)*(l+2));
				_tcscpy_s(sz1,l+2,ret.bstrVal);
				sz2=_tcstok_s(sz1,sep,&tok);
				while(sz2!=NULL){
					ReceivedMessage rmsg;
					ZeroMemory(&rmsg,sizeof(rmsg));
					rmsg.szMsg=new TCHAR[l-30];
					ZeroMemory(rmsg.szMsg,sizeof(TCHAR)*(l-30));
					rmsg.subType=NOTIFIER;
					rmsg.notifyType=DOCUMENTLINK;
					_tcsncpy_s(rmsg.szUNID,sz2,32);
					if(sz2[32]==L':' && sz2[34]==L':') {
						rmsg.notifyType=(sz2[33]==L'1'?USERSTATE:sz2[33]==L'2'?SYSTEMBROADCASE:0);
						_tcscpy_s(rmsg.szMsg,l-30,&sz2[35]);
					}else{
						_tcscpy_s(rmsg.szMsg,l-30,&sz2[33]);
					}
					OnReceivedNotifier(rmsg);
					SAFE_DEL_ARR(rmsg.szMsg);
					sz2=_tcstok_s(NULL,sep,&tok);
				}
				SAFE_DEL_ARR(sz1);
			}
end:
			VariantClear(&ret);
			SAFE_RELEASE(pScript);
			SAFE_RELEASE(pDoc);
			SAFE_RELEASE(pBrowser);
		}
		break;
	}
}
LRESULT MainWin::OnNetMsgReceived(UINT uMsg, WPARAM wParam, LPARAM lParam){
	TCHAR* content=(TCHAR*)lParam;
	ReceivedMessage rmsg;
	ZeroMemory(&rmsg,sizeof(rmsg));
	if(!ParseReceivedMessage(content,&rmsg)) goto end;

	if(rmsg.subType==NOTIFIER && _tcslen(rmsg.szUNID)>0){
		OnReceivedNotifier(rmsg);
	}else if(rmsg.subType==STYLE && _tcslen(rmsg.szMsg)>0){
		OnReceivedStyle(rmsg);
	}else if(rmsg.subType==FILETRANSFER && _tcslen(rmsg.szMsg)>0){
		OnFileTransfer(rmsg);
	}else{
		OnReceivedIM(rmsg);
	}

end:
	SAFE_DEL_ARR(rmsg.szMsg);
	HeapFree(GetProcessHeap(),HEAP_NO_SERIALIZE,(void*)lParam);
	return 0;
}
LRESULT MainWin::OnSendStatusChanged(UINT uMsg, WPARAM wParam, LPARAM lParam){
	TCHAR* sz=(TCHAR*)lParam;
	SendStatus ss={0};
	if(ParseSendStatus(sz,&ss)){
		CWItr itr=m_chatwininfo.find(ss.ipv);
		if(itr!=m_chatwininfo.end() && itr->second->hWnd){
			PostMessage(itr->second->hWnd,WM_SENDSTATUSCHANGED,(WPARAM)ss.msgId,(LPARAM)ss.code);
		}
	}
	HeapFree(GetProcessHeap(),HEAP_NO_SERIALIZE,(void*)lParam);
	return 0;
}
LRESULT MainWin::OnNetError(UINT uMsg, WPARAM wParam, LPARAM lParam){
	//LONG errCode=(LONG)wParam;
	//TCHAR t[100]={0};
	//_stprintf_s(t,L"errcode=%d",errCode);
	//TipMsg(t);
	BSTR bstrErrDesc=(BSTR)lParam;
	if(_tcslen(bstrErrDesc)>0){
		ErrMsg(0,bstrErrDesc);
	}
	return 0;
}
LRESULT MainWin::OnSendIM(UINT uMsg, WPARAM wParam, LPARAM lParam){
	TCHAR* szSendContent=(TCHAR*)lParam;
	size_t l=_tcslen(szSendContent);
	if(l==0) return 0;
	l+=MAX_PATH;

	ULONG ipv=(ULONG)wParam;
	CWItr itr=m_chatwininfo.find(ipv);
	if(itr==m_chatwininfo.end()) {
		SAFE_DEL_PTR(szSendContent);
		ErrMsg(0,L"无法获取接收者信息！");
		return 0;
	}

	TCHAR* szIP=itr->second->ip;
	if(_tcslen(szIP)==0) {
		SAFE_DEL_PTR(szSendContent);
		ErrMsg(0,L"无法获取接收者目标地址信息！");
		return 0;
	}
	
	int intVal=0;
	VARIANT ret;
	IWebBrowser2* iBrowser=NULL;
	IHTMLDocument2	*htmlDoc2=NULL;
	IDispatch* pScript = NULL;
	DISPID idMethod = 0;
	OLECHAR FAR* sMethod = L"sendIM";
	VARIANTARG args[2];
	VariantInit(&args[0]);
	VariantInit(&args[1]);

  HRESULT hr=m_webBrowser.mpWebObject->QueryInterface(IID_IWebBrowser2,(void**)&iBrowser);
	if (FAILED(hr)) return S_OK;
	
	hr=iBrowser->get_Document((IDispatch**)&htmlDoc2);
	if (FAILED(hr)) goto end;
	
	hr=htmlDoc2->get_Script(&pScript);
	if (FAILED(hr)) goto end;
	hr = pScript->GetIDsOfNames(IID_NULL, &sMethod, 1, LOCALE_SYSTEM_DEFAULT,&idMethod);
	if (SUCCEEDED(hr)) {
		args[0].vt=VT_BSTR;
		args[0].bstrVal=SysAllocString(szIP);
		
		args[1].vt=VT_BSTR;
		args[1].bstrVal=SysAllocString(szSendContent);

		DISPPARAMS dpNoArgs = {args,NULL, sizeof(args)/sizeof(args[0]), 0};
		hr = pScript->Invoke(idMethod, IID_NULL, LOCALE_SYSTEM_DEFAULT, DISPATCH_METHOD,&dpNoArgs, &ret, NULL, NULL);
		intVal=ret.intVal;
		if(intVal!=0 || ret.vt==VT_I4){
			PostMessage(itr->second->hWnd,WM_GET_SEND_MSGID,(WPARAM)intVal,lParam);
		}
	}
end:
	VariantClear(&args[0]);
	VariantClear(&args[1]);
	SAFE_RELEASE(pScript);
	SAFE_RELEASE(htmlDoc2);
	SAFE_RELEASE(iBrowser);
	if(intVal==0){
		ErrMsg(0,L"无法获取发送的消息的唯一标识，请联系管理员！");
	}
	return 0;
}
void MainWin::ShowNotifyIconContextMenu(){
	POINT pt;
	GetCursorPos(&pt);
	HMENU hMenu = CreatePopupMenu();
	if(hMenu)
	{
		AppOptions* appOpts = AppOptions::GetInstance();
		InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_HOMEPAGE, _T("打开办公门户首页(&A)"));
		//InsertMenu(hMenu, -1, MF_BYPOSITION|MF_SEPARATOR, SWM_SEP, _T(""));
		//InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_LOGON, _T("登录(&S)"));
		if(!appOpts->AutoLogon) InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_LOGOFF, _T("注销(&D)"));
		
		//EnableMenuItem(hMenu, SWM_LOGON, this->m_blIsLogon?MF_GRAYED:MF_ENABLED);
		EnableMenuItem(hMenu, SWM_HOMEPAGE, this->m_blIsLogon?MF_ENABLED:MF_GRAYED);
		if(!appOpts->AutoLogon) EnableMenuItem(hMenu, SWM_LOGOFF, this->m_blIsLogon ? MF_ENABLED : MF_GRAYED);

		InsertMenu(hMenu, -1, MF_BYPOSITION|MF_SEPARATOR, SWM_SEP, _T(""));
		InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_SHOW, _T("打开办公助手(&C)"));
		InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_EXIT, _T("退出办公助手(&X)"));

		MENUITEMINFO mi;
		ZeroMemory(&mi,sizeof(MENUITEMINFO));
		mi.cbSize=sizeof(MENUITEMINFO);
		mi.fMask=MIIM_STATE;
		mi.fState=MFS_DEFAULT;
		SetMenuItemInfo(hMenu,SWM_SHOW,FALSE,&mi);

		// 注意：要设置窗口为前端窗口否则菜单不会自动关闭
		SetForegroundWindow(this->m_hWnd);

		TrackPopupMenu(hMenu, TPM_BOTTOMALIGN,pt.x, pt.y, 0, this->m_hWnd, NULL);
		DestroyMenu(hMenu);
	}
}
LRESULT CALLBACK MainWin::About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		{
			AppOptions* appOpts=AppOptions::GetInstance();
			TCHAR title[STRLEN_DEFAULT]={0};
			_stprintf_s(title,_T("关于 - %s - 小助手"),appOpts->Title);
			SetWindowText(hDlg,title);
			SetDlgItemText(hDlg,IDC_ABOUTTITLE,appOpts->Title);
			TCHAR desc[STRLEN_1K]={0};
			TCHAR szVer[STRLEN_NORMAL]={0};
			MainWin::GetVersionString(szVer,STRLEN_NORMAL);
			_stprintf_s(desc,_T("授权给：%s\r\n版本：%s\r\n(C) %s 保留所有权利。"),appOpts->Belong,szVer,appOpts->Provider);
			SetDlgItemText(hDlg,IDC_ABOUTDESC,desc);
			return TRUE;
		}
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK) 
		{
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
		}
		break;
	}
	return FALSE;
}
int MainWin::GetVersionString(TCHAR* szVersion,int cchVersionLen){
	TCHAR fn[MAX_PATH]={0};
	int len=GetModuleFileName(NULL,fn,MAX_PATH);
	int ret=0;
	if (len>0){
		DWORD dwHandle=0;
		TCHAR ver[STRLEN_SMALL]={0};
		DWORD cchver = GetFileVersionInfoSize(fn,&dwHandle);
		if (cchver == 0) return 0;
		char* pver = new char[cchver];
		BOOL bret = GetFileVersionInfo(fn,dwHandle,cchver,pver);
		if (!bret) return 0;
		UINT uLen;
		void *pbuf;
		VS_FIXEDFILEINFO pvsf;
		bret = VerQueryValue(pver,L"\\",&pbuf,&uLen);
		if (!bret) return 0;
		memcpy(&pvsf,pbuf,sizeof(VS_FIXEDFILEINFO));
		TCHAR szVer[STRLEN_SMALL]={0};
		int j=0;
		_stprintf_s(szVer,_T("%u.%u.%u.%u"),HIWORD(pvsf.dwProductVersionMS),LOWORD(pvsf.dwProductVersionMS),HIWORD(pvsf.dwProductVersionLS),LOWORD(pvsf.dwProductVersionLS));
		delete[] pver;
		size_t verlen=_tcslen(szVer);
		ZeroMemory(szVersion,cchVersionLen*sizeof(TCHAR));
		if (verlen<=cchVersionLen){
			_tcscpy_s(szVersion,cchVersionLen,szVer);
			ret=verlen;
		}
		else{
			_tcscpy_s(szVersion,cchVersionLen,szVer);
			ret=_tcslen(szVersion);
		}
	}
	return ret;
}
int MainWin::GetIEVersion(int* major,int* minor){
	LONG lResult;
	int iPos,iPos2;
	HKEY hKey;
	
	TCHAR szVAL[STRLEN_NORMAL],szTemp[5];
	TCHAR *pDec,*pDec2;
	DWORD dwSize=STRLEN_NORMAL*sizeof(TCHAR),dwType;

	lResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE,L"SOFTWARE\\Microsoft\\Internet Explorer",0,KEY_QUERY_VALUE,&hKey);
	if(lResult != ERROR_SUCCESS) return lResult;

	lResult=RegQueryValueEx(hKey,L"Version",NULL,&dwType, LPBYTE(szVAL),&dwSize);
	if(lResult != ERROR_SUCCESS){
		RegCloseKey(hKey);
	  return lResult;
	}

	lResult=RegCloseKey(hKey);

	//提取主板本号
	pDec=_tcsstr(szVAL,L".");
	if(pDec==NULL) return -1;
	iPos=pDec-szVAL+1;
	ZeroMemory(szTemp,5*sizeof(TCHAR));
	_tcsncpy_s(szTemp,szVAL,iPos-1);
	*major=_tstoi(szTemp);

	//提取次版本号
	pDec++;
	pDec2=_tcsstr(pDec,L".");
	if(pDec2==NULL){
		*minor=0;
		return -1;
	}
	iPos2=pDec2-szVAL+1;
	ZeroMemory(szTemp,5*sizeof(TCHAR));
	_tcsncpy_s(szTemp,pDec,iPos2-iPos-1);
	*minor=_tstoi(szTemp);

	return 0;
}
BOOL MainWin::CheckUrlConnectable(const TCHAR* const szUrl){
	//DWORD dwFlags =0;
	//BOOL isNetworkAlive=IsNetworkAlive(&dwFlags);	//OR IcmpSendEcho ？
	//if (!isNetworkAlive || dwFlags!=NETWORK_ALIVE_LAN ) return FALSE;
	BOOL ret=InternetCheckConnection(szUrl,FLAG_ICC_FORCE_CONNECTION ,0);
	if (GetLastError()==ERROR_NOT_CONNECTED) ret=FALSE;
	return ret;
}
ULONG MainWin::GetIPValue(TCHAR* szIP){
	if(!szIP || _tcslen(szIP)==0) return 0;
	USES_CONVERSION;
	char* ip=T2A(szIP);
	return inet_addr(ip);
}
__int64 MainWin::TimeDiff(SYSTEMTIME st1,SYSTEMTIME st2){
	ULARGE_INTEGER nFrom, nTo;
	ULONGLONG n64From, n64To;
	__int64 nDiff;
	
	FILETIME fileTimeTemp;
	float fReturn = 0.0f;

	SystemTimeToFileTime(&st1, &fileTimeTemp);
	nFrom.HighPart = fileTimeTemp.dwHighDateTime;
	nFrom.LowPart = fileTimeTemp.dwLowDateTime;
	n64From = nFrom.QuadPart;

	SystemTimeToFileTime(&st2, &fileTimeTemp);
	nTo.HighPart = fileTimeTemp.dwHighDateTime;
	nTo.LowPart = fileTimeTemp.dwLowDateTime;
	n64To =nTo.QuadPart;

	nDiff = (n64From-n64To)/10000000;

	return nDiff;
}
BOOL MainWin::ParseSendStatus(TCHAR* s,SendStatus* pss){
	size_t sl=_tcslen(s);
	if(sl==0 || pss==NULL) return FALSE;
	int idx=0;
	TCHAR szIP[STRLEN_SMALL]={0};
	for (LPTSTR pszz =(LPTSTR)s; *pszz; pszz += _tcslen(pszz) + 1) {
		switch(idx){
		case 0:	//msgid
			{
				TCHAR szMsgid[STRLEN_SMALL]={0};
				_tcscpy_s(szMsgid,pszz);
				pss->msgId=_tcstoul(szMsgid,NULL,10);
			}
			break;
		case 1: //statuscode
			{
				TCHAR szSS[STRLEN_SMALL]={0};
				_tcscpy_s(szSS,pszz);
				pss->code=_tcstoul(szSS,NULL,10);
			}
			break;
		case 2: //ip
			{
				TCHAR szIP[STRLEN_SMALL]={0};
				_tcscpy_s(szIP,pszz);
				pss->ipv=MainWin::GetIPValue(szIP);
			}
			break;
		}
		idx++;
	}
	return TRUE;
}
BOOL MainWin::ParseReceivedMessage(TCHAR* content,ReceivedMessage* prm){
	if(!content || _tcslen(content)==0||!prm) return FALSE;
	int idx=0;
	for (LPTSTR pszz = content; *pszz; pszz += _tcslen(pszz) + 1) {
		switch(idx){
		case 0:	//msgid
			{
			TCHAR szMsgId[STRLEN_SMALL]={0};
			_tcscpy_s(szMsgId,pszz);
			prm->msgId=_tcstoul(szMsgId,NULL,10);
			}
			break;
		case 1:	//src
			_tcscpy_s(prm->szFrom,pszz);
			break;
		case 2:	//dt
			_tcscpy_s(prm->szDT,pszz);
			break;
		case 3:	//msg
			if(_tcsstr(pszz,NOTIFIER_PREFIX)==pszz){	//notifier
				prm->subType=NOTIFIER;
				prm->notifyType=DOCUMENTLINK;
				_tcsncpy_s(prm->szUNID,pszz+_tcslen(NOTIFIER_PREFIX)+1,32);
				size_t l=_tcslen(pszz)-_tcslen(NOTIFIER_PREFIX)-_tcslen(prm->szUNID)+1;
				prm->szMsg=new TCHAR[l];
				ZeroMemory(prm->szMsg,l*sizeof(TCHAR));
				if(pszz[65]==L':' && pszz[67]==L':'){
					prm->notifyType=(pszz[66]==L'1'?USERSTATE:pszz[66]==L'2'?SYSTEMBROADCASE:0);
					_tcscpy_s(prm->szMsg,l,&pszz[68]);
				}else{
					_tcscpy_s(prm->szMsg,l,&pszz[66]);
				}
				//TCHAR* sz=new TCHAR[l];
				//ZeroMemory(sz,l*sizeof(TCHAR));
				//_tcscpy_s(sz,l,pszz+_tcslen(NOTIFIER_PREFIX)+1+_tcslen(prm->szUNID)+1);
				//TCHAR* tmp=_tcschr(sz,L':');
				//if(tmp!=NULL) {
				//	*tmp=L'\0';
				//	prm->notifyType=_tcstol(sz,NULL,10);
				//	_tcscpy_s(prm->szMsg,l,tmp+1);
				//}else{
				//	prm->notifyType=DOCUMENTLINK;
				//	_tcscpy_s(prm->szMsg,l,sz);
				//}
				//SAFE_DEL_ARR(sz);
			}else if(_tcsstr(pszz,STYLE_PREFIX)==pszz){	//style
				size_t l=_tcslen(pszz)-_tcslen(STYLE_PREFIX)+1;
				prm->szMsg=new TCHAR[l];
				ZeroMemory(prm->szMsg,l*sizeof(TCHAR));
				_tcscpy_s(prm->szMsg,l,pszz+_tcslen(STYLE_PREFIX)+1);
				prm->subType=STYLE;
			}else if(_tcsstr(pszz,FILE_TRANSFER_PREFIX)==pszz){	//filetransfer
				size_t l=_tcslen(pszz)-_tcslen(FILE_TRANSFER_PREFIX)+1;
				prm->szMsg=new TCHAR[l];
				ZeroMemory(prm->szMsg,l*sizeof(TCHAR));
				_tcscpy_s(prm->szMsg,l,pszz+_tcslen(FILE_TRANSFER_PREFIX)+1);
				prm->subType=FILETRANSFER;
			}else{												//im
				size_t l=_tcslen(pszz)+1;
				prm->szMsg=new TCHAR[l];
				ZeroMemory(prm->szMsg,l*sizeof(TCHAR));
				_tcscpy_s(prm->szMsg,l,pszz);
			}
			break;
		}
		idx++;
	}
	return TRUE;
}
void MainWin::SetChatWinCaption(ChatWinInfo* pcwi,HWND hWnd){
	if(!hWnd || !pcwi) return;
	TCHAR caption[MAX_PATH]={0};
	_stprintf_s(caption,L"正在与%s(%s)进行即时沟通",pcwi->cn,pcwi->ou0);
	SetWindowText(hWnd,caption);
}
BOOL MainWin::GetLocalMsgId(TCHAR* result,size_t l){
	if(result==NULL || l==0) return FALSE;
	SYSTEMTIME st;
	GetLocalTime(&st);
	FILETIME ft;
	SystemTimeToFileTime(&st, &ft);
	ZeroMemory(result,sizeof(TCHAR)*l);
	_stprintf_s(result,l,L"%u%u",ft.dwHighDateTime,ft.dwLowDateTime);
	return TRUE;
}
BOOL MainWin::CheckSessionBeforeClose(){
	CWItr itr=m_chatwininfo.begin();
	while(itr!=m_chatwininfo.end()){
		if(itr->second) {
			ChatWinInfo *pcwi=(ChatWinInfo*)(itr->second);
			if(pcwi && pcwi->hWnd) {
				ChatWin* pcw=(ChatWin*)GetWindowLongPtr(pcwi->hWnd, GWLP_USERDATA);
				if(pcw==NULL) continue;
				if(!pcw->m_sendinfo.empty()){
					TCHAR tip[STRLEN_1K]={0};
					_stprintf_s(tip,L"退出此程序将导致发送给“%s”的部分消息无法发送，是否确定要关闭？",pcw->m_targetCN);
					if (MessageBox(this->m_hWnd,tip,L"关闭确认",MB_OKCANCEL|MB_ICONQUESTION|MB_DEFBUTTON2)==IDCANCEL) return FALSE;
				}
				if(!pcw->m_ftc.empty()){
					TCHAR tip[STRLEN_1K]={0};
					_stprintf_s(tip,L"退出此程序将导致与“%s”的文件传输过程出现异常，是否确定要关闭？",pcw->m_targetCN);
					if (MessageBox(this->m_hWnd,tip,L"关闭确认",MB_OKCANCEL|MB_ICONQUESTION|MB_DEFBUTTON2)==IDCANCEL) return FALSE;
				}
			}
		}
		itr++;
	}
	return TRUE;
}
HRESULT MainWin::ProcessKeystrokes(IOleObject* pObj, MSG* msg){
	if(msg==NULL || !((msg->wParam>=VK_LEFT && msg->wParam<=VK_DOWN) || msg->wParam==VK_RETURN || msg->wParam==VK_TAB|| GetKeyState(VK_CONTROL)<0)) return 0;
	
	CComPtr<IWebBrowser2> spWebBrowser2=NULL;
	HWND root=GetAncestor(msg->hwnd,GA_ROOTOWNER);
	if(root==this->m_hWnd && this->m_webBrowser.mpWebObject){
		if(this->m_webBrowser.mpWebObject->QueryInterface(IID_IWebBrowser2,(void**)&spWebBrowser2)==S_OK){
			IOleInPlaceActiveObject* pIOIPAO=NULL;
			HRESULT hr = spWebBrowser2->QueryInterface(IID_IOleInPlaceActiveObject,(LPVOID *)&pIOIPAO);
			if (SUCCEEDED(hr)) {
				hr=pIOIPAO->TranslateAccelerator(msg);
				pIOIPAO->Release();
				return hr;
			}
		}
	}else if(root!=NULL){
		ChatWin *pcw=(ChatWin*)GetWindowLongPtr(root, GWLP_USERDATA);
		if(pcw && pcw->m_webBrowserReady && pcw->m_webBrowser.mpWebObject){
			if(pcw->m_webBrowser.mpWebObject->QueryInterface(IID_IWebBrowser2,(void**)&spWebBrowser2)==S_OK){
				IOleInPlaceActiveObject* pIOIPAO=NULL;
				HRESULT hr = spWebBrowser2->QueryInterface(IID_IOleInPlaceActiveObject,(LPVOID *)&pIOIPAO);
				if (SUCCEEDED(hr)) {
					hr=pIOIPAO->TranslateAccelerator(msg);
					pIOIPAO->Release();
					return hr;
				}
			}
		}
	}

	return S_FALSE;
}
void MainWin::SetStatusText(TCHAR* szText){
	SetWindowText(m_hWndStatusbar, szText);
	InvalidateRect(m_hWndStatusbar, NULL, FALSE);
}
void MainWin::SetMenuState(){
	HMENU hMenu=GetMenu(this->m_hWnd);
	EnableMenuItem(hMenu,ID_FILE_LOGON,this->m_blIsLogon?MF_GRAYED:MF_ENABLED);
	EnableMenuItem(hMenu,ID_FILE_LOGOFF,this->m_blIsLogon?MF_ENABLED:MF_GRAYED);
	EnableMenuItem(hMenu,ID_FILE_HOMEPAGE,this->m_blIsLogon?MF_ENABLED:MF_GRAYED);
	//EnableMenuItem(hMenu,ID_FILEREFRESH,this->m_blIsLogon?MF_ENABLED:MF_GRAYED);
	EnableMenuItem(hMenu,ID_FILEOPEN,this->m_blIsLogon?MF_ENABLED:MF_GRAYED);
	EnableMenuItem(hMenu,ID_TOOLS_SUBSCRIBE,this->m_blIsLogon?MF_ENABLED:MF_GRAYED);

	HMENU hFileMenu=GetSubMenu(hMenu,0);
	HMENU hPresenceMenu=GetSubMenu(hFileMenu,7);
	MENUITEMINFO mii7;
	mii7.cbSize=sizeof(MENUITEMINFO);
	mii7.fMask=MIIM_STATE;
	mii7.fState=(this->m_blIsLogon?MF_ENABLED:MF_GRAYED);
	SetMenuItemInfo(hFileMenu,7,TRUE,&mii7);
	for(WORD i=ID_FILE_PRESENCESTATUS_OFFLINE;i<=ID_FILE_PRESENCESTATUS_OUTTOLUNCH;i++){
		MENUITEMINFO mii;
		mii.cbSize=sizeof(MENUITEMINFO);
		mii.fMask=MIIM_STATE;
		mii.fState=(this->m_presence==i?MF_CHECKED:MF_UNCHECKED);
		SetMenuItemInfo(hPresenceMenu,i,FALSE,&mii);
	}
	EnableMenuItem(hPresenceMenu,ID_FILE_PRESENCESTATUS_OFFLINE,MF_BYCOMMAND|MF_GRAYED);
}
void MainWin::ShowNetErrPage(TCHAR* szMsg){
	ZeroMemory(m_curErrMsg,sizeof(TCHAR)*STRLEN_1K);
	if(szMsg!=NULL) _tcscpy_s(m_curErrMsg,szMsg);
	static TCHAR szErrUrl[STRLEN_DEFAULT]={0};
	if(_tcslen(szErrUrl)==0){
		TCHAR fn[MAX_PATH]={0};
		GetModuleFileName(NULL,fn,MAX_PATH);
		_stprintf_s(szErrUrl,L"res://%s/IDH_NET_ERROR",fn);
	}
	if(this->m_webBrowser.mpWebObject!=NULL){
		this->m_webBrowser.NavigateTo(szErrUrl);
	}else{
		this->m_webBrowser.CreateEmbeddedWebControl(szErrUrl);
	}
}
int MainWin::BuildLoginCredential(TCHAR* szOut,size_t cchOut){
	if(szOut==NULL || cchOut<=0) return -1;
	ZeroMemory(szOut,sizeof(TCHAR)*cchOut);
	AppOptions* appOpts=AppOptions::GetInstance();
	if(_tcslen(appOpts->UserName)==0||_tcslen(appOpts->Password)==0) return 0;

	TCHAR szPwdDecrypted[MAX_PATH]={0};
	size_t sPwdDecrypted=MAX_PATH;
	Decrypt(appOpts->Password,szPwdDecrypted,&sPwdDecrypted);

	TCHAR szPlain[STRLEN_DEFAULT]={0};
	_stprintf_s(szPlain,L"%s:%s",appOpts->UserName,szPwdDecrypted);

	TCHAR szEncrypted[STRLEN_1K]={0};
	size_t szEncryptedLen=STRLEN_1K;
	TCHAR szKey[STRLEN_SMALL]={0};
	SYSTEMTIME st;
	FILETIME ft;
	GetSystemTime(&st);
	SystemTimeToFileTime(&st,&ft);

	ULONGLONG qwResult=(((ULONGLONG)ft.dwHighDateTime) << 32) + ft.dwLowDateTime;
	_stprintf_s(szKey,L"%lld",qwResult);
	int offset=((_tcslen(szKey)>8)?_tcslen(szKey)-8:0);
	//TipMsg(szKey);
	Encrypt(szPlain,szEncrypted,&szEncryptedLen,szKey+offset);

	BYTE bs[8]={0};
	bs[0] = (BYTE)(ft.dwLowDateTime & 0x000000ff);
	bs[1] = (BYTE)((ft.dwLowDateTime & 0x0000ff00) >> 8);
	bs[2] = (BYTE)((ft.dwLowDateTime & 0x00ff0000) >> 16);
	bs[3] = (BYTE)((ft.dwLowDateTime & 0xff000000) >> 24);
	bs[4] = (BYTE)(ft.dwHighDateTime & 0x000000ff) ^ bs[0];
	bs[5] = (BYTE)((ft.dwHighDateTime & 0x0000ff00) >> 8) ^ bs[1];
	bs[6] = (BYTE)((ft.dwHighDateTime & 0x00ff0000) >> 16) ^ bs[2];
	bs[7] = (BYTE)((ft.dwHighDateTime & 0xff000000) >> 24) ^ bs[3];
	TCHAR szBsHex[STRLEN_SMALL]={0};
	size_t szBsHexLen=STRLEN_SMALL;
	ToHexString(bs,8,szBsHex,&szBsHexLen);
	
	size_t minLen=_tcslen(szEncrypted)+_tcslen(szBsHex)+1;
	if(cchOut>=minLen){
		size_t idx=0;
		for(size_t i=0;i<_tcslen(szBsHex);i++){
			*(szOut+idx)=szBsHex[i];
			idx++;
		}
		for(size_t i=0;i<_tcslen(szEncrypted);i++){
			*(szOut+idx)=szEncrypted[i];
			idx++;
		}
		//TipMsg(szOut);
		return _tcslen(szOut);
	}
	else return minLen; 
}
void MainWin::PlayWaveResource(UINT resID,LPCTSTR rtName){
	if (resID<=0) return;
	HMODULE h=GetModuleHandle(NULL);
	HRSRC hr=FindResource(h,MAKEINTRESOURCE(resID),rtName);
	if (hr){
		HGLOBAL hg=LoadResource(h,hr);
		if (hg){
			LPCTSTR lp=(LPCTSTR)LockResource(hg);
			if (lp) sndPlaySound(lp,SND_MEMORY|SND_ASYNC);
			FreeResource(hg);
		}
	}
	return;
}
HWND MainWin::NewIMSession(TCHAR* szIP){
	ChatWinInfo* pwci=NULL;
	ULONG ip=MainWin::GetIPValue(szIP);
	CWItr itr=m_chatwininfo.find(ip);
	if(itr!=m_chatwininfo.end()) pwci=itr->second;
	int w=IM_WIDTH;
	int h=IM_HEIGHT;
	int x=(GetSystemMetrics(SM_CXSCREEN)-w)/2;
	int y=(GetSystemMetrics(SM_CYSCREEN)-h)/2;
	HWND hWndChatWin= (pwci&&pwci->hWnd?pwci->hWnd:CreateWindowEx(0, IM_CLASS,IM_TITLE , WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,x,y,w,h,NULL,NULL,GetModuleHandle(NULL),(LPVOID)this));
	if(hWndChatWin==NULL){
		ErrMsg(GetLastError(),L"无法创建即时通信会话窗口：%s");
		return NULL;
	}
	ChatWin* pcw=(ChatWin*)GetWindowLongPtr(hWndChatWin, GWLP_USERDATA);
	if(pcw!=NULL){
		if(pcw->m_mainWin==NULL) pcw->m_mainWin=this;
		_tcscpy_s(pcw->m_targetIP,szIP);
		if(pwci){
			_tcscpy_s(pcw->m_targetCN,pwci->cn);
		}
		if(pcw->m_msgRecorder==NULL){
			TCHAR sc[STRLEN_SMALL]={0};
			_stprintf_s(sc,L"%u",pwci->sc);
			pcw->m_msgRecorder=new MsgRecorder(sc);
		}
	}

	ShowWindow(hWndChatWin,SW_SHOW);
	UpdateWindow(hWndChatWin);
	return hWndChatWin;
}
LRESULT MainWin::OnReceivedIM(ReceivedMessage& rmsg){
	int idx=0;
	TCHAR szIP[STRLEN_SMALL]={0};
	_tcscpy_s(szIP,rmsg.szFrom);
	ULONG ipv=MainWin::GetIPValue(szIP);

	ChatWinInfo* pCWI=NULL;
	CWItr itr=m_chatwininfo.find(ipv);
	if(itr!=m_chatwininfo.end() && itr->second) pCWI=itr->second;
	size_t styleLen=(pCWI?_tcslen(pCWI->style):0);

	size_t l=_tcslen(rmsg.szDT)+_tcslen(rmsg.szFrom)+_tcslen(rmsg.szMsg)+styleLen+MAX_PATH;
	TCHAR* js=new TCHAR[l];
	memset(js,0,sizeof(TCHAR)*l);
	TCHAR lid[STRLEN_SMALL]={0};
	if(rmsg.msgId!=0){
		_stprintf_s(lid,L"%u",rmsg.msgId);
	}else{
		GetLocalMsgId(lid,STRLEN_SMALL);
	}
	TCHAR *szCN=(pCWI?pCWI->cn:NULL);
	if(szCN==NULL || _tcslen(szCN)==0) szCN=rmsg.szFrom;
	if(styleLen>0){_stprintf_s(js,l,L"chatwin.display('%s','%s','%s','%s','%s');",lid,rmsg.szDT,szCN,rmsg.szMsg,pCWI->style);}
	else{_stprintf_s(js,l,L"chatwin.display('%s','%s','%s','%s');",lid,rmsg.szDT,szCN,rmsg.szMsg);}
	
	BOOL recordFlag=FALSE;
	HWND hWndChatWin =NULL;
	if(pCWI && (hWndChatWin=pCWI->hWnd)!=NULL && _tcsstr(rmsg.szMsg,SHAKEHAND_SEND_PREFIX)!=rmsg.szMsg){
		SendMessage(hWndChatWin,WM_NMRECEIVED,(WPARAM)0,(LPARAM)js);
		ShowWindow(hWndChatWin,SW_SHOW);
		recordFlag=TRUE;
	}else{
		if(pCWI==NULL){
			pCWI=new ChatWinInfo;
			ZeroMemory(pCWI,sizeof(ChatWinInfo));
		}
		_tcscpy_s(pCWI->ip,szIP);
		m_chatwininfo[ipv]=pCWI;
		if(_tcsstr(rmsg.szMsg,SHAKEHAND_SEND_PREFIX)==rmsg.szMsg){	//接收到发起者发送的握手请求则记录发起者信息并返回
			idx=0;
			UINT l=_tcslen(rmsg.szMsg)+1;
			TCHAR* sz=new TCHAR[l];
			memset(sz,0,sizeof(TCHAR)*l);
			_tcscpy_s(sz,l,rmsg.szMsg+_tcslen(SHAKEHAND_SEND_PREFIX)+1);
			while(sz[idx]!=L'\0'){
				if(sz[idx]==L'~') sz[idx]=L'\0';
				idx++;
			}
			idx=0;
			for (LPTSTR pszz = sz; *pszz; pszz += _tcslen(pszz) + 1) {
				switch(idx){
				case 0:	//cn
					_tcscpy_s(pCWI->cn,pszz);
					break;
				case 1:	//ou0
					_tcscpy_s(pCWI->ou0,pszz);
					break;
				case 2:	//sc
					{
						TCHAR szSc[STRLEN_SMALL]={0};
						_tcscpy_s(szSc,pszz);
						pCWI->sc=_tcstoul(szSc,NULL,10);
					}
					break;
				}//switch end
				idx++;
				if(idx>2)break;
			}//for end
			SAFE_DEL_ARR(sz);
			SAFE_DEL_ARR(js);
			goto end;
		}

		pCWI->hWnd=NewIMSession(szIP);
		if(pCWI->hWnd==NULL){
			SAFE_DEL_ARR(js);
			goto end;
		}
		SetChatWinCaption(pCWI,pCWI->hWnd);
		SendMessage(pCWI->hWnd,WM_NMRECEIVED,(WPARAM)0,(LPARAM)js);
		recordFlag=TRUE;
	}
	if(recordFlag){
		ChatWin* pcw=(ChatWin*)GetWindowLongPtr(pCWI->hWnd, GWLP_USERDATA);
		if(pcw && pcw->m_msgRecorder) pcw->m_msgRecorder->RecordMsg(lid,rmsg.szMsg,pCWI->cn,rmsg.szDT,pCWI->style);
	}
	this->PlayWaveResource(WAV_MSGNOTIFICATION);
end:
	return 0;
}
LRESULT MainWin::OnReceivedNotifier(ReceivedMessage& rmsg){
	static TCHAR* szOfflineMsg=L"<strong>用户已下线！</strong>";

	BOOL isMyself=FALSE;
	if(rmsg.notifyType==USERSTATE){
		TCHAR chkstr[STRLEN_SMALL]={0};
		if(_tcsstr(rmsg.szUNID,L"40011_")==rmsg.szUNID){
			CWItr itr;
			for(itr=m_chatwininfo.begin();itr!=m_chatwininfo.end();itr++){
				ChatWinInfo *pcwi=(ChatWinInfo*)(itr->second);
				if(!pcwi || !pcwi->hWnd) continue;
				ZeroMemory(chkstr,sizeof(TCHAR)*STRLEN_SMALL);
				_stprintf_s(chkstr,L"_%s_%d",pcwi->ip,pcwi->sc);
				if(_tcsstr(rmsg.szUNID,chkstr)!=NULL){
					PostMessage(pcwi->hWnd,WM_NETERROR,(WPARAM)0,(LPARAM)szOfflineMsg);
					break;
				}
			}
		}

		ZeroMemory(chkstr,sizeof(TCHAR)*STRLEN_SMALL);
		_stprintf_s(chkstr,L"_%u_",m_userInfo.sc);
		isMyself=(_tcsstr(rmsg.szUNID,chkstr)!=NULL);
		if(!isMyself){
			ZeroMemory(chkstr,sizeof(TCHAR)*STRLEN_SMALL);
			_stprintf_s(chkstr,L"_%s_",m_userInfo.ip);
			isMyself=(_tcsstr(rmsg.szUNID,chkstr)!=NULL);
		}
	}
	if(isMyself){
		return 0;
	}else{
		size_t jsl=32+_tcslen(rmsg.szMsg)+MAX_PATH;
		TCHAR* js=new TCHAR[jsl];
		_stprintf_s(js,jsl,L"notification.addItem('%s','%s',%u,%s);",rmsg.szUNID,rmsg.szMsg,rmsg.notifyType,rmsg.notifyType==USERSTATE?L"true":L"false");
		BSTR bstrJS=SysAllocString(js);
		this->m_webBrowser.CallJavaScript(bstrJS);
		SAFE_DEL_ARR(js);
		FREE_SYS_STR(bstrJS);
	}

	InfoNotify* in=InfoNotify::Create(this->m_hWnd);
	if (in) {
		in->SetSkin(IDB_SKIN_MSN);
		in->SetTextFont(L"微软雅黑",90,TN_TEXT_NORMAL,TN_TEXT_UNDERLINE);
 		in->SetTextColor(RGB(0,0,0),RGB(0,0,200));
		RECT rc={0,0,0,0};
		rc.left=10;
		rc.top=40;
		rc.right=in->m_nSkinWidth-10;
		rc.bottom=in->m_nSkinHeight-25;
		in->SetTextRect(rc);
		_tcscpy_s(in->m_szUNID,rmsg.szUNID);
		in->m_notifierType=rmsg.notifyType;
		size_t l=_tcslen(rmsg.szMsg);
		DWORD dwLive=1000*(l/4);
		in->Show(rmsg.szMsg,500,(dwLive<5000?5000:dwLive>60000?60000:dwLive),500);
		this->PlayWaveResource(WAV_INFONOTIFICATION);
		if(pVoice) pVoice->Speak(rmsg.szMsg, SPF_ASYNC, NULL);
	}
	return 0;
}
LRESULT MainWin::OnReceivedStyle(ReceivedMessage& rmsg){
	ULONG ipv=MainWin::GetIPValue(rmsg.szFrom);
	ChatWinInfo* pCWI=NULL;
	CWItr itr=m_chatwininfo.find(ipv);
	if(itr!=m_chatwininfo.end() && itr->second) {
		pCWI=itr->second;
		_tcscpy_s(pCWI->style,rmsg.szMsg);
	}
	return 0;
}
LRESULT MainWin::OnFileTransfer(ReceivedMessage& rmsg){
	TCHAR szMsg[STRLEN_DEFAULT]={0};
	_tcscpy_s(szMsg,rmsg.szMsg);
	int idx=0;
	while(*(szMsg+idx)!=L'\0'){
		if(*(szMsg+idx)==L':') *(szMsg+idx)=L'\0';
		idx++;
	}
	szMsg[_tcslen(rmsg.szMsg)+1]=L'\0';
	DWORD ftfeedback=UNKNOWN;
	DWORD fid=0;
	DWORD fsize=0;
	TCHAR fn[MAX_PATH]={0};
	idx=0;
	for (LPTSTR pszz = szMsg; *pszz; pszz += _tcslen(pszz) + 1) {
		switch(idx){
		case 0:	//文件传输动作标记，包括：询问、接收、拒绝等
			ftfeedback=_tcstoul(pszz,NULL,10);
			if(ftfeedback==UNKNOWN) {ErrMsg(0,L"无法获取用户是同意接收还是拒绝接收文件！");return 0;}
			break;
		case 1:	//fid
			fid=_tcstoul(pszz,NULL,10);
			if(fid==0) {ErrMsg(0,L"无法获取文件的唯一标记！");return 0;}
			break;
		case 2:	//size
			fsize=_tcstoul(pszz,NULL,10);
			if(fsize==0) {ErrMsg(0,L"无法获取文件大小！");return 0;}
			break;
		case 3:	//fn
			_tcscpy_s(fn,pszz);
			if(_tcslen(fn)==0) {ErrMsg(0,L"无法获取文件名！");return 0;}
			break;
		default:
			break;
		}
		idx++;
	}
	TCHAR szIP[STRLEN_SMALL]={0};
	_tcscpy_s(szIP,rmsg.szFrom);

	ULONG ipv=MainWin::GetIPValue(szIP);
	ChatWinInfo* pCWI=NULL;
	CWItr itr=m_chatwininfo.find(ipv);
	if(itr!=m_chatwininfo.end() && itr->second) {
		pCWI=itr->second;
		if(pCWI->hWnd==NULL){
			pCWI->hWnd=NewIMSession(szIP);
			if(pCWI->hWnd==NULL) {return 0;}
			SetChatWinCaption(pCWI,pCWI->hWnd);
		}
		if(ftfeedback==ASK){
			PFTContext pftc=new FTContext;
			ZeroMemory(pftc,sizeof(FTContext));
			pftc->m_id=fid;
			pftc->m_fileSize=fsize;
			_tcscpy_s(pftc->m_fn,fn);
			SendMessage(pCWI->hWnd,WM_FT_ASK,(WPARAM)0,(LPARAM)pftc);
			this->PlayWaveResource(WAV_MSGNOTIFICATION);
		}else if(ftfeedback==CANCEL){
			SendMessage(pCWI->hWnd,WM_FT_CANCEL,(WPARAM)0,(LPARAM)fid);
		}else{
			SendMessage(pCWI->hWnd,(ftfeedback==ACCEPT?WM_FT_ACCEPT:WM_FT_REFUSE),(WPARAM)0,(LPARAM)fid);
		}
	}
	return 0;
}
/*********** CWBEventStatic 成员 ************/
void CWBEventStatic::DocumentComplete(IDispatch *pDisp,VARIANT *URL){
	CComPtr<IWebBrowser2> pWebBrowser2=NULL;
	HRESULT hr=pDisp->QueryInterface(IID_IWebBrowser2,(void**)&pWebBrowser2);
	if (FAILED(hr) || !pWebBrowser2) return ;
	BOOL isLogin=FALSE;																		//登录页标记
	BOOL isIndex=FALSE;																		//小助手内容页标记
	BOOL isSelect=FALSE;																	//选择站点页面标记
	BOOL isRedirect=FALSE;																//选择站点确定后的重定向页面标记
	BOOL isError=FALSE;																		//本地错误页面
	BSTR bstrUrl=NULL;
	TCHAR szUrlParams[MAX_PATH]={0};
	if(pWebBrowser2->get_LocationURL(&bstrUrl)==S_OK){
		if(_tcsstr(bstrUrl,L"/tsim/login.")!=NULL) isLogin=TRUE;
		if(_tcsstr(bstrUrl,L"/tsim/index.")!=NULL) isIndex=TRUE;
		if(_tcsstr(bstrUrl,L"IDH_SELECT_SITES")!=NULL) isSelect=TRUE;
		if(_tcsstr(bstrUrl,L"IDH_REDIRECT_SITE")!=NULL) isRedirect=TRUE;
		if(_tcsstr(bstrUrl,L"IDH_NET_ERROR")!=NULL) isError=TRUE;
		TCHAR* szParamStart=_tcsstr(bstrUrl,L"#");
		if(szParamStart)_tcscpy_s(szUrlParams,szParamStart+1);
		FREE_SYS_STR(bstrUrl);
	}
	if(isLogin) this->OnLoginDocumentComplete(pWebBrowser2,szUrlParams);
	else if(isIndex)this->OnIndexDocumentComplete(pWebBrowser2,szUrlParams);
	else if(isSelect)this->OnSelectSitesDocumentComplete(pWebBrowser2,szUrlParams);
	else if(isRedirect)this->OnSelectSiteOkDocumentComplete(pWebBrowser2,szUrlParams);
	else if(isError)this->OnErrorDocumentComplete(pWebBrowser2,szUrlParams);
}
void CWBEventStatic::OnLoginDocumentComplete(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	m_pMainWin->SetStatusText(L"未登录");
	if(szUrlParams && _tcslen(szUrlParams)>0){
		OnAutoOptionsChange(pWebBrowser2,szUrlParams);
		return ;
	}
	m_pMainWin->m_blIsLogon=FALSE;
	m_pMainWin->SetMenuState();

	BSTR bstrSystemNameId=SysAllocString(L"systemName");
	BSTR bstrBelongId=SysAllocString(L"belong");
	BSTR bstrProviderId=SysAllocString(L"provider");
	BSTR bstrUidId=SysAllocString(L"uid");
	BSTR bstrPwdId=SysAllocString(L"password");
	BSTR bstrRIPId=SysAllocString(L"requestip");
	BSTR bstrCIPId=SysAllocString(L"clientip");
	BSTR bstrSIPId=SysAllocString(L"serverip");
	AppOptions* appOpts=AppOptions::GetInstance();
	//记录系统名称
	if(_tcslen(m_pMainWin->m_szSystemName)==0){
		BSTR bstrSystemName=NULL;
		GetElementValue(pWebBrowser2,bstrSystemNameId,&bstrSystemName);	
		if(bstrSystemName && _tcslen(bstrSystemName)>0){
			_tcscpy_s(m_pMainWin->m_szSystemName,bstrSystemName);
			ZeroMemory(appOpts->Title,sizeof(TCHAR)*MAX_PATH);
			_tcscpy_s(appOpts->Title,bstrSystemName);
			TCHAR szTitle[MAX_PATH+10]={0};
			_stprintf_s(szTitle,L"%s - 办公小助手",bstrSystemName);
			SetWindowText(m_pMainWin->m_hWnd,szTitle);
		}
		FREE_SYS_STR(bstrSystemName);
	}
	//记录系统使用单位
	if(_tcslen(m_pMainWin->m_szBelong)==0){
		BSTR bstrBelong=NULL;
		GetElementValue(pWebBrowser2,bstrBelongId,&bstrBelong);	
		if(bstrBelong && _tcslen(bstrBelong)>0){
			_tcscpy_s(m_pMainWin->m_szBelong,bstrBelong);
			_tcscpy_s(appOpts->Belong,bstrBelong);
		}
		FREE_SYS_STR(bstrBelong);
	}
	//记录系统提供单位
	if(_tcslen(m_pMainWin->m_szProvider)==0){
		BSTR bstrProvider=NULL;
		GetElementValue(pWebBrowser2,bstrProviderId,&bstrProvider);	
		if(bstrProvider && _tcslen(bstrProvider)>0){
			_tcscpy_s(m_pMainWin->m_szProvider,bstrProvider);
			_tcscpy_s(appOpts->Provider,bstrProvider);
		}
		FREE_SYS_STR(bstrProvider);
	}

	//执行名为loaded的js函数
	TCHAR szSetAutoControlJS[MAX_PATH]={0};
	_stprintf_s(szSetAutoControlJS,L"loaded(%s,%s);",appOpts->AutoLogon?L"true":L"false",appOpts->AutoRun?L"true":L"false");
	BSTR bstrSetAutoControlJS=T2BSTR(szSetAutoControlJS);
	this->host->CallJavaScript(bstrSetAutoControlJS);
	FREE_SYS_STR(bstrSetAutoControlJS);

	ReadLoginInfo();	//如果账号密码为空，则尝试先读取
	//同步已保存的账号和密码
	BSTR bstrUidValue = SysAllocString(appOpts->UserName);
	SetElementValue(pWebBrowser2, bstrUidId, bstrUidValue);
	if (appOpts->AutoLogon && _tcslen(appOpts->Password)>0){
		TCHAR szPwdDecrypted[MAX_PATH] = { 0 };
		size_t sPwdDecrypted = MAX_PATH;
		Decrypt(appOpts->Password, szPwdDecrypted, &sPwdDecrypted);
		BSTR bstrPwdValue = SysAllocString(szPwdDecrypted);
		SetElementValue(pWebBrowser2, bstrPwdId, bstrPwdValue);
		FREE_SYS_STR(bstrPwdValue);
	}

	BSTR bstrIPV=NULL;
	//请求ip
	GetElementValue(pWebBrowser2,bstrRIPId,&bstrIPV);
	if(bstrIPV && _tcslen(bstrIPV)>0) _tcscpy_s(m_pMainWin->m_requestip,bstrIPV);
	FREE_SYS_STR(bstrIPV);

	//客户端ip
	GetElementValue(pWebBrowser2,bstrCIPId,&bstrIPV);
	if(bstrIPV && _tcslen(bstrIPV)>0)_tcscpy_s(m_pMainWin->m_clientip,bstrIPV);
	FREE_SYS_STR(bstrIPV);

	//服务器端ip
	GetElementValue(pWebBrowser2,bstrSIPId,&bstrIPV);
	if(bstrIPV && _tcslen(bstrIPV)>0) _tcscpy_s(m_pMainWin->m_serverip,bstrIPV);
	FREE_SYS_STR(bstrIPV);

	//回收bstr资源
	FREE_SYS_STR(bstrSystemNameId);
	FREE_SYS_STR(bstrBelongId);
	FREE_SYS_STR(bstrProviderId);
	FREE_SYS_STR(bstrUidId);
	FREE_SYS_STR(bstrPwdId);
	FREE_SYS_STR(bstrUidValue);
	FREE_SYS_STR(bstrRIPId);
	FREE_SYS_STR(bstrCIPId);
	FREE_SYS_STR(bstrSIPId);

	//自动登录
	if(appOpts->AutoLogon){
		BSTR bstrDoLoginJS=T2BSTR(L"doLogin()");
		this->host->CallJavaScript(bstrDoLoginJS);
		FREE_SYS_STR(bstrDoLoginJS);
	}

	HMENU hMenu=GetMenu(this->m_pMainWin->m_hWnd);
	EnableMenuItem(hMenu,ID_FILE_LOGON,MF_ENABLED);
}
void CWBEventStatic::OnAutoOptionsChange(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	if(!szUrlParams || _tcslen(szUrlParams)==0) return;
	AppOptions* appOpts=AppOptions::GetInstance();
	BOOL checkFlag=(_tcsstr(szUrlParams,L"|1")!=NULL);
	BOOL changedFlag=FALSE;
	if(_tcsstr(szUrlParams,L"autologon")==szUrlParams){														//自动登录
		BOOL oldValue=appOpts->AutoLogon;
		changedFlag=(oldValue!=checkFlag);
		appOpts->AutoLogon=checkFlag;
	}
	if(_tcsstr(szUrlParams,L"autorun")==szUrlParams){															//自动运行
		BOOL oldValue=appOpts->AutoRun;
		changedFlag=(oldValue!=checkFlag);
		appOpts->AutoRun=checkFlag;
		if(changedFlag) RegisterAutoRun(appOpts->AutoRun);
	}
	if(changedFlag) appOpts->Save();
}
void CWBEventStatic::OnIndexDocumentComplete(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	ReadLoginInfo();	//如果账号密码为空，则尝试先读取
	if(_tcsicmp(szUrlParams,L"subscription.close")==0){
		OnSubscribeClosed(pWebBrowser2,szUrlParams);																			//订阅关闭
	}else if(_tcsstr(szUrlParams,L"tsimto")==szUrlParams){															//启动即时聊天
		NewTSIMSession(pWebBrowser2,szUrlParams);
		return;
	}
	if(_tcslen(szUrlParams)==0){
		SetUserInfo(pWebBrowser2,szUrlParams);
		SetPresence(pWebBrowser2,szUrlParams);
		SyncSubscribedServices(pWebBrowser2,szUrlParams);
		SyncSid(pWebBrowser2,szUrlParams);
	
		//正常index.jsp加载完成：显示第一个tab内容（用户树）
		TCHAR* szOnLoadOK=new TCHAR[STRLEN_1K];
		_stprintf_s(szOnLoadOK,STRLEN_1K,L"indexTab.initTab(provideServicesAsTabItems(provideUserServices()));indexTab.showTab();");
		BSTR bstrOnLoadOK=T2BSTR(szOnLoadOK);
		this->host->CallJavaScript(bstrOnLoadOK);
		SAFE_DEL_ARR(szOnLoadOK);
		FREE_SYS_STR(bstrOnLoadOK);

		AppOptions* appOpts=AppOptions::GetInstance();
		//如果自动登录，隐藏注销
		if (appOpts->AutoLogon){
			HMENU hMenu = GetMenu(this->m_pMainWin->m_hWnd);
			HMENU hFileMenu = GetSubMenu(hMenu, 0);
			RemoveMenu(hFileMenu, ID_FILE_LOGOFF, MF_BYCOMMAND);

			TCHAR* szHideLogout = new TCHAR[STRLEN_1K];
			_stprintf_s(szHideLogout, STRLEN_1K, L"var el=document.getElementById('logout');if(el) el.style.display='none';");
			BSTR bstrHideLogout = T2BSTR(szHideLogout);
			this->host->CallJavaScript(bstrHideLogout);
			SAFE_DEL_ARR(szHideLogout);
			FREE_SYS_STR(bstrHideLogout);
		}
	}
}
void CWBEventStatic::NewTSIMSession(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	TCHAR pszzStart[STRLEN_1K]={0};
	_tcscpy_s(pszzStart,szUrlParams+6);
	int idx=0;
	while(pszzStart[idx]!=L'\0'){
		if(pszzStart[idx]==L',')pszzStart[idx]=L'\0';
		idx++;
	}
	pszzStart[idx+1]=L'\0';

	ChatWinInfo* pcwi=NULL;
	CWItr itr;
	ULONG ipv=0;
	idx=0;
	TCHAR szIP[STRLEN_SMALL]={0};
	for (LPTSTR pszz = pszzStart; *pszz; pszz += _tcslen(pszz) + 1) {
		switch(idx){
		case 0:	//to ip
			_tcscpy_s(szIP,pszz);
			ipv=MainWin::GetIPValue(szIP);
			itr=m_pMainWin->m_chatwininfo.find(ipv);
			if(itr!=m_pMainWin->m_chatwininfo.end()){
				pcwi=itr->second;
				if(pcwi!=NULL) ZeroMemory(pcwi,sizeof(ChatWinInfo));
			}
			if(pcwi==NULL){
				pcwi=new ChatWinInfo();
				ZeroMemory(pcwi,sizeof(ChatWinInfo));
				this->m_pMainWin->m_chatwininfo[ipv]=pcwi;
			}
			_tcscpy_s(pcwi->ip,szIP);
			break;
		case 1:	//to ou0
			_tcscpy_s(pcwi->ou0,pszz);
			break;
		case 2:	//to cn
			_tcscpy_s(pcwi->cn,pszz);
			break;
		case 3:	//to sc
			{
				TCHAR szSC[STRLEN_SMALL]={0};
				_tcscpy_s(szSC,pszz);
				pcwi->sc=(UINT)_tstoi(szSC);
			}
			break;
		}
		idx++;
		if(idx>3) break;
	}
	if(_tcslen(szIP)>0){
		BOOL isPrivateNetwork=FALSE;
		static DWORD PrivateNetworks[]={0x0000A8C0/*192.168*/,0x0000000A/*10.*/};	//,0x000010AC/*172.16.*/,0x0000FEA9/*169.254.*/
		for(int i=0;i<2;i++){
			if((ipv & PrivateNetworks[i])==PrivateNetworks[i]){isPrivateNetwork=TRUE;break;}
		}
		if(!isPrivateNetwork){
			isPrivateNetwork=((ipv>=0x0001FEA9/*169.254.1.0*/ && ipv<=0xFFFEFEA9/*169.254.254.255*/)||(ipv>=0x00010AC/*172.16.0.0*/ && ipv<=0xFFFF1FAC/*172.31.255.255*/));
		}
		if(!isPrivateNetwork){
			ErrMsg(0,L"只能与处于同一个内部网络内的用户进行即时沟通！");
			SAFE_DEL_PTR(pcwi);
			return ;
		}
		pcwi->hWnd=this->m_pMainWin->NewIMSession(szIP);
		MainWin::SetChatWinCaption(pcwi,pcwi->hWnd);
		
		TCHAR szShakeHand[MAX_PATH]={0};
		_stprintf_s(szShakeHand,L"tsimActions.sendIM('%s:%s~%s~%u','%s');",SHAKEHAND_SEND_PREFIX,m_pMainWin->m_userInfo.cn,m_pMainWin->m_userInfo.ou0,m_pMainWin->m_userInfo.sc,szIP);
		BSTR bstrJS=SysAllocString(szShakeHand);
		this->host->CallJavaScript(szShakeHand);
		FREE_SYS_STR(bstrJS);
	}
}
void CWBEventStatic::OnSubscribeClosed(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	AppOptions* appOpts=AppOptions::GetInstance();
	BSTR bstrSSId=SysAllocString(L"subcribedServices");
	BSTR bstrSSVal=NULL;
	GetElementValue(pWebBrowser2,bstrSSId,&bstrSSVal);
	FREE_SYS_STR(bstrSSId);
	TCHAR* szSSVal=NULL;
	if(bstrSSVal)szSSVal=OLE2T(bstrSSVal);
	else szSSVal=L"";
	if(_tcsicmp(appOpts->SubscribedServices,szSSVal)!=0){
		appOpts->SaveSubscribedServices(szSSVal);
	}
	FREE_SYS_STR(bstrSSVal);
	TCHAR* szOnSClose=new TCHAR[MAX_PATH];
	_stprintf_s(szOnSClose,MAX_PATH,L"subscription.close();");
	BSTR bstrOnSClose=T2BSTR(szOnSClose);
	this->host->CallJavaScript(bstrOnSClose);
	SAFE_DEL_ARR(szOnSClose);
	FREE_SYS_STR(bstrOnSClose);
}
void CWBEventStatic::SetUserInfo(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	if(this->m_pMainWin->m_userInfo.sc==0){
		BSTR bstrId=SysAllocString(L"tsimfrom");
		BSTR bstrValue=NULL;
		GetElementValue(pWebBrowser2,bstrId,&bstrValue);
		size_t l=_tcslen(bstrValue);
		if(bstrValue && l>0){
			l+=2;
			TCHAR *szTsimFrom=new TCHAR[l];
			ZeroMemory(szTsimFrom,sizeof(TCHAR)*l);
			_tcscpy_s(szTsimFrom,l,bstrValue);
			int idx=0;
			while(szTsimFrom[idx]!=L'\0'){
				if(szTsimFrom[idx]==L',')szTsimFrom[idx]=L'\0';
				idx++;
			}
			szTsimFrom[l-1]=L'\0';
			idx=0;
			for (LPTSTR pszz = szTsimFrom; *pszz; pszz += _tcslen(pszz) + 1) {
				switch(idx){
				case 0:	//ou0
					_tcscpy_s(this->m_pMainWin->m_userInfo.ou0,pszz);
					break;
				case 1:	//cn
					_tcscpy_s(this->m_pMainWin->m_userInfo.cn,pszz);
					break;
				case 2:	//sc
					{
						TCHAR szSC[STRLEN_SMALL]={0};
						_tcscpy_s(szSC,pszz);
						this->m_pMainWin->m_userInfo.sc=(UINT)_tstoi(szSC);
					}
					break;
				}
				idx++;
			} //for end
		}	//if end
	}	//if end
}
void CWBEventStatic::SetPresence(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	BSTR bstrLoginFlagId=SysAllocString(L"loginflag");
	BSTR bstrLoginFlagValue=NULL;
	GetElementValue(pWebBrowser2,bstrLoginFlagId,&bstrLoginFlagValue);
	if(bstrLoginFlagValue && _tcslen(bstrLoginFlagValue)>0 && _tcsicmp(bstrLoginFlagValue,L"1")==0){
		m_pMainWin->m_blIsLogon=TRUE;
		m_pMainWin->SetMenuState();
		m_pMainWin->m_presence=ID_FILE_PRESENCESTATUS_ONLINE;
		m_pMainWin->SetStatusText(L"已登录");
	}
	BSTR bstrPresenceId=SysAllocString(L"presence");
	BSTR bstrPresenceValue=NULL;
	GetElementValue(pWebBrowser2,bstrPresenceId,&bstrPresenceValue);
	if(bstrPresenceValue && _tcslen(bstrPresenceValue)>0){
		m_pMainWin->m_presence=(WORD)_tstol(bstrPresenceValue);
		m_pMainWin->SetMenuState();
	}else{
		TCHAR szSetPresenceJS[STRLEN_1K]={0};
		_stprintf_s(szSetPresenceJS,L"tsimActions.setPresence(%d);",ID_FILE_PRESENCESTATUS_ONLINE);
		BSTR bstrSetPresenceJS=T2BSTR(szSetPresenceJS);
		host->CallJavaScript(bstrSetPresenceJS);
		FREE_SYS_STR(bstrSetPresenceJS);
		m_pMainWin->m_presence=ID_FILE_PRESENCESTATUS_ONLINE;
		m_pMainWin->SetMenuState();
	}

	FREE_SYS_STR(bstrLoginFlagId);
	FREE_SYS_STR(bstrLoginFlagValue);
	FREE_SYS_STR(bstrPresenceId);
	FREE_SYS_STR(bstrPresenceValue);
}
void CWBEventStatic::SyncSubscribedServices(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	AppOptions* appOpts=AppOptions::GetInstance();
	if(_tcslen(szUrlParams)==0 && _tcslen(appOpts->SubscribedServices)>0){				//同步已订阅内容信息
		TCHAR* szSetSSVSetJS=new TCHAR[STRLEN_4K+MAX_PATH];
		_stprintf_s(szSetSSVSetJS,STRLEN_4K+MAX_PATH,L"document.getElementById('subcribedServices').value='%s';",appOpts->SubscribedServices);
		BSTR bstrSetSSVSetJS=T2BSTR(szSetSSVSetJS);
		this->host->CallJavaScript(bstrSetSSVSetJS);
		SAFE_DEL_ARR(szSetSSVSetJS);
		FREE_SYS_STR(bstrSetSSVSetJS);
	}
}
void CWBEventStatic::SyncSid(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	TCHAR szCredential[STRLEN_1K]={0};
	this->m_pMainWin->BuildLoginCredential(szCredential,STRLEN_1K);
	TCHAR szSetCredentialJS[STRLEN_1K]={0};
	_stprintf_s(szSetCredentialJS,L"tsimActions.sid='%s';",szCredential);
	BSTR bstrSetCredentialJS=T2BSTR(szSetCredentialJS);
	this->host->CallJavaScript(bstrSetCredentialJS);
	FREE_SYS_STR(bstrSetCredentialJS);
}
void CWBEventStatic::OnSelectSitesDocumentComplete(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	HMENU hMenu=GetMenu(this->m_pMainWin->m_hWnd);
	EnableMenuItem(hMenu,ID_FILE_LOGON,MF_GRAYED);
	EnableMenuItem(hMenu,ID_FILE_LOGOFF,MF_GRAYED);
	EnableMenuItem(hMenu,ID_FILE_HOMEPAGE,MF_GRAYED);
	EnableMenuItem(hMenu,ID_FILEOPEN,MF_GRAYED);
	EnableMenuItem(hMenu,ID_TOOLS_SUBSCRIBE,MF_GRAYED);

	SiteInfo* pSi=NULL;
	for(int i=0;i<AppOptions::dwSiteCount;i++){
		pSi=&AppOptions::pSiteInfoArr[i];
		if(_tcslen(pSi->Url)==0) continue;
		TCHAR szAddUrlJS[STRLEN_1K]={0};
		_stprintf_s(szAddUrlJS,L"addUrl('%s','%s','%s');",pSi->Url,pSi->Title,pSi->UrlExt);
		BSTR bstrAddUrlJS=T2BSTR(szAddUrlJS);
		this->host->CallJavaScript(bstrAddUrlJS);
		FREE_SYS_STR(bstrAddUrlJS);
	}
}
void CWBEventStatic::OnSelectSiteOkDocumentComplete(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	SiteInfo* pSi=NULL;

	BSTR bstrExtId=SysAllocString(L"ext");
	BSTR bstrExt=NULL;
	GetElementValue(pWebBrowser2,bstrExtId,&bstrExt);
	FREE_SYS_STR(bstrExtId);

	BSTR bstrSiteId=SysAllocString(L"site");
	BSTR bstrSite=NULL;
	GetElementValue(pWebBrowser2,bstrSiteId,&bstrSite);
	FREE_SYS_STR(bstrSiteId);

	ZeroMemory(AppOptions::szCurrentSiteUrl,sizeof(TCHAR)*STRLEN_1K);
	ZeroMemory(AppOptions::szCurrentSiteUrlExt,sizeof(TCHAR)*STRLEN_SMALL);
	if(bstrSite){
		for(int i=0;i<AppOptions::dwSiteCount;i++){
			pSi=&AppOptions::pSiteInfoArr[i];
			if(_tcsicmp(pSi->Url,bstrSite)==0){
				_tcscpy_s(AppOptions::szCurrentSiteUrl,pSi->Url);
				_tcscpy_s(AppOptions::szCurrentSiteUrlExt,pSi->UrlExt);
				break;
			}
		}
	}
	if(_tcslen(AppOptions::szCurrentSiteUrlExt)==0 && bstrExt){
		_tcscpy_s(AppOptions::szCurrentSiteUrlExt,bstrExt);
	}

	FREE_SYS_STR(bstrExt);
	FREE_SYS_STR(bstrSite);
	AppOptions::Reload();
	TCHAR szRedirectJS[STRLEN_1K]={0};
	_tcscpy_s(szRedirectJS,L"redirect();");
	BSTR bstrRedirectJS=T2BSTR(szRedirectJS);
	this->host->CallJavaScript(bstrRedirectJS);
	FREE_SYS_STR(bstrRedirectJS);
}
void CWBEventStatic::OnErrorDocumentComplete(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	TCHAR szUrl[STRLEN_1K]={0};
	_stprintf_s(szUrl,L"http://%s/tsim/index%s",AppOptions::szCurrentSiteUrl,AppOptions::szCurrentSiteUrlExt);
	TCHAR szJS[STRLEN_1K]={0};
	BSTR bstrJS=NULL;
	if(_tcslen(szUrlParams)==0){
		_stprintf_s(szJS,L"setTargetUrl('%s');",szUrl);
		bstrJS=SysAllocString(szJS);
		this->host->CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
	}else if(_tcsstr(szUrlParams,L"retry")==szUrlParams){															//重新连接
		this->m_pMainWin->m_blCanConnect=MainWin::CheckUrlConnectable(szUrl);
		if (this->m_pMainWin->m_blCanConnect){
			this->m_pMainWin->m_webBrowser.NavigateTo(szUrl);
		}
	}
}
void CWBEventStatic::BeforeNavigate2(IDispatch *pDisp, VARIANT *&url, VARIANT *&Flags, VARIANT *&TargetFrameName, VARIANT *&PostData, VARIANT *&Headers, VARIANT_BOOL *&Cancel){
	
}
int CWBEventStatic::GetElementValue(IWebBrowser2* pWB,BSTR szElID,BSTR* pbstrValue){
	int ret=-1;
	*pbstrValue=NULL;
	if (!pWB || !szElID) return ret;
	CComPtr<IHTMLDocument2> doc2=NULL;
	CComPtr<IHTMLDocument3> doc3=NULL;
	CComPtr<IHTMLElement> el=NULL;
	CComPtr<IHTMLInputElement> elx=NULL;
	HRESULT hr=pWB->get_Document((IDispatch**)&doc2);
	if (FAILED(hr)) goto end;
	hr=doc2->QueryInterface(IID_IHTMLDocument3,(void**)&doc3);
	if (FAILED(hr)) goto end;
	hr=doc3->getElementById(szElID,&el);
	if (FAILED(hr) || !el) goto end;
	hr=el->QueryInterface(IID_IHTMLInputElement,(void**)&elx);
	if (FAILED(hr) || !elx) goto end;
	hr=elx->get_value(pbstrValue);
	if (FAILED(hr)) goto end;
	ret=SysStringLen(*pbstrValue);
end:
	return ret;
}
void CWBEventStatic::SetElementValue(IWebBrowser2* pWB,BSTR szElID,BSTR bstrValue){
	int ret=-1;
	if (!pWB || !szElID) return ;
	CComPtr<IHTMLDocument2> doc2=NULL;
	CComPtr<IHTMLDocument3> doc3=NULL;
	CComPtr<IHTMLElement> el=NULL;
	CComPtr<IHTMLInputElement> elx=NULL;
	HRESULT hr=pWB->get_Document((IDispatch**)&doc2);
	if (FAILED(hr)) goto end;
	hr=doc2->QueryInterface(IID_IHTMLDocument3,(void**)&doc3);
	if (FAILED(hr)) goto end;
	hr=doc3->getElementById(szElID,&el);
	if (FAILED(hr) || !el) goto end;
	hr=el->QueryInterface(IID_IHTMLInputElement,(void**)&elx);
	if (FAILED(hr) || !elx) goto end;
	hr=elx->put_value(bstrValue);
	if (FAILED(hr)) goto end;
end:
	return ;
}
void CWBEventStatic::ReadLoginInfo(){	//尝试读取账号密码
	AppOptions* appOpts = AppOptions::GetInstance();
	HUSKEY hKey;
	TCHAR szKey[STRLEN_1K] = { 0 };
	_stprintf_s(szKey, L"Software\\Discoverx2\\%s", AppOptions::szCurrentSiteUrl);
	DWORD dwValueType = REG_SZ;
	DWORD dwSize = sizeof(TCHAR)*MAX_PATH;
	if (SHRegOpenUSKey(szKey, KEY_READ, NULL, &hKey, FALSE) == ERROR_SUCCESS){
		dwSize = sizeof(TCHAR)*MAX_PATH;
		SHRegQueryUSValue(hKey, L"uid", &dwValueType, appOpts->UserName, &dwSize, FALSE, NULL, 0);
		dwSize = sizeof(TCHAR)*MAX_PATH;
		SHRegQueryUSValue(hKey, L"pwd", &dwValueType, appOpts->Password, &dwSize, FALSE, NULL, 0);
		SHRegCloseUSKey(hKey);
	}
}
