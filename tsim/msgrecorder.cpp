#include "stdafx.h"
#include "msgrecorder.h"
#include "options.h"

MsgRecorder::MsgRecorder(TCHAR* pdir){
	m_oldContent=NULL;
	//m_headAppended=FALSE;
	m_hasContent=FALSE;
	m_file=INVALID_HANDLE_VALUE;
	ZeroMemory(m_fn,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(m_pdir,sizeof(TCHAR)*MAX_PATH);
	_tcscpy_s(m_pdir,pdir);
	ZeroMemory(m_cached,sizeof(TCHAR)*CACHE_SIZE);
	SYSTEMTIME st;
	GetLocalTime(&st);
	TCHAR szDt[STRLEN_SMALL]={0};
	//_stprintf_s(szDt,L"%04d%02d%d%02d%02d%02d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	_stprintf_s(szDt,L"%04d%02d%02d", st.wYear, st.wMonth, st.wDay);
	AppOptions* appOpts=AppOptions::GetInstance();
	TCHAR dir[MAX_PATH]={0};
	size_t fll=_tcslen(appOpts->LocalMessagePath);
	if(appOpts->LocalMessagePath[fll-1]==L'\\') _stprintf_s(dir,L"%s%s\\",appOpts->LocalMessagePath,pdir);
	else _stprintf_s(dir,L"%s\\%s\\",appOpts->LocalMessagePath,pdir);
	SHCreateDirectory(NULL,dir);
	_stprintf_s(m_fn,L"%s%s.dat",dir,szDt);

	m_file=CreateFile(m_fn,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if(m_file!=INVALID_HANDLE_VALUE && m_file!=NULL){
		DWORD dwSize=0;
		m_hasContent=((dwSize=GetFileSize(m_file,NULL))!=INVALID_FILE_SIZE && dwSize>0);
		if(m_hasContent){
			DWORD dwBytesRead = 0;
			char *bs = new char[dwSize+1];
			ZeroMemory(bs,dwSize+1);
			if(FALSE==ReadFile(m_file, bs, dwSize, &dwBytesRead, NULL)){
				SAFE_DEL_ARR(bs);
				CloseHandle(m_file);
				m_file=INVALID_HANDLE_VALUE;
				return;
			}
			USES_CONVERSION;
			TCHAR* szJS=A2T(bs);
			size_t l=_tcslen(szJS)+1;
			m_oldContent=new TCHAR[l];
			ZeroMemory(m_oldContent,sizeof(TCHAR)*l);
			_tcscpy_s(m_oldContent,l,szJS);
			SAFE_DEL_ARR(bs);
		}
		CloseHandle(m_file);
		m_file=INVALID_HANDLE_VALUE;
	}

	m_file=CreateFile(m_fn,GENERIC_WRITE,0,NULL,CREATE_NEW,FILE_ATTRIBUTE_NORMAL,NULL);
	if(GetLastError()==ERROR_FILE_EXISTS) m_file=CreateFile(m_fn,FILE_APPEND_DATA,0,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
}
MsgRecorder::~MsgRecorder(){
	SAFE_DEL_ARR(m_oldContent);
	DWORD dwBytesWritten=0;
	char* szBuffer=NULL;
	DWORD dwBytesToWrite=0;
	if(CheckFileHandle(FALSE)){
		//if(!m_headAppended){
		//	if(FALSE == WriteFile(m_file,XML_HEAD,(DWORD)(strlen(XML_HEAD)),&dwBytesWritten,NULL)){
		//		ErrMsg(GetLastError(),L"无法写入文件：%s，请联系系统管理员！");
		//	}
		//	m_headAppended=TRUE;
		//}

		USES_CONVERSION;
		szBuffer=T2A(m_cached);
		dwBytesToWrite=(DWORD)(strlen(szBuffer));
		if(FALSE == WriteFile(m_file,szBuffer,dwBytesToWrite,&dwBytesWritten,NULL)){
			ErrMsg(GetLastError(),L"无法写入文件：%s，请联系系统管理员！");
		}

		//if(FALSE == WriteFile(m_file,XML_TAIL,(DWORD)(strlen(XML_TAIL)),&dwBytesWritten,NULL)){
		//	ErrMsg(GetLastError(),L"无法写入文件：%s，请联系系统管理员！");
		//}
	}
	if(m_file!=INVALID_HANDLE_VALUE)CloseHandle(m_file);
}
BOOL MsgRecorder::RecordMsg(TCHAR* lid,TCHAR* szMsg,TCHAR* szCN,TCHAR* szDT,TCHAR* szStyle){
	size_t l=_tcslen(szMsg)+STRLEN_1K;
	TCHAR *xml=new TCHAR[l];
	ZeroMemory(xml,l*sizeof(TCHAR));
	if(szStyle!=NULL && _tcslen(szStyle)>0){
		_stprintf_s(xml,l,L"%s{id:'%s',cn:'%s',dt:'%s',style:'%s',msg:'%s'}\r\n",(m_hasContent?L",":L""),lid,szCN,szDT,szStyle,szMsg);
	}else{
		_stprintf_s(xml,l,L"%s{id:'%s',cn:'%s',dt:'%s',msg:'%s'}\r\n",(m_hasContent?L",":L""),lid,szCN,szDT,szMsg);
	}
	BOOL ret=Append(xml);
	SAFE_DEL_ARR(xml);
	m_hasContent=ret;
	return ret;
}
BOOL MsgRecorder::RecordError(TCHAR* lid,TCHAR* errDesc){
	if(errDesc==NULL) return TRUE;
	size_t l=_tcslen(errDesc)+STRLEN_SMALL;
	TCHAR *xml=new TCHAR[l];
	ZeroMemory(xml,l*sizeof(TCHAR));
	_stprintf_s(xml,l,L"%s{id:'%s',err:'%s'}\r\n",(m_hasContent?L",":L""),lid,errDesc);
	BOOL ret=Append(xml);
	SAFE_DEL_ARR(xml);
	m_hasContent=ret;
	return ret;
}
BOOL MsgRecorder::RecordInfo(TCHAR* lid,TCHAR* info){
	if(info==NULL) return TRUE;
	size_t l=_tcslen(info)+STRLEN_SMALL;
	TCHAR *xml=new TCHAR[l];
	ZeroMemory(xml,l*sizeof(TCHAR));
	_stprintf_s(xml,l,L"%s{id:'%s',info:'%s'}\r\n",(m_hasContent?L",":L""),lid,info);
	BOOL ret=Append(xml);
	SAFE_DEL_ARR(xml);
	m_hasContent=ret;
	return ret;
}
BOOL MsgRecorder::CheckFileHandle(BOOL alertFlag){
	if(m_file==INVALID_HANDLE_VALUE || m_file==NULL){
		if(alertFlag){
			ErrMsg(0,L"无法创建文件，请联系系统管理员！");
		}
		return FALSE;
	}
	return TRUE;
}
BOOL MsgRecorder::Append(TCHAR* result){
	if(!CheckFileHandle(TRUE)) return FALSE;
	size_t lc=_tcslen(m_cached);
	if(result==NULL) return FALSE;
	size_t l=_tcslen(result);
	if(l==0) return FALSE;
	int offset=(CACHE_SIZE-lc-l);
	if(offset>=1){
		if(lc==0){_tcscpy_s(m_cached,result);}
		else {_tcscat_s(m_cached,result);}
	}else{
		DWORD dwBytesWritten=0;
		char* szBuffer=NULL;
		DWORD dwBytesToWrite=0;

		//char* szBuffer=XML_HEAD;
		//DWORD dwBytesToWrite=(DWORD)(strlen(szBuffer));
		//if(!m_headAppended){
		//	if(FALSE == WriteFile(m_file,XML_HEAD,(DWORD)(strlen(XML_HEAD)),&dwBytesWritten,NULL)){
		//		return FALSE;
		//	}
		//	m_headAppended=TRUE;
		//}

		USES_CONVERSION;
		szBuffer=T2A(m_cached);
		dwBytesToWrite=(DWORD)(strlen(szBuffer));
		if(FALSE == WriteFile(m_file,szBuffer,dwBytesToWrite,&dwBytesWritten,NULL)){
			return FALSE;
		}
		ZeroMemory(m_cached,sizeof(TCHAR)*CACHE_SIZE);

		dwBytesWritten=0;
		szBuffer=T2A(result);
		dwBytesToWrite=(DWORD)(strlen(szBuffer));
		if(FALSE == WriteFile(m_file,szBuffer,dwBytesToWrite,&dwBytesWritten,NULL)){
			return FALSE;
		}
	}
	return TRUE;
}