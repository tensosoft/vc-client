// mainwin.h
//
#pragma once
#include "webhost.h"
#include "infonotify.h"
#include "filetransfer.h"
#include <sapi.h>
#include <map>
using namespace std;

#ifndef _MAINWIN_H_
#define _MAINWIN_H_

#define DeleteWindow(hwnd)	DestroyWindow(hwnd);(hwnd)=0

#define SHAKEHAND_SEND_PREFIX	L"99C1AE348102436A8482131C9F77E6DD"
#define NOTIFIER_PREFIX	L"5CA9C157652D4A42BA051DEE6B2E6928"
#define STYLE_PREFIX L"BD7565A3631846509D4A495629779AB3"
#define FILE_TRANSFER_PREFIX L"B35ADFC0044D4FC1B0C3A55A56119241"

#define WM_NMRECEIVED WM_APP+201
#define WM_SENDSTATUSCHANGED WM_APP+203
#define WM_NETERROR WM_APP+205
#define WM_SENDIM WM_APP+206
#define WM_GET_SEND_MSGID WM_APP+207
#define WM_FT_ACCEPT WM_APP+208
#define WM_FT_REFUSE WM_APP+209
#define WM_FT_ASK WM_APP+210
#define WM_FT_CANCEL WM_APP+211

enum IMMSGSUBTYPE{IM,SHAKEHAND,NOTIFIER,STYLE,FILETRANSFER};
enum NOTIFIERTYPE{DOCUMENTLINK/*文档链接*/,USERSTATE/*用户状态*/,SYSTEMBROADCASE/*系统广播*/};

typedef struct tagReceivedMessage{
	DWORD msgId;
	IMMSGSUBTYPE subType;
	LONG notifyType;
	TCHAR szFrom[STRLEN_SMALL];
	TCHAR szDT[STRLEN_SMALL];
	TCHAR szUNID[STRLEN_SMALL];
	TCHAR* szMsg;
} ReceivedMessage;

typedef struct tagUserInfo{
	UINT sc;
	TCHAR ou0[STRLEN_SMALL];
	TCHAR cn[STRLEN_SMALL];
	TCHAR ip[STRLEN_SMALL];
} UserInfo;

typedef struct tagChatWinInfo{
	HWND hWnd;
	UINT sc;
	TCHAR ou0[STRLEN_SMALL];
	TCHAR cn[STRLEN_SMALL];
	TCHAR ip[STRLEN_SMALL];
	TCHAR style[MAX_PATH];
} ChatWinInfo;

typedef struct tagSendStatus{
	DWORD msgId;
	DWORD code;
	DWORD ipv;
} SendStatus;

typedef pair<ULONG, ChatWinInfo*> CWPair;
typedef map<ULONG, ChatWinInfo*> CWMap;
typedef map<ULONG, ChatWinInfo*>::iterator CWItr;

class MainWin;

typedef struct tagFileSendListenerContext{
	DWORD tid;																//发送文件线程的ID
	HANDLE thread;														//发送文件线程的句柄
	SOCKET socket;
	BOOL waiting;
	MainWin *pmw;
	TCHAR address[STRLEN_SMALL];
	USHORT port;
}FSLContext,*PFSLContext;

//事件处理实现
class CWBEventStatic:public CWBEvent{
public:
	MainWin* m_pMainWin;
	virtual void DocumentComplete(IDispatch *pDisp,VARIANT *URL);
	virtual void BeforeNavigate2( IDispatch *pDisp, VARIANT *&url, VARIANT *&Flags, VARIANT *&TargetFrameName, VARIANT *&PostData, VARIANT *&Headers, VARIANT_BOOL *&Cancel);
	virtual int GetElementValue(IWebBrowser2* pWB,BSTR szElID,BSTR* pbstrValue);
	virtual void SetElementValue(IWebBrowser2* pWB,BSTR szElID,BSTR bstrValue);
private:
	virtual void OnLoginDocumentComplete(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnAutoOptionsChange(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnIndexDocumentComplete(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnSelectSitesDocumentComplete(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnSelectSiteOkDocumentComplete(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnSubscribeClosed(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void NewTSIMSession(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void SetUserInfo(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void SetPresence(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void SyncSubscribedServices(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void SyncSid(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnErrorDocumentComplete(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void ReadLoginInfo();
};

// 主程序窗口对应的类定义
class MainWin
{
public:
	MainWin();
	~MainWin();

	HWND m_hWnd;																									//主窗口句柄
	HWND m_hWndStatic;																						//浏览器宿主容器窗口句柄
	HWND m_hWndStatusbar;																					//状态栏窗口句柄
	BOOL m_blIsLogon;																							//是否已登录
	BOOL m_blCanConnect;																					//网络是否可连接
	WORD m_presence;																							//在线状态
	WebHostBase m_webBrowser;																			//嵌入的WebBrowser对象
	CWBEventStatic m_webBrowserEvent;															//嵌入的WebBrowser对象的事件处理程序
	TCHAR m_szSystemName[MAX_PATH];																//系统名称
	TCHAR m_szBelong[MAX_PATH];																		//系统使用单位
	TCHAR m_szProvider[MAX_PATH];																	//系统提供单位
	CWMap m_chatwininfo;																					//已接收到的即时消息对应的即时消息窗口信息
	UserInfo m_userInfo;																					//当前用户信息
	TCHAR m_curErrMsg[STRLEN_1K];																	//当前错误信息
	TCHAR m_requestip[MAX_PATH];																	//请求ip
	TCHAR m_clientip[MAX_PATH];																		//客户端所有ip
	TCHAR m_serverip[MAX_PATH];																		//服务器端所有ip

	PFSLContext pfslc;																						//
	ISpVoice * pVoice;																						//

	static HRESULT RegisterClass();																																			//注册窗口类
	static LRESULT CALLBACK WindowProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam);								//主窗口使用的窗口过程
	static LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);								//关于对话框使用的窗口过程
	static int GetVersionString(TCHAR* szVersion,int cchVersionLen);																		//获取当前执行程序的版本信息
	static int GetIEVersion(int* major,int* minor);																											//获取本机ie主次版本号
	static BOOL CheckUrlConnectable(const TCHAR* const szUrl);																					//检查到指定url的网络是否可连接
	static ULONG GetIPValue(TCHAR* szIP);																																//取ip地址对应数字值
	static __int64 TimeDiff(SYSTEMTIME st1,SYSTEMTIME st2);																							//计算两个时间的差距
	static BOOL ParseSendStatus(TCHAR* s,SendStatus* pss);																							//
	static BOOL ParseReceivedMessage(TCHAR* content,ReceivedMessage* prm);															//
	static void SetChatWinCaption(ChatWinInfo* pcwi,HWND hWnd);																					//
	static BOOL GetLocalMsgId(TCHAR* result,size_t l);																									//
	
	HRESULT ProcessKeystrokes(IOleObject* pObj, MSG* msg);																							//
	void PlayWaveResource(UINT resID,LPCTSTR rtName=L"WAV");																						//
	void SetStatusText(TCHAR* szText);																																	//设置状态栏文本
	void ShowNotifyIconContextMenu();																																		//显示任务栏通知图标菜单
	void SetMenuState();																																								//设置主窗口菜单状态
	void ShowNetErrPage(TCHAR* szMsg);																																	//显示网络连接错误内容
	int BuildLoginCredential(TCHAR* szOut,size_t cchOut);																								//构造自动登录的凭据
	HWND NewIMSession(TCHAR* szIP);																																			//
	BOOL CheckSessionBeforeClose();																																			//
	LRESULT OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam);																					//处理WM_COMMAND消息
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam);																					//处理WM_CREATE消息
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam);																					//处理WM_DESTROY消息
	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam);																						//处理WM_CLOSE消息
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam);																						//处理WM_SIZE消息
	LRESULT OnGetMinMaxInfo(UINT uMsg, WPARAM wParam, LPARAM lParam);																		//处理WM_GETMINMAXINFO消息
	LRESULT OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam);																					//处理WM_NOFITY消息
	LRESULT OnActiveApp(UINT uMsg, WPARAM wParam, LPARAM lParam);																				//处理WM_ACTIVATEAPP消息
	void OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam);																							//处理WM_TIMER消息
	LRESULT OnNetMsgReceived(UINT uMsg, WPARAM wParam, LPARAM lParam);																	//处理WM_NMRECEIVED消息
	LRESULT OnSendStatusChanged(UINT uMsg, WPARAM wParam, LPARAM lParam);																//处理WM_SENDSTATUSCHANGED消息
	LRESULT OnNetError(UINT uMsg, WPARAM wParam, LPARAM lParam);																				//处理WM_NETERROR消息
	LRESULT OnSendIM(UINT uMsg, WPARAM wParam, LPARAM lParam);																					//处理WM_SENDIM消息
	LRESULT OnReceivedIM(ReceivedMessage& rmsg);
	LRESULT OnReceivedNotifier(ReceivedMessage& rmsg);
	LRESULT OnReceivedStyle(ReceivedMessage& rmsg);
	LRESULT OnFileTransfer(ReceivedMessage& rmsg);
};

#endif //_MAINWIN_H_