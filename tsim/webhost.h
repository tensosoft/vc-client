#pragma once

#ifndef _WEBHOST_H_
#define _WEBHOST_H_

#include <windows.h>
#include <exdisp.h>
#include <exdispid.h>
#include <mshtmhst.h>
#include <mshtml.h>
#include <oaidl.h>

#define NOTIMPLEMENTED __asm{ int 3 }; return E_NOTIMPL

class WebHostBase;

class CWBEvent:public DWebBrowserEvents2{
public:
	WebHostBase* host;
	// IUnknown 成员
  HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid,void ** ppvObject);
	ULONG STDMETHODCALLTYPE AddRef(void);
	ULONG STDMETHODCALLTYPE Release(void);
	//IDispatch
	HRESULT STDMETHODCALLTYPE GetTypeInfoCount(UINT *pctinfo) ;
	HRESULT STDMETHODCALLTYPE GetTypeInfo(UINT iTInfo,LCID lcid,ITypeInfo **ppTInfo);
	HRESULT STDMETHODCALLTYPE GetIDsOfNames(REFIID riid,LPOLESTR *rgszNames,UINT cNames, LCID lcid,DISPID *rgDispId) ;
	HRESULT STDMETHODCALLTYPE Invoke( DISPID  dispIdMember, REFIID  riid, LCID  lcid, WORD  wFlags, DISPPARAMS*  pDispParams, VARIANT*  pVarResult, EXCEPINFO*  pExcepInfo, UINT*  puArgErr ) ;
	//DWebBrowserEvents2
	virtual void BeforeNavigate2( IDispatch *pDisp, VARIANT *&url, VARIANT *&Flags, VARIANT *&TargetFrameName, VARIANT *&PostData, VARIANT *&Headers, VARIANT_BOOL *&Cancel);
	void ClientToHostWindow( long *&CX, long *&CY);
	void CommandStateChange( long Command, VARIANT_BOOL Enable);
	virtual void DocumentComplete(IDispatch *pDisp,VARIANT *URL);
	void DownloadBegin(VOID);
	void DownloadComplete(VOID);
	void FileDownload( VARIANT_BOOL *&ActiveDocument, VARIANT_BOOL *&Cancel);
	void NavigateComplete2( IDispatch *pDisp, VARIANT *URL);
	void NavigateError( IDispatch *pDisp, VARIANT *URL, VARIANT *TargetFrameName, VARIANT *StatusCode, VARIANT_BOOL *&Cancel);
	virtual void NewWindow2( IDispatch **&ppDisp, VARIANT_BOOL *&Cancel);
	void NewWindow3( IDispatch **&ppDisp, VARIANT_BOOL *&Cancel, DWORD dwFlags, BSTR bstrUrlContext, BSTR bstrUrl);
	void OnFullScreen( VARIANT_BOOL FullScreen);
	void OnMenuBar( VARIANT_BOOL MenuBar);
	void OnQuit(VOID);
	void OnStatusBar( VARIANT_BOOL StatusBar);
	void OnTheaterMode( VARIANT_BOOL TheaterMode);
	void OnToolBar( VARIANT_BOOL ToolBar);
	void OnVisible( VARIANT_BOOL Visible);
	void PrintTemplateInstantiation( IDispatch *pDisp);
	void PrintTemplateTeardown( IDispatch *pDisp);
	void PrivacyImpactedStateChange( VARIANT_BOOL PrivacyImpacted);
	void ProgressChange( long Progress, long ProgressMax);
	void PropertyChange( BSTR szProperty);
	void SetSecureLockIcon( LONG SecureLockIcon);
	void StatusTextChange( BSTR Text);
	void TitleChange( BSTR Text);
	void WindowClosing( VARIANT_BOOL IsChildWindow, VARIANT_BOOL *&Cancel);
	void WindowSetHeight( long Height);
	void WindowSetLeft( long Left);
	void WindowSetResizable( VARIANT_BOOL Resizable);
	void WindowSetTop( long Top);
	void WindowSetWidth( long Width);
};

class CNullStorage:public IStorage
{
public:
  // IUnknown 成员
  STDMETHODIMP QueryInterface(REFIID riid,void ** ppvObject);
  STDMETHODIMP_(ULONG) AddRef(void);
  STDMETHODIMP_(ULONG) Release(void);
  // IStorage 成员
  STDMETHODIMP CreateStream(const WCHAR * pwcsName,DWORD grfMode,DWORD reserved1,DWORD reserved2,IStream ** ppstm);
  STDMETHODIMP OpenStream(const WCHAR * pwcsName,void * reserved1,DWORD grfMode,DWORD reserved2,IStream ** ppstm);
  STDMETHODIMP CreateStorage(const WCHAR * pwcsName,DWORD grfMode,DWORD reserved1,DWORD reserved2,IStorage ** ppstg);
  STDMETHODIMP OpenStorage(const WCHAR * pwcsName,IStorage * pstgPriority,DWORD grfMode,SNB snbExclude,DWORD reserved,IStorage ** ppstg);
  STDMETHODIMP CopyTo(DWORD ciidExclude,IID const * rgiidExclude,SNB snbExclude,IStorage * pstgDest);
  STDMETHODIMP MoveElementTo(const OLECHAR * pwcsName,IStorage * pstgDest,const OLECHAR* pwcsNewName,DWORD grfFlags);
  STDMETHODIMP Commit(DWORD grfCommitFlags);
  STDMETHODIMP Revert(void);
  STDMETHODIMP EnumElements(DWORD reserved1,void * reserved2,DWORD reserved3,IEnumSTATSTG ** ppenum);
  STDMETHODIMP DestroyElement(const OLECHAR * pwcsName);
  STDMETHODIMP RenameElement(const WCHAR * pwcsOldName,const WCHAR * pwcsNewName);
  STDMETHODIMP SetElementTimes(const WCHAR * pwcsName,FILETIME const * pctime,FILETIME const * patime,FILETIME const * pmtime);
  STDMETHODIMP SetClass(REFCLSID clsid);
  STDMETHODIMP SetStateBits(DWORD grfStateBits,DWORD grfMask);
  STDMETHODIMP Stat(STATSTG * pstatstg,DWORD grfStatFlag);
};

class CMyFrame: public IOleInPlaceFrame
{
public:
  WebHostBase* host;
  // IUnknown 成员
  STDMETHODIMP QueryInterface(REFIID riid,void ** ppvObject);
  STDMETHODIMP_(ULONG) AddRef(void);
  STDMETHODIMP_(ULONG) Release(void);
  // IOleWindow 成员
	STDMETHODIMP GetWindow(HWND FAR* lphwnd);
	STDMETHODIMP ContextSensitiveHelp(BOOL fEnterMode);
  // IOleInPlaceUIWindow 成员
  STDMETHODIMP GetBorder(LPRECT lprectBorder);
  STDMETHODIMP RequestBorderSpace(LPCBORDERWIDTHS pborderwidths);
  STDMETHODIMP SetBorderSpace(LPCBORDERWIDTHS pborderwidths);
  STDMETHODIMP SetActiveObject(IOleInPlaceActiveObject *pActiveObject,LPCOLESTR pszObjName);
  // IOleInPlaceFrame 成员
  STDMETHODIMP InsertMenus(HMENU hmenuShared,LPOLEMENUGROUPWIDTHS lpMenuWidths);
  STDMETHODIMP SetMenu(HMENU hmenuShared,HOLEMENU holemenu,HWND hwndActiveObject);
  STDMETHODIMP RemoveMenus(HMENU hmenuShared);
  STDMETHODIMP SetStatusText(LPCOLESTR pszStatusText);
  STDMETHODIMP EnableModeless(BOOL fEnable);
  STDMETHODIMP TranslateAccelerator(LPMSG lpmsg,WORD wID);

};

class CMySite
: public IOleClientSite,
  public IOleInPlaceSite,
	public IDocHostUIHandler,
	public IDocHostShowUI
{
public:
  WebHostBase* host;

  // IUnknown 成员
  STDMETHODIMP QueryInterface(REFIID riid,void ** ppvObject);
  STDMETHODIMP_(ULONG) AddRef(void);
  STDMETHODIMP_(ULONG) Release(void);
  // IOleClientSite 成员
  STDMETHODIMP SaveObject();
  STDMETHODIMP GetMoniker(DWORD dwAssign,DWORD dwWhichMoniker,IMoniker ** ppmk);
  STDMETHODIMP GetContainer(LPOLECONTAINER FAR* ppContainer);
  STDMETHODIMP ShowObject();
  STDMETHODIMP OnShowWindow(BOOL fShow);
  STDMETHODIMP RequestNewObjectLayout();
  // IOleWindow
	STDMETHODIMP GetWindow(HWND FAR* lphwnd);
	STDMETHODIMP ContextSensitiveHelp(BOOL fEnterMode);
	// IOleInPlaceSite 成员
	STDMETHODIMP CanInPlaceActivate();
	STDMETHODIMP OnInPlaceActivate();
	STDMETHODIMP OnUIActivate();
	STDMETHODIMP GetWindowContext(LPOLEINPLACEFRAME FAR* lplpFrame,LPOLEINPLACEUIWINDOW FAR* lplpDoc,LPRECT lprcPosRect,LPRECT lprcClipRect,LPOLEINPLACEFRAMEINFO lpFrameInfo);
	STDMETHODIMP Scroll(SIZE scrollExtent);
	STDMETHODIMP OnUIDeactivate(BOOL fUndoable);
	STDMETHODIMP OnInPlaceDeactivate();
	STDMETHODIMP DiscardUndoState();
	STDMETHODIMP DeactivateAndUndo();
	STDMETHODIMP OnPosRectChange(LPCRECT lprcPosRect);
	// IDocHostUIHandler 成员
	STDMETHODIMP ShowContextMenu( DWORD dwID, POINT __RPC_FAR *ppt, IUnknown __RPC_FAR *pcmdTarget, IDispatch __RPC_FAR *pdispReserved);
	STDMETHODIMP GetHostInfo( DOCHOSTUIINFO __RPC_FAR *pInfo);
	STDMETHODIMP ShowUI(DWORD dwID, IOleInPlaceActiveObject __RPC_FAR *pActiveObject, IOleCommandTarget __RPC_FAR *pCommandTarget, IOleInPlaceFrame __RPC_FAR *pFrame, IOleInPlaceUIWindow __RPC_FAR *pDoc);
	STDMETHODIMP HideUI(void);
	STDMETHODIMP UpdateUI(void);
	STDMETHODIMP EnableModeless( BOOL fEnable);
	STDMETHODIMP OnDocWindowActivate( BOOL fEnable);
	STDMETHODIMP OnFrameWindowActivate( BOOL fEnable);
	STDMETHODIMP ResizeBorder( LPCRECT prcBorder, IOleInPlaceUIWindow __RPC_FAR *pUIWindow, BOOL fRameWindow);
	STDMETHODIMP TranslateAccelerator( LPMSG lpMsg, const GUID __RPC_FAR *pguidCmdGroup, DWORD nCmdID);
	STDMETHODIMP GetOptionKeyPath( LPOLESTR __RPC_FAR *pchKey, DWORD dw);
	STDMETHODIMP GetDropTarget( IDropTarget __RPC_FAR *pDropTarget, IDropTarget __RPC_FAR *__RPC_FAR *ppDropTarget);
	STDMETHODIMP GetExternal( IDispatch __RPC_FAR *__RPC_FAR *ppDispatch);
	STDMETHODIMP TranslateUrl( DWORD dwTranslate, OLECHAR __RPC_FAR *pchURLIn, OLECHAR __RPC_FAR *__RPC_FAR *ppchURLOut);
	STDMETHODIMP FilterDataObject( IDataObject __RPC_FAR *pDO, IDataObject __RPC_FAR *__RPC_FAR *ppDORet);
	// IDocHostShowUI成员
	STDMETHODIMP ShowMessage(HWND hwnd, LPOLESTR lpstrText,LPOLESTR lpstrCaption,DWORD dwType,LPOLESTR lpstrHelpFile, DWORD dwHelpContext,LRESULT *plResult);
	STDMETHODIMP ShowHelp(HWND hwnd,LPOLESTR pszHelpFile,UINT uCommand,DWORD dwData,POINT ptMouse,IDispatch *pDispatchObjectHit);
};

class CMyContainer
: public IOleContainer
{
public:
  // IUnknown
  STDMETHODIMP QueryInterface(REFIID riid,void ** ppvObject);
  STDMETHODIMP_(ULONG) AddRef(void);
  STDMETHODIMP_(ULONG) Release(void);
  // IParseDisplayName
  STDMETHODIMP ParseDisplayName(IBindCtx *pbc,LPOLESTR pszDisplayName,ULONG *pchEaten,IMoniker **ppmkOut);
  // IOleContainer
  STDMETHODIMP EnumObjects(DWORD grfFlags,IEnumUnknown **ppenum);
  STDMETHODIMP LockContainer(BOOL fLock);
};

class WebHostBase{
public:
	WebHostBase();
	~WebHostBase();

	void CreateEmbeddedWebControl(TCHAR* url);
  void UnCreateEmbeddedWebControl(void);
	void Resize();
	void EventAdivse(CWBEvent* evt);
	void EventUnadivse();
	void NavigateTo(TCHAR* url);
	HRESULT CallJavaScript(BSTR jsFunc);
  HWND operator =(WebHostBase* rhs);

	HWND m_hParentWnd;			//所在的直接窗口对应的容器窗口
	HWND hwnd;							//所在的直接窗口
  ULONG mcRef;

	DWORD m_dwCookie;

  CNullStorage storage;
  CMySite site;
  CMyFrame frame;

  IOleObject* mpWebObject;

	virtual ULONG AddRef();
  virtual ULONG Release();
};

#endif //_WEBHOST_H_