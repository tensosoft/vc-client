//rtcim.cpp
#include "stdafx.h"
#include "chatwin.h"
#include "options.h"
#include <atlstr.h>

#define IDT_CHECKMSGTIMEOUT		1
#define CHECKMSGTIMEOUT_PERIOD 1000
#define MSG_SEND_TIMEOUT_INSECONDS 30

ChatWin::ChatWin(HWND hwnd,MainWin* pmw){
	m_hWnd=hwnd;
	m_mainWin=pmw;
	m_hWndStatic=NULL;
	ZeroMemory(m_targetIP,sizeof(TCHAR)*STRLEN_SMALL);
	ZeroMemory(m_targetCN,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(m_style,sizeof(TCHAR)*MAX_PATH);
	m_webBrowserReady=FALSE;
	m_pendingDisplayInfo=NULL;
	m_sendMsgColor=(DWORD)RGB(0,0,0);
	m_msgRecorder=NULL;
	m_showHistoryFlag=FALSE;
	m_netOKFlag=TRUE;
	
	m_webBrowser.m_hParentWnd=m_hWnd;
	m_webBrowserEvent.m_pChatWin=this;
	m_webBrowserEvent.host=&m_webBrowser;
}
ChatWin::~ChatWin(){
}
HRESULT ChatWin::RegisterClass(){
	// 注册窗口类
	WNDCLASS wc;
	ATOM atom;

	ZeroMemory(&wc, sizeof(WNDCLASS));
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = (WNDPROC)ChatWin::WindowProc;
	wc.hInstance     = GetModuleHandle(NULL);
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = IM_CLASS;
	wc.hIcon			 = LoadIcon(wc.hInstance,MAKEINTRESOURCE(IDI_SMALL));
	atom = ::RegisterClass(&wc);

	if ( !atom ) return E_FAIL;

	return S_OK;
}
LRESULT CALLBACK ChatWin::WindowProc(HWND hWnd, UINT uMsg,WPARAM wParam,LPARAM lParam){
	ChatWin * me = NULL;
	LRESULT  lr = 0;

	if ( uMsg == WM_CREATE ){
		CREATESTRUCT *pcs=(CREATESTRUCT*)lParam;
		MainWin* pmw=(MainWin*)pcs->lpCreateParams;
		me = new ChatWin(hWnd,pmw);
		lr=me->OnCreate(uMsg,wParam,lParam);
		SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)me);
	}else{
		me = (ChatWin *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
		switch( uMsg ){        
		case WM_DESTROY:
			if(me){
				me->OnDestroy(uMsg,wParam,lParam);
				delete me;					// 删除MainWin对象实例
			}
			break;
		case WM_CLOSE:
			if(me) lr=me->OnClose(uMsg,wParam,lParam);
			break;
		case WM_SIZE:
			if(me) lr=me->OnSize(uMsg,wParam,lParam);
			break;
		case WM_COMMAND:
			if(me) lr=me->OnCommand(uMsg,wParam,lParam);
			break;
		case WM_NOTIFY:
			if(me) lr=me->OnNotify(uMsg,wParam,lParam);
			break;
		case WM_TIMER:
			if(me) me->OnTimer(uMsg,wParam,lParam);
			break;
		case WM_GETMINMAXINFO:
			if(me) lr=me->OnGetMinMaxInfo(uMsg,wParam,lParam);
			break;
		case WM_ACTIVATEAPP:
			if(me) lr=me->OnActiveApp(uMsg,wParam,lParam);
			break;
		case WM_NMRECEIVED:
			if(me) lr=me->OnIMReceived(uMsg,wParam,lParam);
			return 0;
		case WM_SENDSTATUSCHANGED:
			if(me) lr=me->OnSendStatusChanged(uMsg,wParam,lParam);
			return 0;
		case WM_NETERROR:
			if(me) lr=me->OnNetError(uMsg,wParam,lParam);
			return 0;
		case WM_GET_SEND_MSGID:
			if(me) lr=me->OnGetMsgId(uMsg,wParam,lParam);
			return 0;
		case WM_FT_ASK:
			if(me) lr=me->OnAskFile(uMsg,wParam,lParam);
			return 0;
		case WM_FT_ACCEPT:
			if(me) lr=me->OnAcceptFile(uMsg,wParam,lParam);
			return 0;
		case WM_FT_REFUSE:
			if(me) lr=me->OnRefuseFile(uMsg,wParam,lParam);
			return 0;
		case WM_FT_COMPLETE:
			if(me) lr=me->OnFTComplete(uMsg,wParam,lParam);
			return 0;
		case WM_FT_ERROR:
			if(me) lr=me->OnFTError(uMsg,wParam,lParam);
			return 0;
		case WM_FT_PROGRESS:
			if(me) lr=me->OnFTProgress(uMsg,wParam,lParam);
			return 0;
		case WM_FT_SENDFDATA:
			if(me) lr=me->OnFTSendFData(uMsg,wParam,lParam);
			return 0;
		case WM_FT_CANCEL:
			if(me) lr=me->OnFTCanceled(uMsg,wParam,lParam);
			return 0;
		default:
			lr = DefWindowProc( hWnd, uMsg, wParam, lParam );
		}
	}

	return lr;
}
LRESULT ChatWin::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam){
	RECT rc;
	GetClientRect(m_hWnd,&rc);

	//创建WebBrowser控件容器窗口
	if (m_hWndStatic!=NULL) DeleteWindow(m_hWndStatic);
	m_hWndStatic= CreateWindowEx(0,L"STATIC",L"", WS_CHILD | WS_VISIBLE,0, 0,(rc.right-rc.left),(rc.bottom-rc.top) ,m_hWnd, (HMENU)IDC_HEADERSTATIC, GetModuleHandle(NULL), NULL); 
	if (!m_hWndStatic) return -1;
	m_webBrowser.hwnd=m_hWndStatic;
	
	return 0;
}
LRESULT ChatWin::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam){
	RECT rc;
	GetClientRect(m_hWnd, &rc);
	
	int winh=(int)(short)HIWORD(lParam);
	//设置容器控件大小
	int staticw=(int)(short)LOWORD(lParam);
	int statich=(rc.bottom-rc.top);
	SetWindowPos(m_hWndStatic,HWND_TOP,0,0,staticw,statich,SWP_SHOWWINDOW);

	//嵌入浏览器对象（WebBrowser)
	if (!m_webBrowser.mpWebObject){
		TCHAR szUrl[STRLEN_1K]={0};
		TCHAR fn[MAX_PATH]={0};
		int len=GetModuleFileName(NULL,fn,MAX_PATH);
		
		if (len>0){
			_stprintf_s(szUrl,L"res://%s/IDH_MSG_DISPLAY",fn);
		}else{
			_stprintf_s(szUrl,L"about:blank");
		}
		this->m_webBrowser.CreateEmbeddedWebControl(szUrl);
		m_webBrowser.EventAdivse(&m_webBrowserEvent);
		SetTimer(m_hWnd,IDT_CHECKMSGTIMEOUT,CHECKMSGTIMEOUT_PERIOD,NULL);
	}else {
		m_webBrowser.Resize();
	}

	return 0;
}
LRESULT ChatWin::OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam){
	return 0;
}
LRESULT ChatWin::OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam){
	return 0;
}
LRESULT ChatWin::OnActiveApp(UINT uMsg, WPARAM wParam, LPARAM lParam){
	return 0;
}
void ChatWin::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam){
	switch((UINT)wParam){
	case IDT_CHECKMSGTIMEOUT:
		{
			ITROUTINFO itr=m_sendinfo.begin();
			while(itr!=m_sendinfo.end()){
				SendInfo *si=(*itr);
				SYSTEMTIME curdt;
				GetLocalTime(&curdt);
				if(si && MainWin::TimeDiff(curdt,si->msgSentDt)>MSG_SEND_TIMEOUT_INSECONDS) {
					if(si->msg==NULL){
						SAFE_DEL_PTR(si);
						itr=m_sendinfo.erase(itr);
						continue;
					}
					size_t l=_tcslen(si->msg);
					BOOL isStyle=(_tcsstr(si->msg,STYLE_PREFIX)==si->msg);
					BOOL isFileTransfer=(_tcsstr(si->msg,FILE_TRANSFER_PREFIX)==si->msg);
					if(l==0 || isStyle || isFileTransfer) {
						SAFE_DEL_ARR(si->msg);
						SAFE_DEL_PTR(si);
						itr=m_sendinfo.erase(itr);
						continue;
					}

					l+=MAX_PATH;
					TCHAR *js=new TCHAR[l];
					ZeroMemory(js,l*sizeof(TCHAR));
					TCHAR lid[STRLEN_SMALL]={0};
					MainWin::GetLocalMsgId(lid,STRLEN_SMALL);
					_stprintf_s(js,l,L"chatwin.error('%s','消息发送失败，请检查网络或联系系统管理员：<br/>%s');",lid,si->msg);
					BSTR bstrJS=SysAllocString(js);
					this->m_webBrowser.CallJavaScript(bstrJS);
					SAFE_DEL_ARR(js);
					FREE_SYS_STR(bstrJS);

					//允许继续发送
					TCHAR enableSendJS[STRLEN_SMALL]={0};
					_stprintf_s(enableSendJS,L"chatwin.enablesend(%s);",(si->echo?L"true":L"false"));
					BSTR bstrEnableSendJS=SysAllocString(enableSendJS);
					this->m_webBrowser.CallJavaScript(bstrEnableSendJS);
					FREE_SYS_STR(bstrEnableSendJS);

					SAFE_DEL_ARR(si->msg);
					SAFE_DEL_PTR(si);
					itr=m_sendinfo.erase(itr);
				}else{
					++itr;
				}
			}
		}
		break;
	}
}
LRESULT ChatWin::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam){
	if(!this->m_sendinfo.empty()){
		if (MessageBox(this->m_hWnd,L"此操作将导致部分消息无法发送，是否确定要关闭？",L"关闭确认",MB_OKCANCEL|MB_ICONQUESTION|MB_DEFBUTTON2)==IDCANCEL) return 0;
	}
	if(!this->m_ftc.empty()){
		if (MessageBox(this->m_hWnd,L"此操作将导致文件传输出现错误，是否确定要关闭？",L"关闭确认",MB_OKCANCEL|MB_ICONQUESTION|MB_DEFBUTTON2)==IDCANCEL) return 0;
	}
	DestroyWindow(m_hWnd);
	return 0;
}
LRESULT ChatWin::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam){
	KillTimer(m_hWnd,IDT_CHECKMSGTIMEOUT);

	//解除ie嵌入
	this->m_webBrowser.UnCreateEmbeddedWebControl();

	ITROUTINFO itr=m_sendinfo.begin();
	while(itr!=m_sendinfo.end()){
		SendInfo *si=(*itr);
		if(si) {
			SAFE_DEL_ARR(si->msg);
			SAFE_DEL_PTR(si);
		}
		itr++;
	}
	m_sendinfo.clear();

	ITRFTCONTEXT itrft=m_ftc.begin();
	while(itrft!=m_ftc.end()){
		PFTContext x=(*itrft);
		if(x) {
			TCHAR* szMsgOut=new TCHAR[STRLEN_SMALL];
			ZeroMemory(szMsgOut,sizeof(TCHAR)*STRLEN_SMALL);
			_stprintf_s(szMsgOut,STRLEN_SMALL,L"%s:%u:%u",FILE_TRANSFER_PREFIX,CANCEL,x->m_id);
			UINT ip=(UINT)MainWin::GetIPValue(m_targetIP);
			SendMessage(this->m_mainWin->m_hWnd,WM_SENDIM,(WPARAM)ip,(LPARAM)szMsgOut);
			SAFE_DEL_ARR(szMsgOut);
			x->m_canceled=TRUE;
			//等待发送文件内容线程关闭
			if(x->m_thread!=NULL){
				DWORD dwWaitRet=WaitForSingleObject(x->m_thread,1000);
				switch(dwWaitRet){
				case WAIT_OBJECT_0:
					break;
				case WAIT_TIMEOUT:
				default:
					TerminateThread(x->m_thread,0);
					break;
				}
			}
			SAFE_DEL_PTR(x);
			break;
		}
		itrft++;
	}
	m_ftc.clear();

	if(m_mainWin){
		CWItr itr=m_mainWin->m_chatwininfo.find(MainWin::GetIPValue(m_targetIP));
		if(itr!=m_mainWin->m_chatwininfo.end()){
			itr->second->hWnd=NULL;
			//SAFE_DEL_PTR(itr->second);
		}
	}

	SAFE_DEL_PTR(m_msgRecorder);

	this->m_mainWin=NULL;

	return 0;
}
LRESULT ChatWin::OnGetMinMaxInfo(UINT uMsg, WPARAM wParam, LPARAM lParam){
	LPMINMAXINFO p=(LPMINMAXINFO)lParam;

	p->ptMinTrackSize.x = IM_WIDTH;
	p->ptMinTrackSize.y = IM_HEIGHT;

	return 0;
}
LRESULT ChatWin::OnIMReceived(UINT uMsg, WPARAM wParam, LPARAM lParam){
	TCHAR* js=(TCHAR*)lParam;
	if(!js || _tcslen(js)==0) return 0;
	if(!m_webBrowserReady){
		m_pendingDisplayInfo=SysAllocString(js);
	}else{
		BSTR bstrJS=SysAllocString(js);
		this->m_webBrowser.CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
	}
	SAFE_DEL_ARR(js);
	NotifiyUser();
	return 0;
}
LRESULT ChatWin::OnSendStatusChanged(UINT uMsg, WPARAM wParam, LPARAM lParam){
	DWORD msgid=(DWORD)wParam;
	DWORD code=(DWORD)lParam;
	//ITROUTINFO itr;
	vector<SendInfo*>::reverse_iterator itr;
	for(itr=m_sendinfo.rbegin();itr!=m_sendinfo.rend();itr++){
		SendInfo *si=(*itr);
		if(msgid==si->msgId && code==2/*消息被目标成功接收标记*/) {
			TCHAR lid[STRLEN_SMALL]={0};
			if(si->msgId!=0){
				_stprintf_s(lid,L"%u",si->msgId);
			}else{
				MainWin::GetLocalMsgId(lid,STRLEN_SMALL);
			}
			SYSTEMTIME st;
			GetLocalTime(&st);
			TCHAR szDt[STRLEN_SMALL]={0};
			_stprintf_s(szDt,L"%04d-%02d-%02d %02d:%02d:%02d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);

			if(si->echo){
				//显示发送的内容并清空发送内容区
				size_t l=_tcslen(si->msg)+MAX_PATH;
				TCHAR *js=new TCHAR[l];
				ZeroMemory(js,l*sizeof(TCHAR));
				
				_stprintf_s(js,l,L"chatwin.senddone('%s','%s','%s','%s');",lid,szDt,this->m_mainWin->m_userInfo.cn,si->msg);
				BSTR bstrJS=SysAllocString(js);
				this->m_webBrowser.CallJavaScript(bstrJS);
				SAFE_DEL_ARR(js);
				FREE_SYS_STR(bstrJS);
			}
			//记录消息历史记录
			if(si->recordable && this->m_msgRecorder) this->m_msgRecorder->RecordMsg(lid,si->msg,m_mainWin->m_userInfo.cn,szDt,m_style);

			//允许继续发送
			TCHAR enableSendJS[STRLEN_SMALL]={0};
			_stprintf_s(enableSendJS,L"chatwin.enablesend();");
			BSTR bstrEnableSendJS=SysAllocString(enableSendJS);
			this->m_webBrowser.CallJavaScript(bstrEnableSendJS);
			FREE_SYS_STR(bstrEnableSendJS);

			SAFE_DEL_ARR(si->msg);
			SAFE_DEL_PTR(si);
			m_sendinfo.erase((++itr).base());
			break;
		}
	}
	return 0;
}
LRESULT ChatWin::OnNetError(UINT uMsg, WPARAM wParam, LPARAM lParam){
	m_netOKFlag=FALSE;
	//阻止继续发送
	TCHAR* errMsg=(TCHAR*)lParam;
	TCHAR disableSendJS[STRLEN_DEFAULT]={0};
	_stprintf_s(disableSendJS,L"chatwin.disablesend('%s');",errMsg);
	BSTR bstrDisableSendJS=SysAllocString(disableSendJS);
	this->m_webBrowser.CallJavaScript(bstrDisableSendJS);
	FREE_SYS_STR(bstrDisableSendJS);
	return 0;
}
LRESULT ChatWin::OnGetMsgId(UINT uMsg, WPARAM wParam, LPARAM lParam){
	DWORD msgid=(DWORD)wParam;
	if(msgid==0) return 0;
	//ITROUTINFO itr;
	vector<SendInfo*>::reverse_iterator itr;
	for(itr=m_sendinfo.rbegin();itr!=m_sendinfo.rend();itr++){
		SendInfo *si=(*itr);
		if(si && si->msg==(TCHAR*)lParam) {
			si->msgId=msgid;
			break;
		}
	}
	return 0;
}
LRESULT ChatWin::OnAcceptFile(UINT uMsg, WPARAM wParam, LPARAM lParam){
	DWORD fid=(DWORD)lParam;
	if(fid==0){ErrMsg(0,L"无法获取目标文件的唯一标记！");return 0;}
	
	FTContext *pftc=NULL;
	if (!FindFTContext(fid,&pftc) || !pftc){ErrMsg(0,L"无法获取目标文件相关信息！");return 0;}

	TCHAR szMsgIn[STRLEN_1K]={0};
	TCHAR lid[STRLEN_SMALL]={0};
	_stprintf_s(lid,L"ftp_%u",pftc->m_id);

	if(!ReceiveFile(pftc)){
		ErrMsg(0,L"无法接收文件内容，请联系管理员！");
	}else{
		_stprintf_s(szMsgIn,L"chatwin.info('%s','正在接收文件“%s”...');",lid,pftc->m_fn);
	}

	BSTR bstrJS=SysAllocString(szMsgIn);
	m_webBrowser.CallJavaScript(bstrJS);
	FREE_SYS_STR(bstrJS);

	RemoveFTAskLink(fid);

	return 0;
}
LRESULT ChatWin::OnRefuseFile(UINT uMsg, WPARAM wParam, LPARAM lParam){
	DWORD fid=(DWORD)lParam;
	if(fid==0){ErrMsg(0,L"无法获取被拒绝文件的唯一标记！");return 0;}

	BOOL found=FALSE;
	TCHAR szFN[MAX_PATH]={0};
	if(m_ftc.empty()) return 0;
	ITRFTCONTEXT itr;
	for(itr=m_ftc.begin();itr!=m_ftc.end();itr++){
		PFTContext x=(*itr);
		if(x && x->m_id==fid) {
			found=TRUE;
			_tcscpy_s(szFN,x->m_fn);
			SAFE_DEL_PTR(x);
			m_ftc.erase(itr);
			break;
		}
	}

	if(found){
		TCHAR szMsgIn[STRLEN_1K]={0};
		TCHAR lid[STRLEN_SMALL]={0};
		MainWin::GetLocalMsgId(lid,STRLEN_SMALL);
		_stprintf_s(szMsgIn,L"chatwin.info('%s','%s拒绝了对文件：“%s”的接收。');",lid,this->m_targetCN,szFN);

		BSTR bstrJS=SysAllocString(szMsgIn);
		this->m_webBrowser.CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
	}

	return 0;
}
LRESULT ChatWin::OnAskFile(UINT uMsg, WPARAM wParam, LPARAM lParam){
	PFTContext pftc=(PFTContext)lParam;
	if(!pftc||pftc->m_id==0 || pftc->m_fileSize==0||_tcslen(pftc->m_fn)==0) return 0;

	TCHAR lid[STRLEN_SMALL]={0};
	_stprintf_s(lid,L"%u",pftc->m_id);
	
	TCHAR szFTMsg[STRLEN_1K]={0};
	_stprintf_s(szFTMsg,L"chatwin.info('%s','%s给您发送文件：“%s”。<br/><span id=\"ftask_%u\">您可以<a href=\"#acceptfile:%u|%u|%s\">接收</a>或<a href=\"#refusefile:%u|%s\">拒绝</a>此文件。</span>');",/*lid*/lid,/*msg*/this->m_targetCN,pftc->m_fn,/*ask div*/pftc->m_id,/*accept*/pftc->m_id,pftc->m_fileSize,pftc->m_fn,/*refuse*/pftc->m_id,pftc->m_fn);

	if(!m_webBrowserReady){
		m_pendingDisplayInfo=SysAllocString(szFTMsg);
	}else{
		BSTR bstrJS=SysAllocString(szFTMsg);
		this->m_webBrowser.CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
	}

	SAFE_DEL_PTR(pftc);
	return 0;
}
LRESULT ChatWin::OnFTComplete(UINT uMsg, WPARAM wParam, LPARAM lParam){
	FTContext *pftc=(PFTContext)lParam;
	if(!pftc) return 0;

	TCHAR lid[STRLEN_SMALL]={0};
	MainWin::GetLocalMsgId(lid,STRLEN_SMALL);

	TCHAR receivedLink[STRLEN_1K]={0};
	if(!pftc->m_isSender){
		_stprintf_s(receivedLink,L"您可以<a href=\"#openfile:%s\">打开</a>或<a href=\"#explorefile:%s\" title=\"在目标文件夹中查看\">浏览</a>此文件。",pftc->m_fp,pftc->m_fp);
	}
	CAtlString atlStr(receivedLink);
	atlStr.Replace(L"\\",L"\\\\");

	TCHAR log[STRLEN_2K]={0};
	_stprintf_s(log,L"chatwin.info('%s','文件“%s”%s完毕。%s');",lid,pftc->m_fn,(pftc->m_isSender?L"发送":L"接收"),atlStr.GetString());

	BSTR bstrJS=SysAllocString(log);
	m_webBrowser.CallJavaScript(bstrJS);
	FREE_SYS_STR(bstrJS);

	RemoveFTProgress(pftc->m_id);

	if(m_ftc.empty()) return 0;
	ITRFTCONTEXT itr;
	for(itr=m_ftc.begin();itr!=m_ftc.end();itr++){
		PFTContext x=(*itr);
		if(x && (x==pftc||x->m_id==pftc->m_id)) {
			SAFE_DEL_PTR(x);
			m_ftc.erase(itr);
			break;
		}
	}

	return 0;
}
LRESULT ChatWin::OnFTError(UINT uMsg, WPARAM wParam, LPARAM lParam){
	FTContext *pftc=(PFTContext)lParam;
	DWORD dwErrCode=(DWORD)wParam;
	
	TCHAR lid[STRLEN_SMALL]={0};
	MainWin::GetLocalMsgId(lid,STRLEN_SMALL);
	TCHAR errlog[STRLEN_DEFAULT]={0};

	switch(dwErrCode){
	case ERROR_CANCELLED:
		_stprintf_s(errlog,L"chatwin.info('%s','文件“%s”的传输过程被取消。');",lid,(pftc?pftc->m_fn:L""));
		break;
	case WSAENETDOWN:
	case WSAENETUNREACH:
		_stprintf_s(errlog,L"chatwin.info('%s','文件“%s”的传输过程因网络异常而中断。');",lid,(pftc?pftc->m_fn:L""));
		break;
	case WSAENETRESET:
	case WSAECONNABORTED:
	case WSAECONNRESET:
		_stprintf_s(errlog,L"chatwin.info('%s','文件“%s”的传输过程因网络被重置而中断。');",lid,(pftc?pftc->m_fn:L""));
		break;
	case WSAENOBUFS:
		_stprintf_s(errlog,L"chatwin.info('%s','文件“%s”的传输过程因网络缓冲区不足而中断。');",lid,(pftc?pftc->m_fn:L""));
		break;
	case WSAEISCONN:
	case WSAENOTCONN:
	case WSAEHOSTDOWN:
	case WSAEHOSTUNREACH:
	case WSAHOST_NOT_FOUND:
		_stprintf_s(errlog,L"chatwin.info('%s','文件“%s”的传输过程因网络连接异常而中断。');",lid,(pftc?pftc->m_fn:L""));
		break;
	case WSAESHUTDOWN:
		_stprintf_s(errlog,L"chatwin.info('%s','文件“%s”的传输过程因网络连接被关闭而中断。');",lid,(pftc?pftc->m_fn:L""));
		break;
	case WSAETIMEDOUT:
		_stprintf_s(errlog,L"chatwin.info('%s','文件“%s”的传输过程因网络超时而中断。');",lid,(pftc?pftc->m_fn:L""));
		break;
	case WSAECONNREFUSED:
	case WSAEREFUSED:
		_stprintf_s(errlog,L"chatwin.info('%s','文件“%s”的传输过程因连接被拒绝而中断。');",lid,(pftc?pftc->m_fn:L""));
		break;
	case WSASYSCALLFAILURE:
		_stprintf_s(errlog,L"chatwin.info('%s','文件“%s”的传输过程因不可恢复的网络错误而中断。');",lid,(pftc?pftc->m_fn:L""));
		break;
	default:
		_stprintf_s(errlog,L"chatwin.info('%s','%s文件“%s”时出现错误，错误代码为:0x%X，请联系管理员。');",lid,(pftc?(pftc->m_isSender?L"发送":L"接收"):L"传输"),(pftc?pftc->m_fn:L""),dwErrCode);
		break;
	}

	BSTR bstrJS=SysAllocString(errlog);
	m_webBrowser.CallJavaScript(bstrJS);
	FREE_SYS_STR(bstrJS);

	RemoveFTProgress(pftc->m_id);

	if(m_ftc.empty()) return 0;
	ITRFTCONTEXT itr;
	for(itr=m_ftc.begin();itr!=m_ftc.end();itr++){
		PFTContext x=(*itr);
		if(x && (x==pftc||x->m_id==pftc->m_id)) {
			SAFE_DEL_PTR(x);
			m_ftc.erase(itr);
			break;
		}
	}

	return 0;
}
LRESULT ChatWin::OnFTProgress(UINT uMsg, WPARAM wParam, LPARAM lParam){
	FTContext *pftc=(PFTContext)lParam;
	if(!pftc) return 0;
	DWORD p=(DWORD)(((double)(pftc->m_isSender?pftc->m_sentBytes:pftc->m_receivedBytes)/(double)pftc->m_fileSize)*100.0);
	TCHAR js[MAX_PATH]={0};
	_stprintf_s(js,L"chatwin.ftprogress('%u',%u);",pftc->m_id,p);
	BSTR bstrJS=SysAllocString(js);
	this->m_webBrowser.CallJavaScript(bstrJS);
	FREE_SYS_STR(bstrJS);
	return 0;
}
LRESULT ChatWin::OnFTSendFData(UINT uMsg, WPARAM wParam, LPARAM lParam){
	PFTContext pftc=(PFTContext)lParam;
	if(!pftc) return 0;
	if(!SendFile(pftc)){
		ErrMsg(0,L"无法发送文件内容，请联系管理员！");
	}
	return 0;
}
LRESULT ChatWin::OnFTCanceled(UINT uMsg, WPARAM wParam, LPARAM lParam){
	DWORD fid=(DWORD)lParam;
	PFTContext pftc=NULL;
	if(!FindFTContext(fid,&pftc) ||!pftc) return 0;
	OnFTError(WM_FT_ERROR,(WPARAM)ERROR_CANCELLED,(LPARAM)pftc);
	return 0;
}
BOOL ChatWin::FindFTContext(DWORD fid,PFTContext* ppftc){
	*ppftc=NULL;
	if(m_ftc.empty()) return FALSE;
	ITRFTCONTEXT itr;
	for(itr=m_ftc.begin();itr!=m_ftc.end();itr++){
		PFTContext x=(*itr);
		if(x && x->m_id==fid) {
			*ppftc=x;
			return TRUE;
		}
	}
	return FALSE;
}
BOOL ChatWin::SendFile(PFTContext pftc){
	BOOL ret=FALSE;
	if (!pftc) return ret;

	TCHAR szMsgIn[STRLEN_1K]={0};
	TCHAR lid[STRLEN_SMALL]={0};
	_stprintf_s(lid,L"ftp_%u",pftc->m_id);

	_stprintf_s(szMsgIn,L"chatwin.info('%s','正在发送文件“%s”...');",lid,pftc->m_fn);
	BSTR bstrJS=SysAllocString(szMsgIn);
	m_webBrowser.CallJavaScript(bstrJS);
	FREE_SYS_STR(bstrJS);

	pftc->m_thread=CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE)SendFileContent,(LPVOID)pftc,0,&(pftc->m_tid));
	ret=(pftc->m_tid!=0);
	return ret;
}
BOOL ChatWin::ReceiveFile(PFTContext pftc){
	BOOL ret=FALSE;
	if (!pftc) return ret;
	pftc->m_thread=CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE)RecvFileContent,(LPVOID)pftc,0,&(pftc->m_tid));
	ret=(pftc->m_tid!=0);
	return ret;
}
void ChatWin::InitFTContext(PFTContext pftc){
	if(!pftc) return;
	pftc->m_id=GetTickCount();
	pftc->m_hwnd=m_hWnd;
	pftc->m_port=DEFAULT_PORT;
	pftc->m_receivedBytes=0;
	pftc->m_sentBytes=0;
	pftc->m_isSender=FALSE;
	pftc->m_fileSize=0;
	pftc->m_tid=0;
	pftc->m_thread=NULL;
	pftc->m_socket=INVALID_SOCKET;
	pftc->m_canceled=FALSE;

	ZeroMemory(&pftc->m_fn,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(&pftc->m_fp,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(&pftc->m_src,sizeof(TCHAR)*MAX_PATH);
	ZeroMemory(&pftc->m_dest,sizeof(TCHAR)*MAX_PATH);
}
void ChatWin::RemoveFTAskLink(DWORD fid){
	TCHAR js[MAX_PATH]={0};
	_stprintf_s(js,L"chatwin.removeftlink('%u');",fid);
	BSTR bstrJS=SysAllocString(js);
	this->m_webBrowser.CallJavaScript(bstrJS);
	FREE_SYS_STR(bstrJS);
}
void ChatWin::RemoveFTProgress(DWORD fid){
	TCHAR js[MAX_PATH]={0};
	_stprintf_s(js,L"chatwin.removeftprogress('%u');",fid);
	BSTR bstrJS=SysAllocString(js);
	this->m_webBrowser.CallJavaScript(bstrJS);
	FREE_SYS_STR(bstrJS);
}
void ChatWin::NotifiyUser(){
	if (GetForegroundWindow()!=m_hWnd || IsIconic(m_hWnd)){
		FLASHWINFO fwi;
		ZeroMemory(&fwi,sizeof(fwi));
		fwi.cbSize=sizeof(fwi);
		fwi.hwnd=m_hWnd;
		fwi.dwFlags=FLASHW_TRAY|FLASHW_TIMERNOFG;
		if (!IsIconic(m_hWnd)) fwi.dwFlags|=FLASHW_CAPTION;
		fwi.uCount=5000;
		fwi.dwTimeout=0;
		FlashWindowEx(&fwi);
	}
}
/*********** ChatWinIEEventImpl 成员 ************/
void ChatWinIEEventImpl::DocumentComplete(IDispatch *pDisp,VARIANT *URL){
	CComPtr<IWebBrowser2> pWebBrowser2=NULL;
	HRESULT hr=pDisp->QueryInterface(IID_IWebBrowser2,(void**)&pWebBrowser2);
	if (FAILED(hr) || !pWebBrowser2) return ;
	
	if(m_pChatWin->m_pendingDisplayInfo!=NULL){
		this->host->CallJavaScript(m_pChatWin->m_pendingDisplayInfo);
		FREE_SYS_STR(m_pChatWin->m_pendingDisplayInfo);
		m_pChatWin->NotifiyUser();
	}
	
	BSTR bstrUrl=NULL;
	TCHAR szUrlParams[MAX_PATH]={0};
	if(pWebBrowser2->get_LocationURL(&bstrUrl)==S_OK){
		TCHAR* szParamStart=_tcsstr(bstrUrl,L"#");
		if(szParamStart)_tcscpy_s(szUrlParams,szParamStart+1);
		FREE_SYS_STR(bstrUrl);
	}

	if(!m_pChatWin->m_webBrowserReady && _tcslen(szUrlParams)==0){
		TCHAR fn[MAX_PATH]={0};
		int len=GetModuleFileName(NULL,fn,MAX_PATH);
		if(_tcslen(fn)==0 || ERROR_INSUFFICIENT_BUFFER==GetLastError()){
			ErrMsg(0,L"初始化即时消息窗口时出现异常，请联系管理员！");
		}else{
			TCHAR* sz=fn;
			while(*sz!=L'\0'){
				if(*sz==L'\\') *sz=L'/';
				sz++;
			}
			AppOptions* appOpts=AppOptions::GetInstance();
			TCHAR szInitJs[STRLEN_2K]={0};
			_stprintf_s(szInitJs,L"chatwin.init('%s','http://%s/tsim/theme/emotions/','%s','%s');",fn,AppOptions::szCurrentSiteUrl,AppOptions::szCurrentSiteUrlExt,appOpts->SendMsgKey);
			BSTR initjs=SysAllocString(szInitJs);
			this->host->CallJavaScript(initjs);
			FREE_SYS_STR(initjs);
		}
	}
	m_pChatWin->m_webBrowserReady=TRUE;
	if(_tcsstr(szUrlParams,L"send")==szUrlParams){OnSendIMMessage(pWebBrowser2,szUrlParams);}
	if(_tcsstr(szUrlParams,L"close")==szUrlParams){OnClose(pWebBrowser2,szUrlParams);}
	if(_tcsstr(szUrlParams,L"file")==szUrlParams){OnSendFile(pWebBrowser2,szUrlParams);}
	if(_tcsstr(szUrlParams,L"acceptfile")==szUrlParams){OnAcceptFile(pWebBrowser2,szUrlParams);}
	if(_tcsstr(szUrlParams,L"refusefile")==szUrlParams){OnRefuseFile(pWebBrowser2,szUrlParams);}
	if(_tcsstr(szUrlParams,L"pickcolor")==szUrlParams){OnPickColor(pWebBrowser2,szUrlParams);}
	if(_tcsstr(szUrlParams,L"setstyle")==szUrlParams){OnSetStyle(pWebBrowser2,szUrlParams);}
	if(_tcsstr(szUrlParams,L"history")==szUrlParams){OnHistory(pWebBrowser2,szUrlParams);}
	if(_tcsstr(szUrlParams,L"openfile")==szUrlParams){OnOpenFile(pWebBrowser2,szUrlParams);}
	if(_tcsstr(szUrlParams,L"explorefile")==szUrlParams){OnExploreFile(pWebBrowser2,szUrlParams);}
}
void ChatWinIEEventImpl::OnSendIMMessage(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	BSTR bstrElId=SysAllocString(L"sendarea");

	CComPtr<IHTMLDocument2> doc2=NULL;
	CComPtr<IHTMLDocument3> doc3=NULL;
	CComPtr<IHTMLElement> el=NULL;
		
	HRESULT hr=pWebBrowser2->get_Document((IDispatch**)&doc2);
	if (FAILED(hr)) goto end;
	hr=doc2->QueryInterface(IID_IHTMLDocument3,(void**)&doc3);
	if (FAILED(hr)) goto end;
	hr=doc3->getElementById(bstrElId,&el);
	if (FAILED(hr) || !el) goto end;
	BSTR sendContent=NULL;
	hr=el->get_innerHTML(&sendContent);
	if(FAILED(hr) || !sendContent) goto end;
	size_t l=_tcslen(sendContent);
	if(l==0){
		FREE_SYS_STR(sendContent);
		ErrMsg(0,L"您必须输入有效内容！");
		goto end;
	}
	l+=l;

	SendInfo *si=new SendInfo;
	memset(si,0,sizeof(SendInfo));
	si->echo=TRUE;
	si->recordable=TRUE;
	GetLocalTime(&(si->msgSentDt));

	this->m_pChatWin->m_sendinfo.push_back(si);
	si->msg=new TCHAR[l];
	ZeroMemory(si->msg,sizeof(TCHAR)*l);
	TCHAR *d = si->msg;
	TCHAR *s=sendContent;
	while (*s!=L'\0'){
		if(*s!=L'\r' && *s!=L'\n') {
			if(*s!=L'\''){*d++=*s;}
			else{*d++=L'\\';*d++=*s;}
		}
		s++;
	}

	UINT ip=(UINT)MainWin::GetIPValue(this->m_pChatWin->m_targetIP);
	PostMessage(this->m_pChatWin->m_mainWin->m_hWnd,WM_SENDIM,(WPARAM)ip,(LPARAM)si->msg);
	
	//阻止继续发送以等待消息被成功接收或超时
	TCHAR disableSendJS[STRLEN_SMALL]={0};
	_stprintf_s(disableSendJS,L"chatwin.disablesend('正在发送消息...');");
	BSTR bstrDisableSendJS=SysAllocString(disableSendJS);
	this->host->CallJavaScript(bstrDisableSendJS);
	FREE_SYS_STR(bstrDisableSendJS);

	////显示发送的内容并清空发送内容区，
	//SYSTEMTIME st;
	//GetLocalTime(&st);
	//TCHAR szDt[STRLEN_SMALL]={0};
	//_stprintf_s(szDt,L"%04d-%02d-%02d %02d:%02d:%02d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	//l+=MAX_PATH;
	//TCHAR *js=new TCHAR[l];
	//ZeroMemory(js,l*sizeof(TCHAR));
	//TCHAR lid[STRLEN_SMALL]={0};
	//MainWin::GetLocalMsgId(lid,STRLEN_SMALL);
	//_stprintf_s(js,l,L"chatwin.senddone('%s','%s','%s','%s');",lid,szDt,this->m_pChatWin->m_mainWin->m_userInfo.cn,si->msg);
	//BSTR bstrJS=SysAllocString(js);
	//this->host->CallJavaScript(bstrJS);
	//SAFE_DEL_ARR(js);
	//FREE_SYS_STR(bstrJS);
	////记录消息历史记录
	//if(m_pChatWin->m_msgRecorder) m_pChatWin->m_msgRecorder->RecordMsg(lid,si->msg,m_pChatWin->m_mainWin->m_userInfo.cn,szDt,m_pChatWin->m_style);

	FREE_SYS_STR(sendContent);
end:
	FREE_SYS_STR(bstrElId);
}
void ChatWinIEEventImpl::OnClose(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	PostMessage(this->m_pChatWin->m_hWnd,WM_SYSCOMMAND,SC_CLOSE,(LPARAM)0);
}
void ChatWinIEEventImpl::OnSendFile(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	if(!this->m_pChatWin->m_netOKFlag) return;
	TCHAR szResult[MAX_PATH]={0};
	TCHAR szFileTitle[MAX_PATH]={0};
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = this->m_pChatWin->m_hWnd;
	ofn.lpstrFilter = L"所有文件\0*.*\0\0";
	ofn.nFilterIndex = 0;
	ofn.lpstrDefExt=NULL;
	ofn.lpstrFile=szResult;
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrFileTitle = szFileTitle;
	ofn.nMaxFileTitle=MAX_PATH;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrTitle=L"选择要发送的文件";
	ofn.Flags =OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_EXPLORER | OFN_LONGNAMES;	//|=OFN_ALLOWMULTISELECT

	if (GetOpenFileName(&ofn)==TRUE){
		PFTContext pftc=new FTContext;
		m_pChatWin->InitFTContext(pftc);
		pftc->m_isSender=TRUE;
		_tcscpy_s(pftc->m_dest,m_pChatWin->m_targetIP);
		_tcscpy_s(pftc->m_src,m_pChatWin->m_mainWin->m_userInfo.ip);
		_tcscpy_s(pftc->m_fn,szFileTitle);
		_tcscpy_s(pftc->m_fp,szResult);
		HANDLE hFile=CreateFile(pftc->m_fp,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
		if (hFile == INVALID_HANDLE_VALUE) { 
			ErrMsg(GetLastError(),L"无法打开文件：%s");
			return;
		}
		LARGE_INTEGER li={0};
		if(!GetFileSizeEx(hFile,&li)){
			CloseHandle(hFile);
			ErrMsg(GetLastError(),L"无法获取文件大小：%s");
			return;
		}
		if(li.QuadPart==0){
			CloseHandle(hFile);
			ErrMsg(0,L"您不能发送大小为0的文件！");
			return;
		}
		if(li.QuadPart>MAX_FILE_SEND_LENGTH){
			CloseHandle(hFile);
			ErrMsg(0,L"您不能发送大小超过2GB的文件！");
			return;
		}
		pftc->m_fileSize=(DWORD)li.QuadPart;
		CloseHandle(hFile);
		m_pChatWin->m_ftc.push_back(pftc);

		TCHAR szMsgOut[STRLEN_1K]={0};
		//_stprintf_s(szMsgOut,L"%s给您发送文件：“%s”。<br/>您可以<a href=\"#acceptfile:%u,%u,%s\">接收</a>或<a href=\"#refusefile:%u,%s\">拒绝</a>此文件。",/*msg*/this->m_pChatWin->m_mainWin->m_userInfo.cn,szFileTitle,/*accept*/pftc->m_id,pftc->m_fileSize,szFileTitle,/*refuse*/pftc->m_id,szFileTitle);
		_stprintf_s(szMsgOut,L"%s:%u:%u:%u:%s",FILE_TRANSFER_PREFIX,ASK,pftc->m_id,pftc->m_fileSize,szFileTitle);

		TCHAR szMsgIn[STRLEN_1K]={0};
		TCHAR lid[STRLEN_SMALL]={0};
		MainWin::GetLocalMsgId(lid,STRLEN_SMALL);
		_stprintf_s(szMsgIn,L"chatwin.info('%s','等待%s接收文件：“%s”...');",lid,this->m_pChatWin->m_targetCN,szFileTitle);

		SendInfo *si=new SendInfo;
		memset(si,0,sizeof(SendInfo));
		GetLocalTime(&(si->msgSentDt));

		m_pChatWin->m_sendinfo.push_back(si);
		size_t l=_tcslen(szMsgOut)+1;
		si->msg=new TCHAR[l];
		ZeroMemory(si->msg,sizeof(TCHAR)*l);
		_tcscpy_s(si->msg,l,szMsgOut);
		
		UINT ip=(UINT)MainWin::GetIPValue(m_pChatWin->m_targetIP);
		PostMessage(this->m_pChatWin->m_mainWin->m_hWnd,WM_SENDIM,(WPARAM)ip,(LPARAM)si->msg);

		BSTR bstrJS=SysAllocString(szMsgIn);
		this->host->CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
	}
}
void ChatWinIEEventImpl::OnAcceptFile(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = GetForegroundWindow();

	TCHAR szFileTitle[MAX_PATH]={0};
	TCHAR szExtName[MAX_PATH]={0};
	TCHAR szResult[MAX_PATH]={0};
	TCHAR szInitDir[MAX_PATH]={0};
	AppOptions* appOpts=AppOptions::GetInstance();
	_tcscpy_s(szInitDir,appOpts->LocalFilePath);
	
	TCHAR *sz=_tcschr(szUrlParams,L':');
	if(sz==NULL) return;
	sz++;

	int idx=0;
	while(*(sz+idx)!=L'\0'){
		if(*(sz+idx)==L'|') *(sz+idx)=L'\0';
		idx++;
	}
	DWORD fid=0;
	DWORD fsize=0;
	idx=0;
	for (LPTSTR pszz = sz; *pszz; pszz += _tcslen(pszz) + 1) {
		switch(idx){
		case 0:	//file id
			fid=_tcstoul(pszz,NULL,10);
			if(fid==0) {ErrMsg(0,L"无法获取接收文件的唯一标记！");return;}
			break;
		case 1:	//file size
			fsize=_tcstoul(pszz,NULL,10);
			if(fsize==0) {ErrMsg(0,L"无法获取接收文件的大小！");return;}
			break;
		case 2: //file name
			{
			_tcscpy_s(szFileTitle,pszz);
			if(_tcslen(szFileTitle)==0) {ErrMsg(0,L"无法获取接收文件的文件名！");return;}
			TCHAR* sz=_tcsrchr(szFileTitle,L'.');
			if(sz!=NULL)_tcscpy_s(szExtName,++sz);
			_tcscpy_s(szResult,szFileTitle);
			ZeroMemory(szFileTitle,sizeof(TCHAR)*MAX_PATH);
			}
			break;
		default:
			break;
		}
		idx++;
		if(idx>2) break;
	}

	TCHAR filter[STRLEN_SMALL]={0};
	_stprintf_s(filter,L"%s文件|*.%s",szExtName,szExtName);
	filter[_tcslen(filter)+1]=L'\0';
	TCHAR *filterStart=filter;
	while(*(filterStart++)!=L'\0'){
		if(*filterStart==L'|') *filterStart=L'\0';
	}
	
	ofn.lpstrFilter = filter;
	ofn.nFilterIndex = 1;
	ofn.lpstrDefExt=szExtName;
	ofn.lpstrFile=szResult;
	ofn.nMaxFile=MAX_PATH;
	ofn.lpstrFileTitle=szFileTitle;
	ofn.nMaxFileTitle=MAX_PATH;
	ofn.lpstrInitialDir = szInitDir;
	ofn.lpstrTitle=L"保存文件";
	ofn.Flags =  OFN_OVERWRITEPROMPT| OFN_EXPLORER | OFN_LONGNAMES;

	if (GetSaveFileName(&ofn)==TRUE){
		PFTContext pftc=new FTContext;
		m_pChatWin->InitFTContext(pftc);
		pftc->m_id=fid;
		pftc->m_fileSize=fsize;
		_tcscpy_s(pftc->m_dest,m_pChatWin->m_mainWin->m_userInfo.ip);
		_tcscpy_s(pftc->m_src,m_pChatWin->m_targetIP);
		pftc->m_isSender=FALSE;
		_tcscpy_s(pftc->m_fn,szFileTitle);
		_tcscpy_s(pftc->m_fp,szResult);
		m_pChatWin->m_ftc.push_back(pftc);
		PostMessage(this->m_pChatWin->m_hWnd,WM_FT_ACCEPT,(WPARAM)0,(LPARAM)fid);
	}
}
void ChatWinIEEventImpl::OnRefuseFile(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	TCHAR *sz=_tcschr(szUrlParams,L':');
	TCHAR *sz1=_tcschr(szUrlParams,L'|');
	if(sz==NULL || sz1==NULL) return;
	TCHAR* fid=sz+1;
	if(_tcslen(fid)==0) return ;
	*sz1=L'\0';
	TCHAR* fn=sz1+1;
	if(_tcslen(fn)==0) return ;

	TCHAR szMsgOut[STRLEN_1K]={0};
	_stprintf_s(szMsgOut,L"%s:%u:%s",FILE_TRANSFER_PREFIX,REFUSE,fid);

	SendInfo *si=new SendInfo;
	memset(si,0,sizeof(SendInfo));
	GetLocalTime(&(si->msgSentDt));
	m_pChatWin->m_sendinfo.push_back(si);
	size_t l=_tcslen(szMsgOut)+1;
	si->msg=new TCHAR[l];
	ZeroMemory(si->msg,sizeof(TCHAR)*l);
	_tcscpy_s(si->msg,l,szMsgOut);
		
	UINT ip=(UINT)MainWin::GetIPValue(m_pChatWin->m_targetIP);
	PostMessage(this->m_pChatWin->m_mainWin->m_hWnd,WM_SENDIM,(WPARAM)ip,(LPARAM)si->msg);

	TCHAR szMsgIn[STRLEN_1K]={0};
	TCHAR lid[STRLEN_SMALL]={0};
	MainWin::GetLocalMsgId(lid,STRLEN_SMALL);
	_stprintf_s(szMsgIn,L"chatwin.info('%s','您拒绝了对文件“%s”的接收。');",lid,fn);

	BSTR bstrJS=SysAllocString(szMsgIn);
	this->host->CallJavaScript(bstrJS);
	FREE_SYS_STR(bstrJS);
	DWORD dwFid=_tcstoul(fid,NULL,10);
	this->m_pChatWin->RemoveFTAskLink(dwFid);
}
void ChatWinIEEventImpl::OnPickColor(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	CHOOSECOLOR cc;
	static COLORREF acrCustClr[16];
	
	ZeroMemory(&cc, sizeof(cc));
	cc.lStructSize = sizeof(cc);
	cc.hwndOwner = m_pChatWin->m_hWnd;
	cc.lpCustColors = (LPDWORD) acrCustClr;
	cc.rgbResult = m_pChatWin->m_sendMsgColor;
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;
 
	if (ChooseColor(&cc)==TRUE) {
		m_pChatWin->m_sendMsgColor = cc.rgbResult;
		TCHAR szResult[STRLEN_SMALL]={0};
		_stprintf_s(szResult,L"chatwin.setColor('#%02X%02X%02X');",GetRValue(cc.rgbResult),GetGValue(cc.rgbResult),GetBValue(cc.rgbResult));

		BSTR bstrJS=SysAllocString(szResult);
		this->host->CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
	}
}
void ChatWinIEEventImpl::OnSetStyle(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	ULONG ipv=(UINT)MainWin::GetIPValue(m_pChatWin->m_targetIP);
	CWItr itr=m_pChatWin->m_mainWin->m_chatwininfo.find(ipv);
	if(itr==m_pChatWin->m_mainWin->m_chatwininfo.end()) return;
	ChatWinInfo* pcw=itr->second;
	if(!pcw) return;
	
	static OLECHAR FAR* sMethod = L"getStyle";
	IHTMLDocument2	*htmlDoc2=NULL;
	IDispatch* pScript = NULL;
	DISPID idMethod = 0;
	VARIANT ret={0};
	
	HRESULT hr=pWebBrowser2->get_Document((IDispatch**)&htmlDoc2);
	if (FAILED(hr)) goto end;
	
	hr=htmlDoc2->get_Script(&pScript);
	if (FAILED(hr)) goto end;
	hr = pScript->GetIDsOfNames(IID_NULL, &sMethod, 1, LOCALE_SYSTEM_DEFAULT,&idMethod);
	if (SUCCEEDED(hr)) {
		DISPPARAMS dpNoArgs = {NULL,NULL, 0, 0};
		hr = pScript->Invoke(idMethod, IID_NULL, LOCALE_SYSTEM_DEFAULT, DISPATCH_METHOD,&dpNoArgs, &ret, NULL, NULL);
		if(ret.vt==VT_BSTR && ret.bstrVal!=NULL && _tcslen(ret.bstrVal)>0){
			_tcscpy_s(pcw->style,ret.bstrVal);
			_tcscpy_s(m_pChatWin->m_style,ret.bstrVal);
			TCHAR szSend[STRLEN_DEFAULT]={0};
			_stprintf_s(szSend,L"%s:%s",STYLE_PREFIX,ret.bstrVal);
			SendInfo *si=new SendInfo;
			memset(si,0,sizeof(SendInfo));
			GetLocalTime(&(si->msgSentDt));
			this->m_pChatWin->m_sendinfo.push_back(si);
			size_t l=_tcslen(szSend)+1;
			si->msg=new TCHAR[l];
			ZeroMemory(si->msg,sizeof(TCHAR)*l);
			_tcscpy_s(si->msg,l,szSend);
			UINT ip=(UINT)MainWin::GetIPValue(this->m_pChatWin->m_targetIP);
			PostMessage(this->m_pChatWin->m_mainWin->m_hWnd,WM_SENDIM,(WPARAM)ip,(LPARAM)si->msg);
		}
	}
end:
	SAFE_RELEASE(pScript);
	SAFE_RELEASE(htmlDoc2);
	FREE_SYS_STR(ret.bstrVal);
	return ;
}
void ChatWinIEEventImpl::OnHistory(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	if(_tcsrchr(szUrlParams,L':')!=NULL){
		OnHistoryIndex(pWebBrowser2,szUrlParams);
		return ;
	}
	if(m_pChatWin->m_showHistoryFlag){
		BSTR bstrJS=SysAllocString(L"chatwin.scrolltotop();");
		host->CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
		return ;
	}
	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize;
	TCHAR szDir[MAX_PATH]={0};
	
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError=0;
  
	AppOptions* appOpts=AppOptions::GetInstance();
	size_t fll=_tcslen(appOpts->LocalMessagePath);
	if(appOpts->LocalMessagePath[fll-1]==L'\\') _stprintf_s(szDir,L"%s%s\\*.dat",appOpts->LocalMessagePath,m_pChatWin->m_msgRecorder->m_pdir);
	else _stprintf_s(szDir,L"%s\\%s\\*.dat",appOpts->LocalMessagePath,m_pChatWin->m_msgRecorder->m_pdir);

	CAtlString atlStr(L"");
	hFind = FindFirstFile(szDir, &ffd);
	if(INVALID_HANDLE_VALUE == hFind) goto end;	
	do{
		if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) continue;
		filesize.LowPart = ffd.nFileSizeLow;
		filesize.HighPart = ffd.nFileSizeHigh;
		if(filesize.QuadPart==0) continue;
		if(atlStr.GetLength()>0) atlStr+=L",\r\n";
		atlStr+=L"'";
		atlStr+=ffd.cFileName;
		atlStr+=L"'";
	} while (FindNextFile(hFind, &ffd) != 0);
	if(atlStr.GetLength()>0){
		atlStr.Insert(0,L"chatwin.sethistoryfiles([");
		atlStr+=L"]);";
		BSTR bstrJS=atlStr.AllocSysString();
		this->host->CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
	}
	//dwError = GetLastError();
	//if (dwError != ERROR_NO_MORE_FILES){}
end:
	m_pChatWin->m_showHistoryFlag=TRUE;
	FindClose(hFind);
	return ;
}
void ChatWinIEEventImpl::OnHistoryIndex(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	TCHAR* szStart=_tcschr(szUrlParams,L':');
	TCHAR* szEnd=_tcsrchr(szUrlParams,L':');
	int idx=0;
	if(szStart!=NULL && szEnd>szStart){
		size_t len=szEnd-szStart;
		TCHAR szIdx[STRLEN_SMALL]={0};
		_tcsncpy_s(szIdx,szStart+1,szEnd-szStart-1);
		idx=_tstoi(szIdx);
	}
	
	TCHAR* sz=_tcsrchr(szUrlParams,L':');
	if(sz==NULL) return;
	
	TCHAR szDir[MAX_PATH]={0};
	
	HANDLE hFile = INVALID_HANDLE_VALUE;
	DWORD dwError=0;
  
	AppOptions* appOpts=AppOptions::GetInstance();
	size_t fll=_tcslen(appOpts->LocalMessagePath);
	if(appOpts->LocalMessagePath[fll-1]==L'\\') _stprintf_s(szDir,L"%s%s\\%s.dat",appOpts->LocalMessagePath,m_pChatWin->m_msgRecorder->m_pdir,sz+1);
	else _stprintf_s(szDir,L"%s\\%s\\%s.dat",appOpts->LocalMessagePath,m_pChatWin->m_msgRecorder->m_pdir,sz+1);
	
	TCHAR* szContent=NULL;
	TCHAR* szJS=NULL;
	BSTR bstrJS=NULL;
	char *bs = NULL;
	if(_tcsicmp(szDir,m_pChatWin->m_msgRecorder->m_fn)==0){
		szContent=m_pChatWin->m_msgRecorder->m_oldContent;
	}else{
		hFile=CreateFile(szDir,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
		if (hFile == INVALID_HANDLE_VALUE){
			ErrMsg(GetLastError(),L"无法打开保存历史记录的文件：%s");
			return;
		}
		DWORD dwfilesize=GetFileSize(hFile,NULL);
		DWORD dwBytesRead = 0;
		bs= new char[dwfilesize+1];
		ZeroMemory(bs,dwfilesize+1);
		if(FALSE==ReadFile(hFile, bs, dwfilesize, &dwBytesRead, NULL)){
			goto end;
		}
		USES_CONVERSION;
		szContent=A2T(bs);
	}

	if(szContent!=NULL){
		size_t l= _tcslen(szContent);
		if(l==0){
			goto end;
		}
		size_t jslen=l+MAX_PATH;
		szJS=new TCHAR[jslen];
		ZeroMemory(szJS,sizeof(TCHAR)*jslen);
		_stprintf_s(szJS,jslen,L"chatwin.showhistory([%s],%d);",szContent,idx);
		bstrJS=SysAllocString(szJS);
		host->CallJavaScript(bstrJS);
	}

end:
	SAFE_DEL_ARR(bs);
	SAFE_DEL_ARR(szJS);
	FREE_SYS_STR(bstrJS);
	CloseHandle(hFile);
}
void ChatWinIEEventImpl::OnOpenFile(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	TCHAR* sz=_tcschr(szUrlParams,L':');
	if(sz==NULL) return;
	sz++;
	if(_tcslen(sz)==0) return;
	CAtlString atlStr(sz);
	atlStr.Replace(L"%20",L" ");

	SHELLEXECUTEINFO ShExecInfo;
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
  ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS | SEE_MASK_FLAG_DDEWAIT;
	ShExecInfo.lpVerb = L"open";
	ShExecInfo.lpFile =atlStr.GetString();
	ShExecInfo.lpParameters = NULL;
  ShExecInfo.lpDirectory = NULL;
  ShExecInfo.nShow = SW_SHOWNORMAL;
  ShExecInfo.hInstApp = NULL;
	ShExecInfo.hwnd = NULL;
	if (!ShellExecuteEx(&ShExecInfo)){
		ErrMsg(GetLastError(),L"启动文件时出现错误：%s");
	}
}
void ChatWinIEEventImpl::OnExploreFile(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams){
	TCHAR* sz=_tcschr(szUrlParams,L':');
	if(sz==NULL) return;
	sz++;
	if(_tcslen(sz)==0) return;
	CAtlString atlStr(sz);
	atlStr.Replace(L"%20",L" ");

	SHELLEXECUTEINFO ShExecInfo;
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
  ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS | SEE_MASK_FLAG_DDEWAIT;
	ShExecInfo.lpVerb = L"open";
	ShExecInfo.lpFile =L"explorer.exe";
	TCHAR szParam[MAX_PATH+100]={0};
	_stprintf_s(szParam,L"/select,\"%s\"",atlStr.GetString());
	ShExecInfo.lpParameters = szParam;
  ShExecInfo.lpDirectory = NULL;
  ShExecInfo.nShow = SW_SHOWNORMAL;
  ShExecInfo.hInstApp = NULL;
	ShExecInfo.hwnd = NULL;

	if (!ShellExecuteEx(&ShExecInfo)){
		ErrMsg(GetLastError(),L"查看文件时出现错误：%s");
	}
}
void ChatWinIEEventImpl::BeforeNavigate2(IDispatch *pDisp, VARIANT *&url, VARIANT *&Flags, VARIANT *&TargetFrameName, VARIANT *&PostData, VARIANT *&Headers, VARIANT_BOOL *&Cancel){
	//NOOP
}
int ChatWinIEEventImpl::GetElementValue(IWebBrowser2 *pWB,BSTR szElID,BSTR* pbstrValue){
	int ret=-1;
	*pbstrValue=NULL;
	if (!pWB || !szElID) return ret;
	CComPtr<IHTMLDocument2> doc2=NULL;
	CComPtr<IHTMLDocument3> doc3=NULL;
	CComPtr<IHTMLElement> el=NULL;
	CComPtr<IHTMLInputElement> elx=NULL;
	HRESULT hr=pWB->get_Document((IDispatch**)&doc2);
	if (FAILED(hr)) goto end;
	hr=doc2->QueryInterface(IID_IHTMLDocument3,(void**)&doc3);
	if (FAILED(hr)) goto end;
	hr=doc3->getElementById(szElID,&el);
	if (FAILED(hr) || !el) goto end;
	hr=el->QueryInterface(IID_IHTMLInputElement,(void**)&elx);
	if (FAILED(hr) || !elx) goto end;
	hr=elx->get_value(pbstrValue);
	if (FAILED(hr)) goto end;
	ret=SysStringLen(*pbstrValue);
end:
	return ret;
}
void ChatWinIEEventImpl::SetElementValue(IWebBrowser2 *pWB,BSTR szElID,BSTR bstrValue){
	int ret=-1;
	if (!pWB || !szElID) return ;
	CComPtr<IHTMLDocument2> doc2=NULL;
	CComPtr<IHTMLDocument3> doc3=NULL;
	CComPtr<IHTMLElement> el=NULL;
	CComPtr<IHTMLInputElement> elx=NULL;
	HRESULT hr=pWB->get_Document((IDispatch**)&doc2);
	if (FAILED(hr)) goto end;
	hr=doc2->QueryInterface(IID_IHTMLDocument3,(void**)&doc3);
	if (FAILED(hr)) goto end;
	hr=doc3->getElementById(szElID,&el);
	if (FAILED(hr) || !el) goto end;
	hr=el->QueryInterface(IID_IHTMLInputElement,(void**)&elx);
	if (FAILED(hr) || !elx) goto end;
	hr=elx->put_value(bstrValue);
	if (FAILED(hr)) goto end;
end:
	return ;
}