// chatwin.h

#ifndef _CHATWIN_H_
#define _CHATWIN_H_

#include "mainwin.h"
#include "webhost.h"
#include "filetransfer.h"
#include <vector>
using namespace std;

typedef struct tagSendInfo{
	DWORD msgId;
	TCHAR* msg;
	BOOL recordable;
	BOOL echo;
	SYSTEMTIME msgSentDt;
} SendInfo;

#include "msgrecorder.h"

typedef vector<SendInfo*> V_OUTINFO;
typedef vector<SendInfo*>::iterator ITROUTINFO;

typedef vector<PFTContext> V_FTCONTEXT;
typedef vector<PFTContext>::iterator ITRFTCONTEXT;

// 即时消息窗口定义
#define IM_TITLE  L"即时消息"
#define IM_CLASS  L"TSRTCIM"
#define IM_WIDTH   550
#define IM_HEIGHT  420

enum FILE_TRANSFER_FEEDBACK{UNKNOWN,ASK/*文件共享请求*/,ACCEPT/*接收者拒同意收文件*/,REFUSE/*接收者拒绝接收文件*/,CANCEL/*传输过程被取消*/};

class ChatWin;

//事件处理实现
class ChatWinIEEventImpl:public CWBEvent{
public:
	ChatWin* m_pChatWin;
	virtual void DocumentComplete(IDispatch *pDisp,VARIANT *URL);
	virtual void BeforeNavigate2( IDispatch *pDisp, VARIANT *&url, VARIANT *&Flags, VARIANT *&TargetFrameName, VARIANT *&PostData, VARIANT *&Headers, VARIANT_BOOL *&Cancel);
	virtual int GetElementValue(IWebBrowser2* pWB,BSTR szElID,BSTR* pbstrValue);
	virtual void SetElementValue(IWebBrowser2* pWB,BSTR szElID,BSTR bstrValue);
private:
	virtual void OnSendIMMessage(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnClose(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnSendFile(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnAcceptFile(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnRefuseFile(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnPickColor(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnSetStyle(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnHistory(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnHistoryIndex(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnOpenFile(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
	virtual void OnExploreFile(IWebBrowser2* pWebBrowser2,TCHAR* szUrlParams);
};

// 即时消息会话窗口对应的类
class ChatWin{
public:
	ChatWin(HWND hwnd,MainWin* pmw);
	~ChatWin();

	HWND m_hWnd;																		//窗口句柄
	HWND m_hWndStatic;															//浏览器宿主容器窗口句柄
	TCHAR m_targetIP[STRLEN_SMALL];									//目标ip地址
	TCHAR m_targetCN[MAX_PATH];											//目标用户名
	TCHAR m_style[MAX_PATH];												//样式

	MainWin* m_mainWin;															//主窗口对象
	DWORD m_sendMsgColor;														//

	WebHostBase m_webBrowser;												//嵌入的WebBrowser对象
	ChatWinIEEventImpl m_webBrowserEvent;						//嵌入的WebBrowser对象的事件处理程序
	BOOL m_webBrowserReady;													//嵌入的WebBrowser内容是否装载完毕
	BOOL m_netOKFlag;																//网络正常标记
	BSTR m_pendingDisplayInfo;											//等待呈现的消息
	V_OUTINFO m_sendinfo;														//发送出去但待确认是否已被成功接收的消息
	BOOL m_showHistoryFlag;													//是否显示过历史记录
	V_FTCONTEXT m_ftc;															//待发送的文件上下文信息

	MsgRecorder* m_msgRecorder;											//

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam);																					//处理WM_CREATE消息
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam);																						//处理WM_SIZE消息
	LRESULT OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam);																					//处理WM_COMMAND消息
	LRESULT OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam);																					//处理WM_NOFITY消息
	LRESULT OnActiveApp(UINT uMsg, WPARAM wParam, LPARAM lParam);																				//处理WM_ACTIVATEAPP消息
	void OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam);																							//处理WM_TIMER消息
	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam);																						//处理WM_CLOSE消息
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam);																					//处理WM_DESTROY消息
	LRESULT OnGetMinMaxInfo(UINT uMsg, WPARAM wParam, LPARAM lParam);																		//处理WM_GETMINMAXINFO消息
	LRESULT OnIMReceived(UINT uMsg, WPARAM wParam, LPARAM lParam);																			//处理WM_NMRECEIVED消息
	LRESULT OnSendStatusChanged(UINT uMsg, WPARAM wParam, LPARAM lParam);																//处理WM_SENDSTATUSCHANGED消息
	LRESULT OnNetError(UINT uMsg, WPARAM wParam, LPARAM lParam);																				//处理WM_NETERROR消息
	LRESULT OnGetMsgId(UINT uMsg, WPARAM wParam, LPARAM lParam);																				//处理WM_GET_SEND_MSGID消息
	LRESULT OnAcceptFile(UINT uMsg, WPARAM wParam, LPARAM lParam);																			//处理WM_FT_ACCEPT消息
	LRESULT OnRefuseFile(UINT uMsg, WPARAM wParam, LPARAM lParam);																			//处理WM_FT_REFUSE消息
	LRESULT OnAskFile(UINT uMsg, WPARAM wParam, LPARAM lParam);																					//处理WM_FT_ASK消息
	LRESULT OnFTComplete(UINT uMsg, WPARAM wParam, LPARAM lParam);																			//处理WM_FT_COMPLETE消息
	LRESULT OnFTError(UINT uMsg, WPARAM wParam, LPARAM lParam);																					//处理WM_FT_ERROR消息
	LRESULT OnFTProgress(UINT uMsg, WPARAM wParam, LPARAM lParam);																			//处理WM_FT_PROGRESS消息
	LRESULT OnFTSendFData(UINT uMsg, WPARAM wParam, LPARAM lParam);																			//处理WM_FT_SENDFDATA消息
	LRESULT OnFTCanceled(UINT uMsg, WPARAM wParam, LPARAM lParam);																			//处理WM_FT_CANCEL消息

	BOOL FindFTContext(DWORD fid,PFTContext* ppftc);
	BOOL SendFile(PFTContext pftc);
	BOOL ReceiveFile(PFTContext pftc);
	void InitFTContext(PFTContext pftc);
	void RemoveFTAskLink(DWORD fid);
	void RemoveFTProgress(DWORD fid);
	void NotifiyUser();

	static HRESULT RegisterClass();																																			// 注册窗口类
	static LRESULT CALLBACK WindowProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam);								// 窗口过程
};

#endif //_CHATWIN_H_