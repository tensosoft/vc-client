#pragma once

class UpdateEntry
{
public:
	UpdateEntry(void);
	UpdateEntry(TCHAR* szTargetPath,TCHAR* szMD5,TCHAR* szFileName,TCHAR* szLocalFileName);
	~UpdateEntry(void);
	UINT GetTargetPath(TCHAR* szRet,size_t size);
	UINT GetMD5(TCHAR* szRet,size_t size);
	UINT GetFileName(TCHAR* szRet,size_t size);
	UINT GetLocalFileName(TCHAR* szRet,size_t size);
private:
	TCHAR m_szTargetPath[MAX_PATH];
	TCHAR m_szMD5[STRLEN_SMALL];
	TCHAR m_szFileName[MAX_PATH];
	TCHAR m_szLocalFileName[MAX_PATH];
};