// updaterapp.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "updaterapp.h"
//#include "../commonfiles/MD5.h"
//#include "../commonfiles/convutil.h"
#include "updatercontext.h"
#include <shlguid.h>
#include <exdispid.h>
#include <Shlobj.h>
#include <shellapi.h>
#include <shlwapi.h>
#include <urlmon.h>
#include <wininet.h>
#include <tlhelp32.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "unzip.h"

#pragma comment(lib,"shlwapi.lib")
#pragma comment(lib,"comctl32.lib")
#pragma comment(lib,"urlmon.lib")
#pragma comment(lib,"wininet.lib")

#define WIDTH 400
#define HEIGHT 185

#define WINDOW_CAPTION L"客户端组件自动安装与更新程序"
#define WINDOW_CLASS_NAME L"discoverx2updater"

#define IDC_PROGRESSBAR	1002
#define IDC_TITLE	1003
#define IDC_TIP	1004
#define IDT_TIMER 1100
#define TIMER_ELAPSE 500

//#define IDC_TMPTEXT 1005

#define UPDATER_INI_FN	L"updater.ini"					//本机客户端组件安装目录下记录已经安装的安装包信息的ini文件名
#define UPDATER_INI_DT_KEY	L"dt"								//获取本机客户端组件安装目录下记录的已经安装的安装包信息的安装日期信息的配置项名称
#define UPDATER_FILE_PATH_NAME	L"/client/"			//下载安装包时，安装包文件所在的相对于webapp跟路径的路径名。
#define UPDATER_UNZIP_DIR	L"d2utmp"							//保存安装包内容解压后的临时文件相对于系统临时目录的子目录名

// 全局变量:
typedef enum {NOFLAG=0,IE=1,MSWORD=2,MSEXCEL=4} ProcessFlag;								//常见需要提示关闭的进程标记
DWORD dwProcessFlag=NOFLAG;																									//实际需要提示关闭的进程标记
HINSTANCE hInst;																														//当前实例
enum {Help=0,Updater,Install/*,BuildIni*/} doType=Help;											//当前执行的命令类型：帮助、更新、安装
TCHAR szDownloadUpdaterZipUrl[STRLEN_1K]={0};																//更新时作为命令行参数传入的下载更新内容zip包的路径
TCHAR szInitInstallUrl[STRLEN_1K]={0};																			//客户端组件已经安装但是系统是初次运行时对应的初始化安装时作为命令行参数传入的url基地址
TCHAR szRootUrl[STRLEN_1K]={0};																							//安装时用于保存url根路径信息
TCHAR szRootUrlSSL[STRLEN_1K]={0};																					//安装时用于保存ssl形式的根路径
TCHAR szRootAppUrl[STRLEN_1K]={0};																					//安装时用于保存应用程序url根路径信息
TCHAR szExtName[STRLEN_SMALL]={0};																					//扩展名，.jsp或.aspx
TCHAR szSystemTitle[MAX_PATH]={0};																					//系统名称


// 此代码模块中包含的函数的前向声明:
ATOM	RegisterClass(HINSTANCE hInstance);
BOOL	InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
DWORD WINAPI InstallThreadProc(LPVOID lpParam);
BOOL Update(HWND hWnd);
void LaunchLogin(HWND hWnd);
DWORD WINAPI UpdateThreadProc(LPVOID lpParam);
BOOL GetRootUrl(HWND hWnd);																									//尝试获取url路径
BOOL AddTrustedSite(TCHAR** szUrls,size_t size,HWND hWnd);									//设置指定url为安全站点
BOOL CallInstallScript(TCHAR* szPath,HWND hWnd);														//执行安装脚本
INT_PTR CALLBACK InputUrlDialogProc(HWND hwndDlg,UINT uMsg,WPARAM wParam,LPARAM lParam);	//url地址对话框方法
BOOL RegInit(TCHAR* rootUrl,HWND hWnd);																			//注册表初始化
BOOL CheckProcesses();																											//检查所有进程中是否包含要提示关闭的进程
BOOL TipCloseProcess(HWND hWnd);																						//提示用户关闭建议关闭的进程
BOOL ParseTargetSite(TCHAR* ts,size_t tsSize);															//解析安装针对的目标站点信息
TCHAR* ParseTrustSites(size_t* pCnt);																				//解析安装时额外添加的安全站点信息

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // 将实例句柄存储在全局变量中

	 int screenWidth=GetSystemMetrics(SM_CXSCREEN);
	 int screenHeight=GetSystemMetrics(SM_CYSCREEN);
   hWnd = CreateWindow(WINDOW_CLASS_NAME, WINDOW_CAPTION, WS_OVERLAPPED | WS_CAPTION |WS_MINIMIZEBOX| WS_SYSMENU ,(screenWidth-WIDTH)/2, (screenHeight-HEIGHT)/2, WIDTH, HEIGHT, NULL, NULL, hInstance, NULL);

   if (!hWnd) return FALSE;

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}


ATOM RegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_UPDATERAPP));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= GetSysColorBrush(COLOR_3DFACE);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_UPDATERAPP);
	wcex.lpszClassName	= WINDOW_CLASS_NAME;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}


int APIENTRY _tWinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPTSTR lpCmdLine,int nCmdShow)
{
	CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);

	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	
	if(lpCmdLine && (_tcsstr(lpCmdLine,L"-i")!=NULL || _tcsstr(lpCmdLine,L"/i")!=NULL)){
		doType=Install;
		size_t paramLen=_tcslen(lpCmdLine);
		if(paramLen>=(STRLEN_1K-MAX_PATH)){
			ErrMsg(0,L"提供的访问系统的Url基地址太长！");
			return FALSE;
		}
		if(paramLen>3){
			ZeroMemory(szInitInstallUrl,sizeof(TCHAR)*STRLEN_1K);
			_tcscpy_s(szInitInstallUrl,lpCmdLine+3);
		}
	}	else if(lpCmdLine && (_tcsstr(lpCmdLine,L"-u")!=NULL || _tcsstr(lpCmdLine,L"/u")!=NULL)){
		HANDLE hMutex=OpenMutex(MUTEX_ALL_ACCESS,FALSE,L"discoverx2_updater");
		if(hMutex){ReleaseMutex(hMutex);}
		else{
			ErrMsg(0,L"您必须通过更新检测器启动自动更新程序！");
			CoUninitialize();
			return FALSE;
		}
		size_t paramLen=_tcslen(lpCmdLine);
		if(paramLen==0 || paramLen<=3){
			ErrMsg(0,L"更新包下载路径未提供！");
			return FALSE;
		}
		if(paramLen>=(STRLEN_1K-MAX_PATH)){
			ErrMsg(0,L"更新包下载路径太长！");
			return FALSE;
		}
		ZeroMemory(szDownloadUpdaterZipUrl,sizeof(TCHAR)*STRLEN_1K);
		_tcscpy_s(szDownloadUpdaterZipUrl,lpCmdLine+3);
		doType=Updater;
	}	else{
		doType=Help;
	}

	if(doType==Help){
		TCHAR szHelp[STRLEN_1K]={0};
		_stprintf_s(szHelp,L"使用方法：\r\nupdater.exe [/|-][i|u|h]\r\n参数说明：\r\n /i [url],-i [url]:表示执行系统第一次访问的初始化，其中可选的url为访问系统的目标路径；\r\n /u url,-u url:表示自动更新（只能通过更新检测器调用）；\r\n /h,-h:表示显示使用方法。\r\n如果不提供参数则默认为显示使用方法。");	//\r\n /b,-b:表示为服务器端生成更新检测ini文件；
		TipMsg(szHelp);
		return FALSE;
	}

	INITCOMMONCONTROLSEX ccx;
	ccx.dwSize=sizeof(ccx);
	ccx.dwICC=ICC_PROGRESS_CLASS;
	InitCommonControlsEx(&ccx);

	//注册窗口类
	RegisterClass(hInstance);

	// 执行应用程序初始化:
	if (!InitInstance (hInstance, nCmdShow)) {ErrMsg(GetLastError(),L"启动应用程序时发生错误：%s");return FALSE;}

	MSG msg;
	HACCEL hAccelTable;
	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_UPDATERAPP));
	// 主消息循环:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	CoUninitialize();

	return (int) msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
		{
			HWND hwndTitle=CreateWindowEx(0,L"STATIC", L"进度：",WS_CHILD | WS_VISIBLE ,10,30,100,20,hWnd, (HMENU)IDC_TITLE, hInst, NULL);

			//开始|关闭按钮
			HWND hwndBtn = CreateWindow( L"BUTTON",L"开始",WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,WIDTH-90,HEIGHT-60,74,22,hWnd,(HMENU)IDC_CLOSE,hInst,NULL);
			int cyVScroll = GetSystemMetrics(SM_CYVSCROLL); 

			HWND hwndPG = CreateWindowEx(0, PROGRESS_CLASS,(LPTSTR) NULL, WS_CHILD | WS_VISIBLE,10,50, WIDTH-28, cyVScroll,hWnd, (HMENU)IDC_PROGRESSBAR, hInst, NULL);
			HWND hwndTip=CreateWindowEx(0,L"STATIC", L"",WS_CHILD | WS_VISIBLE ,10,75,WIDTH-28,20,hWnd,(HMENU)IDC_TIP, hInst, NULL);
			//HWND hwndTmpTxt=CreateWindowEx(0,L"EDIT", L"",WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL ,10,75,WIDTH-28,50,hWnd,(HMENU)IDC_TMPTEXT, hInst, NULL);

			HFONT ft=(HFONT)GetStockObject(DEFAULT_GUI_FONT);
			SendMessage(hwndTitle,WM_SETFONT,(WPARAM)ft,1);
			SendMessage(hwndBtn,WM_SETFONT,(WPARAM)ft,1);
			SendMessage(hwndTip,WM_SETFONT,(WPARAM)ft,1);
			//SendMessage(hwndTmpTxt,WM_SETFONT,(WPARAM)ft,1);
			DeleteObject(ft);
			if(doType>=1){
				if(!SetTimer(hWnd,IDT_TIMER,TIMER_ELAPSE,(TIMERPROC)NULL)){
					TipMsg(L"请单击“开始”按钮！");
					break;
				}
			}
		}
		break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// 分析菜单选择:
		switch (wmId)
		{
		case IDC_CLOSE:
			{	
				TCHAR btnTxt[MAX_PATH]={0};
				GetDlgItemText(hWnd,IDC_CLOSE,btnTxt,MAX_PATH);
				if(_tcsicmp(btnTxt,L"开始")==0){
					HWND hwndBtn=GetDlgItem(hWnd,IDC_CLOSE);
					EnableWindow(hwndBtn,FALSE);
					switch(doType){
					case Install:
						if(TipCloseProcess(hWnd)!=IDCANCEL)	CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE)InstallThreadProc,(LPVOID)hWnd,0,NULL);
						break;
					case Updater:
						if(TipCloseProcess(hWnd)!=IDCANCEL)	CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE)UpdateThreadProc,(LPVOID)hWnd,0,NULL);
						break;
					//case BuildIni:
					//	CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE)BuildIniThreadProc,(LPVOID)hWnd,0,NULL);
					//	break;
					}
					SetDlgItemText(hWnd,IDC_CLOSE,L"关闭");
					EnableWindow(hwndBtn,TRUE);
				}else{	//关闭
					DestroyWindow(hWnd);
					break;
				}
			}
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_TIMER: 
		if(wParam==IDT_TIMER) 
		{
			KillTimer(hWnd,IDT_TIMER);
			SendDlgItemMessage(hWnd,IDC_CLOSE,BM_CLICK,0,0);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: 在此添加任意绘图代码...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		KillTimer(hWnd,IDT_TIMER);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

DWORD WINAPI UpdateThreadProc(LPVOID lpParam){
	HWND hWnd=(HWND)lpParam;
	BOOL ret=Update(hWnd);
	if(ret) LaunchLogin(hWnd);

	SetDlgItemText(hWnd,IDC_TIP,(ret?L"完成！":L"更新过程出现异常！"));
	SetDlgItemText(hWnd,IDC_CLOSE,L"关闭");
	if(!ret){
		HWND hwndPB=GetDlgItem(hWnd,IDC_PROGRESSBAR);
		SendMessage(hwndPB,PBM_SETRANGE,0,MAKELPARAM(0,1)); 
		SendMessage(hwndPB,PBM_SETSTEP,(WPARAM)1,0); 
	}
	HWND hwndBtn=GetDlgItem(hWnd,IDC_CLOSE);
	EnableWindow(hwndBtn,TRUE);
	return ret;
}
BOOL Update(HWND hwnd){
	BOOL ret=FALSE;
	
	TCHAR szZipFile[MAX_PATH]={0};						//更新包zip临时文件路径
	TCHAR szManifestFile[MAX_PATH]={0};				//更新包zip临时文件对应的清单文件路径
	TCHAR szUnzipBaseDir[MAX_PATH]={0};				//临时解压路径
	{//begin
		//0.初始化
		TCHAR szInstallFolder[MAX_PATH]={0};		//安装路径
		int pathLen=GetModuleFileName(NULL,szInstallFolder,MAX_PATH);
		TCHAR* occur=_tcsrchr(szInstallFolder,L'\\');
		if(occur)	*(occur+1)=L'\0';

		TCHAR szWorkFolder[MAX_PATH]={0};				//工作路径
		ZeroMemory(szWorkFolder,sizeof(TCHAR)*MAX_PATH);
		HRESULT hResult=SHGetFolderPath(NULL, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, NULL, 0, szWorkFolder);
		if (!SUCCEEDED(hResult)) {
			ErrMsg(GetLastError(),_T("无法获取工作路径！"));
			goto clean;
		}
		_tcscat_s(szWorkFolder,WORK_FOLDER);
		CreateDirectory(szWorkFolder,NULL);
		_tcscat_s(szWorkFolder,L"\\");

		TCHAR szCurrentIni[MAX_PATH]={0};				//本地updater.ini路径
		_tcscpy_s(szCurrentIni,szInstallFolder);
		_tcscat_s(szCurrentIni,UPDATER_INI_FN);

		TCHAR szTempFolder[MAX_PATH]={0};				//临时路径
		pathLen=GetTempPath(MAX_PATH,szTempFolder);
		if(pathLen>0 && pathLen<=(MAX_PATH-14)){
			if(GetTempFileName(szTempFolder,L"d2u",0,szZipFile)==0){
				ErrMsg(GetLastError(),L"无法创建临时文件：%s");
				goto clean;
			}
			if(GetTempFileName(szTempFolder,L"d2m",0,szManifestFile)==0){
				ErrMsg(GetLastError(),L"无法创建临时文件：%s");
				goto clean;
			}
		}
		else{
			ErrMsg(GetLastError(),L"路径太长！");
			goto clean;
		}

		//1.下载清单文件
		SetDlgItemText(hwnd,IDC_TIP,L"正在下载更新包清单文件...");
		TCHAR szDownloadUpdaterManifestUrl[STRLEN_1K]={0};
		_tcsncpy_s(szDownloadUpdaterManifestUrl,szDownloadUpdaterZipUrl,_tcslen(szDownloadUpdaterZipUrl)-4);
		_tcscat_s(szDownloadUpdaterManifestUrl,L".manifest");
		if(URLDownloadToFile(NULL,szDownloadUpdaterManifestUrl,szManifestFile,0,NULL)!=S_OK){
			ErrMsg(0,L"无法下载更新包清单文件，请联系管理员配置服务器上的更新信息！");
			goto clean;
		}
		//1.1计算需要更新的文件
		UpdaterContext currentContext(szManifestFile);
	
		HWND hwndPB=GetDlgItem(hwnd,IDC_PROGRESSBAR);
		SendMessage(hwndPB,PBM_SETRANGE,0,MAKELPARAM(0,currentContext.m_updateEntries.size()+4)); 
		SendMessage(hwndPB,PBM_SETSTEP,(WPARAM)1,0);

		SendMessage(hwndPB,PBM_STEPIT,0,0);													//step 1

		//2.下载更新包
		SetDlgItemText(hwnd,IDC_TIP,L"正在下载更新包...");
		if(URLDownloadToFile(NULL,szDownloadUpdaterZipUrl,szZipFile,0,NULL)!=S_OK){
			ErrMsg(0,L"无法下载更新包，请联系管理员配置服务器上的更新信息！");
			goto clean;
		}

		SendMessage(hwndPB,PBM_STEPIT,0,0);													//step 2

		//3.解压下载的文件。
		SetDlgItemText(hwnd,IDC_TIP,L"正在分析并读取更新包...");
		HZIP hz = OpenZip(szZipFile,0);
		if(!hz){
			ErrMsg(0,L"无法读取下载的更新包！");
			goto clean;
		}
		
		ZeroMemory(szUnzipBaseDir,sizeof(TCHAR)*MAX_PATH);
		_tcscpy_s(szUnzipBaseDir,szTempFolder);
		_tcscat_s(szUnzipBaseDir,UPDATER_UNZIP_DIR);
		_tcscat_s(szUnzipBaseDir,L"\\");
		SetUnzipBaseDir(hz,szUnzipBaseDir);

		ZIPENTRY zei;
		GetZipItem(hz,-1,&zei);
		int numitems=zei.index;
		for (int zi=0; zi<numitems; zi++)
		{
			ZIPENTRY ze;
			GetZipItem(hz,zi,&ze);
			UnzipItem(hz, zi, ze.name);
		}
		CloseZip(hz);

		SendMessage(hwndPB,PBM_STEPIT,0,0);													//step 3

		//4.循环处理待更新文件
		BOOL needReboot=FALSE;
		UpdateEntryIterator updateitr;
		for(updateitr=currentContext.m_updateEntries.begin();updateitr!=currentContext.m_updateEntries.end();updateitr++){
			UpdateEntry* ue=(UpdateEntry*)(*updateitr);
			TCHAR szTargetPath[MAX_PATH]={0};
			TCHAR szFileName[MAX_PATH]={0};
			TCHAR szLocalFileName[MAX_PATH]={0};
			TCHAR szMD5[STRLEN_SMALL]={0};
			ue->GetFileName(szFileName,MAX_PATH);
			ue->GetLocalFileName(szLocalFileName,MAX_PATH);
			ue->GetTargetPath(szTargetPath,MAX_PATH);
			ue->GetMD5(szMD5,STRLEN_SMALL);
			
			TCHAR szTip[MAX_PATH]={0};
			_tcscpy_s(szTip,L"正在更新：");
			_tcscat_s(szTip,szFileName);
			SetDlgItemText(hwnd,IDC_TIP,szTip);

			TCHAR szSrc[MAX_PATH]={0};					//要更新文件的源文件
			TCHAR szDst[MAX_PATH]={0};					//要更新文件的目标文件
			
			_tcscpy_s(szSrc,szUnzipBaseDir);
			_tcscat_s(szSrc,szFileName);

			if(!PathFileExists(szSrc)){
				ErrMsg(GetLastError(),L"待更新文件不存在！");
				goto clean;
			}

			//获取目标文件路径
			if(_tcsstr(szTargetPath,TARGETPATH_INSTALLFOLDER)!=NULL){
				_tcscpy_s(szDst,szInstallFolder);
			}
			else if(_tcsstr(szTargetPath,TARGETPATH_WORKFOLDER)!=NULL){
				_tcscpy_s(szDst,szWorkFolder);
			}
			else{
				_tcscpy_s(szDst,szInstallFolder);
			}
			_tcscat_s(szDst,szLocalFileName);
			
			//更新
			if(!PathFileExists(szDst)){	//目标文件不存在，则直接移动
				if(MoveFileEx(szSrc,szDst,MOVEFILE_COPY_ALLOWED|MOVEFILE_WRITE_THROUGH)==FALSE){
					ErrMsg(GetLastError(),L"无法更新目标文件！");
					goto clean;
				}
			}
			else{	//否则尝试覆盖，覆盖不成功则提示用户重起覆盖
				if(MoveFileEx(szSrc,szDst,MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING|MOVEFILE_WRITE_THROUGH)==FALSE){
					TCHAR szFNTmp[MAX_PATH]={0};
					_tcscpy_s(szFNTmp,szDst);
					_tcscat_s(szFNTmp,L".bak");
					MoveFileEx(szSrc, szFNTmp, MOVEFILE_COPY_ALLOWED|MOVEFILE_WRITE_THROUGH);	//先移动到目标文件名.bak
					MoveFileEx(szDst, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);					//重起时删除老目标文件。
					MoveFileEx(szFNTmp, szDst, MOVEFILE_DELAY_UNTIL_REBOOT);			//把.bak为结尾的新文件改名为要更新的文件（目标文件）。
					if(!needReboot) needReboot=TRUE;
				}//if end
			}//else end
			SendMessage(hwndPB,PBM_STEPIT,0,0);					//step ++
		}//for end

		//6.记录已更新的更新包文件名
		SetDlgItemText(hwnd,IDC_TIP,L"记录更新结果...");
		TCHAR szUpdatedFile[MAX_PATH]={0};																	//已更新的更新包对应的文件名
		TCHAR *szLastSeparatorPos=_tcsrchr(szDownloadUpdaterZipUrl,L'/');		//已更新的更新包对应的文件名的位置
		if(szLastSeparatorPos) _tcscpy_s(szUpdatedFile,szLastSeparatorPos+1);
		TCHAR szLocalUpdaterIni[MAX_PATH]={0};															//客户端组件安装目录下记录已更新信息的updater.ini
		_tcscpy_s(szLocalUpdaterIni,szInstallFolder);
		_tcscat_s(szLocalUpdaterIni,UPDATER_INI_FN);
		 SYSTEMTIME stLocal;
		GetLocalTime(&stLocal);
		TCHAR szDt[STRLEN_NORMAL]={0};
		_stprintf_s(szDt,L"%d-%02d-%02d %02d:%02d:%02d",stLocal.wYear,stLocal.wMonth,stLocal.wDay,stLocal.wHour,stLocal.wMinute,stLocal.wSecond);
		WritePrivateProfileString(szUpdatedFile,UPDATER_INI_DT_KEY,szDt,szLocalUpdaterIni);

		SendMessage(hwndPB,PBM_STEPIT,0,0);					//step last

		//如果需要重起，那么提示
		if(needReboot){
			int askRet=MessageBox(0,L"您需要重新启动计算机才能完成更新，是否现在重新启动？",L"更新提示",MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON1);
			if (askRet==IDNO) {ret=TRUE;goto clean;}
			HANDLE hToken; 
			TOKEN_PRIVILEGES tkp; 
			if (OpenProcessToken(GetCurrentProcess(),TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)){
				LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME,&tkp.Privileges[0].Luid);
				tkp.PrivilegeCount = 1;
				tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
				AdjustTokenPrivileges(hToken, FALSE, &tkp, 0,(PTOKEN_PRIVILEGES)NULL, 0);
				if (GetLastError() == ERROR_SUCCESS){
					return ExitWindowsEx(EWX_REBOOT|EWX_FORCE|EWX_FORCEIFHUNG,SHTDN_REASON_MAJOR_APPLICATION | SHTDN_REASON_MINOR_INSTALLATION | SHTDN_REASON_FLAG_PLANNED);
				}
			}
			ErrMsg(GetLastError(),L"更新过程出现异常，请联系管理员！\r\n%s");
			goto clean;
		}
		
		ret=TRUE;
	}//end
clean:
	//删除下载后解压的临时文件目录。
	SHFILEOPSTRUCT fileop;
  fileop.hwnd   = NULL;
  fileop.wFunc  = FO_DELETE;
  fileop.pFrom  = szUnzipBaseDir;
  fileop.pTo    = NULL;
  fileop.fFlags = FOF_NOCONFIRMATION|FOF_SILENT|FOF_NOERRORUI;
	fileop.fAnyOperationsAborted = FALSE;
  fileop.lpszProgressTitle     = NULL;
  fileop.hNameMappings         = NULL;
	SHFileOperation(&fileop);
	RemoveDirectory(szUnzipBaseDir);

	//删除临时目录下的更新包文件
	DeleteFile(szZipFile);
	//删除临时目录下的更新包清单文件（如果有的话）
	DeleteFile(szManifestFile);
	return ret;
}
void LaunchLogin(HWND hwnd){
	OleInitialize(NULL);
	CLSID clsid;
	LPUNKNOWN punk=NULL;
	CComQIPtr<IWebBrowser2> m_pInetExplorer;
	CLSIDFromProgID (OLESTR("InternetExplorer.Application"), &clsid);
	HRESULT hr = CoCreateInstance (clsid, NULL, CLSCTX_SERVER, IID_IUnknown, (LPVOID *) &punk);
	if (SUCCEEDED(hr)){
		punk->QueryInterface (IID_IWebBrowser2, (LPVOID *) &m_pInetExplorer);
		punk->Release();
		HWND hWndIE=NULL;
		m_pInetExplorer->get_HWND((SHANDLE_PTR*)&hWndIE);
		if(hWndIE) ShowWindow(hWndIE,SW_SHOWMAXIMIZED);
		hr = m_pInetExplorer->put_StatusBar(VARIANT_TRUE);
		hr = m_pInetExplorer->put_ToolBar(VARIANT_TRUE);
		hr = m_pInetExplorer->put_MenuBar(VARIANT_TRUE);
		hr = m_pInetExplorer->put_Visible(VARIANT_TRUE);
		VARIANT vars[4];
		memset(vars,0,sizeof(vars));

		TCHAR* szUrl=_tcsstr(szDownloadUpdaterZipUrl+8,UPDATER_FILE_PATH_NAME);
		if(szUrl!=NULL) *(szUrl+1)=L'\0';
		BSTR bstrUrl=SysAllocString(szDownloadUpdaterZipUrl);
		m_pInetExplorer->Navigate(bstrUrl,vars,vars+1,vars+2,vars+3);
		
		FREE_SYS_STR(bstrUrl);
	}
	OleUninitialize();
}

DWORD WINAPI InstallThreadProc(LPVOID lpParam){
	HWND hWnd=(HWND)lpParam;
	BOOL ret=TRUE;
	HWND hwndPB=GetDlgItem(hWnd,IDC_PROGRESSBAR);
	SendMessage(hwndPB,PBM_SETRANGE,0,MAKELPARAM(0,8));
	SendMessage(hwndPB,PBM_SETSTEP,(WPARAM)1,0);
	SetDlgItemText(hWnd,IDC_TIP,L"初始化安装...");

	TCHAR szUrl[STRLEN_2K]={0};
	if(_tcslen(szInitInstallUrl)==0) ParseTargetSite(szInitInstallUrl,STRLEN_1K);
	if(_tcslen(szInitInstallUrl)==0){
		if(_tcslen(szRootUrl)==0 || _tcslen(szRootUrlSSL)==0) GetRootUrl(hWnd);
		if(_tcslen(szRootUrl)==0 || _tcslen(szRootUrlSSL)==0){
			SetDlgItemText(hWnd,IDC_TIP,L"等待输入URL地址...");
			if(DialogBoxParam(GetModuleHandle(NULL),MAKEINTRESOURCE(IDD_INPUTURL),hWnd,(DLGPROC)InputUrlDialogProc,(LPARAM)szUrl)==IDCANCEL){
				ZeroMemory(szUrl,sizeof(TCHAR)*STRLEN_2K);
			}
			if(_tcslen(szUrl)==0 && MessageBox(hWnd,L"没有提供有效的登录系统的URL地址，您可以选择继续安装，但是客户端部分功功能可能会无法正常工作！是否继续安装？",L"安装提示",MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON1)==IDNO){
				SetDlgItemText(hWnd,IDC_TIP,L"安装被取消！");
				return FALSE;
			}
			_tcscpy_s(szDownloadUpdaterZipUrl,szUrl);
		}
	}else{
		_tcscpy_s(szDownloadUpdaterZipUrl,szInitInstallUrl);
		_tcscpy_s(szUrl,szInitInstallUrl);
		if(szUrl[_tcslen(szUrl)-1]!=L'/') _tcscat_s(szUrl,L"/");
		_tcscat_s(szUrl,L"login.");
	}

	if(_tcslen(szUrl)>0){
		//szRootAppUrl
		TCHAR tmpRootAppUrl[STRLEN_1K]={0};
		_tcscpy_s(tmpRootAppUrl,szUrl);
		TCHAR* szFile=_tcsstr(tmpRootAppUrl,L"/login.");
		if(szFile) szFile[0]=L'\0';
		szFile=_tcsstr(tmpRootAppUrl,L"://");
		if(szFile) {_tcscpy_s(szRootAppUrl,szFile+3);}

		TCHAR* szPath=_tcschr(szUrl+8,L'/');
		if(szPath) szPath[0]=L'\0';
		szPath=_tcschr(szUrl+8,L':');
		if(szPath) szPath[0]=L'\0';
		if(_tcsstr(szUrl,L"http://")) {
			_tcscpy_s(szRootUrl,szUrl);
			_tcscpy_s(szRootUrlSSL,L"https://");
			_tcscat_s(szRootUrlSSL,szUrl+7);
		}else if(_tcsstr(szUrl,L"https://")){
			_tcscpy_s(szRootUrlSSL,szUrl);
			_tcscpy_s(szRootUrl,L"http://");
			_tcscat_s(szRootUrl,szUrl+8);
		}
	}

	if(_tcslen(szRootUrl)==0 || _tcslen(szRootUrlSSL)==0){
		if(MessageBox(hWnd,L"您提供的地址不是有效地地址，您可以选择继续安装，但是客户端部分功能可能会无法正常工作！是否继续安装？",L"安装提示",MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON1)==IDNO){
			SetDlgItemText(hWnd,IDC_TIP,L"安装被取消！");
			return FALSE;
		}
	}
	
	BOOL ret1=TRUE;
	//创建工作目录和环境变量
	TCHAR szWorkFolder[MAX_PATH]={0};
	ZeroMemory(szWorkFolder,sizeof(TCHAR)*MAX_PATH);
	if(SHGetFolderPath(NULL, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, NULL, 0, szWorkFolder)==S_OK){
		_tcscat_s(szWorkFolder,WORK_FOLDER);
		CreateDirectory(szWorkFolder,NULL);
		HKEY hKey=NULL;
		TCHAR szKeyName[STRLEN_1K]=L"System\\CurrentControlSet\\Control\\Session Manager\\Environment";
		if(RegOpenKeyEx(HKEY_LOCAL_MACHINE,szKeyName, 0, KEY_ALL_ACCESS , &hKey)==ERROR_SUCCESS){
			RegSetValueEx(hKey,L"DISCOVERX2_DATA_DIR",0,REG_SZ,(const BYTE*)szWorkFolder,_tcslen(szWorkFolder)*sizeof(TCHAR));
			TCHAR pdir[MAX_PATH]={0};
			if(GetModuleFileName(NULL,pdir,MAX_PATH)!=0 && _tcslen(pdir)>0){
				TCHAR* occur=_tcsrchr(pdir,L'\\');
				if(occur) {
					*(occur)=_T('\0');
					RegSetValueEx(hKey,L"DISCOVERX2_PROGRAMS_DIR",0,REG_SZ,(const BYTE*)pdir,_tcslen(pdir)*sizeof(TCHAR));
				}
			}
			RegCloseKey(hKey);
			SendMessageTimeout(HWND_BROADCAST,WM_SETTINGCHANGE,NULL,(LPARAM)L"Environment",SMTO_NORMAL,100,NULL);
		}
	}

	SetDlgItemText(hWnd,IDC_TIP,L"执行安装脚本...");
	ret1=CallInstallScript(L"install.bat",hWnd);

	SendMessage(hwndPB,PBM_STEPIT,0,0);			//step 1

	SetDlgItemText(hWnd,IDC_TIP,L"添加安全站点...");

	size_t tssCnt=0;
	size_t cnt=0;
	TCHAR* tss=ParseTrustSites(&cnt);
	tssCnt=cnt+2;
	TCHAR **szUrls=new TCHAR*[tssCnt];
	szUrls[0]=new TCHAR[STRLEN_1K];
	_tcscpy_s(szUrls[0],STRLEN_1K,szRootUrl);
	szUrls[1]=new TCHAR[STRLEN_1K];
	_tcscpy_s(szUrls[1],STRLEN_1K,szRootUrlSSL);
	if(cnt>0){
		int idx=2;
		for (LPTSTR pszz =(LPTSTR)tss; *pszz; pszz += _tcslen(pszz) + 1) {
			szUrls[idx]=new TCHAR[STRLEN_1K];
			_tcscpy_s(szUrls[idx],STRLEN_1K,pszz);
			idx++;
		}
	}
	//szUrls[2]=L"http://*.tensosoft.com";
	//szUrls[3]=L"https://*.tensosoft.com";
	//szUrls[4]=L"http://*.tensosoft.cn";
	//szUrls[5]=L"https://*.tensosoft.cn";
	BOOL ret2=AddTrustedSite(szUrls,tssCnt,hWnd);
	//SAFE_DEL_ARR(szUrls[0]);
	//SAFE_DEL_ARR(szUrls[1]);
	for(int i=0;i<tssCnt;i++){SAFE_DEL_ARR(szUrls[i]);}
	SAFE_DEL_ARR(szUrls);
	SAFE_DEL_ARR(tss);

	SetDlgItemText(hWnd,IDC_TIP,L"初始化注册表...");
	BOOL ret3=RegInit(szRootAppUrl,hWnd);
	Sleep(300);
	SendMessage(hwndPB,PBM_STEPIT,0,0);		//step 2
	ret=(ret1 && ret2 && ret3);
	SetDlgItemText(hWnd,IDC_TIP,(ret?L"完成！":L"安装过程出现异常！"));
	SetDlgItemText(hWnd,IDC_CLOSE,L"关闭");
	if(!ret){
		HWND hwndPB=GetDlgItem(hWnd,IDC_PROGRESSBAR);
		SendMessage(hwndPB,PBM_SETRANGE,0,MAKELPARAM(0,1)); 
		SendMessage(hwndPB,PBM_SETSTEP,(WPARAM)1,0); 
	}else{
		LaunchLogin(hWnd);
	}

	HWND hwndBtn=GetDlgItem(hWnd,IDC_CLOSE);
	EnableWindow(hwndBtn,TRUE);
	return ret;
}
BOOL GetRootUrl(HWND hWnd)
{
	DWORD dwEntrySize;
	LPINTERNET_CACHE_ENTRY_INFO lpCacheEntry;
	DWORD MAX_CACHE_ENTRY_INFO_SIZE = 4096;
	HANDLE hCacheDir;
	int nCount=0;

	//SendDlgItemMessage(hWnd,IDC_TMPTEXT,LB_RESETCONTENT,0,0);

	SetCursor(LoadCursor(NULL,IDC_WAIT));

	dwEntrySize = MAX_CACHE_ENTRY_INFO_SIZE;
	lpCacheEntry = (LPINTERNET_CACHE_ENTRY_INFO) new TCHAR[dwEntrySize];
	lpCacheEntry->dwStructSize = dwEntrySize;
	TCHAR szClientInstall[STRLEN_1K]={0};
	TCHAR szClientExe[STRLEN_1K]={0};
again:
	if (!(hCacheDir = FindFirstUrlCacheEntry(L"visited:",lpCacheEntry,&dwEntrySize))){
		SetCursor(LoadCursor(NULL,IDC_ARROW));
		SAFE_DEL_ARR(lpCacheEntry);
		switch(GetLastError()){
		case ERROR_NO_MORE_ITEMS:
			FindCloseUrlCache(hCacheDir);
			goto end;
		case ERROR_INSUFFICIENT_BUFFER:
			lpCacheEntry = (LPINTERNET_CACHE_ENTRY_INFO) new TCHAR[dwEntrySize];
			lpCacheEntry->dwStructSize = dwEntrySize;
			goto again;
			break;
		default:
			ErrMsg(GetLastError(),L"发生错误：%s");
			FindCloseUrlCache(hCacheDir);
			SetCursor(LoadCursor(NULL,IDC_ARROW));
			return FALSE;
		}
	}
	TCHAR szCrlf[3]=L"\r\n";
	if((lpCacheEntry->CacheEntryType & URLHISTORY_CACHE_ENTRY)==URLHISTORY_CACHE_ENTRY && _tcsstr(lpCacheEntry->lpszSourceUrlName,L"/client/") && _tcsstr(lpCacheEntry->lpszSourceUrlName,L"file:///")==NULL){
		//TipMsg(lpCacheEntry->lpszSourceUrlName);
		TCHAR* szUrl=_tcschr(lpCacheEntry->lpszSourceUrlName,L'@');
		TCHAR szUrlLwr[STRLEN_1K]={0};
		_tcscpy_s(szUrlLwr,szUrl+1);
		_tcslwr_s(szUrlLwr);
		if(_tcsstr(szUrlLwr,L"/client/client.exe")) {_tcscpy_s(szClientExe,szUrlLwr);_tcscpy_s(szDownloadUpdaterZipUrl,szUrlLwr);}
		if(_tcsstr(szUrlLwr,L"/client/clientinstall.htm")) _tcscpy_s(szClientInstall,szUrlLwr);
	}
	nCount++;
	SAFE_DEL_ARR(lpCacheEntry);
	do{
		dwEntrySize = MAX_CACHE_ENTRY_INFO_SIZE;
		lpCacheEntry = (LPINTERNET_CACHE_ENTRY_INFO) new TCHAR[dwEntrySize];
		lpCacheEntry->dwStructSize = dwEntrySize;
retry:
		if (!FindNextUrlCacheEntry(hCacheDir,lpCacheEntry,&dwEntrySize)){
			SetCursor(LoadCursor(NULL,IDC_ARROW));
			SAFE_DEL_ARR(lpCacheEntry);
			switch(GetLastError()){
			case ERROR_NO_MORE_ITEMS: 
				FindCloseUrlCache(hCacheDir);
				goto end;
			case ERROR_INSUFFICIENT_BUFFER:
				lpCacheEntry = (LPINTERNET_CACHE_ENTRY_INFO) new TCHAR[dwEntrySize];
				lpCacheEntry->dwStructSize = dwEntrySize;
				goto retry;
				break;
			default:
				ErrMsg(GetLastError(),L"发生错误：%s");
				FindCloseUrlCache(hCacheDir);
				return FALSE;
			}
		}
		if((lpCacheEntry->CacheEntryType & URLHISTORY_CACHE_ENTRY)==URLHISTORY_CACHE_ENTRY && _tcsstr(lpCacheEntry->lpszSourceUrlName,L"/client/") && _tcsstr(lpCacheEntry->lpszSourceUrlName,L"file:///")==NULL){
			//TipMsg(lpCacheEntry->lpszSourceUrlName);
			TCHAR* szUrl=_tcschr(lpCacheEntry->lpszSourceUrlName,L'@');
			TCHAR szUrlLwr[STRLEN_1K]={0};
			_tcscpy_s(szUrlLwr,szUrl+1);
			_tcslwr_s(szUrlLwr);
			if(_tcsstr(szUrlLwr,L"/client/client.exe")) {_tcscpy_s(szClientExe,szUrlLwr);_tcscpy_s(szDownloadUpdaterZipUrl,szUrlLwr);}
			if(_tcsstr(szUrlLwr,L"/client/clientinstall.htm")) _tcscpy_s(szClientInstall,szUrlLwr);
		}
		nCount++;
		SAFE_DEL_ARR(lpCacheEntry);
	}while (TRUE);
end:
	if(_tcslen(szClientExe)>0 && _tcslen(szClientInstall)>0){
		//szRootAppUrl
		TCHAR tmpRootAppUrl[STRLEN_1K]={0};
		_tcscpy_s(tmpRootAppUrl,szClientExe);
		TCHAR* szFile=_tcsstr(tmpRootAppUrl,L"/client/client.exe");
		if(szFile) szFile[0]=L'\0';
		szFile=_tcsstr(tmpRootAppUrl,L"://");
		if(szFile) {_tcscpy_s(szRootAppUrl,szFile+3);}

		TCHAR* szPath=_tcschr(szClientExe+8,L'/');
		if(szPath) szPath[0]=L'\0';
		szPath=_tcschr(szClientExe+8,L':');
		if(szPath) szPath[0]=L'\0';
		szPath=_tcschr(szClientInstall+8,L'/');
		if(szPath) szPath[0]=L'\0';
		szPath=_tcschr(szClientInstall+8,L':');
		if(szPath) szPath[0]=L'\0';
	}
	if(_tcsicmp(szClientExe,szClientInstall)==0){
		TCHAR* szPath=_tcschr(szClientExe+8,L':');
		if(szPath) szPath[0]=L'\0';
		if(_tcsstr(szClientExe,L"http://")) {
			_tcscpy_s(szRootUrl,szClientExe);
			_tcscpy_s(szRootUrlSSL,L"https://");
			_tcscat_s(szRootUrlSSL,szClientExe+7);
		}else if(_tcsstr(szClientExe,L"https://")){
			_tcscpy_s(szRootUrlSSL,szClientExe);
			_tcscpy_s(szRootUrl,L"http://");
			_tcscat_s(szRootUrl,szClientExe+8);
		}
		//SendDlgItemMessage(hWnd,IDC_TMPTEXT,EM_REPLACESEL,0,(LPARAM)(szRootUrl));
		//SendDlgItemMessage(hWnd,IDC_TMPTEXT,EM_REPLACESEL,0,(LPARAM)szCrlf);
		//SendDlgItemMessage(hWnd,IDC_TMPTEXT,EM_REPLACESEL,0,(LPARAM)(szRootUrlSSL));
	}
	else{ZeroMemory(szRootAppUrl,sizeof(TCHAR)*STRLEN_1K);}
	SetCursor(LoadCursor(NULL,IDC_ARROW));
	return TRUE;
}

BOOL AddTrustedSite(TCHAR** szUrls,size_t size,HWND hWnd){
	if(!szUrls || size==0) return FALSE;
	
	HUSKEY hKey;
	LSTATUS status=SHRegOpenUSKey(L"Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings\\Zones\\2",KEY_ALL_ACCESS ,NULL,&hKey,FALSE);
	if(status!=ERROR_SUCCESS) return FALSE;
	DWORD dwType=REG_DWORD;
	DWORD dwValue=0;
	DWORD dwValueSize=sizeof(dwValue);
	DWORD dwDefaultValue=0;
	DWORD dwDefaultValueSize=sizeof(dwDefaultValue);
	status=SHRegQueryUSValue(hKey,L"Flags",&dwType,(LPVOID)&dwValue,&dwValueSize,FALSE,&dwDefaultValue,dwDefaultValueSize);
	if(status!=ERROR_SUCCESS) {SHRegCloseUSKey(hKey);return FALSE;}
	BOOL needRestoreValue=FALSE;
	if((dwValue & 4) ==4){
		DWORD dwNewValue=dwValue-4;
		DWORD dwNewValueSize=sizeof(dwNewValue);
		status=SHRegWriteUSValue(hKey,L"Flags",REG_DWORD,(const void*)&dwNewValue,dwNewValueSize,SHREGSET_FORCE_HKCU);
		if(status!=ERROR_SUCCESS) {SHRegCloseUSKey(hKey);return FALSE;}
		needRestoreValue=TRUE;
	}
	CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);
	IInternetSecurityManager* pInetSecMgr;
	HRESULT hr = CoCreateInstance(CLSID_InternetSecurityManager, NULL, CLSCTX_ALL,IID_IInternetSecurityManager, (void **)&pInetSecMgr);   
	if(FAILED(hr)) {SHRegCloseUSKey(hKey);CoUninitialize();return FALSE;}
	int stepCnt=0;
	int maxStepCnt=6;
	for(size_t i=0;i<size;i++){
		if(_tcslen(*(szUrls+i))==0) continue;
		TCHAR szTip[STRLEN_2K]={0};
		_stprintf_s(szTip,L"添加安全站点:%s",*(szUrls+i));
		SetDlgItemText(hWnd,IDC_TIP,szTip);
		hr=pInetSecMgr->SetZoneMapping(URLZONE_TRUSTED,*(szUrls+i),SZM_CREATE);
		if(hr==E_ACCESSDENIED){
			ErrMsg(0,L"无法添加可信站点，部分客户端组件功能可能将无法正常使用！");
		}else{
			Sleep(300);
			if(stepCnt<maxStepCnt) SendDlgItemMessage(hWnd,IDC_PROGRESSBAR,PBM_STEPIT,0,0);
			stepCnt++;
		}
	}
	if(stepCnt<maxStepCnt){
		for(int i=0;i<(maxStepCnt-stepCnt);i++){
			SendDlgItemMessage(hWnd,IDC_PROGRESSBAR,PBM_STEPIT,0,0);
		}
	}
	pInetSecMgr->Release();
	CoUninitialize();
	if(needRestoreValue) {
		status=SHRegWriteUSValue(hKey,L"Flags",REG_DWORD,(const void*)&dwValue,dwValueSize,SHREGSET_FORCE_HKCU);
		if(status!=ERROR_SUCCESS) {SHRegCloseUSKey(hKey);return FALSE;}
	}
	SHRegCloseUSKey(hKey);
	
	return TRUE;
}
BOOL CallInstallScript(TCHAR* szPath,HWND hWnd){
	if(!szPath || _tcslen(szPath)==0) return FALSE;
	SHELLEXECUTEINFO ShExecInfo;
	ZeroMemory(&ShExecInfo,sizeof(SHELLEXECUTEINFO));
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS | SEE_MASK_FLAG_DDEWAIT;
	ShExecInfo.lpVerb = L"open";
	ShExecInfo.lpFile =szPath;
	ShExecInfo.lpParameters = L" >install.log";
	ShExecInfo.lpDirectory = NULL;
	ShExecInfo.nShow = SW_HIDE;
	ShExecInfo.hInstApp = NULL;
	ShExecInfo.hwnd = NULL;
	if (ShellExecuteEx(&ShExecInfo)==TRUE){
		WaitForSingleObject(ShExecInfo.hProcess,5000);
	}

	return TRUE;
}

INT_PTR CALLBACK InputUrlDialogProc(HWND hwndDlg,UINT uMsg,WPARAM wParam,LPARAM lParam){
	switch (uMsg) { 
	case WM_INITDIALOG:
		{
			SetWindowLongPtr(hwndDlg, DWLP_USER, lParam);
		}
		return TRUE;
	case WM_COMMAND: 
		switch (LOWORD(wParam)) 
		{ 
		case IDOK: 
			{
				TCHAR* szUrl=(TCHAR*)GetWindowLongPtr(hwndDlg, DWLP_USER);
				GetDlgItemText(hwndDlg,IDC_URL,szUrl,STRLEN_2K);
			}
		case IDCANCEL: 
			EndDialog(hwndDlg, wParam); 
			return LOWORD(wParam); 
		}
	} 
	return FALSE; 
}
BOOL RegInit(TCHAR* rootUrl,HWND hWnd){
	if(!rootUrl || _tcslen(rootUrl)==0) return FALSE;
	HKEY hKey=NULL;
	TCHAR szKeyName[STRLEN_1K]={0};
	LONG status=FALSE;

	//当前网站
	_stprintf_s(szKeyName,L"Software\\Discoverx2\\%s",rootUrl);
	LONG lRes = RegOpenKeyEx(HKEY_CURRENT_USER,szKeyName, 0, KEY_ALL_ACCESS , &hKey);
	BOOL bExistsAndSuccess=(lRes == ERROR_SUCCESS);
	if(bExistsAndSuccess && hKey) {
		status=ERROR_SUCCESS;
		RegCloseKey(hKey);
	}else if(RegCreateKeyEx(HKEY_CURRENT_USER,szKeyName,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&hKey,NULL)==ERROR_SUCCESS){
		RegCloseKey(hKey);
		status=ERROR_SUCCESS;
	}

	//腾硕网站
	TCHAR ext[STRLEN_SMALL]=L".jsp";
	TCHAR title[MAX_PATH]=L"腾硕协作应用社区";
	hKey=NULL;
	ZeroMemory(szKeyName,sizeof(TCHAR)*STRLEN_1K);
	_stprintf_s(szKeyName,L"Software\\Discoverx2\\www.tensosoft.com");
	if(RegCreateKeyEx(HKEY_CURRENT_USER,szKeyName,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&hKey,NULL)==ERROR_SUCCESS){
		RegSetValueEx(hKey,L"ext",0,REG_SZ,(const BYTE*)ext,_tcslen(ext)*sizeof(TCHAR));
		RegSetValueEx(hKey,L"title",0,REG_SZ,(const BYTE*)title,_tcslen(title)*sizeof(TCHAR));
		RegSetValueEx(hKey,NULL,0,REG_SZ,(const BYTE*)title,_tcslen(title)*sizeof(TCHAR));
		RegCloseKey(hKey);
	}

	//体验中心
	ZeroMemory(szKeyName,sizeof(TCHAR)*STRLEN_1K);
	ZeroMemory(title,sizeof(TCHAR)*MAX_PATH);
	_tcscpy_s(title,L"腾硕在线体验中心");
	_stprintf_s(szKeyName,L"Software\\Discoverx2\\demo.tensosoft.cn");
	hKey=NULL;
	if(RegCreateKeyEx(HKEY_CURRENT_USER,szKeyName,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&hKey,NULL)==ERROR_SUCCESS){
		RegSetValueEx(hKey,L"ext",0,REG_SZ,(const BYTE*)ext,_tcslen(ext)*sizeof(TCHAR));
		RegSetValueEx(hKey,L"title",0,REG_SZ,(const BYTE*)title,_tcslen(title)*sizeof(TCHAR));
		RegSetValueEx(hKey,NULL,0,REG_SZ,(const BYTE*)title,_tcslen(title)*sizeof(TCHAR));
		RegCloseKey(hKey);
	}

	if(status!=ERROR_SUCCESS) return FALSE;
	return TRUE;
}

BOOL CheckProcesses(){
	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;

	hProcessSnap = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
	if( hProcessSnap == INVALID_HANDLE_VALUE )
	{
		ErrMsg(GetLastError(),L"无法创建系统进程快照：%s");
		return FALSE;
	}

	pe32.dwSize = sizeof( PROCESSENTRY32 );

	if( !Process32First( hProcessSnap, &pe32 ) )
	{
		ErrMsg(GetLastError(),L"无法获取系统首进程快照：%s");
		CloseHandle( hProcessSnap );
		return FALSE;
	}
	dwProcessFlag=NOFLAG;
	do
	{
		TCHAR szExeFile[MAX_PATH]={0};
		_tcscpy_s(szExeFile,pe32.szExeFile);
		if(_tcsicmp(szExeFile,L"iexplore.exe")==0){dwProcessFlag|=IE;}
		//if(_tcsicmp(szExeFile,L"winword.exe")==0){dwProcessFlag|=MSWORD;}
		//if(_tcsicmp(szExeFile,L"excel.exe")==0){dwProcessFlag|=MSEXCEL;}
		//if(_tcsicmp(szExeFile,L"msexcel.exe")==0){dwProcessFlag|=MSEXCEL;}
	} while(Process32Next(hProcessSnap, &pe32));

	CloseHandle(hProcessSnap);
	return TRUE;
}

BOOL TipCloseProcess(HWND hWnd){
again:
	CheckProcesses();
	if(dwProcessFlag==NOFLAG) return TRUE;
	TCHAR szTip[STRLEN_DEFAULT]={0};
	TCHAR szDoName[STRLEN_SMALL]={0};
	_tcscpy_s(szDoName,(doType==Install?L"安装":L"更新"));
	BOOL ret=0;
	if((dwProcessFlag & IE)==IE){
		ZeroMemory(szTip,sizeof(TCHAR)*STRLEN_DEFAULT);
		_stprintf_s(szTip,L"系统检测到您的IE浏览器还在运行，为了保证系统%s正常，建议您继续后续的%s步骤之前，关闭所有正在运行的IE浏览器。\r\n单击“取消”以取消%s；\r\n在您关闭所有IE浏览器之后单击“重试”；\r\n单击“继续”以强行%s。",szDoName,szDoName,szDoName,szDoName);
		ret=MessageBox(hWnd,szTip,L"提示",MB_CANCELTRYCONTINUE|MB_ICONEXCLAMATION|MB_DEFBUTTON2);
		if(ret==IDTRYAGAIN) goto again;
	}
	if((dwProcessFlag & MSWORD)==MSWORD){
		ZeroMemory(szTip,sizeof(TCHAR)*STRLEN_DEFAULT);
		_stprintf_s(szTip,L"系统检测到您的Word程序正在运行，为了保证系统%s正常，建议您继续后续的%s步骤之前，保存并关闭所有正在运行的Word程序。\r\n单击“取消”以取消%s；\r\n在您关闭所有Word程序之后单击“重试”；\r\n单击“继续”以强行%s。",szDoName,szDoName,szDoName,szDoName);
		ret=MessageBox(hWnd,szTip,L"提示",MB_CANCELTRYCONTINUE|MB_ICONEXCLAMATION|MB_DEFBUTTON2);
		if(ret==IDTRYAGAIN) goto again;
	}
	if((dwProcessFlag & MSEXCEL)==MSEXCEL){
		ZeroMemory(szTip,sizeof(TCHAR)*STRLEN_DEFAULT);
		_stprintf_s(szTip,L"系统检测到您的Excel程序正在运行，为了保证系统%s正常，建议您继续后续的%s步骤之前，保存并关闭所有正在运行的Excel程序。\r\n单击“取消”以取消%s；\r\n在您关闭所有Excel程序之后单击“重试”；\r\n单击“继续”以强行%s。",szDoName,szDoName,szDoName,szDoName);
		ret=MessageBox(hWnd,szTip,L"提示",MB_CANCELTRYCONTINUE|MB_ICONEXCLAMATION|MB_DEFBUTTON2);
		if(ret==IDTRYAGAIN) goto again;
	}
	return ret;
}

TCHAR* ParseTrustSites(size_t* pCnt){
	*pCnt=0;
	TCHAR szInstallFolder[MAX_PATH]={0};		//安装路径
	int pathLen=GetModuleFileName(NULL,szInstallFolder,MAX_PATH);
	TCHAR* occur=_tcsrchr(szInstallFolder,L'\\');
	if(occur)	*(occur+1)=L'\0';
	_tcscat_s(szInstallFolder,L"trustsites.txt");
	if(!PathFileExists(szInstallFolder)) return NULL;

	char buffer[STRLEN_1K]={0};
  ifstream ifs(szInstallFolder);
	
	struct stat fs;
	USES_CONVERSION;
	char* fna=T2A(szInstallFolder);
	stat(fna, &fs );
	long stSize=fs.st_size;
	TCHAR* res=new TCHAR[stSize];
	ZeroMemory(res,sizeof(TCHAR)*stSize);
	size_t cnt=0;
	if(!ifs.bad()){
		size_t suml=0;
		while (!ifs.eof() ){
			ifs.getline(buffer,STRLEN_1K);
			if(buffer[0]=='#') continue;
			USES_CONVERSION;
			TCHAR* t=A2T(buffer);
			size_t l=_tcslen(t);
			if(l==0) continue;
			_tcscpy_s(res+suml,stSize-suml,t);
			suml+=(l+1);
			cnt++;
		}
	}
	*pCnt=cnt;
	return res;
}
BOOL ParseTargetSite(TCHAR* ts,size_t tsSize){
	if(!ts) return FALSE;
	ZeroMemory(ts,sizeof(TCHAR)*tsSize);
	TCHAR szInstallFolder[MAX_PATH]={0};		//安装路径
	int pathLen=GetModuleFileName(NULL,szInstallFolder,MAX_PATH);
	TCHAR* occur=_tcsrchr(szInstallFolder,L'\\');
	if(occur)	*(occur+1)=L'\0';
	_tcscat_s(szInstallFolder,L"targetsite.txt");
	if(!PathFileExists(szInstallFolder)) return FALSE;

	char buffer[STRLEN_1K]={0};
  ifstream ifs(szInstallFolder);
	
	if(!ifs.bad()){
		while (!ifs.eof() ){
			ifs.getline(buffer,STRLEN_1K);
			if(buffer[0]=='#') continue;
			USES_CONVERSION;
			TCHAR* t=A2T(buffer);
			size_t l=_tcslen(t);
			if(l==0) continue;
			_tcscpy_s(ts,tsSize,t);
		}
	}
	return _tcslen(ts);
}