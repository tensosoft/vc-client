//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by updaterapp.rc
//
#define IDC_MYICON                      2
#define IDD_UPDATERAPP_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDI_UPDATERAPP                  107
#define IDI_SMALL                       108
#define IDC_UPDATERAPP                  109
#define IDR_MAINFRAME                   128
#define IDD_INPUTURL                    129
#define IDC_URL                         1000
#define IDC_CLOSE                       1001
#define IDC_INPUTURLTIP                 1002
#define IDC_INPUTURLTITLE               1003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
