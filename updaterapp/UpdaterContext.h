#pragma once

#include "updateentry.h"

typedef std::vector<UpdateEntry*> UpdateEntryVector;
typedef std::vector<UpdateEntry*>::iterator UpdateEntryIterator;

class UpdaterContext
{
public:
	UpdaterContext(TCHAR* szUpdaterIniPath);
	~UpdaterContext(void);
	UpdateEntryVector m_updateEntries;
	//BOOL CheckUpdate(UpdateEntry* ue);	//通过指定条目与内部条目相比较后返回是否需要更新标记。
private:
	UpdaterContext(void);
	TCHAR m_szUpdaterIniPath[MAX_PATH];
};