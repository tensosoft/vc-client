#include "StdAfx.h"
#include "UpdateEntry.h"

UpdateEntry::UpdateEntry(TCHAR* szTargetPath,TCHAR* szMD5,TCHAR* szFileName,TCHAR* szLocalFileName){
	size_t len=_tcslen(szTargetPath);
	if(len>0 && len<MAX_PATH){
		ZeroMemory(this->m_szTargetPath,sizeof(TCHAR)*MAX_PATH);
		_tcscpy_s(m_szTargetPath,szTargetPath);
	}

	len=_tcslen(szMD5);
	if(len>0 && len<STRLEN_SMALL){
		ZeroMemory(this->m_szMD5,sizeof(TCHAR)*STRLEN_SMALL);
		_tcscpy_s(m_szMD5,szMD5);
	}

	len=_tcslen(szFileName);
	if(len>0 && len<MAX_PATH){
		ZeroMemory(this->m_szFileName,sizeof(TCHAR)*MAX_PATH);
		_tcscpy_s(m_szFileName,szFileName);
	}

	len=_tcslen(szLocalFileName);
	if(len>0 && len<MAX_PATH){
		ZeroMemory(this->m_szLocalFileName,sizeof(TCHAR)*MAX_PATH);
		_tcscpy_s(m_szLocalFileName,szLocalFileName);
	}
}
UINT UpdateEntry::GetTargetPath(TCHAR* szRet,size_t size){
	ZeroMemory(szRet,sizeof(TCHAR)*size);
	size_t len=_tcslen(m_szTargetPath);
	if(len<=(size-1)){
		_tcscpy_s(szRet,size,m_szTargetPath);
	}
	else if(len>(size-1)){
		_tcsncpy_s(szRet,size,m_szTargetPath,size-1);
	}
	return len;
}
UINT UpdateEntry::GetMD5(TCHAR* szRet,size_t size){
	ZeroMemory(szRet,sizeof(TCHAR)*size);
	size_t len=_tcslen(m_szMD5);
	if(len<=(size-1)){
		_tcscpy_s(szRet,size,m_szMD5);
	}
	else if(len>(size-1)){
		_tcsncpy_s(szRet,size,m_szMD5,size-1);
	}
	return len;
}
UINT UpdateEntry::GetFileName(TCHAR* szRet,size_t size){
	ZeroMemory(szRet,sizeof(TCHAR)*size);
	size_t len=_tcslen(m_szFileName);
	if(len<=(size-1)){
		_tcscpy_s(szRet,size,m_szFileName);
	}
	else if(len>(size-1)){
		_tcsncpy_s(szRet,size,m_szFileName,size-1);
	}
	return len;
}

UINT UpdateEntry::GetLocalFileName(TCHAR* szRet,size_t size){
	ZeroMemory(szRet,sizeof(TCHAR)*size);
	size_t len=_tcslen(m_szLocalFileName);
	if(len<=(size-1)){
		_tcscpy_s(szRet,size,m_szLocalFileName);
	}
	else if(len>(size-1)){
		_tcsncpy_s(szRet,size,m_szLocalFileName,size-1);
	}
	return len;
}

UpdateEntry::UpdateEntry(void)
{
}

UpdateEntry::~UpdateEntry(void)
{
}