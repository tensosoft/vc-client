// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // 从 Windows 头中排除极少使用的资料
// Windows 头文件:
#include <windows.h>

// C 运行时头文件
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <vector>
#include <fstream>
#include <iostream>
using namespace std;

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 某些 CString 构造函数将是显式的

#include "../commonfiles/discoverx2const.h"

#include <atlbase.h>
#include <atlstr.h>
#include <atlconv.h>
using namespace ATL;
#include <commctrl.h>

//更新包清单文件中，获取每个待更新文件对应的配置信息的配置项名称
#define TARGETPATH_KEY	L"targetpath"												//用于获取待更新文件的目标路径。
#define	ACTUALFILENAME_KEY	L"filename"											//用于获取待更新文件对应的实际文件名，可选，如果不提供此值，则与更新包中包含的文件名相同。
#define MD5_KEY	L"md5"																			//用于获取待更新文件内容对应的md5结果，可选

//待更新文件的目标路径的变量名。
#define TARGETPATH_INSTALLFOLDER	L"%INSTALLFOLDER%"				//表示客户端组件安装目录，如果待更新文件的目标路径不提供，则默认为此结果。
#define TARGETPATH_WORKFOLDER	L"%WORKFOLDER%"								//表示客户端工作目录