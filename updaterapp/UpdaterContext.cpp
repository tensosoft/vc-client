#include "StdAfx.h"
#include "UpdaterContext.h"
#include <shlwapi.h>

UpdaterContext::UpdaterContext(TCHAR* szUpdaterIniPath)
{
	m_updateEntries.clear();
	ZeroMemory(m_szUpdaterIniPath,sizeof(TCHAR)*MAX_PATH);
	size_t pathLen=_tcslen(szUpdaterIniPath);
	if(pathLen>0 && pathLen<MAX_PATH){
		_tcscpy_s(m_szUpdaterIniPath,szUpdaterIniPath);
	}
	if(_tcslen(m_szUpdaterIniPath)==0){
		ErrMsg(0,L"更新配置文件未指定或不合法！");
		return;
	}
	if(!PathFileExists(m_szUpdaterIniPath)){
		ErrMsg(0,L"更新配置文件不存在！");
		return ;
	}

	TCHAR szBuffer[STRLEN_8K]={0};
	ZeroMemory(szBuffer,sizeof(TCHAR)*STRLEN_8K);
	DWORD dwCopyed=GetPrivateProfileSectionNames(szBuffer,STRLEN_8K,this->m_szUpdaterIniPath);
	TCHAR szSectionName[MAX_PATH]={0};
	for(size_t i=0;i<(dwCopyed-1);i++){
		if(i==0 || (szBuffer[i]==L'\0' && i<(dwCopyed-1))){
			ZeroMemory(szSectionName,sizeof(TCHAR)*MAX_PATH);
			_tcscpy_s(szSectionName,&(szBuffer[(i==0?0:i+1)]));
			TCHAR szTargetPath[MAX_PATH]={0};
			TCHAR szLocalFileName[MAX_PATH]={0};
			TCHAR szMD5[STRLEN_SMALL]={0};

			GetPrivateProfileString(szSectionName,MD5_KEY,L"",szMD5,STRLEN_SMALL,this->m_szUpdaterIniPath);
			GetPrivateProfileString(szSectionName,TARGETPATH_KEY,TARGETPATH_INSTALLFOLDER,szTargetPath,MAX_PATH,this->m_szUpdaterIniPath);
			GetPrivateProfileString(szSectionName,ACTUALFILENAME_KEY,szSectionName,szLocalFileName,MAX_PATH,this->m_szUpdaterIniPath);

			UpdateEntry* ue=new UpdateEntry(szTargetPath,szMD5,szSectionName,szLocalFileName);
			m_updateEntries.push_back(ue);
		}
	}//for end
}

UpdaterContext::UpdaterContext(void){}

UpdaterContext::~UpdaterContext(void){
	UpdateEntryIterator itr;
	for(itr=m_updateEntries.begin();itr!=m_updateEntries.end();itr++){
		UpdateEntry* ue=(UpdateEntry*)(*itr);
		delete ue;
	}
	m_updateEntries.clear();
}

/*
BOOL UpdaterContext::CheckUpdate(UpdateEntry* ue){
	if(!ue) return FALSE;

	UpdateEntryIterator itr1;

	BOOL found=FALSE;

	TCHAR szFileName2[MAX_PATH]={0};
	TCHAR szMD52[STRLEN_SMALL]={0};
	ue->GetFileName(szFileName2,MAX_PATH);
	ue->GetMD5(szMD52,STRLEN_SMALL);
	for(itr1=m_updateEntries.begin();itr1!=m_updateEntries.end();itr1++){
		UpdateEntry* ue1=(UpdateEntry*)(*itr1);
		TCHAR szFileName1[MAX_PATH]={0};
		TCHAR szMD51[STRLEN_SMALL]={0};
		ue1->GetFileName(szFileName1,MAX_PATH);
		ue1->GetMD5(szMD51,STRLEN_SMALL);
		
		found=FALSE;
		if(_tcsicmp(szFileName1,szFileName2)==0){
			found=TRUE;
			if(_tcsicmp(szMD51,szMD52)==0) return FALSE;	//md5相同则不用更新
			else return TRUE;
		}

	}//for end
	if(!found) return TRUE;	//找不到则说明需要更新。
	return FALSE;
}
*/