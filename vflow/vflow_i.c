

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu Apr 10 11:17:40 2014
 */
/* Compiler settings for vflow.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IVisualFlow,0x11B8C2E7,0x0C44,0x4FDC,0x88,0xB5,0x56,0xA7,0x56,0x67,0x46,0xE1);


MIDL_DEFINE_GUID(IID, LIBID_vflowLib,0x17D03127,0xDCDB,0x42DA,0xA3,0x27,0x3B,0x28,0x32,0x19,0x98,0xE7);


MIDL_DEFINE_GUID(IID, DIID__IVisualFlowEvents,0x2FED2760,0x6DFA,0x4F53,0xBD,0xE8,0x2D,0xD4,0xD8,0x68,0xC9,0x4A);


MIDL_DEFINE_GUID(CLSID, CLSID_VisualFlow,0x995F19E8,0x8547,0x42CA,0xAF,0x39,0xB6,0xAF,0x27,0xE5,0x74,0x53);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



