#ifndef __VISUALFLOWRESOURCE_H__
#define __VISUALFLOWRESOURCE_H__
#pragma once

#define IDC_CANVAS 1001
#define IDC_TOOLBAR 1002

#define	IDC_CLOSE                       1100
#define	IDC_SAVE                        1101
#define	IDC_OPEN                        1102
#define	IDC_NEW                         1103
#define	IDC_DELETE                      1104
#define	IDC_EXPORT                      1105
#define	IDC_SELECT                      1106
#define	IDC_NODE												1107
#define	IDC_CONNECT    									1108
#define	IDC_PROPERTY                    1109

#define TOOLBAR_HEIGHT 22

#define IDC_WEBHOST 1200

//节点绘制相关常数。
#define DEFAULT_FONT_SIZE 12
#define DEFAULT_BGCOLOR	RGB(89,187,92)
#define DEFAULT_COLOR	RGB(255,255,255)
#define DEFAULT_BGCOLOR_HOT	RGB(12,114,217)
#define DEFAULT_COLOR_HOT	RGB(255,255,255)

#define DEFAULT_NODE_HEIGHT 24

#endif // __VISUALFLOWRESOURCE_H__