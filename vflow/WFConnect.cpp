#include "StdAfx.h"
#include "WFConnect.h"
#define _USE_MATH_DEFINES 
#include <cmath> 

WFConnect::WFConnect(void)
{
	m_src=NULL;
	m_dst=NULL;
	m_from.X=0.0F;
	m_from.Y=0.0F;
	m_to.X=0.0F;
	m_to.Y=0.0F;
	m_selected=FALSE;
	m_color=DEFAULT_BGCOLOR;
	m_pPath=NULL;
	m_pRgn=NULL;
}

BOOL WFConnect::Draw(Graphics& graphics){
	CalcMe(graphics);

	Pen pen(Color(GetRValue(this->m_color),GetGValue(this->m_color),GetBValue(this->m_color)),1.3F);
	AdjustableArrowCap arr(3.0F,3.0F,TRUE);
	pen.SetEndCap(LineCapCustom);
	pen.SetCustomEndCap(&arr);
	graphics.DrawLine(&pen,this->m_from,this->m_to);

	return TRUE;
}

BOOL WFConnect::IsInRegion(Point pt,Graphics* graphics){
	if(!this->m_pPath) return FALSE;
	return m_pPath->IsVisible(pt,graphics);
}

BOOL WFConnect::CalcMe(Graphics& graphics){
	if(this->m_selected){
		this->m_color=DEFAULT_BGCOLOR_HOT;
	}
	else{
		this->m_color=DEFAULT_BGCOLOR;
	}

	//开始节点中心位置
	float x1=m_src->m_pRC->X+m_src->m_pRC->Width/2.0F;
	float y1=m_src->m_pRC->Y+m_src->m_pRC->Height/2.0F;
	//目标节点中心位置
	float x2=m_dst->m_pRC->X+m_dst->m_pRC->Width/2.0F;
	float y2=m_dst->m_pRC->Y+m_dst->m_pRC->Height/2.0F;

	//计算连接起止点
	float theta = ::atan2f(y2 - y1, x2 - x1);
	this->m_from= GetPoint(theta, m_src->m_pRC);
	this->m_to= GetPoint(theta+3.14F, m_dst->m_pRC);

	//初始化用于判断选中状态的GraphicsPath
	SAFE_DEL_PTR(m_pPath);
	this->m_pPath=new GraphicsPath();
	this->m_pPath->AddLine(this->m_from,this->m_to);
	Pen pen(Color(255,255,255),8);
	this->m_pPath->Widen(&pen);
	this->m_pPath->CloseFigure();

	SAFE_DEL_PTR(m_pRgn);
	this->m_pRgn=new Region(this->m_pPath);
	
	return TRUE;
}

PointF WFConnect::GetPoint(float theta,RectF* r){
	float cx = r->X+r->Width/2.0F;
	float cy = r->Y+r->Height/2.0F;
	float w = r->Width/2.0F;
	float h = r->Height/2.0F;
	float d =::sqrtf(w*w+h*h);
	float x = cx + d*::cos(theta);
	float y = cy + d*::sin(theta);
	PointF p(0.0F,0.0F);
	if(x<r->X){		//OUT_LEFT
		p.X = cx - w;
		p.Y = cy - w*((y-cy)/(x-cx));
	}
	else if(x>(r->X +r->Width)){	//OUT_RIGHT
		p.X = cx + w;
		p.Y = cy + w*((y-cy)/(x-cx));
	}
	else if(y < r->Y){	//OUT_TOP
		p.X = cx - h*((x-cx)/(y-cy));
		p.Y = cy - h;
	}
	else if(y >( r->Y +r->Height)){	//OUT_BOTTOM
		p.X = cx + h*((x-cx)/(y-cy));
		p.Y = cy + h;
	}
	return p;
}

WFConnect::~WFConnect(void)
{
	SAFE_DEL_PTR(m_pRgn);
	SAFE_DEL_PTR(m_pPath);
}