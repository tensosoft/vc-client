

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu Apr 10 11:17:40 2014
 */
/* Compiler settings for vflow.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __vflow_i_h__
#define __vflow_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IVisualFlow_FWD_DEFINED__
#define __IVisualFlow_FWD_DEFINED__
typedef interface IVisualFlow IVisualFlow;
#endif 	/* __IVisualFlow_FWD_DEFINED__ */


#ifndef ___IVisualFlowEvents_FWD_DEFINED__
#define ___IVisualFlowEvents_FWD_DEFINED__
typedef interface _IVisualFlowEvents _IVisualFlowEvents;
#endif 	/* ___IVisualFlowEvents_FWD_DEFINED__ */


#ifndef __VisualFlow_FWD_DEFINED__
#define __VisualFlow_FWD_DEFINED__

#ifdef __cplusplus
typedef class VisualFlow VisualFlow;
#else
typedef struct VisualFlow VisualFlow;
#endif /* __cplusplus */

#endif 	/* __VisualFlow_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IVisualFlow_INTERFACE_DEFINED__
#define __IVisualFlow_INTERFACE_DEFINED__

/* interface IVisualFlow */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IVisualFlow;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("11B8C2E7-0C44-4FDC-88B5-56A7566746E1")
    IVisualFlow : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE AddNode( 
            /* [in] */ BSTR szUnid,
            /* [in] */ BSTR szCaption,
            /* [in] */ LONG lLeft,
            /* [in] */ LONG lTop) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE AddConnect( 
            /* [in] */ BSTR szSrcUnid,
            /* [in] */ BSTR szDstUnid) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE ClearAll( void) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE RefreshDisplay( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IVisualFlowVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IVisualFlow * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IVisualFlow * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IVisualFlow * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IVisualFlow * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IVisualFlow * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IVisualFlow * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IVisualFlow * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddNode )( 
            IVisualFlow * This,
            /* [in] */ BSTR szUnid,
            /* [in] */ BSTR szCaption,
            /* [in] */ LONG lLeft,
            /* [in] */ LONG lTop);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddConnect )( 
            IVisualFlow * This,
            /* [in] */ BSTR szSrcUnid,
            /* [in] */ BSTR szDstUnid);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearAll )( 
            IVisualFlow * This);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RefreshDisplay )( 
            IVisualFlow * This);
        
        END_INTERFACE
    } IVisualFlowVtbl;

    interface IVisualFlow
    {
        CONST_VTBL struct IVisualFlowVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IVisualFlow_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IVisualFlow_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IVisualFlow_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IVisualFlow_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IVisualFlow_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IVisualFlow_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IVisualFlow_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IVisualFlow_AddNode(This,szUnid,szCaption,lLeft,lTop)	\
    ( (This)->lpVtbl -> AddNode(This,szUnid,szCaption,lLeft,lTop) ) 

#define IVisualFlow_AddConnect(This,szSrcUnid,szDstUnid)	\
    ( (This)->lpVtbl -> AddConnect(This,szSrcUnid,szDstUnid) ) 

#define IVisualFlow_ClearAll(This)	\
    ( (This)->lpVtbl -> ClearAll(This) ) 

#define IVisualFlow_RefreshDisplay(This)	\
    ( (This)->lpVtbl -> RefreshDisplay(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IVisualFlow_INTERFACE_DEFINED__ */



#ifndef __vflowLib_LIBRARY_DEFINED__
#define __vflowLib_LIBRARY_DEFINED__

/* library vflowLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_vflowLib;

#ifndef ___IVisualFlowEvents_DISPINTERFACE_DEFINED__
#define ___IVisualFlowEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IVisualFlowEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IVisualFlowEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("2FED2760-6DFA-4F53-BDE8-2DD4D868C94A")
    _IVisualFlowEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IVisualFlowEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IVisualFlowEvents * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IVisualFlowEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IVisualFlowEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IVisualFlowEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IVisualFlowEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IVisualFlowEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IVisualFlowEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IVisualFlowEventsVtbl;

    interface _IVisualFlowEvents
    {
        CONST_VTBL struct _IVisualFlowEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IVisualFlowEvents_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _IVisualFlowEvents_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _IVisualFlowEvents_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _IVisualFlowEvents_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _IVisualFlowEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _IVisualFlowEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _IVisualFlowEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IVisualFlowEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_VisualFlow;

#ifdef __cplusplus

class DECLSPEC_UUID("995F19E8-8547-42CA-AF39-B6AF27E57453")
VisualFlow;
#endif
#endif /* __vflowLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


