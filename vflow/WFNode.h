#ifndef __WFNODE_H__
#define __WFNODE_H__

#pragma once

#include "visualflowresource.h"
#include <gdiplus.h>
using namespace Gdiplus;

#define TEXTOUT_X_OFFSET 6
#define TEXTOUT_Y_OFFSET 4

class WFNode
{
public:
	WFNode(void);
	~WFNode(void);

	DWORD m_bgColor;																						//背景色
	DWORD m_color;																							//前景色
	BOOL m_selected;																						//是否选中
	TCHAR m_unid[STRLEN_SMALL];																	//unid
	TCHAR m_caption[STRLEN_NORMAL];															//标题
	TCHAR m_fontFamily[STRLEN_SMALL];														//字体
	UINT m_fontSize;																						//字号，像素
	UINT m_x;																										//x坐标，像素
	UINT m_y;																										//y坐标，像素
	Region* m_pRgn;																							//节点对应Region
	GraphicsPath* m_pPath;																			//节点对应Path
	RectF* m_pRC;																								//节点对应Rect
	BOOL Draw(Graphics& graphics);															//绘制节点
	BOOL DrawFiction(Graphics& graphics,Color* color=NULL);			//绘制虚拟节点
	BOOL IsInRegion(Point pt,Graphics* graphics);								//位置是否在节点范围内
private:
	BOOL CalcMe(Graphics& graphics);														//计算位置、颜色、大小等，为绘制做准备
	UINT m_width;																								//宽度，像素
	UINT m_height;																							//高度，像素
};

#endif