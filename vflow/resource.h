//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by vflow.rc
//
#define IDS_PROJNAME                    100
#define IDR_VFLOW                       101
#define IDR_VISUALFLOW                  103
#define IDD_VISUALFLOW                  104
#define IDB_VISUALFLOW                  201
#define IDB_CLOSE                       202
#define IDB_SAVE                        203
#define IDB_OPEN                        204
#define IDB_NEW                         205
#define IDB_DELETE                      206
#define IDB_EXPORT                      207
#define IDB_SELECT                      208
#define IDB_NODE                        209
#define IDB_CONNECT                     210
#define IDB_PROPERTY                    211
#define IDD_DLGPROPERTY                 212
#define IDR_HTMLPROPERTY                213
#define IDR_CONTEXTMENU                 311
#define IDM_EXIT                        32772
#define IDM_DELETE                      32773
#define IDM_EXPORT                      32774
#define IDM_SAVE                        32775
#define IDM_SELECT                      32776
#define IDM_NODE                        32777
#define IDM_CONNECT                     32778
#define IDM_PROPERTY                    32779
#define IDM_ANALYZE                     32781

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        214
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
