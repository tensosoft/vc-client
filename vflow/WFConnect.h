#ifndef __WFCONNECT_H__
#define __WFCONNECT_H__

#pragma once

#include "visualflowresource.h"
#include <gdiplus.h>
using namespace Gdiplus;
#include "WFNode.h"

class WFConnect
{
public:
	WFConnect(void);
	~WFConnect(void);

	WFNode* m_src;																					//起点节点，必须
	WFNode* m_dst;																					//终点节点，必须
	PointF m_from;																					//起点位置
	PointF m_to;																						//终点位置
	BOOL m_selected;																				//是否选中，默认为FALSE
	DWORD m_color;																					//线条颜色
	GraphicsPath* m_pPath;																	//连接对应Path
	Region* m_pRgn;																					//连接对应Region
	BOOL Draw(Graphics& graphics);													//绘制连接
	BOOL IsInRegion(Point pt,Graphics* graphics);						//位置是否在连接范围内
private:
	BOOL CalcMe(Graphics& graphics);														//计算位置、颜色、大小等，为绘制做准备
	PointF GetPoint(float theta,RectF* r);
};

#endif