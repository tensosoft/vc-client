// VisualFlow.h : VisualFlow 的声明
#pragma once
#include "Resource.h"
#include "vflow_i.h"
#include "_IVisualFlowEvents_CP.h"
#include "visualflowresource.h"
#include "webhost.h"
#include "WFNode.h"
#include "WFConnect.h"
#include <Exdisp.h>
#include <gdiplus.h>
using namespace Gdiplus;

#include <vector>
using namespace std;

typedef std::vector<WFNode*> WFNodeVector;
typedef std::vector<WFNode*>::iterator WFNodeIterator;
typedef std::vector<WFConnect*> WFConnectVector;
typedef std::vector<WFConnect*>::iterator WFConnectIterator;

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif

DEFINE_GUID(SID_STopLevelBrowser,       0x4C96BE40L, 0x915C, 0x11CF, 0x99, 0xD3, 0x00, 0xAA, 0x00, 0x4A, 0xE8, 0x37);
#define SID_SWebBrowserApp    IID_IWebBrowserApp

#pragma comment(lib, "gdiplus.lib")
#pragma comment(lib, "comctl32.lib")

HHOOK g_hhk;		//删除对话框钩子句柄
HHOOK g_hhkdlg;	//属性对话框钩子句柄
HWND g_hwndDlg;	//属性对话框句柄
class CWBEventProperty:public CWBEvent{
public:
	CComPtr<IHTMLDocument2> m_spParentHtmlDoc2;
	VisualFlow* m_pVf;
	CWBEventProperty():m_spParentHtmlDoc2(NULL),m_pVf(NULL){}
	virtual void DocumentComplete(IDispatch *pDisp,VARIANT *URL);
};

// VisualFlow
class ATL_NO_VTABLE VisualFlow :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CStockPropImpl<VisualFlow, IVisualFlow>,
	public IPersistStreamInitImpl<VisualFlow>,
	public IOleControlImpl<VisualFlow>,
	public IOleObjectImpl<VisualFlow>,
	public IOleInPlaceActiveObjectImpl<VisualFlow>,
	public IViewObjectExImpl<VisualFlow>,
	public IOleInPlaceObjectWindowlessImpl<VisualFlow>,
	public ISupportErrorInfo,
	public IConnectionPointContainerImpl<VisualFlow>,
	public CProxy_IVisualFlowEvents<VisualFlow>,
	public IObjectWithSiteImpl<VisualFlow>,
	public IServiceProviderImpl<VisualFlow>,
	public IPersistStorageImpl<VisualFlow>,
	public ISpecifyPropertyPagesImpl<VisualFlow>,
	public IQuickActivateImpl<VisualFlow>,
	public IDataObjectImpl<VisualFlow>,
	public IProvideClassInfo2Impl<&CLSID_VisualFlow, &__uuidof(_IVisualFlowEvents), &LIBID_vflowLib>,
	public IPropertyNotifySinkCP<VisualFlow>,
	public IObjectSafetyImpl<VisualFlow, INTERFACESAFE_FOR_UNTRUSTED_CALLER|INTERFACESAFE_FOR_UNTRUSTED_DATA>,
	public CComCoClass<VisualFlow, &CLSID_VisualFlow>,
	public CComCompositeControl<VisualFlow>
{
public:
	VisualFlow():m_spWebBrowser2(NULL),m_spTopWebBrowser2(NULL),m_spHtmlDoc2(NULL)
	{
		m_blIsInIframe=FALSE;
		m_hToolbar=0;
		m_hCanvas=0;
		m_wpOrigCanvasProc=NULL;
		
		m_drawType=VisualFlow::dtSelect;

		m_bWindowOnly = TRUE;
		CalcExtent(m_sizeExtent);
		m_sizeNatural.cx=m_sizeExtent.cx;
		m_sizeNatural.cy=m_sizeExtent.cy;
		
		INITCOMMONCONTROLSEX ccx;
		ccx.dwSize=sizeof(ccx);
		ccx.dwICC=ICC_BAR_CLASSES;
		InitCommonControlsEx(&ccx);

		m_dwWinVersion=GetVersion();

		m_pDraggingRgn=NULL;
		m_dragging=FALSE;
		m_pDraggingNode=NULL;
		
		ZeroMemory(m_urlBase,sizeof(TCHAR)*STRLEN_1K);
		m_backendPlatform=bpJava;
		//初始化GDI+
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	}

DECLARE_OLEMISC_STATUS(OLEMISC_RECOMPOSEONRESIZE |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_INSIDEOUT |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST
)

DECLARE_REGISTRY_RESOURCEID(IDR_VISUALFLOW)


BEGIN_COM_MAP(VisualFlow)
	COM_INTERFACE_ENTRY(IVisualFlow)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IViewObjectEx)
	COM_INTERFACE_ENTRY(IViewObject2)
	COM_INTERFACE_ENTRY(IViewObject)
	COM_INTERFACE_ENTRY(IOleInPlaceObjectWindowless)
	COM_INTERFACE_ENTRY(IOleInPlaceObject)
	COM_INTERFACE_ENTRY2(IOleWindow, IOleInPlaceObjectWindowless)
	COM_INTERFACE_ENTRY(IOleInPlaceActiveObject)
	COM_INTERFACE_ENTRY(IOleControl)
	COM_INTERFACE_ENTRY(IOleObject)
	COM_INTERFACE_ENTRY(IPersistStreamInit)
	COM_INTERFACE_ENTRY2(IPersist, IPersistStreamInit)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
	COM_INTERFACE_ENTRY(ISpecifyPropertyPages)
	COM_INTERFACE_ENTRY(IQuickActivate)
	COM_INTERFACE_ENTRY(IPersistStorage)
	COM_INTERFACE_ENTRY(IDataObject)
	COM_INTERFACE_ENTRY(IProvideClassInfo)
	COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY(IObjectWithSite)
	COM_INTERFACE_ENTRY(IServiceProvider)
	COM_INTERFACE_ENTRY_IID(IID_IObjectSafety, IObjectSafety)
END_COM_MAP()

BEGIN_PROP_MAP(VisualFlow)
	PROP_DATA_ENTRY("_cx", m_sizeExtent.cx, VT_UI4)
	PROP_DATA_ENTRY("_cy", m_sizeExtent.cy, VT_UI4)
	// 示例项
	// PROP_ENTRY_TYPE("属性名", dispid, clsid, vtType)
	// PROP_PAGE(CLSID_StockColorPage)
END_PROP_MAP()

BEGIN_CONNECTION_POINT_MAP(VisualFlow)
	CONNECTION_POINT_ENTRY(IID_IPropertyNotifySink)
	CONNECTION_POINT_ENTRY(__uuidof(_IVisualFlowEvents))
END_CONNECTION_POINT_MAP()

BEGIN_MSG_MAP(VisualFlow)
	//NOTIFY_HANDLER(IDC_CANVAS,NM_RDOWN,NotifyHandler);
	COMMAND_HANDLER(IDC_CANVAS,STN_CLICKED, OnCanvasClick)
	COMMAND_HANDLER(IDC_CANVAS,STN_DBLCLK, OnCanvasDblClick)
	COMMAND_HANDLER(IDC_CLOSE		,STN_CLICKED, OnClose)
	COMMAND_HANDLER(IDM_EXIT		,STN_CLICKED, OnClose)
	COMMAND_HANDLER(IDC_SAVE    ,STN_CLICKED,OnSave)
	COMMAND_HANDLER(IDM_SAVE    ,STN_CLICKED,OnSave)
	COMMAND_HANDLER(IDM_ANALYZE ,STN_CLICKED,OnAnalyze)
	COMMAND_HANDLER(IDC_OPEN    ,STN_CLICKED,OnOpen)
	COMMAND_HANDLER(IDC_NEW     ,STN_CLICKED,OnNew) 
	COMMAND_HANDLER(IDC_DELETE  ,STN_CLICKED,OnDelete)
	COMMAND_HANDLER(IDM_DELETE  ,STN_CLICKED,OnDelete)
	COMMAND_HANDLER(IDC_EXPORT  ,STN_CLICKED,OnExport)
	COMMAND_HANDLER(IDM_EXPORT  ,STN_CLICKED,OnExport)
	COMMAND_HANDLER(IDC_NODE    ,STN_CLICKED,OnNode)
	COMMAND_HANDLER(IDM_NODE    ,STN_CLICKED,OnNode)
	COMMAND_HANDLER(IDC_CONNECT ,STN_CLICKED,OnConnect)
	COMMAND_HANDLER(IDM_CONNECT ,STN_CLICKED,OnConnect)
	COMMAND_HANDLER(IDC_SELECT  ,STN_CLICKED,OnSelect)
	COMMAND_HANDLER(IDM_SELECT  ,STN_CLICKED,OnSelect)
	COMMAND_HANDLER(IDC_PROPERTY,STN_CLICKED,OnProperty)
	COMMAND_HANDLER(IDM_PROPERTY,STN_CLICKED,OnProperty)
	MESSAGE_HANDLER(WM_SIZE,MessageHandler)
	MESSAGE_HANDLER(WM_INITDIALOG,MessageHandler)
	MESSAGE_HANDLER(WM_DRAWITEM,MessageHandler)
	MESSAGE_HANDLER(WM_NOTIFY,MessageHandler)
	MESSAGE_HANDLER(WM_DESTROY,MessageHandler)
	CHAIN_MSG_MAP(CComCompositeControl<VisualFlow>)
END_MSG_MAP()

	// 处理程序原型:
	LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);	//控件消息处理函数
	LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);									//控件默认Notify消息处理函数
	LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);	//控件默认Command消息处理函数
	
	//以下为画布的单击/双击事件处理程序。
	LRESULT OnCanvasClick(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnCanvasDblClick(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	//以下为工具栏按钮的单击事件处理程序。
	LRESULT OnClose(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnSave(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnAnalyze(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnOpen(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnNew(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);     
	LRESULT OnDelete(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);  
	LRESULT OnExport(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);  
	LRESULT OnNode(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);    
	LRESULT OnConnect(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled); 
	LRESULT OnSelect(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);  
	LRESULT OnProperty(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

BEGIN_SINK_MAP(VisualFlow)
	//请确保事件处理程序含有 __stdcall 调用约定
END_SINK_MAP()

	STDMETHOD(OnAmbientPropertyChange)(DISPID dispid)
	{
		if (dispid == DISPID_AMBIENT_BACKCOLOR)
		{
			SetBackgroundColorFromAmbient();
			FireViewChange();
		}
		return IOleControlImpl<VisualFlow>::OnAmbientPropertyChange(dispid);
	}
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
	{
		static const IID* arr[] =
		{
			&IID_IVisualFlow,
		};

		for (int i=0; i<sizeof(arr)/sizeof(arr[0]); i++)
		{
			if (InlineIsEqualGUID(*arr[i], riid))
				return S_OK;
		}
		return S_FALSE;
	}
	STDMETHOD(SetClientSite)(IOleClientSite *pClientSite) 
	{
		HRESULT hr = S_OK;
		IServiceProvider *isp, *isp2 = NULL;
		if (!pClientSite)
		{
			m_spWebBrowser2.Release();
			m_spWebBrowser2=NULL;
			return S_OK;
		}  
		else
		{
			IOleContainer* pContainer = NULL;
			
			//获取html文档对象
			pClientSite->GetContainer( &pContainer );
			if ( pContainer != NULL ){
				pContainer->QueryInterface(IID_IHTMLDocument2,reinterpret_cast<void **>(&m_spHtmlDoc2));
				pContainer->Release();
			}
			if(m_spHtmlDoc2 && _tcslen(m_urlBase)==0){
				BSTR bstrUrl=NULL;
				m_spHtmlDoc2->get_URL(&bstrUrl);
				if(bstrUrl){
					_stprintf_s(m_urlBase,_T("%s"),bstrUrl);
					if(_tcsstr(m_urlBase,L".aspx")) m_backendPlatform= bpDotNet;
					TCHAR* szTmp=_tcsrchr(m_urlBase,L'/');
					if(szTmp) (*(szTmp+1))=L'\0';
				}
			}

			hr = pClientSite->QueryInterface(IID_IServiceProvider, reinterpret_cast<void **>(&isp));
			if (FAILED(hr)) 
			{
				hr = S_OK;
				goto cleanup;
			}
			//顶层WebBrowser2对象
			hr = isp->QueryService(SID_STopLevelBrowser, IID_IServiceProvider, reinterpret_cast<void **>(&isp2));
			if (FAILED(hr))
			{
				hr = S_OK;
				goto cleanup;
			}
			hr = isp2->QueryService(SID_SWebBrowserApp, IID_IWebBrowser2, reinterpret_cast<void **>(&m_spTopWebBrowser2));
			if (FAILED(hr)) 
			{
				hr = S_OK;
				goto cleanup;
			}
			//直接WebBrowser2对象
			hr = isp->QueryService(IID_IWebBrowserApp, IID_IWebBrowser2,reinterpret_cast<void **>(&m_spWebBrowser2));
			if (FAILED(hr)) 
			{
				hr = S_OK;
				goto cleanup;
			}
			if(m_spTopWebBrowser2 && m_spWebBrowser2){
				BSTR url1=NULL;
				BSTR url2=NULL;
				if(m_spTopWebBrowser2->get_LocationURL(&url1)==S_OK && m_spWebBrowser2->get_LocationURL(&url2)==S_OK){
					if(_tcsicmp(OLE2T(url1),OLE2T(url2))!=0){
						m_blIsInIframe=TRUE;
					}
					SysFreeString(url1);
					SysFreeString(url2);
				}
			}
		cleanup:
			if(isp){ isp->Release();isp=NULL;}
			if(isp2){ isp2->Release(),isp2=NULL;}
			return IOleObject_SetClientSite(pClientSite);
		}
	}

// IViewObjectEx
	DECLARE_VIEW_STATUS(VIEWSTATUS_SOLIDBKGND | VIEWSTATUS_OPAQUE)

	enum { IDD = IDD_VISUALFLOW };
	STDMETHOD(_InternalQueryService)(REFGUID guidService, REFIID riid, void** ppvObject)
	{
		return E_NOTIMPL;
	}

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
		ClearWFConnectVector();
		ClearWFNodeVector();
		SAFE_DEL_PTR(m_pDraggingRgn);
		GdiplusShutdown(gdiplusToken);
	}
	WNDPROC m_wpOrigCanvasProc;																																		//m_hCanvas控件的原始窗口消息处理函数
	
	static LRESULT CALLBACK CanvasSubclassProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam);	//子类化的m_hCanvas控件窗口消息处理函数
	static LRESULT CALLBACK PropertyDialogProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam);	//属性对话框窗口函数
	static LRESULT CALLBACK KeyboardPropDlgProc(int code,WPARAM wParam,LPARAM lParam);						//属性对话框键盘消息钩子函数
	static LRESULT CALLBACK CBTDeleteMsgBoxProc(INT nCode, WPARAM wParam, LPARAM lParam);					//设置删除对话框的按钮标题的钩子函数
	static HRESULT Invoke(IDispatch* pDisp,TCHAR* szPropName,WORD wFlag,VARIANT *pvResult,int cArgs...);	//调用自动化属性/方法。
	static HRESULT ProcessKeystrokes(IOleObject* pObj, MSG msg);																									//处理属性对话框嵌入浏览器键盘事件

	enum DrawType {																																								//指示当前绘制类型的枚举
		dtSelect=0,
		dtNode=1,
		dtConnect=2
	};
	enum BackendPlatform{
		bpJava=0,
		bpDotNet=1
	};
	BOOL ClearWFNodeVector(){																																			//清理包含WFNode的Vector
		WFNodeIterator itr;
		for(itr=m_vWFNodes.begin();itr!=m_vWFNodes.end();itr++){
			WFNode* p=(WFNode*)(*itr);
			delete p;
		}
		m_vWFNodes.clear();
		return TRUE;
	}
	BOOL ClearWFConnectVector(){																																	//清理包含WFConnect的Vector
		WFConnectIterator itr;
		for(itr=m_vWFConnects.begin();itr!=m_vWFConnects.end();itr++){
			WFConnect* p=(WFConnect*)(*itr);
			delete p;
		}
		m_vWFConnects.clear();
		return TRUE;
	}

	HRESULT CallJs(TCHAR* js);																																		//在宿主所在浏览器窗口中执行指定js代码
	BOOL GetUNID(TCHAR* szUNID,size_t nSize);																											//获取UNID
	DrawType m_drawType;																																					//当前绘图类型
	CWBEventProperty m_WBEvent;																																		//属性窗口的嵌入浏览器事件
	DWORD m_dwWinVersion;																																					//当前操作系统版本信息
	WFNodeVector m_vWFNodes;																																			//节点
	WFConnectVector m_vWFConnects;																																//连接
	POINTS m_mouseDownPT;																																					//鼠标移动时的位置
	POINTS m_mouseMovePT;																																					//鼠标按下移动时的位置
	POINTS m_mouseUpPT;																																						//鼠标释放时的位置
	POINTS m_origMouseMovePT;																																			//鼠标按下移动的原来位置
	
	BOOL DrawToGraphics(Graphics& graphics);																											//绘制节点和连接到指定graphics
	BOOL DrawByBuffered(Graphics& graphics);																											//用双缓冲方式绘制节点和连接到指定graphics
	BOOL GetSelected(void** selected);																														//获取选中的对象：返回0表示没选中任何节点或连接，返回1表示节点选中，返回-1表示连接选中，selected返回选中的对象指针。

	Region* m_pDraggingRgn;																																				//正在拖动的节点Region
	BOOL m_dragging;																																							//是否正在拖动节点标记
	WFNode* m_pDraggingNode;																																			//被拖动的节点
private:
	CComPtr<IWebBrowser2> m_spWebBrowser2;																												//控件所在直接IWebBrowser2对象
	CComPtr<IWebBrowser2> m_spTopWebBrowser2;																											//控件所在顶层IWebBrowser2对象
	CComPtr<IHTMLDocument2> m_spHtmlDoc2;																													//控件所在IHTMLDocument2对象
	TCHAR m_urlBase[STRLEN_1K];																																		//控件宿主文档的url地址
	BackendPlatform m_backendPlatform;																														//后台平台：java/.net
	BOOL m_blIsInIframe;																																					//控件宿主是否在iframe中的标记
	GdiplusStartupInput gdiplusStartupInput;																											//gdi+启动选项。
	ULONG_PTR           gdiplusToken;																															//gdi+令牌
	HWND m_hToolbar;																																							//工具栏窗口句柄
	HWND m_hCanvas;																																								//画布窗口句柄
	
	BOOL InitToolbarButtons();																																		//初始化工具栏
	BOOL GetToolbarButtonTip(LPNMTBGETINFOTIP lptbgit);																						//获取工具栏按钮提示
	BOOL OnSize();																																								//控件改变大小时自动调整内部包含的控件的大小
	BOOL OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);												//控件创建时初始化子控件等
	BOOL OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);												//控件接收到WM_NOTIFY通知时的处理
	BOOL OnCanvasDrawItem(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);								//画布子控件接收到DrawItem通知时的处理
	BOOL OnDestory(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);											//控件销毁的处理
	BOOL SetTBBtnState();																																					//设置工具栏中节点、连接、选择三个按钮的状态。
public:
	STDMETHOD(AddNode)(BSTR szUnid,BSTR szCaption,LONG lLeft,LONG lTop);
	STDMETHOD(AddConnect)(BSTR szSrcUnid,BSTR szDstUnid);
	STDMETHOD(ClearAll)(void);
	STDMETHOD(RefreshDisplay)(void);
};

OBJECT_ENTRY_AUTO(__uuidof(VisualFlow), VisualFlow)
