#include "StdAfx.h"
#include "WFNode.h"

WFNode::WFNode(void)
{
	m_selected=FALSE;
	m_bgColor=DEFAULT_BGCOLOR;
	m_color=DEFAULT_COLOR;
	ZeroMemory(m_caption,sizeof(TCHAR)*STRLEN_NORMAL);
	ZeroMemory(m_unid,sizeof(TCHAR)*STRLEN_SMALL);
	ZeroMemory(m_fontFamily,sizeof(TCHAR)*STRLEN_SMALL);
	m_fontSize=DEFAULT_FONT_SIZE;
	m_width=0;
	m_height=DEFAULT_NODE_HEIGHT;
	m_x=0;
	m_y=0;
	m_pRC=NULL;
	m_pRgn=NULL;
	m_pPath=NULL;
}
BOOL WFNode::CalcMe(Graphics& graphics){
	if(this->m_x<0) this->m_x=0;
	if(this->m_y<0) this->m_y=0;

	if(this->m_selected){
		this->m_bgColor=DEFAULT_BGCOLOR_HOT;
		this->m_color=DEFAULT_COLOR_HOT;
	}
	else{
		this->m_bgColor=DEFAULT_BGCOLOR;
		this->m_color=DEFAULT_COLOR;
	}

	if(_tcslen(m_fontFamily)==0) _tcscpy_s(m_fontFamily,L"微软雅黑");	//L"宋体"
	FontFamily fontFamily(m_fontFamily);
	Font font(&fontFamily, this->m_fontSize, FontStyleRegular, UnitPixel);

	//计算文字宽度
	PointF origin(0.0f, 0.0f);
	RectF boundRect;
	graphics.MeasureString(this->m_caption,_tcslen(this->m_caption),&font,origin,&boundRect);

	//计算节点实际矩形位置
	SAFE_DEL_PTR(m_pRC);
	m_pRC=new RectF(boundRect.X+this->m_x,boundRect.Y+this->m_y,boundRect.Width+(TEXTOUT_X_OFFSET*2),boundRect.Height+(TEXTOUT_Y_OFFSET*2));	//节点实际矩形位置
	//RectF rc(boundRect.X+this->m_x,boundRect.Y+this->m_y,boundRect.Width+(TEXTOUT_X_OFFSET*2),this->m_height+TEXTOUT_Y_OFFSET);	//节点实际矩形位置

	//计算节点形状Path
	SAFE_DEL_PTR(m_pPath);
	m_pPath=new GraphicsPath();
	UINT d=10;	//圆角直径
	m_pPath->AddArc(m_pRC->X,m_pRC->Y, d, d, 180, 90);
	m_pPath->AddArc(m_pRC->X + m_pRC->Width - d, m_pRC->Y, d, d, 270, 90);
	m_pPath->AddArc(m_pRC->X + m_pRC->Width - d, m_pRC->Y + m_pRC->Height - d, d, d, 0, 90);
	m_pPath->AddArc(m_pRC->X,m_pRC->Y + m_pRC->Height - d, d, d, 90, 90);
  //path.AddLine(rc.X, rc.Y + rc.Height - d, rc.X, rc.Y + d/2);
	m_pPath->CloseFigure();

	//计算节点Region
	SAFE_DEL_PTR(m_pRgn);
	m_pRgn=new Region(m_pPath);

	return TRUE;
}
BOOL WFNode::Draw(Graphics& graphics){
	this->CalcMe(graphics);

	//绘制节点外形
	Pen pen(Color(GetRValue(this->m_bgColor),GetGValue(this->m_bgColor),GetBValue(this->m_bgColor)),1);
	graphics.DrawPath(&pen,m_pPath);

	//填充节点形状
	SolidBrush brushBG(Color(GetRValue(this->m_bgColor),GetGValue(this->m_bgColor),GetBValue(this->m_bgColor)));
	graphics.FillPath(&brushBG,m_pPath);

	//输出文字
	if(_tcslen(m_fontFamily)==0) _tcscpy_s(m_fontFamily,L"微软雅黑");	//L"宋体"
	FontFamily fontFamily(m_fontFamily);
	Font font(&fontFamily, this->m_fontSize, FontStyleRegular, UnitPixel);
	SolidBrush brushText(Color(GetRValue(this->m_color),GetGValue(this->m_color),GetBValue(this->m_color)));
	StringFormat sf(StringFormatFlagsNoWrap);
	graphics.DrawString(this->m_caption, -1, &font,PointF(m_pRC->X+TEXTOUT_X_OFFSET,m_pRC->Y+TEXTOUT_Y_OFFSET), &sf, &brushText);

	return TRUE;
}

BOOL WFNode::DrawFiction(Graphics& graphics,Color* color){
	this->CalcMe(graphics);
	if(this->m_pPath){
		Pen pen(color?*color:Color(GetRValue(this->m_bgColor),GetGValue(this->m_bgColor),GetBValue(this->m_bgColor)),1);
		pen.SetDashStyle(DashStyleDot);
		graphics.DrawPath(&pen,m_pPath);
	}
	return TRUE;
}

BOOL WFNode::IsInRegion(Point pt,Graphics* graphics){
	if(!m_pRgn) return FALSE;
	return m_pRgn->IsVisible(pt,graphics);
}

WFNode::~WFNode(void)
{
	SAFE_DEL_PTR(m_pRC);
	SAFE_DEL_PTR(m_pRgn);
	SAFE_DEL_PTR(m_pPath);
}
