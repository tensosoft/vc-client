// VisualFlow.cpp : VisualFlow 的实现
#include "stdafx.h"
#include "VisualFlow.h"
#include <shlguid.h>

// VisualFlow
LRESULT VisualFlow::MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled){
	switch(uMsg){
	case WM_INITDIALOG:
		OnCreate(uMsg,wParam,lParam,bHandled);
		break;
	case WM_SIZE:
		OnSize();
		break;
	case WM_DRAWITEM:
		OnCanvasDrawItem(uMsg,wParam,lParam,bHandled);
		break;
	case WM_NOTIFY:
		OnNotify(uMsg,wParam,lParam,bHandled);
		break;
	case WM_DESTROY:
		OnDestory(uMsg,wParam,lParam,bHandled);
		break;
	}
	
	return S_OK;
}
BOOL VisualFlow::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled){
	RECT rc={0,0};
	GetClientRect(&rc);
	this->m_hCanvas= CreateWindowEx(0,L"STATIC",L"",WS_CHILD| WS_VISIBLE|SS_OWNERDRAW|SS_NOTIFY,0,0,0,0,this->m_hWnd, (HMENU)IDC_CANVAS, _AtlBaseModule.GetModuleInstance(), NULL);

	m_wpOrigCanvasProc = (WNDPROC)::SetWindowLongPtr(m_hCanvas,GWLP_WNDPROC, (LONG_PTR)VisualFlow::CanvasSubclassProc);
	::SetWindowLongPtr(m_hCanvas, GWLP_USERDATA, (LONG_PTR)this);

	DWORD dwTBStyle=TBSTYLE_LIST | TBSTYLE_FLAT | TBSTYLE_TOOLTIPS | TBSTYLE_TRANSPARENT | TBSTYLE_WRAPABLE | CCS_TOP | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE|WS_EX_LEFT | WS_EX_LTRREADING | WS_EX_RIGHTSCROLLBAR|CCS_NODIVIDER |TBSTYLE_EX_DRAWDDARROWS|CCS_NOPARENTALIGN|CCS_NORESIZE|TBSTYLE_EX_MIXEDBUTTONS;//|CCS_NODIVIDER
	this->m_hToolbar=CreateWindowEx(0,TOOLBARCLASSNAME,(LPTSTR)NULL,dwTBStyle , 0, 0, 0, TOOLBAR_HEIGHT, this->m_hWnd,(HMENU)IDC_TOOLBAR,_AtlBaseModule.GetModuleInstance(), NULL);
	InitToolbarButtons();
	return TRUE;
}
BOOL VisualFlow::OnSize(){
	SIZE sz={0,0};
	AtlHiMetricToPixel(&m_sizeExtent,&sz);
	
	if(this->m_hToolbar){
		::MoveWindow(this->m_hToolbar,0,0,sz.cx,TOOLBAR_HEIGHT,TRUE);
	}
	if(this->m_hCanvas){
		RECT rc;
		::GetClientRect(m_hToolbar,&rc);
		LONG tbHeight=(rc.bottom-rc.top)+2;
		::MoveWindow(this->m_hCanvas,1,tbHeight,sz.cx-2, sz.cy-tbHeight,TRUE);
	}
	return TRUE;
}
BOOL VisualFlow::OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled){
	LPNMHDR pnmh=(LPNMHDR)lParam;
	if(IDC_TOOLBAR ==pnmh->idFrom && TBN_GETINFOTIP==pnmh->code){
		GetToolbarButtonTip((LPNMTBGETINFOTIP)lParam);
	}
	else if(IDC_TOOLBAR ==pnmh->idFrom && NM_CUSTOMDRAW==pnmh->code){	//工具栏背景
		LPNMTBCUSTOMDRAW lpNMCustomDraw = (LPNMTBCUSTOMDRAW) lParam;
		RECT rc;
		::GetClientRect(this->m_hToolbar,&rc);
		HBRUSH hBrush=CreateSolidBrush(GetSysColor(COLOR_3DFACE));//CreateSolidBrush(RGB(0xef,0xee,0xe2));
		FillRect(lpNMCustomDraw->nmcd.hdc, &rc,hBrush);
		DeleteObject(hBrush);
	}
	return TRUE;
}
BOOL VisualFlow::OnCanvasDrawItem(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled){
	LPDRAWITEMSTRUCT lpDrawItem = (LPDRAWITEMSTRUCT)lParam;
	Graphics graphics(lpDrawItem->hDC);
	DrawByBuffered(graphics);

	return TRUE;
}
BOOL VisualFlow::OnDestory(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled){
	//取消绘图控件子类化
	::SetWindowLongPtr(m_hCanvas, GWLP_WNDPROC, (LONG_PTR)m_wpOrigCanvasProc);
	return TRUE;
}
BOOL VisualFlow::InitToolbarButtons(){
	if(!this->m_hToolbar) return FALSE;
	HIMAGELIST himl; 
	HBITMAP hbmp;
	static TBBUTTON btns[12];
	static TCHAR* btnTexts[10]={L"关闭",L"保存",L"打开",L"新增",L"删除",L"导出",L"选择",L"流程环节",L"环节连接",L"属性"};

	SendMessage(this->m_hToolbar,TB_SETEXTENDEDSTYLE ,0,0x00000008);
	if ((himl = ImageList_Create(16,16,ILC_COLOR24|ILC_MASK,4,0)) != NULL){
		for (WORD idx=IDB_CLOSE;idx<=IDB_PROPERTY;idx++){
			hbmp =LoadBitmap(_AtlBaseModule.GetResourceInstance(), MAKEINTRESOURCE(idx));
			ImageList_AddMasked(himl, hbmp, RGB(255,255,255)); 
			DeleteObject(hbmp);
		}
		SendMessage(this->m_hToolbar,TB_SETIMAGELIST,0,(LPARAM)himl); 
	}

	int idx=0;
	BOOL isSep=FALSE;
	for (int i=0;i<12;i++){
		isSep=(i==6|| i==10);
		
		btns[i].fsState=TBSTATE_ENABLED;
		btns[i].dwData=0;
		if(!isSep){
			btns[i].iBitmap=idx;
			btns[i].idCommand=IDC_CLOSE+idx;
			btns[i].iString=(INT_PTR)btnTexts[idx];
			btns[i].fsStyle=TBSTYLE_AUTOSIZE|TBSTYLE_BUTTON|BTNS_SHOWTEXT;
			if(i>=7 && i<=9) btns[i].fsStyle|=BTNS_CHECKGROUP;
		}
		else btns[i].fsStyle=BTNS_SEP;

		if(!isSep) idx++;
	}

	SendMessage(this->m_hToolbar, TB_BUTTONSTRUCTSIZE, (WPARAM)sizeof(TBBUTTON), 0);
	SendMessage(this->m_hToolbar, TB_ADDBUTTONS,(WPARAM)12,(LPARAM)(LPTBBUTTON)&btns); 
	::ShowWindow(this->m_hToolbar, SW_SHOWNORMAL);
	SetTBBtnState();

	return TRUE;
}
BOOL VisualFlow::SetTBBtnState(){
	::SendMessage(this->m_hToolbar,TB_SETSTATE,IDC_OPEN, MAKELONG(TBSTATE_HIDDEN , 0));
	::SendMessage(this->m_hToolbar,TB_SETSTATE,IDC_NEW, MAKELONG(TBSTATE_HIDDEN , 0));
	switch(this->m_drawType){
		case VisualFlow::dtSelect:
			::SendMessage(this->m_hToolbar,TB_SETSTATE,IDC_SELECT, MAKELONG(TBSTATE_ENABLED|TBSTATE_CHECKED|TBSTATE_PRESSED, 0));
			::SendMessage(this->m_hToolbar,TB_SETSTATE,IDC_NODE, MAKELONG(TBSTATE_ENABLED, 0));
			::SendMessage(this->m_hToolbar,TB_SETSTATE,IDC_CONNECT, MAKELONG(TBSTATE_ENABLED, 0));
			break;
		case VisualFlow::dtNode:
			::SendMessage(this->m_hToolbar,TB_SETSTATE,IDC_NODE, MAKELONG(TBSTATE_ENABLED|TBSTATE_CHECKED|TBSTATE_PRESSED, 0));
			::SendMessage(this->m_hToolbar,TB_SETSTATE,IDC_SELECT, MAKELONG(TBSTATE_ENABLED, 0));
			::SendMessage(this->m_hToolbar,TB_SETSTATE,IDC_CONNECT, MAKELONG(TBSTATE_ENABLED, 0));
			break;
		case VisualFlow::dtConnect:
			::SendMessage(this->m_hToolbar,TB_SETSTATE,IDC_CONNECT, MAKELONG(TBSTATE_ENABLED|TBSTATE_CHECKED|TBSTATE_PRESSED, 0));
			::SendMessage(this->m_hToolbar,TB_SETSTATE,IDC_NODE, MAKELONG(TBSTATE_ENABLED, 0));
			::SendMessage(this->m_hToolbar,TB_SETSTATE,IDC_SELECT, MAKELONG(TBSTATE_ENABLED, 0));
			break;
	}
	::UpdateWindow(this->m_hToolbar);
	return TRUE;
}
BOOL VisualFlow::GetToolbarButtonTip(LPNMTBGETINFOTIP lptbgit){
	static TCHAR* btnTips[10]={L"关闭流程定义窗口",L"保存流程",L"打开流程",L"新增流程",L"删除选中对象",L"导出流程",L"对象选择工具",L"新增流程环节",L"新增流程环节连接",L"打开对象属性对话框"};
	lptbgit->pszText=btnTips[lptbgit->iItem-IDC_CLOSE];
	return TRUE;
}

BOOL VisualFlow::DrawToGraphics(Graphics& graphics){
	graphics.SetPageUnit(UnitPixel);
	graphics.SetSmoothingMode(SmoothingModeAntiAlias);

	WFNodeIterator itrwfn;
	for(itrwfn=m_vWFNodes.begin();itrwfn!=m_vWFNodes.end();itrwfn++){
		WFNode* pwfn=(WFNode*)(*itrwfn);
		pwfn->Draw(graphics);
	}

	WFConnectIterator itrwfc;
	for(itrwfc=m_vWFConnects.begin();itrwfc!=m_vWFConnects.end();itrwfc++){
		WFConnect* pwfc=(WFConnect*)(*itrwfc);
		pwfc->Draw(graphics);
	}

	return TRUE;
}
BOOL VisualFlow::DrawByBuffered(Graphics& graphics){
	RECT rect;
	::GetClientRect(m_hCanvas,&rect);
	Rect rc(rect.left,rect.top,rect.right-rect.left,rect.bottom-rect.top);

	Bitmap bmp(rc.Width,rc.Height);
	Graphics* pGraphics=Graphics::FromImage(&bmp);
	Color bgColor(255,255,255);
	pGraphics->Clear(bgColor);
	DrawToGraphics(*pGraphics);

	graphics.DrawImage(&bmp,rc.X,rc.Y,rc.Width,rc.Height);
	
	SAFE_DEL_PTR(pGraphics);

	return TRUE;
}

BOOL VisualFlow::GetUNID(TCHAR* szUNID,size_t nSize){
	ZeroMemory(szUNID,sizeof(TCHAR)*nSize);
	GUID guid;
	HRESULT hResult;
	hResult=CoCreateGuid(&guid);
	OLECHAR szBuffer[39]={0};
	if (hResult==S_OK){
		if (StringFromGUID2(guid,szBuffer,39)!=0){
			int j=0;
			for (int i=1;i<37;i++){
				if (i!=9 && i!=14 && i!=19 && i!=24) szUNID[j++]=szBuffer[i];
			}
		}
	}
	return TRUE;
}

BOOL VisualFlow::GetSelected(void** selected){
	int selectType=0;	//0表示没有选中，1表示选中的是节点，-1表示选中的是连接
	
	*selected=0;

	WFNodeIterator itrwfn;
	for(itrwfn=m_vWFNodes.begin();itrwfn!=m_vWFNodes.end();itrwfn++){
		WFNode* pwfn=(WFNode*)(*itrwfn);
		if(pwfn->m_selected) {
			selectType=1;
			*selected=(void*)pwfn;
			break;
		}
	}
	if(selectType==0){
		WFConnectIterator itrwfc;
		for(itrwfc=m_vWFConnects.begin();itrwfc!=m_vWFConnects.end();itrwfc++){
			WFConnect* pwfc=(WFConnect*)(*itrwfc);
			if(pwfc->m_selected) {
				selectType=-1;
				*selected=(void*)pwfc;
				break;
			}
		}
	}
	return selectType;
}

LRESULT VisualFlow::OnClose(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	TCHAR szJs[STRLEN_1K]={0};
	_stprintf_s(szJs,L"closeWorkflow();");
	this->CallJs(szJs);
	return S_OK;
}
LRESULT VisualFlow::OnSave(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	TCHAR szJs[STRLEN_1K]={0};
	_stprintf_s(szJs,L"saveWorkflow();");
	this->CallJs(szJs);
	return S_OK;
}
LRESULT VisualFlow::OnAnalyze(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	TCHAR szJs[STRLEN_1K]={0};
	_stprintf_s(szJs,L"analyzeWorkflow();");
	this->CallJs(szJs);
	return S_OK;
}
LRESULT VisualFlow::OnOpen(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	MessageBox(L"TODO://open");
	return S_OK;
}
LRESULT VisualFlow::OnNew(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	MessageBox(L"TODO://new");
	return S_OK;
}
LRESULT VisualFlow::OnDelete(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	int selectType=0;	//0表示没有选中，1表示选中的是节点，-1表示选中的是连接
	WFNode* pwfnSelected=NULL;		//选中的节点
	WFConnect* pwfcSelected=NULL;	//选中的连接

	void* selected=NULL;
	selectType=GetSelected(&selected);

	BOOL biDir=FALSE;		//是否双向连接
	TCHAR szMsg[STRLEN_1K]={0};
	if(selectType>0){
		if(selected) {pwfnSelected=(WFNode*)(selected);}
		if(pwfnSelected) _stprintf_s(szMsg,L"是否要删除“%s”环节？", pwfnSelected->m_caption);
		else _tcscpy_s(szMsg,L"是否要删除选中的环节？");
	}
	else if(selectType<0){
		if(selected) {pwfcSelected=(WFConnect*)(selected);}
		if(pwfcSelected && pwfcSelected->m_src && pwfcSelected->m_dst){
			WFConnectIterator itrwfc;
			for(itrwfc=m_vWFConnects.begin();itrwfc!=m_vWFConnects.end();itrwfc++){
				WFConnect* pwfc=(WFConnect*)(*itrwfc);
				if(pwfc->m_src==pwfcSelected->m_dst && pwfc->m_dst==pwfcSelected->m_src){	//有双向连接
					biDir=TRUE;
					_stprintf_s(szMsg,L"要删除从“%s”到“%s”的连接（环节出口），请单击“删除1”；\r\n要删除从“%s”到“%s”的连接（环节出口），请单击“删除2”；\r\n单击“取消”返回。", pwfcSelected->m_src->m_caption,pwfcSelected->m_dst->m_caption,pwfcSelected->m_dst->m_caption,pwfcSelected->m_src->m_caption);
					break;
				} //if end
			}//for end
			if(!biDir) _stprintf_s(szMsg,L"是否要删除从“%s”到“%s”的连接（环节出口）？", pwfcSelected->m_src->m_caption,pwfcSelected->m_dst->m_caption);
		}
		else _tcscpy_s(szMsg,L"是否要删除选中的环节连接？");
	}
	else{
		_tcscpy_s(szMsg,L"是否要删除此流程？");
	}

	if(!biDir){
		if((biDir=MessageBox(szMsg,L"删除确认",MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON1))==IDNO){
			return S_OK;
		}
	}
	else{
		g_hhk=SetWindowsHookEx(WH_CBT,&VisualFlow::CBTDeleteMsgBoxProc,0,GetCurrentThreadId());
		biDir=MessageBox(szMsg,L"删除确认",MB_YESNOCANCEL|MB_ICONINFORMATION|MB_DEFBUTTON3);
		if(biDir==IDCANCEL){
			return S_OK;
		}
	}

	switch (selectType){
	case 0:	//删除流程
		if(this->m_spWebBrowser2) {
			TCHAR szJs[STRLEN_1K]={0};
			_stprintf_s(szJs,L"deleteWorkflow();");
			this->CallJs(szJs);
			//this->m_spWebBrowser2->Quit();
		}
		break;
	case 1:	//删除节点
		{
			BOOL found=TRUE;
			while(found){
				found=FALSE;
				WFConnectIterator itrwfc;
				for(itrwfc=m_vWFConnects.begin();itrwfc!=m_vWFConnects.end();itrwfc++){
					WFConnect* pwfc=(WFConnect*)(*itrwfc);
					if(pwfc->m_src==pwfnSelected || pwfc->m_dst==pwfnSelected){
						m_vWFConnects.erase(itrwfc);
						delete pwfc;
						found=TRUE;
						break;
					} //if end
				}//for end
			}//while end
			WFNodeIterator itrwfn;
			for(itrwfn=m_vWFNodes.begin();itrwfn!=m_vWFNodes.end();itrwfn++){
				WFNode* pwfn=(WFNode*)(*itrwfn);
				if(pwfn==pwfnSelected) {
					TCHAR szJs[STRLEN_1K]={0};
					_stprintf_s(szJs,L"deleteActivity('%s');",pwfn->m_unid);
					this->CallJs(szJs);
					m_vWFNodes.erase(itrwfn);
					delete pwfn;
					break;
				}
			}
			::InvalidateRect(m_hCanvas, NULL, TRUE);
			::UpdateWindow(m_hCanvas);
		}
		break;
	case -1:	//删除连接
		{
			WFConnectIterator itrwfc;
			for(itrwfc=m_vWFConnects.begin();itrwfc!=m_vWFConnects.end();itrwfc++){
				WFConnect* pwfc=(WFConnect*)(*itrwfc);
				if(biDir==IDYES && pwfc==pwfcSelected){
					TCHAR szJs[STRLEN_1K]={0};
					_stprintf_s(szJs,L"deleteTransition('%s','%s');",pwfc->m_src->m_unid,pwfc->m_dst->m_unid);
					this->CallJs(szJs);
					m_vWFConnects.erase(itrwfc);
					delete pwfc;
					break;
				}
				else if(biDir==IDNO && pwfc->m_src==pwfcSelected->m_dst && pwfc->m_dst==pwfcSelected->m_src){
					TCHAR szJs[STRLEN_1K]={0};
					_stprintf_s(szJs,L"deleteTransition('%s','%s');",pwfc->m_src->m_unid,pwfc->m_dst->m_unid);
					this->CallJs(szJs);
					m_vWFConnects.erase(itrwfc);
					delete pwfc;
					break;
				}
			}
			::InvalidateRect(m_hCanvas, NULL, TRUE);
			::UpdateWindow(m_hCanvas);
		}
		break;
	}
	
	return S_OK;
}  
LRESULT VisualFlow::OnExport(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	TCHAR szJs[STRLEN_1K]={0};
	_stprintf_s(szJs,L"exportWorkflow();");
	this->CallJs(szJs);
	return S_OK;
}  
LRESULT VisualFlow::OnNode(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	m_drawType=VisualFlow::dtNode;
	SetTBBtnState();
	return S_OK;
}    
LRESULT VisualFlow::OnConnect(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	m_drawType=VisualFlow::dtConnect;
	SetTBBtnState();
	return S_OK;
} 
LRESULT VisualFlow::OnSelect(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	m_drawType=VisualFlow::dtSelect;
	SetTBBtnState();
	return S_OK;
}  
LRESULT VisualFlow::OnProperty(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	TCHAR js[STRLEN_SMALL]={0};
	_tcscpy_s(js,L"getWorkflowXml();");
	this->CallJs(js);

	//取对话框上级窗口句柄
	HWND hwndBrowser=NULL;
	this->m_spTopWebBrowser2->get_HWND((SHANDLE_PTR*)&hwndBrowser);
	if(!hwndBrowser){
		IServiceProvider* pServiceProvider = NULL;
		if (SUCCEEDED(this->m_spTopWebBrowser2->QueryInterface(IID_IServiceProvider, (void**)&pServiceProvider)))
		{
			IOleWindow* pWindow = NULL;
			if (SUCCEEDED(pServiceProvider->QueryService(SID_SShellBrowser,IID_IOleWindow,(void**)&pWindow)))
			{
				if (SUCCEEDED(pWindow->GetWindow(&hwndBrowser))){}
				pWindow->Release();
			}
			pServiceProvider->Release();
		} 
	}
	
	INT_PTR nResult=::DialogBoxParam(_AtlBaseModule.GetModuleInstance(),MAKEINTRESOURCE(IDD_DLGPROPERTY),(hwndBrowser?hwndBrowser:this->m_hWnd),(DLGPROC)VisualFlow::PropertyDialogProc,(LPARAM)this);
	if(LOWORD(nResult)==IDCANCEL){
		return S_OK;
	}

	this->ClearAll();
	ZeroMemory(js,sizeof(TCHAR)*STRLEN_SMALL);
	_tcscpy_s(js,L"displayWorkflow();");
	this->CallJs(js);
	return S_OK;
}

LRESULT VisualFlow::OnCanvasClick(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	//画布单击
	return S_OK;
}
LRESULT VisualFlow::OnCanvasDblClick(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	//画布双击：打开属性窗
	BOOL blHandled=FALSE;
	OnProperty(STN_CLICKED,IDC_PROPERTY,m_hToolbar,blHandled);
	return S_OK;
}
LRESULT CALLBACK VisualFlow::CanvasSubclassProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam){
	VisualFlow* me = NULL;
	me = (VisualFlow*)::GetWindowLongPtr(hWnd, GWLP_USERDATA);
	switch (uMsg)
	{
	case WM_CONTEXTMENU:	//画布右键单击（显示上下文菜单）
		{
			HMENU hMenu=LoadMenu(_AtlBaseModule.GetResourceInstance(),MAKEINTRESOURCE(IDR_CONTEXTMENU));
			if(hMenu){
				HMENU hMenuPopup = GetSubMenu(hMenu, 0); 
				if(hMenuPopup){
					MENUITEMINFO mii;
					mii.cbSize=sizeof(MENUITEMINFO);
					mii.fMask=MIIM_STATE;
					mii.fState=MFS_DEFAULT;
					SetMenuItemInfo(hMenuPopup,0,TRUE,&mii);
					
					for(UINT idx=2;idx<=4;idx++){
						MENUITEMINFO miix;
						miix.cbSize=sizeof(MENUITEMINFO);
						miix.fMask=MIIM_STATE;
						miix.fState=(((UINT)me->m_drawType)==(idx-2)?MFS_CHECKED:MFS_UNCHECKED);
						SetMenuItemInfo(hMenuPopup,idx,TRUE,&miix);
					}

					//for(UINT idx=10;idx>=9;idx--){
					//	::DeleteMenu(hMenuPopup,idx,MF_BYPOSITION);
					//}

					POINT pt = { 0,0 }; 
					pt.x = GET_X_LPARAM(lParam); 
					pt.y = GET_Y_LPARAM(lParam);
					if(pt.x==-1 && pt.y==-1) GetCursorPos(&pt);
					TrackPopupMenu(hMenuPopup, TPM_LEFTALIGN | TPM_LEFTBUTTON,pt.x, pt.y, 0,me->m_hWnd, NULL);
				}
				DestroyMenu(hMenu);
			}
		}
		break;
	case WM_SETCURSOR:
		{
			if(me->m_drawType==VisualFlow::dtNode || me->m_drawType==VisualFlow::dtConnect) SetCursor(LoadCursor(NULL,IDC_CROSS));
			else SetCursor(LoadCursor(NULL,IDC_ARROW));
			return TRUE;
		}
	case WM_RBUTTONDOWN:
	case WM_LBUTTONDOWN:
		me->m_mouseDownPT=MAKEPOINTS(lParam);
		::SetCapture(hWnd); 
		if(me->m_drawType==VisualFlow::dtSelect){
			int xPos = GET_X_LPARAM(lParam); 
			int yPos = GET_Y_LPARAM(lParam);
			Point pt(xPos,yPos);
			HDC hDC=::GetDC(hWnd);
			Graphics graphics(hDC);
			WFNodeIterator itrwfn;
			for(itrwfn=me->m_vWFNodes.begin();itrwfn!=me->m_vWFNodes.end();itrwfn++){
				WFNode* pwfn=(WFNode*)(*itrwfn);
				if(pwfn->IsInRegion(pt,&graphics)){pwfn->m_selected=TRUE;}
				else {pwfn->m_selected=FALSE;}
			}
			WFConnectIterator itrwfc;
			for(itrwfc=me->m_vWFConnects.begin();itrwfc!=me->m_vWFConnects.end();itrwfc++){
				WFConnect* pwfc=(WFConnect*)(*itrwfc);
				if(pwfc->IsInRegion(pt,&graphics)){pwfc->m_selected=TRUE;}
				else {pwfc->m_selected=FALSE;}
			}
			::ReleaseDC(hWnd,hDC);
			::InvalidateRect(hWnd, NULL, TRUE);
			::UpdateWindow(hWnd);
		}
		break;
	case WM_LBUTTONUP:
		::ReleaseCapture(); 
		me->m_mouseUpPT=MAKEPOINTS(lParam);
		if(me->m_drawType==VisualFlow::dtNode){			//绘制节点
			//设置其它节点和连接的选中状态为false
			WFNodeIterator itrwfn;
			for(itrwfn=me->m_vWFNodes.begin();itrwfn!=me->m_vWFNodes.end();itrwfn++){
				WFNode* pwfn=(WFNode*)(*itrwfn);
				pwfn->m_selected=FALSE;
			}
			WFConnectIterator itrwfc;
			for(itrwfc=me->m_vWFConnects.begin();itrwfc!=me->m_vWFConnects.end();itrwfc++){
				WFConnect* pwfc=(WFConnect*)(*itrwfc);
				pwfc->m_selected=FALSE;
			}

			WFNode* wfn=new WFNode();
			me->GetUNID(wfn->m_unid,64);
			TCHAR szCaption[STRLEN_SMALL]={0};
			_stprintf_s(szCaption,L"新增环节%d",me->m_vWFNodes.size()+1);
			_tcscpy_s(wfn->m_caption,szCaption);
			if(((DWORD)(LOBYTE(LOWORD(me->m_dwWinVersion))))<=5) _tcscpy_s(wfn->m_fontFamily,L"宋体");
			else _tcscpy_s(wfn->m_fontFamily,L"微软雅黑");
			wfn->m_x=me->m_mouseDownPT.x;
			wfn->m_y=me->m_mouseDownPT.y;
			wfn->m_selected=TRUE;
			me->m_vWFNodes.push_back(wfn);

			TCHAR szJs[STRLEN_1K]={0};
			_stprintf_s(szJs,L"addActivity('%s','%s',%d,%d);",wfn->m_unid,wfn->m_caption,wfn->m_x,wfn->m_y);
			me->CallJs(szJs);

			::InvalidateRect(hWnd, NULL, TRUE);
			::UpdateWindow(hWnd);
			break;
		}
		else if(me->m_drawType==VisualFlow::dtConnect){	//绘制连接
			WFNode* src=NULL;
			WFNode* dst=NULL;
			
			HDC hDC=::GetDC(hWnd);
			Graphics graphics(hDC);

			WFNodeIterator itrwfn;
			for(itrwfn=me->m_vWFNodes.begin();itrwfn!=me->m_vWFNodes.end();itrwfn++){
				WFNode* pwfn=(WFNode*)(*itrwfn);
				pwfn->m_selected=FALSE;
				if(pwfn->IsInRegion(Point(me->m_mouseDownPT.x,me->m_mouseDownPT.y),&graphics)) src=pwfn;
				if(pwfn->IsInRegion(Point(me->m_mouseUpPT.x,me->m_mouseUpPT.y),&graphics)) dst=pwfn;
			}

			if(src && dst && (src!=dst)){
				//判断原来是否已经有连接了
				BOOL connected=FALSE;
				WFConnectIterator itrwfc;
				for(itrwfc=me->m_vWFConnects.begin();itrwfc!=me->m_vWFConnects.end();itrwfc++){
					WFConnect* pwfc=(WFConnect*)(*itrwfc);
					if(pwfc->m_src==src && pwfc->m_dst==dst) {connected=TRUE;pwfc->m_selected=TRUE;}
					else {pwfc->m_selected=FALSE;}
				}

				//如果没有连接
				if(!connected){
					WFConnect* pwfc=new WFConnect();
					pwfc->m_src=src;
					pwfc->m_dst=dst;
					pwfc->m_selected=TRUE;
					me->m_vWFConnects.push_back(pwfc);
					WFNodeIterator itrwfn;
					for(itrwfn=me->m_vWFNodes.begin();itrwfn!=me->m_vWFNodes.end();itrwfn++){
						WFNode* pwfn=(WFNode*)(*itrwfn);
						pwfn->m_selected=FALSE;
					}

					TCHAR szJs[STRLEN_1K]={0};
					_stprintf_s(szJs,L"addTransition('%s','%s');",src->m_unid,dst->m_unid);
					me->CallJs(szJs);
				}
			}
			::ReleaseDC(hWnd,hDC);
			::InvalidateRect(hWnd, NULL, TRUE);
			::UpdateWindow(hWnd);
			break;
		}
		else if(me->m_drawType==VisualFlow::dtSelect && me->m_dragging){	//拖动节点位置释放
			if(me->m_pDraggingNode){
				TCHAR szJS[STRLEN_NORMAL]={0};
				_stprintf_s(szJS,L"setActivityPosition('%s',%d,%d);",me->m_pDraggingNode->m_unid,me->m_pDraggingNode->m_x,me->m_pDraggingNode->m_y);
				me->CallJs(szJS);
			}
		}
	case WM_RBUTTONUP:
		::ReleaseCapture(); 
		if(me->m_dragging){
			me->m_pDraggingNode=NULL;
			me->m_dragging=FALSE;
			::InvalidateRect(hWnd, NULL, TRUE);
			::UpdateWindow(hWnd);
		}
		break;
	case WM_MOUSEMOVE:
		me->m_mouseMovePT=MAKEPOINTS(lParam);
		if(me->m_drawType==VisualFlow::dtSelect && (wParam & MK_LBUTTON)==MK_LBUTTON){	//绘制拖动的虚拟节点
			HDC hDC=::GetDC(hWnd);
			Graphics graphics(hDC);
			Point pt(me->m_mouseMovePT.x,me->m_mouseMovePT.y);

			WFNode* pwfnd=NULL;
			
			WFNodeIterator itr;
			for(itr=me->m_vWFNodes.begin();itr!=me->m_vWFNodes.end();itr++){
				WFNode* pwfn=(WFNode*)(*itr);
				if(pwfn->IsInRegion(pt,&graphics)) {pwfnd=pwfn;break;}
			}

			if(pwfnd){
				me->m_pDraggingNode=pwfnd;
				if(!me->m_dragging){
					me->m_origMouseMovePT.x=pwfnd->m_x;
					me->m_origMouseMovePT.y=pwfnd->m_y;
					me->m_dragging=TRUE;
					SAFE_DEL_PTR(me->m_pDraggingRgn);
					me->m_pDraggingRgn=new Region(pwfnd->m_pPath);
				}
				
				RECT rect;
				::GetClientRect(hWnd,&rect);
				Rect rc(rect.left,rect.top,rect.right-rect.left,rect.bottom-rect.top);
				Region* pRgn=new Region(rc);
				pRgn->Exclude(me->m_pDraggingRgn);
				WFNodeIterator itrwfn;
				for(itrwfn=me->m_vWFNodes.begin();itrwfn!=me->m_vWFNodes.end();itrwfn++){
					WFNode* pwfn=(WFNode*)(*itrwfn);
					if(pwfn!=pwfnd) pRgn->Exclude(pwfn->m_pRgn);
				}
				WFConnectIterator itrwfc;
				for(itrwfc=me->m_vWFConnects.begin();itrwfc!=me->m_vWFConnects.end();itrwfc++){
					WFConnect* pwfc=(WFConnect*)(*itrwfc);
					pRgn->Exclude(pwfc->m_pRgn);
				}

				graphics.SetClip(pRgn);
				Color clr(255,255,255);
				pwfnd->DrawFiction(graphics,&clr);
				if((me->m_origMouseMovePT.x-me->m_mouseDownPT.x+me->m_mouseMovePT.x)>0){
					pwfnd->m_x=me->m_origMouseMovePT.x+(me->m_mouseMovePT.x-me->m_mouseDownPT.x);
				}
				if((me->m_origMouseMovePT.y-me->m_mouseDownPT.y+me->m_mouseMovePT.y)>0){
					pwfnd->m_y=me->m_origMouseMovePT.y+(me->m_mouseMovePT.y-me->m_mouseDownPT.y);
				}
				pwfnd->DrawFiction(graphics);

				SAFE_DEL_PTR(pRgn);
			}
			::ReleaseDC(hWnd,hDC);
		}
		else if(me->m_drawType==VisualFlow::dtConnect && (wParam & MK_LBUTTON)==MK_LBUTTON){	//绘制拖动的虚线
			HDC hDC=::GetDC(hWnd);
			Graphics graphics(hDC);
			RECT rect;
			::GetClientRect(hWnd,&rect);
			Rect rc(rect.left,rect.top,rect.right-rect.left,rect.bottom-rect.top);
			Region* pRgn=new Region(rc);
			WFNodeIterator itrwfn;
			for(itrwfn=me->m_vWFNodes.begin();itrwfn!=me->m_vWFNodes.end();itrwfn++){
				WFNode* pwfn=(WFNode*)(*itrwfn);
				pRgn->Exclude(pwfn->m_pRgn);
			}
			WFConnectIterator itrwfc;
			for(itrwfc=me->m_vWFConnects.begin();itrwfc!=me->m_vWFConnects.end();itrwfc++){
				WFConnect* pwfc=(WFConnect*)(*itrwfc);
				pRgn->Exclude(pwfc->m_pRgn);
			}
			graphics.SetClip(pRgn);
			Pen penErase(Color(255,255,255));
			penErase.SetDashStyle(DashStyleDot);
			graphics.DrawLine(&penErase,me->m_mouseDownPT.x,me->m_mouseDownPT.y,me->m_origMouseMovePT.x,me->m_origMouseMovePT.y);

			Pen pen(Color(GetRValue(DEFAULT_BGCOLOR),GetGValue(DEFAULT_BGCOLOR),GetBValue(DEFAULT_BGCOLOR)));
			pen.SetDashStyle(DashStyleDot);
			graphics.DrawLine(&pen,me->m_mouseDownPT.x,me->m_mouseDownPT.y,me->m_mouseMovePT.x,me->m_mouseMovePT.y);

			me->m_origMouseMovePT.x=me->m_mouseMovePT.x;
			me->m_origMouseMovePT.y=me->m_mouseMovePT.y;

			SAFE_DEL_PTR(pRgn);
			::ReleaseDC(hWnd,hDC);
		}
		else me->m_dragging=FALSE;
		break;
	case WM_KEYUP:
		switch (wParam){
		case VK_DELETE:
			::SendMessage(me->m_hWnd,WM_COMMAND,MAKEWPARAM(IDC_DELETE,STN_CLICKED),0);
			break;
		case 'p':
		case 'P':
			::SendMessage(me->m_hWnd,WM_COMMAND,MAKEWPARAM(IDC_PROPERTY,STN_CLICKED),0);
			break;
		case 'A':
		case 'a':
			::SendMessage(me->m_hWnd,WM_COMMAND,MAKEWPARAM(IDC_SELECT,STN_CLICKED),0);
			break;
		case 'D':
		case 'd':
			::SendMessage(me->m_hWnd,WM_COMMAND,MAKEWPARAM(IDC_NODE,STN_CLICKED),0);
			break;
		case 'C':
		case 'c':
			::SendMessage(me->m_hWnd,WM_COMMAND,MAKEWPARAM(IDC_CONNECT,STN_CLICKED),0);
			break;
		case 's':
		case 'S':
			{
			SHORT nVirtKey = GetKeyState(VK_CONTROL); 
			if (nVirtKey & 0x8000) ::SendMessage(me->m_hWnd,WM_COMMAND,MAKEWPARAM(IDC_SAVE,STN_CLICKED),0);
			}
			break;
		} 
		break;
	case WM_DESTROY:
		SAFE_DEL_PTR(me);
		break;
	}
	return CallWindowProc(me->m_wpOrigCanvasProc,hWnd, uMsg, wParam, lParam);
}

HRESULT VisualFlow::ProcessKeystrokes(IOleObject* pObj, MSG msg){
	CComPtr<IWebBrowser2> spWebBrowser2=NULL;
	if(pObj->QueryInterface(IID_IWebBrowser2,(void**)&spWebBrowser2)==S_OK){
		IOleInPlaceActiveObject* pIOIPAO=NULL;
		HRESULT hr = spWebBrowser2->QueryInterface(IID_IOleInPlaceActiveObject,(LPVOID *)&pIOIPAO);
		if (SUCCEEDED(hr)) {
			hr=pIOIPAO->TranslateAccelerator(&msg);
			pIOIPAO->Release();
			return hr;
		}
	}
	return S_FALSE;
}

LRESULT CALLBACK VisualFlow::KeyboardPropDlgProc(int code,WPARAM wParam,LPARAM lParam){
	if(code<0) return CallNextHookEx(g_hhkdlg,code,wParam,lParam);
	//本身可以处理的键忽略
	if(wParam==VK_LEFT || wParam==VK_RIGHT || wParam==VK_BACK) return CallNextHookEx(g_hhkdlg,code,wParam,lParam);

	VisualFlow* me=NULL;
	WebHostBase* pWebHostBase=NULL;
	me = (VisualFlow*)::GetWindowLongPtr(g_hwndDlg, GWLP_USERDATA);
	pWebHostBase=(WebHostBase*)::GetWindowLongPtr(g_hwndDlg, DWLP_USER);
	MSG dummy;
	dummy.hwnd = g_hwndDlg;
	dummy.message = (lParam & 0x80000000)?WM_KEYUP:WM_KEYDOWN;
	dummy.wParam = wParam;
	dummy.lParam = lParam;
	if (pWebHostBase) VisualFlow::ProcessKeystrokes(pWebHostBase->mpWebObject, dummy);

	return CallNextHookEx(g_hhkdlg,code,wParam,lParam);
}

LRESULT CALLBACK VisualFlow::PropertyDialogProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam) 
{ 
	VisualFlow* me=NULL;
	WebHostBase* pWebHostBase=NULL;
	if(uMsg!=WM_INITDIALOG) {
		me = (VisualFlow*)::GetWindowLongPtr(hWnd, GWLP_USERDATA);
		pWebHostBase=(WebHostBase*)::GetWindowLongPtr(hWnd, DWLP_USER);
	}
	switch (uMsg)
	{	
	case WM_INITDIALOG:
		{
			me=(VisualFlow*)lParam;
			if(me) ::SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)me);
			pWebHostBase=new WebHostBase();
			HWND hWebHost=CreateWindowEx(0,L"STATIC",L"",WS_CHILD| WS_VISIBLE ,0,0,710,375,hWnd, (HMENU)IDC_WEBHOST, _AtlBaseModule.GetModuleInstance(), NULL);
			pWebHostBase->hwnd=hWebHost;
			pWebHostBase->m_hParentWnd=hWnd;
			pWebHostBase->UnCreateEmbeddedWebControl();
			TCHAR targetUrl[STRLEN_1K]={0};
			//TCHAR fn[MAX_PATH]={0};
			//int fnLen=GetModuleFileName(_AtlBaseModule.m_hInst,fn,MAX_PATH);
			//_stprintf_s(targetUrl,_T("res://%s/%d"),fn,IDR_HTMLPROPERTY);
			_stprintf_s(targetUrl,_T("%swfprop.%s"),me->m_urlBase,(me->m_backendPlatform==bpJava?L"jsp":L"aspx"));
			pWebHostBase->CreateEmbeddedWebControl(targetUrl);
			::SetWindowLongPtr(hWnd, DWLP_USER, (LONG_PTR)pWebHostBase);
			pWebHostBase->Resize();
			me->m_WBEvent.host=pWebHostBase;
			me->m_WBEvent.m_spParentHtmlDoc2=me->m_spHtmlDoc2;
			me->m_WBEvent.m_pVf=me;
			pWebHostBase->EventAdivse(&me->m_WBEvent);
			g_hhkdlg=SetWindowsHookEx(WH_KEYBOARD,&VisualFlow::KeyboardPropDlgProc,0,GetCurrentThreadId());
			if(g_hhkdlg){g_hwndDlg=hWnd;}
			return TRUE;
		}
		break;
	case WM_COMMAND: 
		switch (LOWORD(wParam)) 
		{ 
		case IDOK: 
			{
				HWND hWndFocus=::GetFocus();
				if(::GetDlgItem(hWnd,IDOK)!=hWndFocus){
					::SendDlgItemMessage(hWnd,IDOK,WM_KEYDOWN,VK_RETURN,(LPARAM)0);
					return S_OK;
				}
				CComPtr<IWebBrowser2> spWebBrowser2=NULL;
				CComPtr<IHTMLDocument2> spHtmlDoc2=NULL;
				CComPtr<IHTMLDocument> spHtmlDoc=NULL;
				CComPtr<IDispatch> spScript=NULL;
				CComPtr<IHTMLDocument> spHtmlDocMe=NULL;
				CComPtr<IDispatch> spScriptMe=NULL;
				
				TCHAR* szPropName=L"transferText";
				CComVariant vaResult;
				CComVariant vaResultMe;

				//根据更改更新transferText结果
				TCHAR szJS[STRLEN_SMALL]={0};
				
				_tcscpy_s(szJS,L"getWorkflowJson();");
				BSTR bstrJS=T2BSTR(szJS);
				pWebHostBase->CallJavaScript(bstrJS);
				FREE_SYS_STR(bstrJS);

				/*****************************************************************/
				//读取transferText属性
				HRESULT hr=pWebHostBase->mpWebObject->QueryInterface(IID_IWebBrowser2,(void**)&spWebBrowser2);
				if(FAILED(hr)){
					::MessageBox(NULL,L"无法获取浏览器对象！",L"错误",MB_OK|MB_ICONERROR);
					goto clean;
				}
				hr=spWebBrowser2->get_Document((IDispatch**)&spHtmlDoc2);
				if(FAILED(hr)){
					::MessageBox(NULL,L"无法获取窗口文档信息！",L"错误",MB_OK|MB_ICONERROR);
					goto clean;
				}
				hr=spHtmlDoc2->QueryInterface(IID_IHTMLDocument,(void**)&spHtmlDocMe);
				if(FAILED(hr)){
					::MessageBox(NULL,L"无法获取文档信息！",L"错误",MB_OK|MB_ICONERROR);
					goto clean;
				}
				hr=spHtmlDocMe->get_Script(&spScriptMe);
				if(FAILED(hr)){
					::MessageBox(NULL,L"无法获取窗口信息！",L"错误",MB_OK|MB_ICONERROR);
					goto clean;
				}
				hr=VisualFlow::Invoke(spScriptMe,szPropName,DISPATCH_PROPERTYGET,&vaResultMe,0);
				if(FAILED(hr)){
					::MessageBox(NULL,L"无法获取“流程信息”属性值！",L"错误",MB_OK|MB_ICONERROR);
					goto clean;
				}
				
				/*****************************************************************/
				//设置读取到的transferText结果到上级窗口（可视化流程窗口）
				hr=me->m_WBEvent.m_spParentHtmlDoc2->QueryInterface(IID_IHTMLDocument,(void**)&spHtmlDoc);
				if(FAILED(hr)){
					::MessageBox(NULL,L"无法获取上级文档信息！",L"错误",MB_OK|MB_ICONERROR);
					goto clean;
				}
				hr=spHtmlDoc->get_Script(&spScript);
				if(FAILED(hr)){
					::MessageBox(NULL,L"无法获取上级窗口信息！",L"错误",MB_OK|MB_ICONERROR);
					goto clean;
				}
				hr=VisualFlow::Invoke(spScript,szPropName,DISPATCH_PROPERTYPUT,&vaResult,1,vaResultMe);
				if(FAILED(hr)){
					::MessageBox(NULL,L"无法设置“流程信息”属性信息！",L"错误",MB_OK|MB_ICONERROR);
					goto clean;
				}

				ZeroMemory(szJS,sizeof(TCHAR)*STRLEN_SMALL);
				_tcscpy_s(szJS,L"setResource();");
				bstrJS=T2BSTR(szJS);
				me->CallJs(bstrJS);
				FREE_SYS_STR(bstrJS);
			}
clean:
		case IDCANCEL:
			::EndDialog(hWnd, wParam); 
			return TRUE; 
		}//switch end
		break;
	case WM_DESTROY:
		UnhookWindowsHookEx(g_hhkdlg);
		g_hhkdlg=NULL;
		g_hwndDlg=NULL;
		if(pWebHostBase) {
			pWebHostBase->UnCreateEmbeddedWebControl();
			SAFE_DEL_PTR(pWebHostBase);
		}
		break;
	}//switch uMsg
	return S_OK; 
}

LRESULT CALLBACK VisualFlow::CBTDeleteMsgBoxProc(INT nCode, WPARAM wParam, LPARAM lParam){
	HWND hChildWnd;

	if (nCode == HCBT_ACTIVATE)
	{
		hChildWnd = (HWND)wParam;
		UINT result;
		if (::GetDlgItem(hChildWnd,IDYES)!=NULL)
		{
			result= ::SetDlgItemText(hChildWnd,IDYES,L"删除1");
		}
		if (::GetDlgItem(hChildWnd,IDNO)!=NULL)
		{
			result= ::SetDlgItemText(hChildWnd,IDNO,L"删除2");
		}
		
		UnhookWindowsHookEx(g_hhk);
	}
	else CallNextHookEx(g_hhk, nCode, wParam, lParam);
	return 0;
}

LRESULT VisualFlow::CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled){
	return S_OK;
}
LRESULT VisualFlow::NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled){
	return S_OK;
}

void CWBEventProperty::DocumentComplete(IDispatch *pDisp,VARIANT *URL){
	CComPtr<IHTMLDocument2> spHtmlDoc2=NULL;
	CComPtr<IHTMLDocument> spHtmlDoc=NULL;
	CComPtr<IDispatch> spScript=NULL;
	CComPtr<IHTMLDocument> spHtmlDocMe=NULL;
	CComPtr<IDispatch> spScriptMe=NULL;

	TCHAR* szPropName=L"transferText";
	CComVariant vaResult;
	CComVariant vaResultMe;
	CComPtr<IWebBrowser2> spWebBrowser2=NULL;
	
	/*****************************************************************/
	//读取上级窗口transferText属性
	HRESULT hr=this->m_spParentHtmlDoc2->QueryInterface(IID_IHTMLDocument,(void**)&spHtmlDoc);
	if(FAILED(hr)){
		MessageBox(NULL,L"无法获取上级文档信息！",L"错误",MB_OK|MB_ICONERROR);
		goto clean;
	}
	hr=spHtmlDoc->get_Script(&spScript);
	if(FAILED(hr)){
		MessageBox(NULL,L"无法获取上级窗口信息！",L"错误",MB_OK|MB_ICONERROR);
		goto clean;
	}
	hr=VisualFlow::Invoke(spScript,szPropName,DISPATCH_PROPERTYGET,&vaResult,0);
	if(FAILED(hr)){
		MessageBox(NULL,L"无法获取“流程信息”属性信息！",L"错误",MB_OK|MB_ICONERROR);
		goto clean;
	}
	
	/*****************************************************************/
	//设置读取到的transferText属性
	hr=pDisp->QueryInterface(IID_IWebBrowser2,(void**)&spWebBrowser2);
	if(FAILED(hr)){
		MessageBox(NULL,L"无法获取浏览器对象！",L"错误",MB_OK|MB_ICONERROR);
		goto clean;
	}
	hr=spWebBrowser2->get_Document((IDispatch**)&spHtmlDoc2);
	if(FAILED(hr)){
		MessageBox(NULL,L"无法获取窗口文档信息！",L"错误",MB_OK|MB_ICONERROR);
		goto clean;
	}
	hr=spHtmlDoc2->QueryInterface(IID_IHTMLDocument,(void**)&spHtmlDocMe);
	if(FAILED(hr)){
		MessageBox(NULL,L"无法获取文档信息！",L"错误",MB_OK|MB_ICONERROR);
		goto clean;
	}
	hr=spHtmlDocMe->get_Script(&spScriptMe);
	if(FAILED(hr)){
		MessageBox(NULL,L"无法获取窗口信息！",L"错误",MB_OK|MB_ICONERROR);
		goto clean;
	}
	hr=VisualFlow::Invoke(spScriptMe,szPropName,DISPATCH_PROPERTYPUT,&vaResultMe,1,vaResult);
	if(FAILED(hr)){
		MessageBox(NULL,L"无法设置“流程信息”属性值！",L"错误",MB_OK|MB_ICONERROR);
		goto clean;
	}

	TCHAR activityUnid[STRLEN_SMALL]={0};
	//设置默认配置项
	if(this->m_pVf){
		WFNodeIterator itr;
		for(itr=this->m_pVf->m_vWFNodes.begin();itr!=this->m_pVf->m_vWFNodes.end();itr++){
			WFNode* p=(WFNode*)(*itr);
			if(p->m_selected){
				_tcscpy_s(activityUnid,p->m_unid);
				break;
			}
		}
	}
	if(_tcslen(activityUnid)>0){
		TCHAR szJS1[STRLEN_NORMAL]={0};
		_stprintf_s(szJS1,L"WFPropActions.selectedActivityUnid='%s';",activityUnid);
		BSTR bstrJS=T2BSTR(szJS1);
		host->CallJavaScript(bstrJS);
		FREE_SYS_STR(bstrJS);
	}

	//ATLASSERT(&dp2==0);
	/*****************************************************************/
	//设置背景色
	DWORD clr=GetSysColor(COLOR_3DFACE);
	TCHAR szJS[STRLEN_NORMAL]={0};
	_stprintf_s(szJS,L"WFPropActions.setBG('#%02X%02X%02X');",GetRValue(clr),GetGValue(clr),GetBValue(clr));
	BSTR bstrJS=T2BSTR(szJS);
	host->CallJavaScript(bstrJS);
	FREE_SYS_STR(bstrJS);
clean:
	return;
}

STDMETHODIMP VisualFlow::AddNode(BSTR szUnid,BSTR szCaption,LONG lLeft,LONG lTop)
{
	if(!szUnid || _tcslen(szUnid)==0) return S_OK;
	if(!szCaption || _tcslen(szCaption)==0) return S_OK;
	DWORD dwMajorVersion = (DWORD)(LOBYTE(LOWORD(m_dwWinVersion)));
	WFNode* wfn=new WFNode();
	_tcscpy_s(wfn->m_unid,szUnid);
	_tcscpy_s(wfn->m_caption,szCaption);
	if(dwMajorVersion<=5) _tcscpy_s(wfn->m_fontFamily,L"宋体");
	else _tcscpy_s(wfn->m_fontFamily,L"微软雅黑");
	wfn->m_x=(UINT)lLeft;
	wfn->m_y=(UINT)lTop;
	m_vWFNodes.push_back(wfn);

	return S_OK;
}

STDMETHODIMP VisualFlow::AddConnect(BSTR szSrcUnid,BSTR szDstUnid)
{
	if(!szSrcUnid || _tcslen(szSrcUnid)==0) return S_OK;
	if(!szDstUnid || _tcslen(szDstUnid)==0) return S_OK;
	if(_tcsicmp(szSrcUnid,szDstUnid)==0) return S_OK;
	WFNodeIterator itr;
	WFNode* pSrc=NULL;
	WFNode* pDst=NULL;
	for(itr=m_vWFNodes.begin();itr!=m_vWFNodes.end();itr++){
		WFNode* p=(WFNode*)(*itr);
		if (_tcsicmp(p->m_unid,szSrcUnid)==0){
			pSrc=p;
		}
		if (_tcsicmp(p->m_unid,szDstUnid)==0){
			pDst=p;
		}
	}
	if(pSrc && pDst){
	WFConnect* wfc=new WFConnect();
		wfc->m_src=pSrc;
		wfc->m_dst=pDst;
		m_vWFConnects.push_back(wfc);
	}
	return S_OK;
}
STDMETHODIMP VisualFlow::RefreshDisplay(void){
	::InvalidateRect(this->m_hCanvas, NULL, TRUE);
	::UpdateWindow(this->m_hCanvas);
	return S_OK;
}

HRESULT VisualFlow::CallJs(TCHAR* js){
	if(!js ||_tcslen(js)==0) return S_OK;
	if(!m_spHtmlDoc2) return S_OK;
	HRESULT hr=S_OK;
	IHTMLWindow2* win=NULL;
	hr=m_spHtmlDoc2->get_parentWindow(&win);
	if (FAILED(hr)) goto end;
	BSTR bstrLanguage=SysAllocString(L"javascript");
	BSTR bstrJs=SysAllocString(js);
	VARIANT ret;
	hr=win->execScript(bstrJs,bstrLanguage,&ret);
	FREE_SYS_STR(bstrLanguage);
	FREE_SYS_STR(bstrJs);
end:
	if (win) win->Release();
	return hr;
}
HRESULT VisualFlow::Invoke(IDispatch* pDisp,TCHAR* szPropName,WORD wFlag,VARIANT *pvResult,int cArgs...){
	va_list marker;
	va_start(marker, cArgs);
	if(!pDisp || !szPropName || _tcslen(szPropName)==0) return S_OK;
	DISPPARAMS dp = { NULL, NULL, 0, 0 };
	DISPID dispidNamed = DISPID_PROPERTYPUT;
	DISPID dispID;
	HRESULT hr;
	BSTR bstrPropName=T2BSTR(szPropName);
	hr = pDisp->GetIDsOfNames(IID_NULL, &bstrPropName, 1,LOCALE_SYSTEM_DEFAULT,&dispID);
	if(FAILED(hr)) {FREE_SYS_STR(bstrPropName);return hr;}
	FREE_SYS_STR(bstrPropName);

	VARIANT *pArgs = new VARIANT[cArgs+1];
	for(int i=0; i<cArgs; i++) {
		pArgs[i] = va_arg(marker, VARIANT);
	}

	dp.cArgs = cArgs;
	dp.rgvarg = pArgs;

	if(wFlag & DISPATCH_PROPERTYPUT) {
		dp.cNamedArgs = 1;
		dp.rgdispidNamedArgs = &dispidNamed;
	}
	hr = pDisp->Invoke(dispID, IID_NULL, LOCALE_SYSTEM_DEFAULT, wFlag,&dp, pvResult, NULL, NULL);

	va_end(marker);
	delete [] pArgs;

	return S_OK;
}

HRESULT VisualFlow::ClearAll(void){
	this->ClearWFNodeVector();
	this->ClearWFConnectVector();
	return S_OK;
}