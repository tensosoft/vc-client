//所有vc模块使用的一些通用定义

#ifndef __DISCOVERX2CONST_H__
#define __DISCOVERX2CONST_H__
#pragma once

#include <windows.h>

//字符串缓冲区常数定义
#define STRLEN_SMALL	64
#define STRLEN_NORMAL	128
#define	STRLEN_DEFAULT	512	
#define	STRLEN_1K	1024
#define	STRLEN_2K	2048
#define	STRLEN_4K	4096
#define	STRLEN_8K	8192

//内存释放宏
#define FREE_SYS_STR(x) {if(x){SysFreeString(x);(x)=NULL;}}
#define SAFE_DEL_PTR(x) {if(x){ delete (x);(x)=NULL;}}
#define SAFE_DEL_ARR(x) {if(x){delete[] (x);(x)=NULL;}}
#define SAFE_RELEASE(x) {if(x){(x)->Release();(x)=NULL;}}

//消息提示对话框标题定义
#define MSGBOX_CAPTION _T("提示")
#define WARNING_MSGBOX_CAPTION _T("警告")
#define ERROR_MSGBOX_CAPTION _T("错误")

//工作目录名
#define WORK_FOLDER	_T("\\discoverx2")

//错误消息
#define ErrMsg(dwErrorCode,szTemplet) \
		BOOL hasTemplet_errmsg=TRUE;\
		if (!szTemplet || _tcslen(szTemplet)==0) hasTemplet_errmsg=FALSE;\
		if(dwErrorCode==0 && hasTemplet_errmsg){\
			MessageBox(GetForegroundWindow(),szTemplet,ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);\
		}\
		else{\
		TCHAR* szPos_errmsg=NULL;\
		if (hasTemplet_errmsg) szPos_errmsg=_tcsstr((TCHAR*)szTemplet,_T("%s"));\
		LPVOID lpMsgBuf_errmsg;\
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |FORMAT_MESSAGE_FROM_SYSTEM,NULL,dwErrorCode,MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),(LPTSTR) &lpMsgBuf_errmsg,0, NULL );\
		TCHAR szTip_errmsg[STRLEN_DEFAULT]={0};\
		_stprintf_s(szTip_errmsg,(szPos_errmsg?szTemplet:_T("%s")),(lpMsgBuf_errmsg==NULL?_T("未知错误"):lpMsgBuf_errmsg));\
		LocalFree(lpMsgBuf_errmsg);\
		MessageBox(GetForegroundWindow(),szTip_errmsg,ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);}

//提示消息
#define TipMsg(szTip) MessageBox(GetForegroundWindow(),szTip,MSGBOX_CAPTION,MB_OK|MB_ICONINFORMATION)

//警告消息
#define WarnMsg(szTip) MessageBox(GetForegroundWindow(),szTip,WARNING_MSGBOX_CAPTION,MB_OK|MB_ICONWARNING)

#endif	//__DISCOVERX2CONST_H__