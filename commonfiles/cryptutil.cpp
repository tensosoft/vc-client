//加解密文本的实现
#include "stdafx.h"
#include "cryptutil.h"
#include <Wincrypt.h>
BOOL Encrypt(const TCHAR* szRaw,TCHAR* szOut,size_t* iOutSize,TCHAR* szKey){
	BOOL ret=FALSE;
	if (szRaw==NULL || _tcslen(szRaw)==0 || szOut==NULL || *iOutSize<=0) return FALSE;
	char* rawBytes=NULL;
	char* utf8Key=NULL;
	DWORD rawByteslen=FromUTF16(szRaw,NULL,0);
	if (rawByteslen<=0) return FALSE;
	rawBytes=new char[rawByteslen];
	FromUTF16(szRaw,rawBytes,rawByteslen);
	rawByteslen=strlen(rawBytes);
	DWORD dwLen=rawByteslen;
	BOOL fResult = FALSE;
  HCRYPTPROV hProv = NULL;
  HCRYPTKEY hSessionKey = NULL;
  BYTE* pbBuffer=NULL;

	fResult = CryptAcquireContext(&hProv, NULL,MS_DEF_PROV,PROV_RSA_FULL,CRYPT_VERIFYCONTEXT);
  if (fResult)
  {
		int keyLen=FromUTF16(szKey,NULL,0);
		if (keyLen>0) keyLen+=1;
		utf8Key=new char[keyLen];
		FromUTF16(szKey,utf8Key,keyLen);
		struct {
			BLOBHEADER header;
			DWORD cbKeySize;
			BYTE rgbKeyData [8];
		} keyBlob;
		keyBlob.header.bType = PLAINTEXTKEYBLOB;
		keyBlob.header.bVersion = CUR_BLOB_VERSION;
		keyBlob.header.reserved = 0;
		keyBlob.header.aiKeyAlg = CALG_DES;
		keyBlob.cbKeySize = 8;
		memcpy(keyBlob.rgbKeyData, utf8Key, 8);
		fResult=CryptImportKey(hProv, (BYTE*)&keyBlob, sizeof(keyBlob), 0, 0, &hSessionKey);
		if (fResult){
			fResult=CryptSetKeyParam(hSessionKey,KP_IV,KEY_IV,0);
			//if(fResult){
			//	BYTE kpmode=CRYPT_MODE_CFB;
			//	fResult=CryptSetKeyParam(hSessionKey,KP_MODE,&kpmode ,0);
			//}
			if(fResult) fResult = CryptEncrypt(hSessionKey, 0, TRUE, 0, NULL,&dwLen,rawByteslen);
			if (fResult && dwLen>0){
				dwLen+=1;
				pbBuffer=new BYTE[dwLen];
				ZeroMemory(pbBuffer,dwLen);
				memcpy(pbBuffer,rawBytes,rawByteslen);
				DWORD encryptedBytesLen=rawByteslen;
				fResult = CryptEncrypt(hSessionKey, NULL, TRUE, 0, pbBuffer,&encryptedBytesLen,dwLen);
				if (fResult){
					//TCHAR sz[100]={0};
					//_stprintf_s(sz,L"rawByteslen=%d,encryptedBytesLen=%d,iOutSize=%d",rawByteslen,encryptedBytesLen,*iOutSize);
					//TipMsg(sz);
					ToHexString((const unsigned char*)pbBuffer,encryptedBytesLen,szOut,iOutSize);
					ret=TRUE;
				}//if end
				else{
					ErrMsg(GetLastError(),L"发生错误！");
					ret=FALSE;
				}
			}//if end
		}//if end
  }//if end
	SAFE_DEL_ARR(rawBytes);
	SAFE_DEL_ARR(utf8Key);
	SAFE_DEL_ARR(pbBuffer);
	if (hSessionKey != NULL) CryptDestroyKey(hSessionKey);
	if (hProv != NULL) CryptReleaseContext(hProv, 0);
	return ret;
}
BOOL Decrypt(TCHAR* szRaw,TCHAR* szOut,size_t* iOutSize,TCHAR* szKey){
	USES_CONVERSION;
	BOOL ret=FALSE;
	char *utf8Key=NULL;
	if (szRaw==NULL || _tcslen(szRaw)==0 || szOut==NULL || *iOutSize<=0) return FALSE;
	DWORD bytesLen=_tcslen(szRaw)/2;
	BYTE* bBuffer=new BYTE[bytesLen];
	ZeroMemory(bBuffer,bytesLen);
	if(!FromHexString(szRaw,bBuffer,(size_t*)&bytesLen)) goto end;
	BOOL fResult = FALSE;
  HCRYPTPROV hProv = NULL;
  HCRYPTKEY hSessionKey = NULL;
	fResult = CryptAcquireContext(&hProv, NULL,MS_DEF_PROV,PROV_RSA_FULL,CRYPT_VERIFYCONTEXT);
  if (fResult)
  {
		int keyLen=FromUTF16(szKey,NULL,0);
		if (keyLen>0) keyLen+=1;
		utf8Key=new char[keyLen];
		FromUTF16(szKey,utf8Key,keyLen);
		struct {
			BLOBHEADER header;
			DWORD cbKeySize;
			BYTE rgbKeyData [8];
		} keyBlob;
		keyBlob.header.bType = PLAINTEXTKEYBLOB;
		keyBlob.header.bVersion = CUR_BLOB_VERSION;
		keyBlob.header.reserved = 0;
		keyBlob.header.aiKeyAlg = CALG_DES;
		keyBlob.cbKeySize = 8;
		memcpy(keyBlob.rgbKeyData, utf8Key, 8);
		fResult=CryptImportKey(hProv, (BYTE*)&keyBlob, sizeof(keyBlob), 0, 0, &hSessionKey);
		if (fResult){
			fResult=CryptSetKeyParam(hSessionKey,KP_IV,KEY_IV,0);
			//if(fResult){
			//	BYTE kpmode=CRYPT_MODE_CFB;
			//	fResult=CryptSetKeyParam(hSessionKey,KP_MODE,&kpmode ,0);
			//}
			if (fResult){
				fResult = CryptDecrypt(hSessionKey, NULL, TRUE, 0, bBuffer,&bytesLen);
				if (fResult && bytesLen>0){
					char* szBuffer=new char[bytesLen+1];
					ZeroMemory(szBuffer,bytesLen+1);
					memcpy(szBuffer,bBuffer,bytesLen);
					BOOL utf16Size=ToUTF16(szBuffer,NULL,0);
					wchar_t * utf16=new wchar_t[utf16Size];
					ToUTF16(szBuffer,utf16,utf16Size);
					_tcscpy_s(szOut,*iOutSize,W2T(utf16));
					SAFE_DEL_ARR(utf16);
					SAFE_DEL_ARR(szBuffer);
					ret=TRUE;
				}//if end
			}//if end
		}//if end
  }//if end
end:
	SAFE_DEL_ARR(utf8Key);
	SAFE_DEL_ARR(bBuffer);
	if (hSessionKey != NULL) CryptDestroyKey(hSessionKey);
	if (hProv != NULL) CryptReleaseContext(hProv, 0);
	return ret;
}

BOOL FromUTF16( IN const wchar_t * utf16,char* dst,size_t dstSize){
	if (utf16 == NULL || *utf16 == L'\0') return FALSE;
	const int utf16Length = wcslen( utf16 ) + 1;
	//先计算转换后大小
	int utf8Size = ::WideCharToMultiByte(
		CP_UTF8,			//转 UTF-8
		0,						//缺省标记
		utf16,				//数据源，其格式为UTF-16（windows默认unicode）编码
		utf16Length,	//数据源包含字符长度（包括结尾的\0部分）
		NULL,					//不使用，只计算长度
		0,						//不使用
		NULL, NULL		//不使用
		);
	
	if (utf8Size==0) return FALSE;
	if (dstSize==0) return utf8Size;
	ZeroMemory(dst,dstSize);
	int result = ::WideCharToMultiByte(
		CP_UTF8,			//转 UTF-8
		0,						//缺省标记
		utf16,				//数据源，其格式为UTF-16（windows默认unicode）编码
		utf16Length,	//数据源包含字符长度（包括结尾的\0部分）
		dst,					//转换结果
		dstSize>=utf8Size?utf8Size:dstSize,			//结果长度
		NULL, NULL		//不使用
		);
	if ( result == 0)	return FALSE;
	return utf8Size;
}
BOOL ToUTF16(IN const char * utf8 ,wchar_t * dst,size_t dstSize){
	if (utf8 == NULL || *utf8 == L'\0') return FALSE;
	const int utf8ByteCount = strlen( utf8 ) + 1;

	//先计算转换后大小
	int utf16Size = ::MultiByteToWideChar(
		CP_UTF8,
		MB_ERR_INVALID_CHARS,
		utf8,
		utf8ByteCount,
		NULL,
		0
		);
	if ( utf16Size == 0 )return FALSE;
	if (dstSize==0) return utf16Size;
	ZeroMemory(dst,dstSize);
	int result = ::MultiByteToWideChar(
		CP_UTF8,
		MB_ERR_INVALID_CHARS,
		utf8,
		utf8ByteCount,
		dst,
		dstSize>=utf16Size?utf16Size:dstSize
		);

	if ( result == 0 )return FALSE;
	return utf16Size;
}

BOOL ToHexString(const unsigned char * bytes,size_t bytesSize,TCHAR* szResult,size_t* resultSize){
	if (!bytes) return FALSE;
	if((*resultSize)<(bytesSize*2+1)) {
		*resultSize=(bytesSize*2+1);
		return FALSE;
	}
	if(szResult){
		SecureZeroMemory(szResult,sizeof(TCHAR)*(*resultSize));
		unsigned char b=0;
		for (int i=0;i<bytesSize;i++) {
			b=bytes[i];
			szResult[i*2]=HEXES[(b & 0xF0) >> 4];
			szResult[i*2+1]=HEXES[(b & 0x0F)];
		}
	}
	*resultSize=(bytesSize*2+1);
	return TRUE;
}
BOOL FromHexString(const TCHAR* szHexString,unsigned char * bytes,size_t* bytesSize){
	if(!szHexString) return FALSE;
	size_t hexStringLen=_tcslen(szHexString);
	if(hexStringLen==0) return FALSE;
	if((hexStringLen % 2)!=0) return FALSE;
	if((*bytesSize)<(hexStringLen/2)) {
		*bytesSize=hexStringLen/2;
		return FALSE;
	}
	if(bytes){
		SecureZeroMemory(bytes,sizeof(unsigned char)*(*bytesSize));
		for(int i=0;i<hexStringLen;i+=2){
			TCHAR ch1=szHexString[i];
			TCHAR ch2=szHexString[i+1];
			int dig1, dig2;
			if(isdigit(ch1)) dig1 = ch1 - L'0';
			else if(ch1>=L'A' && ch1<=L'F') dig1 = ch1 - L'A' + 10;
			else if(ch1>=L'a' && ch1<=L'f') dig1 = ch1 - L'a' + 10;
			if(isdigit(ch2)) dig2 = ch2 - L'0';
			else if(ch2>=L'A' && ch2<=L'F') dig2 = ch2 -L'A' + 10;
			else if(ch2>=L'a' && ch2<=L'f') dig2 = ch2 - L'a' + 10;
			bytes[i / 2] = (unsigned char)(dig1*16 + dig2);
		}
	}
	*bytesSize=hexStringLen/2;
	return TRUE;
}