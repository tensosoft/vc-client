#ifndef __CONVUTIL_H__
#define __CONVUTIL_H__
#pragma once
namespace convutil {
	//Optimized Function to convert an unsigned char to a Hex string of length 2
	void Char2Hex(unsigned char ch, char* szHex);
	//Function to convert a Hex string of length 2 to an unsigned char
	bool Hex2Char(char const* szHex, unsigned char& rch);

	//Function to convert binary string to hex string
	void Binary2Hex(unsigned char const* pucBinStr, int iBinSize, char* pszHexStr);

	//Function to convert hex string to binary string
	bool Hex2Binary(char const* pszHexStr, unsigned char* pucBinStr, int iBinSize);
}
#endif // __UTILITIES_H__