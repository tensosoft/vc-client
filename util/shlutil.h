//shlobj.h有关的函数调用的封装，因为shlobj.h不能直接被组件类包含（会导致Shell的uuid定义重复）。

#ifndef __SHLUTIL_H__
#define __SHLUTIL_H__
#pragma once

#define CSIDL_PERSONAL                  0x0005
#define CSIDL_MYPICTURES                0x0027
#define CSIDL_FLAG_CREATE               0x8000
#define CSIDL_APPDATA                   0x001a
#define CSIDL_LOCAL_APPDATA             0x001c
HRESULT(shlutil_SelectFolder)(BSTR* szFolderResult);
HRESULT(shlutil_GetFolderPath)(TCHAR* szFolderResult, DWORD dwFlags);

#endif	//__SHLUTIL_H__
