

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu Apr 10 11:17:26 2014
 */
/* Compiler settings for util.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __util_i_h__
#define __util_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IUNIDProvider_FWD_DEFINED__
#define __IUNIDProvider_FWD_DEFINED__
typedef interface IUNIDProvider IUNIDProvider;
#endif 	/* __IUNIDProvider_FWD_DEFINED__ */


#ifndef __IShell_FWD_DEFINED__
#define __IShell_FWD_DEFINED__
typedef interface IShell IShell;
#endif 	/* __IShell_FWD_DEFINED__ */


#ifndef __ICommonDialogs_FWD_DEFINED__
#define __ICommonDialogs_FWD_DEFINED__
typedef interface ICommonDialogs ICommonDialogs;
#endif 	/* __ICommonDialogs_FWD_DEFINED__ */


#ifndef __ITextFileWriter_FWD_DEFINED__
#define __ITextFileWriter_FWD_DEFINED__
typedef interface ITextFileWriter ITextFileWriter;
#endif 	/* __ITextFileWriter_FWD_DEFINED__ */


#ifndef __ICrypto_FWD_DEFINED__
#define __ICrypto_FWD_DEFINED__
typedef interface ICrypto ICrypto;
#endif 	/* __ICrypto_FWD_DEFINED__ */


#ifndef __IMSOfficeUtil_FWD_DEFINED__
#define __IMSOfficeUtil_FWD_DEFINED__
typedef interface IMSOfficeUtil IMSOfficeUtil;
#endif 	/* __IMSOfficeUtil_FWD_DEFINED__ */


#ifndef __IBrowserUtil_FWD_DEFINED__
#define __IBrowserUtil_FWD_DEFINED__
typedef interface IBrowserUtil IBrowserUtil;
#endif 	/* __IBrowserUtil_FWD_DEFINED__ */


#ifndef __UNIDProvider_FWD_DEFINED__
#define __UNIDProvider_FWD_DEFINED__

#ifdef __cplusplus
typedef class UNIDProvider UNIDProvider;
#else
typedef struct UNIDProvider UNIDProvider;
#endif /* __cplusplus */

#endif 	/* __UNIDProvider_FWD_DEFINED__ */


#ifndef __Shell_FWD_DEFINED__
#define __Shell_FWD_DEFINED__

#ifdef __cplusplus
typedef class Shell Shell;
#else
typedef struct Shell Shell;
#endif /* __cplusplus */

#endif 	/* __Shell_FWD_DEFINED__ */


#ifndef __CommonDialogs_FWD_DEFINED__
#define __CommonDialogs_FWD_DEFINED__

#ifdef __cplusplus
typedef class CommonDialogs CommonDialogs;
#else
typedef struct CommonDialogs CommonDialogs;
#endif /* __cplusplus */

#endif 	/* __CommonDialogs_FWD_DEFINED__ */


#ifndef __TextFileWriter_FWD_DEFINED__
#define __TextFileWriter_FWD_DEFINED__

#ifdef __cplusplus
typedef class TextFileWriter TextFileWriter;
#else
typedef struct TextFileWriter TextFileWriter;
#endif /* __cplusplus */

#endif 	/* __TextFileWriter_FWD_DEFINED__ */


#ifndef __Crypto_FWD_DEFINED__
#define __Crypto_FWD_DEFINED__

#ifdef __cplusplus
typedef class Crypto Crypto;
#else
typedef struct Crypto Crypto;
#endif /* __cplusplus */

#endif 	/* __Crypto_FWD_DEFINED__ */


#ifndef __MSOfficeUtil_FWD_DEFINED__
#define __MSOfficeUtil_FWD_DEFINED__

#ifdef __cplusplus
typedef class MSOfficeUtil MSOfficeUtil;
#else
typedef struct MSOfficeUtil MSOfficeUtil;
#endif /* __cplusplus */

#endif 	/* __MSOfficeUtil_FWD_DEFINED__ */


#ifndef __BrowserUtil_FWD_DEFINED__
#define __BrowserUtil_FWD_DEFINED__

#ifdef __cplusplus
typedef class BrowserUtil BrowserUtil;
#else
typedef struct BrowserUtil BrowserUtil;
#endif /* __cplusplus */

#endif 	/* __BrowserUtil_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IUNIDProvider_INTERFACE_DEFINED__
#define __IUNIDProvider_INTERFACE_DEFINED__

/* interface IUNIDProvider */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IUNIDProvider;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A5C80CDD-7DF9-4A7C-BA06-1CBE2DE38C5B")
    IUNIDProvider : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetGUID( 
            /* [retval][out] */ BSTR *szGUID) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetUNID( 
            /* [retval][out] */ BSTR *szUNID) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IUNIDProviderVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IUNIDProvider * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IUNIDProvider * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IUNIDProvider * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IUNIDProvider * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IUNIDProvider * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IUNIDProvider * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IUNIDProvider * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetGUID )( 
            IUNIDProvider * This,
            /* [retval][out] */ BSTR *szGUID);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetUNID )( 
            IUNIDProvider * This,
            /* [retval][out] */ BSTR *szUNID);
        
        END_INTERFACE
    } IUNIDProviderVtbl;

    interface IUNIDProvider
    {
        CONST_VTBL struct IUNIDProviderVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IUNIDProvider_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IUNIDProvider_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IUNIDProvider_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IUNIDProvider_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IUNIDProvider_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IUNIDProvider_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IUNIDProvider_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IUNIDProvider_GetGUID(This,szGUID)	\
    ( (This)->lpVtbl -> GetGUID(This,szGUID) ) 

#define IUNIDProvider_GetUNID(This,szUNID)	\
    ( (This)->lpVtbl -> GetUNID(This,szUNID) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IUNIDProvider_INTERFACE_DEFINED__ */


#ifndef __IShell_INTERFACE_DEFINED__
#define __IShell_INTERFACE_DEFINED__

/* interface IShell */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IShell;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A23EBD08-4FDF-4146-9D44-9FBCAB5551E0")
    IShell : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetSystemFolder( 
            /* [retval][out] */ BSTR *szFolder) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetWorkFolder( 
            /* [defaultvalue][in] */ BSTR szSubFolder,
            /* [retval][out] */ BSTR *szFolder) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetWorkFolderTemp( 
            /* [retval][out] */ BSTR *szFolder) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetWindowsVersionString( 
            /* [retval][out] */ BSTR *szVer) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFileVersionString( 
            /* [in] */ BSTR szFile,
            /* [retval][out] */ BSTR *szVer) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetWindowsUserName( 
            /* [retval][out] */ BSTR *szUser) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPersonalFolder( 
            /* [retval][out] */ BSTR *szFolder) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFileLength( 
            /* [in] */ BSTR szFilePath,
            /* [retval][out] */ ULONG *ulFileLength) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetInstallPath( 
            /* [retval][out] */ BSTR *szPath) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE ClearTempWorkFolder( void) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteTempFile( 
            /* [in] */ BSTR szFileName,
            /* [retval][out] */ VARIANT_BOOL *bSuccess) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE FileExists( 
            /* [in] */ BSTR szFilePath,
            /* [retval][out] */ VARIANT_BOOL *bSuccess) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE LaunchFile( 
            /* [in] */ BSTR szFilePath,
            /* [defaultvalue][in] */ BSTR szVerb,
            /* [defaultvalue][in] */ VARIANT_BOOL blWait,
            /* [retval][out] */ VARIANT_BOOL *bSuccess) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE FindAssocProgram( 
            /* [in] */ BSTR szFilePath,
            /* [defaultvalue][in] */ BSTR szVerb,
            /* [retval][out] */ BSTR *szProgram) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE CheckFileOpened( 
            /* [in] */ BSTR szFilePath,
            /* [retval][out] */ VARIANT_BOOL *bSuccess) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetAppDataFolder( 
            /* [retval][out] */ BSTR *szFolder) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetLocalAppDataFolder( 
            /* [retval][out] */ BSTR *szFolder) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetScreenWidth( 
            /* [retval][out] */ INT *res) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetScreenHeight( 
            /* [retval][out] */ INT *res) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFileIcon( 
            /* [in] */ BSTR szFile,
            /* [retval][out] */ BSTR *szIconFilePath) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFileMD5( 
            /* [in] */ BSTR szFilePath,
            /* [retval][out] */ BSTR *szMD5Hash) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE AddEventLog( 
            /* [in] */ BSTR szMsg,
            /* [defaultvalue][in] */ BSTR szSource = L"腾硕协作应用构建平台",
            /* [defaultvalue][in] */ INT iType = 0) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE EnumFilesSize( 
            /* [in] */ BSTR szDir,
            /* [retval][out] */ ULONG *size) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE EnumFilesCount( 
            /* [in] */ BSTR szDir,
            /* [retval][out] */ ULONG *count) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE EnumFiles( 
            /* [in] */ BSTR szDir,
            /* [retval][out] */ BSTR *szFiles) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetLocalIPAddress( 
            /* [retval][out] */ BSTR *ret) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE ReadText( 
            /* [in] */ BSTR szText) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IShellVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IShell * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IShell * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IShell * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IShell * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IShell * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IShell * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IShell * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetSystemFolder )( 
            IShell * This,
            /* [retval][out] */ BSTR *szFolder);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetWorkFolder )( 
            IShell * This,
            /* [defaultvalue][in] */ BSTR szSubFolder,
            /* [retval][out] */ BSTR *szFolder);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetWorkFolderTemp )( 
            IShell * This,
            /* [retval][out] */ BSTR *szFolder);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetWindowsVersionString )( 
            IShell * This,
            /* [retval][out] */ BSTR *szVer);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFileVersionString )( 
            IShell * This,
            /* [in] */ BSTR szFile,
            /* [retval][out] */ BSTR *szVer);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetWindowsUserName )( 
            IShell * This,
            /* [retval][out] */ BSTR *szUser);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPersonalFolder )( 
            IShell * This,
            /* [retval][out] */ BSTR *szFolder);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFileLength )( 
            IShell * This,
            /* [in] */ BSTR szFilePath,
            /* [retval][out] */ ULONG *ulFileLength);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetInstallPath )( 
            IShell * This,
            /* [retval][out] */ BSTR *szPath);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearTempWorkFolder )( 
            IShell * This);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteTempFile )( 
            IShell * This,
            /* [in] */ BSTR szFileName,
            /* [retval][out] */ VARIANT_BOOL *bSuccess);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FileExists )( 
            IShell * This,
            /* [in] */ BSTR szFilePath,
            /* [retval][out] */ VARIANT_BOOL *bSuccess);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *LaunchFile )( 
            IShell * This,
            /* [in] */ BSTR szFilePath,
            /* [defaultvalue][in] */ BSTR szVerb,
            /* [defaultvalue][in] */ VARIANT_BOOL blWait,
            /* [retval][out] */ VARIANT_BOOL *bSuccess);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindAssocProgram )( 
            IShell * This,
            /* [in] */ BSTR szFilePath,
            /* [defaultvalue][in] */ BSTR szVerb,
            /* [retval][out] */ BSTR *szProgram);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CheckFileOpened )( 
            IShell * This,
            /* [in] */ BSTR szFilePath,
            /* [retval][out] */ VARIANT_BOOL *bSuccess);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetAppDataFolder )( 
            IShell * This,
            /* [retval][out] */ BSTR *szFolder);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetLocalAppDataFolder )( 
            IShell * This,
            /* [retval][out] */ BSTR *szFolder);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetScreenWidth )( 
            IShell * This,
            /* [retval][out] */ INT *res);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetScreenHeight )( 
            IShell * This,
            /* [retval][out] */ INT *res);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFileIcon )( 
            IShell * This,
            /* [in] */ BSTR szFile,
            /* [retval][out] */ BSTR *szIconFilePath);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFileMD5 )( 
            IShell * This,
            /* [in] */ BSTR szFilePath,
            /* [retval][out] */ BSTR *szMD5Hash);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddEventLog )( 
            IShell * This,
            /* [in] */ BSTR szMsg,
            /* [defaultvalue][in] */ BSTR szSource,
            /* [defaultvalue][in] */ INT iType);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EnumFilesSize )( 
            IShell * This,
            /* [in] */ BSTR szDir,
            /* [retval][out] */ ULONG *size);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EnumFilesCount )( 
            IShell * This,
            /* [in] */ BSTR szDir,
            /* [retval][out] */ ULONG *count);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EnumFiles )( 
            IShell * This,
            /* [in] */ BSTR szDir,
            /* [retval][out] */ BSTR *szFiles);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetLocalIPAddress )( 
            IShell * This,
            /* [retval][out] */ BSTR *ret);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ReadText )( 
            IShell * This,
            /* [in] */ BSTR szText);
        
        END_INTERFACE
    } IShellVtbl;

    interface IShell
    {
        CONST_VTBL struct IShellVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IShell_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IShell_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IShell_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IShell_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IShell_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IShell_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IShell_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IShell_GetSystemFolder(This,szFolder)	\
    ( (This)->lpVtbl -> GetSystemFolder(This,szFolder) ) 

#define IShell_GetWorkFolder(This,szSubFolder,szFolder)	\
    ( (This)->lpVtbl -> GetWorkFolder(This,szSubFolder,szFolder) ) 

#define IShell_GetWorkFolderTemp(This,szFolder)	\
    ( (This)->lpVtbl -> GetWorkFolderTemp(This,szFolder) ) 

#define IShell_GetWindowsVersionString(This,szVer)	\
    ( (This)->lpVtbl -> GetWindowsVersionString(This,szVer) ) 

#define IShell_GetFileVersionString(This,szFile,szVer)	\
    ( (This)->lpVtbl -> GetFileVersionString(This,szFile,szVer) ) 

#define IShell_GetWindowsUserName(This,szUser)	\
    ( (This)->lpVtbl -> GetWindowsUserName(This,szUser) ) 

#define IShell_GetPersonalFolder(This,szFolder)	\
    ( (This)->lpVtbl -> GetPersonalFolder(This,szFolder) ) 

#define IShell_GetFileLength(This,szFilePath,ulFileLength)	\
    ( (This)->lpVtbl -> GetFileLength(This,szFilePath,ulFileLength) ) 

#define IShell_GetInstallPath(This,szPath)	\
    ( (This)->lpVtbl -> GetInstallPath(This,szPath) ) 

#define IShell_ClearTempWorkFolder(This)	\
    ( (This)->lpVtbl -> ClearTempWorkFolder(This) ) 

#define IShell_DeleteTempFile(This,szFileName,bSuccess)	\
    ( (This)->lpVtbl -> DeleteTempFile(This,szFileName,bSuccess) ) 

#define IShell_FileExists(This,szFilePath,bSuccess)	\
    ( (This)->lpVtbl -> FileExists(This,szFilePath,bSuccess) ) 

#define IShell_LaunchFile(This,szFilePath,szVerb,blWait,bSuccess)	\
    ( (This)->lpVtbl -> LaunchFile(This,szFilePath,szVerb,blWait,bSuccess) ) 

#define IShell_FindAssocProgram(This,szFilePath,szVerb,szProgram)	\
    ( (This)->lpVtbl -> FindAssocProgram(This,szFilePath,szVerb,szProgram) ) 

#define IShell_CheckFileOpened(This,szFilePath,bSuccess)	\
    ( (This)->lpVtbl -> CheckFileOpened(This,szFilePath,bSuccess) ) 

#define IShell_GetAppDataFolder(This,szFolder)	\
    ( (This)->lpVtbl -> GetAppDataFolder(This,szFolder) ) 

#define IShell_GetLocalAppDataFolder(This,szFolder)	\
    ( (This)->lpVtbl -> GetLocalAppDataFolder(This,szFolder) ) 

#define IShell_GetScreenWidth(This,res)	\
    ( (This)->lpVtbl -> GetScreenWidth(This,res) ) 

#define IShell_GetScreenHeight(This,res)	\
    ( (This)->lpVtbl -> GetScreenHeight(This,res) ) 

#define IShell_GetFileIcon(This,szFile,szIconFilePath)	\
    ( (This)->lpVtbl -> GetFileIcon(This,szFile,szIconFilePath) ) 

#define IShell_GetFileMD5(This,szFilePath,szMD5Hash)	\
    ( (This)->lpVtbl -> GetFileMD5(This,szFilePath,szMD5Hash) ) 

#define IShell_AddEventLog(This,szMsg,szSource,iType)	\
    ( (This)->lpVtbl -> AddEventLog(This,szMsg,szSource,iType) ) 

#define IShell_EnumFilesSize(This,szDir,size)	\
    ( (This)->lpVtbl -> EnumFilesSize(This,szDir,size) ) 

#define IShell_EnumFilesCount(This,szDir,count)	\
    ( (This)->lpVtbl -> EnumFilesCount(This,szDir,count) ) 

#define IShell_EnumFiles(This,szDir,szFiles)	\
    ( (This)->lpVtbl -> EnumFiles(This,szDir,szFiles) ) 

#define IShell_GetLocalIPAddress(This,ret)	\
    ( (This)->lpVtbl -> GetLocalIPAddress(This,ret) ) 

#define IShell_ReadText(This,szText)	\
    ( (This)->lpVtbl -> ReadText(This,szText) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IShell_INTERFACE_DEFINED__ */


#ifndef __ICommonDialogs_INTERFACE_DEFINED__
#define __ICommonDialogs_INTERFACE_DEFINED__

/* interface ICommonDialogs */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICommonDialogs;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("2BDD966C-8795-4D50-92E4-2679C8ED7ACA")
    ICommonDialogs : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE SelectFolder( 
            /* [retval][out] */ BSTR *szFolderResult) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE SelectFiles( 
            /* [defaultvalue][in] */ BSTR szDefaultFileName,
            /* [defaultvalue][in] */ BSTR szTitle,
            /* [defaultvalue][in] */ BSTR szFilter,
            /* [defaultvalue][in] */ VARIANT_BOOL blAllowMultiSelect,
            /* [retval][out] */ BSTR *szFile) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE SaveFile( 
            /* [defaultvalue][in] */ BSTR szDefaultFileName,
            /* [defaultvalue][in] */ BSTR szTitle,
            /* [defaultvalue][in] */ BSTR szFilter,
            /* [retval][out] */ BSTR *szFile) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE ChooseColor( 
            /* [defaultvalue][in] */ BSTR szDefaultColor,
            /* [retval][out] */ BSTR *szResult) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICommonDialogsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICommonDialogs * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICommonDialogs * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICommonDialogs * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICommonDialogs * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICommonDialogs * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICommonDialogs * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICommonDialogs * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SelectFolder )( 
            ICommonDialogs * This,
            /* [retval][out] */ BSTR *szFolderResult);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SelectFiles )( 
            ICommonDialogs * This,
            /* [defaultvalue][in] */ BSTR szDefaultFileName,
            /* [defaultvalue][in] */ BSTR szTitle,
            /* [defaultvalue][in] */ BSTR szFilter,
            /* [defaultvalue][in] */ VARIANT_BOOL blAllowMultiSelect,
            /* [retval][out] */ BSTR *szFile);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SaveFile )( 
            ICommonDialogs * This,
            /* [defaultvalue][in] */ BSTR szDefaultFileName,
            /* [defaultvalue][in] */ BSTR szTitle,
            /* [defaultvalue][in] */ BSTR szFilter,
            /* [retval][out] */ BSTR *szFile);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ChooseColor )( 
            ICommonDialogs * This,
            /* [defaultvalue][in] */ BSTR szDefaultColor,
            /* [retval][out] */ BSTR *szResult);
        
        END_INTERFACE
    } ICommonDialogsVtbl;

    interface ICommonDialogs
    {
        CONST_VTBL struct ICommonDialogsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICommonDialogs_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICommonDialogs_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICommonDialogs_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICommonDialogs_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICommonDialogs_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICommonDialogs_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICommonDialogs_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICommonDialogs_SelectFolder(This,szFolderResult)	\
    ( (This)->lpVtbl -> SelectFolder(This,szFolderResult) ) 

#define ICommonDialogs_SelectFiles(This,szDefaultFileName,szTitle,szFilter,blAllowMultiSelect,szFile)	\
    ( (This)->lpVtbl -> SelectFiles(This,szDefaultFileName,szTitle,szFilter,blAllowMultiSelect,szFile) ) 

#define ICommonDialogs_SaveFile(This,szDefaultFileName,szTitle,szFilter,szFile)	\
    ( (This)->lpVtbl -> SaveFile(This,szDefaultFileName,szTitle,szFilter,szFile) ) 

#define ICommonDialogs_ChooseColor(This,szDefaultColor,szResult)	\
    ( (This)->lpVtbl -> ChooseColor(This,szDefaultColor,szResult) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICommonDialogs_INTERFACE_DEFINED__ */


#ifndef __ITextFileWriter_INTERFACE_DEFINED__
#define __ITextFileWriter_INTERFACE_DEFINED__

/* interface ITextFileWriter */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ITextFileWriter;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E2F5D84B-CB34-47DA-A98F-55E657B547FB")
    ITextFileWriter : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE CreateFile( 
            /* [in] */ BSTR szFileName) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE AppendContent( 
            /* [in] */ BSTR szContent) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE SaveFile( 
            /* [retval][out] */ VARIANT_BOOL *blSuccess) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ITextFileWriterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ITextFileWriter * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ITextFileWriter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ITextFileWriter * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ITextFileWriter * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ITextFileWriter * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ITextFileWriter * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ITextFileWriter * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateFile )( 
            ITextFileWriter * This,
            /* [in] */ BSTR szFileName);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AppendContent )( 
            ITextFileWriter * This,
            /* [in] */ BSTR szContent);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SaveFile )( 
            ITextFileWriter * This,
            /* [retval][out] */ VARIANT_BOOL *blSuccess);
        
        END_INTERFACE
    } ITextFileWriterVtbl;

    interface ITextFileWriter
    {
        CONST_VTBL struct ITextFileWriterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ITextFileWriter_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ITextFileWriter_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ITextFileWriter_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ITextFileWriter_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ITextFileWriter_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ITextFileWriter_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ITextFileWriter_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ITextFileWriter_CreateFile(This,szFileName)	\
    ( (This)->lpVtbl -> CreateFile(This,szFileName) ) 

#define ITextFileWriter_AppendContent(This,szContent)	\
    ( (This)->lpVtbl -> AppendContent(This,szContent) ) 

#define ITextFileWriter_SaveFile(This,blSuccess)	\
    ( (This)->lpVtbl -> SaveFile(This,blSuccess) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ITextFileWriter_INTERFACE_DEFINED__ */


#ifndef __ICrypto_INTERFACE_DEFINED__
#define __ICrypto_INTERFACE_DEFINED__

/* interface ICrypto */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICrypto;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("173CE8E1-8174-4930-B6D4-C3234B0C2CB1")
    ICrypto : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE EncryptText( 
            /* [in] */ BSTR szRaw,
            /* [defaultvalue][in] */ BSTR szPwd,
            /* [retval][out] */ BSTR *szEncrypted) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE DecryptText( 
            /* [in] */ BSTR szEncrypted,
            /* [defaultvalue][in] */ BSTR szPwd,
            /* [retval][out] */ BSTR *szDecrypted) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetMD5( 
            /* [in] */ BSTR szIn,
            /* [retval][out] */ BSTR *szResult) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICryptoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICrypto * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICrypto * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICrypto * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICrypto * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICrypto * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICrypto * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICrypto * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EncryptText )( 
            ICrypto * This,
            /* [in] */ BSTR szRaw,
            /* [defaultvalue][in] */ BSTR szPwd,
            /* [retval][out] */ BSTR *szEncrypted);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DecryptText )( 
            ICrypto * This,
            /* [in] */ BSTR szEncrypted,
            /* [defaultvalue][in] */ BSTR szPwd,
            /* [retval][out] */ BSTR *szDecrypted);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetMD5 )( 
            ICrypto * This,
            /* [in] */ BSTR szIn,
            /* [retval][out] */ BSTR *szResult);
        
        END_INTERFACE
    } ICryptoVtbl;

    interface ICrypto
    {
        CONST_VTBL struct ICryptoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICrypto_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICrypto_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICrypto_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICrypto_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICrypto_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICrypto_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICrypto_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICrypto_EncryptText(This,szRaw,szPwd,szEncrypted)	\
    ( (This)->lpVtbl -> EncryptText(This,szRaw,szPwd,szEncrypted) ) 

#define ICrypto_DecryptText(This,szEncrypted,szPwd,szDecrypted)	\
    ( (This)->lpVtbl -> DecryptText(This,szEncrypted,szPwd,szDecrypted) ) 

#define ICrypto_GetMD5(This,szIn,szResult)	\
    ( (This)->lpVtbl -> GetMD5(This,szIn,szResult) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICrypto_INTERFACE_DEFINED__ */


#ifndef __IMSOfficeUtil_INTERFACE_DEFINED__
#define __IMSOfficeUtil_INTERFACE_DEFINED__

/* interface IMSOfficeUtil */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IMSOfficeUtil;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C925C9FA-EEBA-4071-8F4B-080FA7A5ED09")
    IMSOfficeUtil : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetMSOfficeAppMainVersion( 
            /* [defaultvalue][in] */ SHORT appId,
            /* [retval][out] */ SHORT *mainVer) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE SetMSOfficeAPPMacroSecurity( 
            /* [defaultvalue][in] */ SHORT appId,
            /* [defaultvalue][in] */ SHORT secLevel,
            /* [defaultvalue][in] */ SHORT mainVer,
            /* [retval][out] */ VARIANT_BOOL *blRet) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE AddMSOfficeAppTrustedLocation( 
            /* [in] */ BSTR szLocation,
            /* [defaultvalue][in] */ SHORT appId,
            /* [retval][out] */ VARIANT_BOOL *blRet) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMSOfficeUtilVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMSOfficeUtil * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMSOfficeUtil * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMSOfficeUtil * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMSOfficeUtil * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMSOfficeUtil * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMSOfficeUtil * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMSOfficeUtil * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetMSOfficeAppMainVersion )( 
            IMSOfficeUtil * This,
            /* [defaultvalue][in] */ SHORT appId,
            /* [retval][out] */ SHORT *mainVer);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetMSOfficeAPPMacroSecurity )( 
            IMSOfficeUtil * This,
            /* [defaultvalue][in] */ SHORT appId,
            /* [defaultvalue][in] */ SHORT secLevel,
            /* [defaultvalue][in] */ SHORT mainVer,
            /* [retval][out] */ VARIANT_BOOL *blRet);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddMSOfficeAppTrustedLocation )( 
            IMSOfficeUtil * This,
            /* [in] */ BSTR szLocation,
            /* [defaultvalue][in] */ SHORT appId,
            /* [retval][out] */ VARIANT_BOOL *blRet);
        
        END_INTERFACE
    } IMSOfficeUtilVtbl;

    interface IMSOfficeUtil
    {
        CONST_VTBL struct IMSOfficeUtilVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMSOfficeUtil_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMSOfficeUtil_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMSOfficeUtil_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMSOfficeUtil_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IMSOfficeUtil_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IMSOfficeUtil_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IMSOfficeUtil_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IMSOfficeUtil_GetMSOfficeAppMainVersion(This,appId,mainVer)	\
    ( (This)->lpVtbl -> GetMSOfficeAppMainVersion(This,appId,mainVer) ) 

#define IMSOfficeUtil_SetMSOfficeAPPMacroSecurity(This,appId,secLevel,mainVer,blRet)	\
    ( (This)->lpVtbl -> SetMSOfficeAPPMacroSecurity(This,appId,secLevel,mainVer,blRet) ) 

#define IMSOfficeUtil_AddMSOfficeAppTrustedLocation(This,szLocation,appId,blRet)	\
    ( (This)->lpVtbl -> AddMSOfficeAppTrustedLocation(This,szLocation,appId,blRet) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMSOfficeUtil_INTERFACE_DEFINED__ */


#ifndef __IBrowserUtil_INTERFACE_DEFINED__
#define __IBrowserUtil_INTERFACE_DEFINED__

/* interface IBrowserUtil */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IBrowserUtil;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("22BD1883-CDE4-42BC-AD99-77A287768BB4")
    IBrowserUtil : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE CloseHostBrowser( void) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE MaxHostBrowserWindow( void) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetSecurityZone( 
            /* [in] */ BSTR url,
            /* [retval][out] */ INT *securityZoneIndex) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE SnapPageToFile( 
            /* [in] */ BSTR url,
            /* [in] */ BSTR fn) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IBrowserUtilVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IBrowserUtil * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IBrowserUtil * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IBrowserUtil * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IBrowserUtil * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IBrowserUtil * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IBrowserUtil * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IBrowserUtil * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CloseHostBrowser )( 
            IBrowserUtil * This);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *MaxHostBrowserWindow )( 
            IBrowserUtil * This);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetSecurityZone )( 
            IBrowserUtil * This,
            /* [in] */ BSTR url,
            /* [retval][out] */ INT *securityZoneIndex);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SnapPageToFile )( 
            IBrowserUtil * This,
            /* [in] */ BSTR url,
            /* [in] */ BSTR fn);
        
        END_INTERFACE
    } IBrowserUtilVtbl;

    interface IBrowserUtil
    {
        CONST_VTBL struct IBrowserUtilVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IBrowserUtil_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IBrowserUtil_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IBrowserUtil_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IBrowserUtil_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IBrowserUtil_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IBrowserUtil_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IBrowserUtil_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IBrowserUtil_CloseHostBrowser(This)	\
    ( (This)->lpVtbl -> CloseHostBrowser(This) ) 

#define IBrowserUtil_MaxHostBrowserWindow(This)	\
    ( (This)->lpVtbl -> MaxHostBrowserWindow(This) ) 

#define IBrowserUtil_GetSecurityZone(This,url,securityZoneIndex)	\
    ( (This)->lpVtbl -> GetSecurityZone(This,url,securityZoneIndex) ) 

#define IBrowserUtil_SnapPageToFile(This,url,fn)	\
    ( (This)->lpVtbl -> SnapPageToFile(This,url,fn) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IBrowserUtil_INTERFACE_DEFINED__ */



#ifndef __utilLib_LIBRARY_DEFINED__
#define __utilLib_LIBRARY_DEFINED__

/* library utilLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_utilLib;

EXTERN_C const CLSID CLSID_UNIDProvider;

#ifdef __cplusplus

class DECLSPEC_UUID("E9EE6841-0520-47DE-BF19-65E1C4FEA3E5")
UNIDProvider;
#endif

EXTERN_C const CLSID CLSID_Shell;

#ifdef __cplusplus

class DECLSPEC_UUID("44BDB306-F161-4FFB-BD79-BDA61D7F4671")
Shell;
#endif

EXTERN_C const CLSID CLSID_CommonDialogs;

#ifdef __cplusplus

class DECLSPEC_UUID("88D28178-C2D2-4BAC-AFA8-3323B5630F39")
CommonDialogs;
#endif

EXTERN_C const CLSID CLSID_TextFileWriter;

#ifdef __cplusplus

class DECLSPEC_UUID("E55F6B99-51E4-498B-BE3A-D476DD226C49")
TextFileWriter;
#endif

EXTERN_C const CLSID CLSID_Crypto;

#ifdef __cplusplus

class DECLSPEC_UUID("5F62E991-AAFD-437F-8312-62B13F673984")
Crypto;
#endif

EXTERN_C const CLSID CLSID_MSOfficeUtil;

#ifdef __cplusplus

class DECLSPEC_UUID("8E569191-7B69-4EEC-BF4C-5EE7A46469D1")
MSOfficeUtil;
#endif

EXTERN_C const CLSID CLSID_BrowserUtil;

#ifdef __cplusplus

class DECLSPEC_UUID("01A33637-7CC8-4E1E-8689-0FB5B628CED5")
BrowserUtil;
#endif
#endif /* __utilLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


