//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by util.rc
//
#define IDS_PROJNAME                    100
#define IDR_UTIL                        101
#define IDR_UNIDPROVIDER                102
#define IDR_SHELL                       103
#define IDR_COMMONDIALOGS               104
#define IDR_TEXTFILEWRITER              105
#define IDR_CRYPTO                      106
#define IDR_MSOFFICEUTIL                107
#define IDR_BROWSERUTIL                 108
#define IDH_SNAP												3000
// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           109
#endif
#endif
