// BrowserUtil.h : BrowserUtil 的声明

#pragma once
#include "resource.h"       // 主符号
#include "util_i.h"
#include <Exdisp.h>

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif

DEFINE_GUID(SID_STopLevelBrowser,       0x4C96BE40L, 0x915C, 0x11CF, 0x99, 0xD3, 0x00, 0xAA, 0x00, 0x4A, 0xE8, 0x37);
#define SID_SWebBrowserApp    IID_IWebBrowserApp
DEFINE_GUID(IID_IShellBrowser,       0x000214E2L, 0, 0, 0xC0,0,0,0,0,0,0,0x46);
#define SID_SShellBrowser	IID_IShellBrowser
// BrowserUtil

class ATL_NO_VTABLE BrowserUtil :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<BrowserUtil, &CLSID_BrowserUtil>,
	public IObjectWithSiteImpl<BrowserUtil>,
	public IDispatchImpl<IBrowserUtil, &IID_IBrowserUtil, &LIBID_utilLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IObjectSafetyImpl<BrowserUtil,INTERFACESAFE_FOR_UNTRUSTED_CALLER |INTERFACESAFE_FOR_UNTRUSTED_DATA>
{
public:
	BrowserUtil():m_spWebBrowser2(NULL)
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_BROWSERUTIL)


BEGIN_COM_MAP(BrowserUtil)
	COM_INTERFACE_ENTRY(IBrowserUtil)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IObjectWithSite)
	COM_INTERFACE_ENTRY(IObjectSafety)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{

	}
	//获取浏览器对象的引用
	STDMETHODIMP BrowserUtil::SetSite(IUnknown *pUnkSite)
	{
		if (!pUnkSite)
		{
			m_spWebBrowser2.Release();
			m_spWebBrowser2=NULL;
			return S_OK;
		}
		CComQIPtr<IServiceProvider> spProv(pUnkSite);
		if (spProv && !this->m_spWebBrowser2)
		{
			spProv->QueryService(IID_IWebBrowserApp, IID_IWebBrowser2,reinterpret_cast<void **>(&m_spWebBrowser2));
			/*CComPtr<IServiceProvider> spProv2=NULL;
			HRESULT hr = spProv->QueryService(SID_STopLevelBrowser, IID_IServiceProvider, reinterpret_cast<void **>(&spProv2));
			if (SUCCEEDED(hr) && spProv2){
				 hr=spProv2->QueryService(SID_SWebBrowserApp,IID_IWebBrowser2, reinterpret_cast<void **>(&m_spWebBrowser2));
			}*/
		}
		return S_OK;
	}
public:
	STDMETHOD(CloseHostBrowser)();
	STDMETHOD(MaxHostBrowserWindow)();
	STDMETHOD(GetSecurityZone)(BSTR url,INT* securityZoneIndex);
	STDMETHOD(SnapPageToFile)(BSTR url,BSTR fn);
private:
	CComPtr<IWebBrowser2> m_spWebBrowser2;		//浏览器对象
};

OBJECT_ENTRY_AUTO(__uuidof(BrowserUtil), BrowserUtil)
