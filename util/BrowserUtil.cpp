// BrowserUtil.cpp : BrowserUtil 的实现

#include "stdafx.h"
#include "BrowserUtil.h"
#include <urlmon.h>
#include <atlimage.h>
#include <exdispid.h>
#include "snap.h"
#include "shlutil.h"

// BrowserUtil
STDMETHODIMP BrowserUtil::CloseHostBrowser(){
	if (!this->m_spWebBrowser2){
		WarnMsg(_T("必须在浏览器中调用此方法或无法获取到宿主浏览器！"));
		return S_OK;
	}
	this->m_spWebBrowser2->Quit();
	return S_OK;
}

STDMETHODIMP BrowserUtil::MaxHostBrowserWindow(){
	if (!this->m_spWebBrowser2){
		WarnMsg(_T("必须在浏览器中调用此方法或无法获取到宿主浏览器！"));
		return S_OK;
	}
	HWND hWnd=0;
	if(this->m_spWebBrowser2->get_HWND((long*)&hWnd)==S_OK && hWnd){
		ShowWindow(hWnd,SW_MAXIMIZE);
	}
	return S_OK;
}

STDMETHODIMP BrowserUtil::GetSecurityZone(BSTR url,INT* securityZoneIndex){
	*securityZoneIndex=-1;
	if(url==NULL || _tcslen(url)==0){
		ErrMsg(0,L"您必须提供一个有效url地址！");
		return S_OK;
	}
	IInternetSecurityManager* pInetSecMgr;
	HRESULT hr = CoCreateInstance(CLSID_InternetSecurityManager, NULL, CLSCTX_ALL,IID_IInternetSecurityManager, (void **)&pInetSecMgr); 
	if (SUCCEEDED(hr))
	{
		DWORD dwZone;
		hr = pInetSecMgr->MapUrlToZone(url, &dwZone, 0);
		if (hr == S_OK) *securityZoneIndex=(int)dwZone;
		pInetSecMgr->Release();
	} 
	return S_OK;
}

STDMETHODIMP BrowserUtil::SnapPageToFile(BSTR url,BSTR fn){
	if(!url || _tcslen(url)==0){
		ErrMsg(0,L"没有指定url地址！");
		return S_OK;
	}

	if(!fn || _tcslen(fn)==0){
		ErrMsg(0,L"没有提供目标文件名！");
		return S_OK;
	}

	const TCHAR* szUnknownErrMsg=L"抓图时出现异常！";

	HRESULT hr = _Main.Init(NULL,	_AtlBaseModule.GetModuleInstance(), &myGUID);
	if (FAILED(hr)){
		ErrMsg(0,szUnknownErrMsg);
		return S_OK;
	}

	if (!AtlAxWinInit()){
		ErrMsg(0,szUnknownErrMsg);
		return S_OK;
	}

	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR	gdiplusToken;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	TCHAR szPath[MAX_PATH]={0};
	if(!_tcschr(fn,L'\\') && !_tcschr(fn,L'/')){
		hr = shlutil_GetFolderPath(szPath, CSIDL_MYPICTURES);
		_tcscat_s(szPath,L"\\");
		_tcscat_s(szPath,fn);
	}

	CMain	MainWnd(OLE2T(url)/*url地址*/, (_tcslen(szPath)>0?szPath:OLE2T(fn))/*目标文件名*/, 500/*页面装载完成后等待多少时间再抓图*/, 1);

	int sw=GetSystemMetrics(SM_CXSCREEN);
	int sh=GetSystemMetrics(SM_CYSCREEN);
	RECT rcMain	=	{	0, 0,	sw, sh };
	MainWnd.Create(NULL, rcMain, L"DiscoverxPageSnap", WS_POPUP);
	
	//最大等待时间
	MainWnd.SetTimer(ID_TIMEOUTTIMER,	10000);

	MSG	msg;
	while	(GetMessage(&msg,	NULL,	0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	_Main.Term();

	Gdiplus::GdiplusShutdown(gdiplusToken);
	return S_OK;
}