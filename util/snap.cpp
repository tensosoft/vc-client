#include "stdafx.h"
#include "snap.h"

//////////////////////////////////////////////////////////////////
// 实现 CEventSink
//////////////////////////////////////////////////////////////////
STDMETHODIMP CEventSink::GetTypeInfoCount(UINT*	pctinfo)
{
	return E_NOTIMPL;
}

STDMETHODIMP CEventSink::GetTypeInfo(UINT	itinfo,	LCID lcid,ITypeInfo** pptinfo)
{
	return E_NOTIMPL;
}

STDMETHODIMP CEventSink::GetIDsOfNames(REFIID	riid,	LPOLESTR*	rgszNames, UINT	cNames,	LCID lcid, DISPID* rgdispid)
{
	return E_NOTIMPL;
}

STDMETHODIMP CEventSink::Invoke(DISPID dispid, REFIID	riid,	LCID lcid,WORD wFlags, DISPPARAMS* pdispparams,VARIANT* pvarResult, EXCEPINFO*	pexcepinfo,UINT*	puArgErr)
{
	switch (dispid)	{
	case DISPID_DOCUMENTCOMPLETE:	{
		IDispatch* pDisp = pdispparams->rgvarg[1].pdispVal;
		if (!m_pMain->m_pWebBrowser.IsEqualObject(pDisp)) break;
		BOOL bSuccess	=	m_pMain->DelayedSnapshot();
		break;
	}
	case DISPID_NAVIGATEERROR: {
		IDispatch* pDisp = pdispparams->rgvarg[4].pdispVal;
		if (!m_pMain->m_pWebBrowser.IsEqualObject(pDisp)) break;
		::ErrMsg(0,L"错误：无法打开网页！");
		m_pMain->PostMessage(WM_CLOSE);
		break;
	}
	default:
		break;
	}
	return S_OK;
}

//////////////////////////////////////////////////////////////////
// 实现 CMain
//////////////////////////////////////////////////////////////////
LRESULT	CMain::OnCreate(UINT nMsg, WPARAM	wParam,	LPARAM lParam, BOOL& bHandled)
{
	HRESULT	hr;
	RECT old;
	IUnknown * pUnk	=	NULL;
	GetClientRect(&old);

	m_hwndWebBrowser = ::CreateWindow(_T(ATLAXWIN_CLASS),	m_URI,/*WS_POPUP|*/WS_CHILD|WS_DISABLED, old.top,	old.left,	old.right,old.bottom,	m_hWnd,	NULL,	_AtlBaseModule.GetModuleInstance(), NULL);	//::GetModuleHandle(NULL)
	if(m_hwndWebBrowser==NULL) {
		return 1;
	}

	hr = AtlAxGetControl(m_hwndWebBrowser, &m_pWebBrowserUnk);

	if (FAILED(hr) || m_pWebBrowserUnk ==	NULL){
		return 1;
	}

	hr = m_pWebBrowserUnk->QueryInterface(IID_IWebBrowser2,	(void**)&m_pWebBrowser);
	if (FAILED(hr)){
		return 1;
	}

	// Set whether it	should be	silent
	m_pWebBrowser->put_Silent(m_bSilent	?	VARIANT_TRUE : VARIANT_FALSE);
	hr = CComObject<CEventSink>::CreateInstance(&m_pEventSink);
	if (FAILED(hr)){
		return 1;
	}

	m_pEventSink->m_pMain	=	this;
	hr = AtlAdvise(m_pWebBrowserUnk, m_pEventSink->GetUnknown(),DIID_DWebBrowserEvents2, &m_dwCookie);
	if (FAILED(hr)){
		return 1;
	}

	return 0;
}


LRESULT	CMain::OnSize(UINT nMsg, WPARAM	wParam,	LPARAM lParam, BOOL& bHandled)
{
	if (m_hwndWebBrowser !=	NULL)::MoveWindow(m_hwndWebBrowser, 0,	0, LOWORD(lParam), HIWORD(lParam), TRUE);
	return 0;
}

LRESULT CMain::OnTimer(UINT	nMsg,	WPARAM wParam, LPARAM	lParam,	BOOL&	bHandled)
{
	if (wParam !=	ID_DELAYTIMER	&& wParam !=	ID_TIMEOUTTIMER){KillTimer(wParam); return 0;}	//其它Timer则删除之并返回。
	//否则抓图
	KillTimer(wParam);
	if(wParam==ID_DELAYTIMER) SaveSnapshot();
	PostMessage(WM_CLOSE);
	return 0;
}

LRESULT	CMain::OnDestroy(UINT	nMsg,	WPARAM wParam, LPARAM	lParam,	BOOL&	bHandled)
{
	HRESULT	hr;
	if (m_dwCookie !=	0) hr = AtlUnadvise(m_pWebBrowserUnk, DIID_DWebBrowserEvents2,	m_dwCookie);
	m_pWebBrowser.Release();
	m_pWebBrowserUnk.Release();
	PostQuitMessage(0);
	return 0;
}

BOOL CMain::DelayedSnapshot()
{
	if (m_uDelay ==	0) {
		BOOL bSuccess	=	SaveSnapshot();
		PostMessage(WM_CLOSE);
		return bSuccess;
	}
	m_nIDEvent = ID_DELAYTIMER;
	if (!SetTimer(m_nIDEvent,	m_uDelay)) PostMessage(WM_CLOSE);

	return 0;
}

BOOL CMain::SaveSnapshot(void){
	if (m_hwndWebBrowser ==	NULL) {
		::MessageBox(GetForegroundWindow(),L"错误：无法获取网页对象！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}
	long bodyHeight, bodyWidth,	rootHeight,	rootWidth, height, width;

	CComPtr<IDispatch> pDispatch;
	HRESULT	hr = m_pWebBrowser->get_Document(&pDispatch);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法获取网页文档！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}

	CComPtr<IHTMLDocument2>	spDocument;
	hr = pDispatch->QueryInterface(IID_IHTMLDocument2, (void**)&spDocument);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法获取网页文档！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}

	CComPtr<IHTMLElement>	spBody;
	hr = spDocument->get_body(&spBody);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法获取网页内容！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}

	CComPtr<IHTMLElement2> spBody2;
	hr = spBody->QueryInterface(IID_IHTMLElement2, (void**)&spBody2);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法获取网页内容！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}

	hr = spBody2->get_scrollHeight(&bodyHeight);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法获取网页内容高度！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}

	hr = spBody2->get_scrollWidth(&bodyWidth);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法获取网页内容宽度！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}

	CComPtr<IHTMLDocument3>	spDocument3;
	hr = pDispatch->QueryInterface(IID_IHTMLDocument3, (void**)&spDocument3);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法获取网页文档！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}

	// We	also need	to get the dimensions	from the <html>	due	to quirks
	// and standards mode	differences. Perhaps this	should instead check
	// whether we	are	in quirks	mode?	How	does it	work with	IE8?
	CComPtr<IHTMLElement>	spHtml;
	hr = spDocument3->get_documentElement(&spHtml);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法获取网页根节点！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}

	CComPtr<IHTMLElement2> spHtml2;
	hr = spHtml->QueryInterface(IID_IHTMLElement2, (void**)&spHtml2);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法获取网页文档！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}

	hr = spHtml2->get_scrollHeight(&rootHeight);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法获取网页高度！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}

	hr = spHtml2->get_scrollWidth(&rootWidth);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法获取网页宽度！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}

	width	=	bodyWidth;
	height = rootHeight	>	bodyHeight ? rootHeight	:	bodyHeight;

	if(width>32767 || height>32767){
		::MessageBox(GetForegroundWindow(),L"错误：要抓取的网页太大！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}
	::MoveWindow(m_hwndWebBrowser, 0,	0, width,	height,	TRUE);

	CComPtr<IViewObject2>	spViewObject;

	hr = spDocument3->QueryInterface(IID_IViewObject2, (void**)&spViewObject);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法获取初始化视图对象！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}

	RECTL	rcBounds = { 0,	0, width,	height };
	CImage image;

	image.Create(width,	height,	24);

	HDC	imgDc	=	image.GetDC();
	hr = spViewObject->Draw(DVASPECT_CONTENT,	-1,	NULL,	NULL,	imgDc,imgDc, &rcBounds,	NULL,	NULL,	0);
	if (FAILED(hr)){
		::MessageBox(GetForegroundWindow(),L"错误：无法抓取网页图像！",ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONERROR);
		return true;
	}
	image.ReleaseDC();
	hr = image.Save(m_fileName);
	if (FAILED(hr)){
		HDC hSrcDc=image.GetDC();
		HDC hDc=::CreateCompatibleDC(hSrcDc);
		int cx=image.GetWidth();
		int cy=image.GetHeight();
		HBITMAP hBmp=::CreateCompatibleBitmap(::GetDC(this->m_hWnd),cx,cy);
		HBITMAP hOldBmp=(HBITMAP)::SelectObject(hDc,hBmp);
		::BitBlt(hDc, 0, 0, cx,cy,hSrcDc, 0 , 0, SRCCOPY); 
		::OpenClipboard(this->m_hWnd);
		::EmptyClipboard();
		::SetClipboardData(CF_BITMAP,hBmp);
		::CloseClipboard();
		::SelectObject(hDc,hOldBmp);
		::DeleteDC(hSrcDc);
		::DeleteDC(hDc);
		::DeleteObject(hBmp); 
		image.ReleaseDC();
		::MessageBox(GetForegroundWindow(),L"抓图成功但是无法自动保存！程序已将当前网页的抓图拷贝到剪贴板中，您可以将图片粘贴出来然后手动编辑保存它。",MSGBOX_CAPTION,MB_OK|MB_ICONINFORMATION);
		return false;
	}

	TCHAR szTip[STRLEN_4K]={0};
	_stprintf_s(szTip,L"抓取的网页图片成功保存到“%s”",m_fileName);
	::MessageBox(GetForegroundWindow(),szTip,MSGBOX_CAPTION,MB_OK|MB_ICONINFORMATION);

	SHELLEXECUTEINFO ShExecInfo;
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
  ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS | SEE_MASK_FLAG_DDEWAIT;
	ShExecInfo.lpVerb = L"open";
	ShExecInfo.lpFile =L"explorer.exe";
	TCHAR szParam[MAX_PATH+100]={0};
	_stprintf_s(szParam,L"/select,\"%s\"",m_fileName);
	ShExecInfo.lpParameters = szParam;
  ShExecInfo.lpDirectory = NULL;
  ShExecInfo.nShow = SW_SHOWNORMAL;
  ShExecInfo.hInstApp = NULL;
	ShExecInfo.hwnd = NULL;

	ShellExecuteEx(&ShExecInfo);

	return false;
}