// TextFileWriter.cpp : TextFileWriter 的实现

#include "stdafx.h"
#include "TextFileWriter.h"
#include "shell.h"
// TextFileWriter
STDMETHODIMP TextFileWriter::CreateFile(BSTR szFileName){
	ATLASSERT(szFileName);
	if (_tcslen(szFileName)<=0) {
		WarnMsg(_T("没有提供有效的文件名！"));
		return S_OK;
	}
	if(_tcschr(szFileName,L'\\')){
		WarnMsg(_T("只能提供不带路径信息的文件名！"));
		return S_OK;
	}
	TCHAR* szExt=_tcsrchr(szFileName,L'.');
	if(szExt && _tcsicmp(szExt,L".txt")!=0 && _tcsicmp(szExt,L".xml")!=0 && _tcsicmp(szExt,L".htm")!=0){
		WarnMsg(_T("只允许以“.txt”、“.xml”、“.htm”作为扩展名！"));
		return S_OK;
	}

	this->m_blClosed=FALSE;
	HRESULT hr=S_OK;
	CComPtr<IShell> shl=NULL;
	BSTR bstrTempPath=NULL;
	CoInitialize(NULL);
	{
		hr=CoCreateInstance(__uuidof(Shell),NULL,CLSCTX_INPROC_SERVER,__uuidof(IShell),(void**)&shl);
		if (FAILED(hr)){
			WarnMsg(_T("无法创建外壳工具组件，可能客户端组件没有安装或注册正常！"));
			goto clean;
		}
		hr=shl->GetWorkFolderTemp(&bstrTempPath);
		if (FAILED(hr)){
			WarnMsg(_T("无法获取临时目录，可能客户端组件没有安装或注册正常！"));
			goto clean;
		}
		_stprintf_s(this->m_szFilePath,_T("%s%s"),bstrTempPath,szFileName);
	}
clean:
	CoUninitialize();
	return S_OK;
}

STDMETHODIMP TextFileWriter::AppendContent(BSTR szContent){
	if(!szContent || _tcslen(szContent)==0) return S_OK;
	CComBSTR str(_T(""));
	if(this->m_szContent){
		str.Append(this->m_szContent);
		delete this->m_szContent;
		m_szContent=NULL;
	}
	str.AppendBSTR(szContent);
	size_t newLen=str.Length()+1;
	this->m_szContent=new TCHAR[newLen];
	ZeroMemory(this->m_szContent,newLen*sizeof(TCHAR));
	_tcscpy_s(this->m_szContent,newLen,str.Detach());
	return S_OK;
}

STDMETHODIMP TextFileWriter::SaveFile(VARIANT_BOOL* blSuccess){
	*blSuccess=FALSE;
	if (this->m_blClosed) {
		WarnMsg(_T("文件内容已经保存并关闭，不能再次写入！"));
		return S_OK;
	}

	if (_tcslen(this->m_szFilePath)==0){
		WarnMsg(_T("没有提供有效文件名！"));
		return S_OK;
	}
	
	//创建文件
	HANDLE hFile=INVALID_HANDLE_VALUE; 
	hFile = ::CreateFile(this->m_szFilePath,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hFile == INVALID_HANDLE_VALUE) {
		ErrMsg(GetLastError(),_T("无法创建目标文件，请确认文件名是否正确并检查磁盘空间、权限等！"));
		return S_OK;
	}
	//写入文件
	DWORD dwBytesWritten;
	USES_CONVERSION;
	_acp=54936;
	char* szBuffer=T2A(m_szContent);//CT2AEX<>(this->m_szContent,CP_UTF8);
	DWORD dwBytesToWrite=(DWORD)(strlen(szBuffer));
	if (!::WriteFile(hFile, szBuffer, dwBytesToWrite,&dwBytesWritten, NULL)){
		ErrMsg(GetLastError(),_T("无法写入内容到目标文件！"));
		goto clean;
	}
	*blSuccess=TRUE;
clean:
	CloseHandle(hFile);
	if (this->m_szContent){
		delete this->m_szContent;
		m_szContent=NULL;
	}
	this->m_blClosed=TRUE;
	return S_OK;
}
