// CommonDialogs.cpp : CommonDialogs 的实现

#include "stdafx.h"
#include "CommonDialogs.h"
#include "shlutil.h"

// CommonDialogs

STDMETHODIMP CommonDialogs::SelectFolder(BSTR* szFolderResult)
{
	*szFolderResult=NULL;
	HRESULT hr=shlutil_SelectFolder(szFolderResult);
	return hr;
}

STDMETHODIMP CommonDialogs::SelectFiles(BSTR szDefaultFileName,BSTR szTitle,BSTR szFilter,VARIANT_BOOL blAllowMultiSelect,BSTR* szFile){
	*szFile=NULL;
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = GetForegroundWindow();

	const size_t MAX_SIZE=1024;
	TCHAR szResult[MAX_SIZE]={0};
	if (szDefaultFileName && _tcslen(szDefaultFileName)>0){
		_tcsncpy_s(szResult,szDefaultFileName,MAX_PATH);
	}
	
	size_t szFilterSize=_tcslen(szFilter)+2;
	TCHAR* lptstrFilter=new TCHAR[szFilterSize];
	_tcscpy_s(lptstrFilter,szFilterSize,szFilter);
	for(size_t i=0;i<szFilterSize;i++){
		if (*(lptstrFilter+i)==_T('|') || *(lptstrFilter+i)==_T(',')){
			*(lptstrFilter+i)=_T('\0');
		}
	}
	*(lptstrFilter+szFilterSize-1)=_T('\0');
	*(lptstrFilter+szFilterSize-2)=_T('\0');
	ofn.lpstrFilter = lptstrFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrDefExt=NULL;
	ofn.lpstrFile=szResult;
	ofn.nMaxFile = MAX_SIZE;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle=0;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrTitle=szTitle;

	ofn.Flags =   OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_EXPLORER | OFN_LONGNAMES;;
	if (blAllowMultiSelect) ofn.Flags |=OFN_ALLOWMULTISELECT;
	if (GetOpenFileName(&ofn)==TRUE){
		if (!blAllowMultiSelect){
			*szFile=T2BSTR(szResult);
		}
		else{
			TCHAR szPath[MAX_PATH]={0};
			_tcscpy_s(szPath,szResult);
			if(szResult[ofn.nFileOffset-1]==_T('\0')){
				CComBSTR str(_T(""));
				BOOL appendPathFlag=TRUE;
				for(int i=ofn.nFileOffset;i<MAX_SIZE;i++){
					if (szResult[i]==_T('\0')) {
						appendPathFlag=TRUE;
						continue;
					}
					if(appendPathFlag){
						if (str.Length()>0) str.Append(_T('|'));
						str.Append(szPath);
						str.Append(_T('\\'));
						appendPathFlag=FALSE;
					}
					str.Append(szResult[i]);
				}
				str.CopyTo(szFile);
			}
			else *szFile=T2BSTR(szResult);
		}
	}
	else{
		if (CommDlgExtendedError()==FNERR_BUFFERTOOSMALL){
			WarnMsg(_T("对不起，您不能一次性选择太多文件！"));
		}
	}
	delete[] lptstrFilter;
	return S_OK;
}
STDMETHODIMP CommonDialogs::SaveFile(BSTR szDefaultFileName,BSTR szTitle,BSTR szFilter,BSTR* szFile){
	*szFile=NULL;
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = GetForegroundWindow();

	TCHAR szExtName[MAX_PATH]={0};
	TCHAR szResult[MAX_PATH]={0};
	if (szDefaultFileName && _tcslen(szDefaultFileName)>0){
		_tcsncpy_s(szResult,szDefaultFileName,MAX_PATH);
		TCHAR* szTmp=_tcsrchr(szResult,_T('.'));
		if (szTmp) _tcsncpy_s(szExtName,szTmp+1,MAX_PATH);
	}
	
	size_t szFilterSize=_tcslen(szFilter)+2;
	TCHAR* lptstrFilter=new TCHAR[szFilterSize];
	_tcscpy_s(lptstrFilter,szFilterSize,szFilter);
	for(size_t i=0;i<szFilterSize;i++){
		if (*(lptstrFilter+i)==_T('|') || *(lptstrFilter+i)==_T(',')){
			*(lptstrFilter+i)=_T('\0');
		}
	}
	*(lptstrFilter+szFilterSize-1)=_T('\0');
	*(lptstrFilter+szFilterSize-2)=_T('\0');
	ofn.lpstrFilter = lptstrFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrDefExt=szExtName;
	ofn.lpstrFile=szResult;
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle=0;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrTitle=szTitle;

	ofn.Flags =  OFN_OVERWRITEPROMPT| OFN_EXPLORER | OFN_LONGNAMES;

	if (GetSaveFileName(&ofn)==TRUE){
		*szFile=T2BSTR(szResult);
	}
	delete[] lptstrFilter;
	return S_OK;
}

STDMETHODIMP CommonDialogs::ChooseColor(BSTR szDefaultColor,BSTR* szResult){
	*szResult=NULL;
	CHOOSECOLOR cc;
	static COLORREF acrCustClr[16];
	
	ZeroMemory(&cc, sizeof(cc));
	cc.lStructSize = sizeof(cc);
	cc.hwndOwner = NULL;
	cc.lpCustColors = (LPDWORD) acrCustClr;
	size_t len=0;
	if(szDefaultColor!=NULL && (len=_tcslen(szDefaultColor))>0 && _tcschr(szDefaultColor,L'#') && (len==4 || len==7)){
		TCHAR initColor[STRLEN_SMALL]={0};
		TCHAR *sz=szDefaultColor+1;
		int idx=5;
		if(len==4){
			while(*sz!=L'\0'){
				initColor[idx]=*sz;
				initColor[idx-1]=*sz;
				idx-=2;
				sz++;
			}
		}else{
			_tcscpy_s(initColor,sz+4);
			_tcsncat_s(initColor,sz+2,2);
			_tcsncat_s(initColor,sz,2);
		}
		
		m_color=_tcstoul(initColor,NULL,16);
	}
	cc.rgbResult = m_color;
	
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;
 
	if (::ChooseColor(&cc)!=FALSE) {
		m_color=cc.rgbResult;
		TCHAR result[STRLEN_SMALL]={0};
		_stprintf_s(result,L"#%02X%02X%02X",GetRValue(cc.rgbResult),GetGValue(cc.rgbResult),GetBValue(cc.rgbResult));
		*szResult=SysAllocString(result);
	}
	return S_OK;
}