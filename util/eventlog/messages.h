#ifndef __MESSAGES_H__
#define __MESSAGES_H__

////////////////////////////////////////
// 事件日志分类
//
// 必须第一个出现在消息文件中
//
//
//  Values are 32 bit values laid out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//


//
// Define the severity codes
//


//
// MessageId: CATEGORY_NOTHING
//
// MessageText:
//
// 无
//
#define CATEGORY_NOTHING                 0x00000001L

////////////////////////////////////////
// 事件消息
//
//
// MessageId: EVENT_ERROR
//
// MessageText:
//
// %1：%2
//
#define EVENT_ERROR                      0x00000002L

//
// MessageId: EVENT_WARNING
//
// MessageText:
//
// %1：%2
//
#define EVENT_WARNING                    0x00000003L

//
// MessageId: EVENT_INFORMATION
//
// MessageText:
//
// %1：%2
//
#define EVENT_INFORMATION                0x00000004L

////////////////////////////////////////
// 额外消息
//
//事件日志来源
//
// MessageId: IDS_SOURCE
//
// MessageText:
//
// Web ActiveX 控件
//
#define IDS_SOURCE                       0x000003E8L


#endif  //__MESSAGES_H__

