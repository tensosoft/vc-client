#include "stdafx.h"
#include "eventlog.h"

namespace eventlog {

	UINT DoReportEvent(WORD wType, DWORD dwID,LPCTSTR  pFormat,LPCTSTR pSource)
	{
		HANDLE   hEventSource=NULL;
		
		TCHAR szSource[STRLEN_SMALL]={0};
		_tcscpy_s(szSource,L"腾硕协作平台");

		//注册表中添加事件源
		//AddEventSource(szSource,0);

		//TCHAR    chMsg[4096];
		//LPTSTR   lpszStrings[1];
		//va_list pArg;
		//va_start(pArg, pFormat);
		//_vstprintf(chMsg, pFormat, pArg);
		//va_end(pArg);
		//lpszStrings[0] = chMsg;

		//写入日志
		hEventSource = RegisterEventSource(NULL, szSource);
		if (hEventSource != NULL)
		{
			PCTSTR aInsertions[] = { pSource, pFormat};
			BOOL bSuccess=ReportEvent(
				hEventSource,
				wType,
				CATEGORY_NOTHING,
				dwID,
				NULL,
				2,
				0,
				aInsertions,
				NULL
			);
			DeregisterEventSource(hEventSource);
			return (UINT)bSuccess;
		}
		else return 0;
		return 0;
	}
}	// namespace util
