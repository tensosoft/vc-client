;#ifndef __MESSAGES_H__
;#define __MESSAGES_H__
;


LanguageNames =
    (
    	Chinese = 0x0804:Messages_CHN
			English = 0x0409:Messages_ENU
    )


;////////////////////////////////////////
;// 事件日志分类
;//
;// 必须第一个出现在消息文件中
;//

MessageId       = 1
SymbolicName    = CATEGORY_NOTHING
Severity		= Success
Language        = Chinese
无
.
Language        = English
Nothing
.

;////////////////////////////////////////
;// 事件消息
;//

MessageId       = +1
SymbolicName    = EVENT_ERROR
Language        = Chinese
%1：%2
.
Language        = English
Error From 1%:%2
.

MessageId       = +1
SymbolicName    = EVENT_WARNING
Language        = Chinese
%1：%2
.
Language        = English
Warning From 1%:%2
.

MessageId       = +1
SymbolicName    = EVENT_INFORMATION
Language        = Chinese
%1：%2
.
Language        = English
Information From 1%:%2
.



;////////////////////////////////////////
;// 额外消息
;//

;//事件日志来源
MessageId       = 1000
SymbolicName    = IDS_SOURCE
Language        = Chinese
Web ActiveX 控件
.
Language        = English
Web ActiveX Control
.

;
;#endif  //__MESSAGES_H__
;
