#ifndef __EVENTLOG_H__
#define __EVENTLOG_H__
#pragma once
#include ".\messages.h"
namespace eventlog {
	//记录事件日志
	//wType:类型，如错误、警告、信息等
	//dwID:日志ID
	//pFormat:日志内容字符串
	//pSource:引发日志的日志程序
	//调用方法示例：DoReportEvent(EVENTLOG_ERROR_TYPE,1001,"内容","FileOpenDialog");
	//成功则返回非零数字，否则返回零
	UINT DoReportEvent(WORD wType, DWORD dwID,LPCTSTR  pFormat,LPCTSTR pSource=L"腾硕协作平台");
}

#endif // __EVENTLOG_H__