// MSOfficeUtil.cpp : MSOfficeUtil 的实现

#include "stdafx.h"
#include "MSOfficeUtil.h"
#include <shlwapi.h>
#include "shell.h"

// MSOfficeUtil
STDMETHODIMP MSOfficeUtil::GetMSOfficeAppMainVersion(SHORT appId,SHORT* mainVer){
	*mainVer=0;
	if(appId>1 || appId<0) {
		WarnMsg(_T("无效MS Office程序标记参数，必须为0（Word）或1（Excel）！"));
		return S_OK;
	}
	HRESULT hr=S_OK;
	BSTR bstrFN=NULL;
	BSTR bstrProgram=NULL;
	BSTR bstrVer=NULL;
	TCHAR* szAppName=(appId==0?_T("Word"):_T("Excel"));
	TCHAR* szExtName=(appId==0?_T(".doc"):_T(".xls"));
	CComPtr<IShell> shl=NULL;
	CoInitialize(NULL);
	{
		hr=CoCreateInstance(__uuidof(Shell),NULL,CLSCTX_INPROC_SERVER,__uuidof(IShell),(void**)&shl);
		if (FAILED(hr)){
			WarnMsg(_T("无法创建外壳工具组件，可能客户端组件没有安装或注册正常！"));
			goto clean;
		}
		bstrFN=SysAllocString(szExtName);
		hr=shl->FindAssocProgram(bstrFN,_T("open"),&bstrProgram);
		if (FAILED(hr)){
			TCHAR szTip[STRLEN_DEFAULT]={0};
			_stprintf_s(szTip,_T("请确认%s是否正确安装！"),szAppName);
			WarnMsg(szTip);
			goto clean;
		}
		hr=shl->GetFileVersionString(bstrProgram,&bstrVer);
		if (FAILED(hr) || !bstrVer){
			TCHAR szTip[STRLEN_DEFAULT]={0};
			_stprintf_s(szTip,_T("无法获取“%s”版本号！"),bstrProgram);
			WarnMsg(szTip);
			goto clean;
		}
		TCHAR szMainVer[STRLEN_SMALL]={0};
		ZeroMemory(szMainVer,sizeof(TCHAR)*STRLEN_SMALL);
		for(size_t i=0;i<_tcslen(bstrVer);i++){
			if ((*(bstrVer+i))==_T('.')) break;
			else *(szMainVer+i)=*(bstrVer+i);
		}
		*mainVer=(SHORT)_tstol(szMainVer);
	}
clean:
	CoUninitialize();
	FREE_SYS_STR(bstrFN);
	FREE_SYS_STR(bstrProgram);
	FREE_SYS_STR(bstrVer);
	return S_OK;
}
STDMETHODIMP MSOfficeUtil::SetMSOfficeAPPMacroSecurity(SHORT appId,SHORT secLevel,SHORT mainVer,VARIANT_BOOL* blRet){
	if(appId>1 || appId<0) {
		WarnMsg(_T("无效MS Office程序标记参数，必须为0（Word）或1（Excel）！"));
		return S_OK;
	}

	TCHAR* szAppName=(appId==0?_T("Word"):_T("Excel"));

	if(secLevel<0 || secLevel>4){
		WarnMsg(_T("无效宏安全级别参数，必须为1-4之间的某个值！"));
		return S_OK;
	}

	SHORT mainVerNum=mainVer;
	if(mainVerNum<=8 || mainVerNum>=13){
		mainVerNum=0;
		this->GetMSOfficeAppMainVersion(appId,&mainVerNum);
	}
	if(mainVerNum<=8 || mainVerNum>=13){
		TCHAR szTip[STRLEN_DEFAULT]={0};
		_stprintf_s(szTip,_T("无法获取%s版本号，请确认%s是否正确安装！"),szAppName,szAppName);
		WarnMsg(szTip);
		return S_OK;
	}

	TCHAR* szRegValueKey=(mainVerNum<12?_T("Level"):_T("VBAWarnings"));

	DWORD actualLevel=secLevel;
	if(actualLevel<0) actualLevel=0;
	if(actualLevel>4) actualLevel=4;

	TCHAR szRegKey[STRLEN_DEFAULT]={0};
	ZeroMemory(szRegKey,STRLEN_DEFAULT*sizeof(TCHAR));
	_stprintf_s(szRegKey,_T("Software\\Microsoft\\Office\\%d.0\\%s\\Security"),mainVerNum,szAppName);

	switch (mainVerNum){
		case 9:
		case 10:
			if(actualLevel>2) actualLevel=2;
			break;
		case 11:
			 if(actualLevel>3) actualLevel=3;
			break;
		case 12:
			break;
		default:
			break;
	}

	if(actualLevel==0) SHDeleteValue(HKEY_CURRENT_USER,szRegKey,szRegValueKey);
	else SHSetValue(HKEY_CURRENT_USER,szRegKey,szRegValueKey,REG_DWORD,(LPCVOID)&actualLevel,sizeof(actualLevel));

	return S_OK;
}
STDMETHODIMP MSOfficeUtil::AddMSOfficeAppTrustedLocation(BSTR szLocation,SHORT appId,VARIANT_BOOL* blRet){
	* blRet=VARIANT_FALSE;
	if (!szLocation || SysStringLen(szLocation)==0 || !PathIsDirectory(szLocation)){
		WarnMsg(_T("必须指定一个有效路径！"));
		return S_OK;
	}
	if(PathIsUNC(szLocation) || PathIsNetworkPath(szLocation)){
		WarnMsg(_T("不能指定网络路径！"));
		return S_OK;
	}
	if(appId>1 || appId<0) {
		WarnMsg(_T("无效MS Office程序标记参数，必须为0（Word）或1（Excel）！"));
		return S_OK;
	}

	SHORT mainVerNum=0;
	this->GetMSOfficeAppMainVersion(appId,&mainVerNum);
	if(mainVerNum<12){
		WarnMsg(_T("只能为MS Office 2007及以上版本系统设置可信位置！"));
		return S_OK;
	}

	TCHAR* szAppName=(appId==0?_T("Word"):_T("Excel"));

	TCHAR szRegKey[STRLEN_DEFAULT]={0};
	ZeroMemory(szRegKey,STRLEN_DEFAULT*sizeof(TCHAR));
	_stprintf_s(szRegKey,_T("Software\\Microsoft\\Office\\%d.0\\%s\\Security\\Trusted Locations"),mainVerNum,szAppName);

	HUSKEY hKey=NULL;
	LSTATUS status=0;
	if((status=SHRegOpenUSKey(szRegKey,KEY_ALL_ACCESS,NULL,&hKey,FALSE))!=ERROR_SUCCESS){
		ErrMsg(status,_T("%s"));
		return S_OK;
	}
	DWORD dwSubKeyCount=0;
	if (SHRegQueryInfoUSKey(hKey,&dwSubKeyCount,NULL,NULL,NULL,SHREGENUM_HKCU)==ERROR_SUCCESS){
		BOOL found=FALSE;
		for(DWORD i=0;i<dwSubKeyCount;i++){
			ZeroMemory(szRegKey,STRLEN_DEFAULT*sizeof(TCHAR));
			_stprintf_s(szRegKey,_T("Software\\Microsoft\\Office\\%d.0\\%s\\Security\\Trusted Locations\\Location%d"),mainVerNum,szAppName,i);
			TCHAR szPathValue[MAX_PATH]={0};
			DWORD dwPathValueSize=sizeof(szPathValue);
			if(SHRegGetUSValue(szRegKey,L"Path",NULL,szPathValue,&dwPathValueSize,FALSE,NULL,0)==ERROR_SUCCESS){
				if(_tcsstr(szPathValue,szLocation)!=NULL){found=TRUE;break;}
			}
		}
		if(!found){
			ZeroMemory(szRegKey,STRLEN_DEFAULT*sizeof(TCHAR));
			_stprintf_s(szRegKey,_T("Software\\Microsoft\\Office\\%d.0\\%s\\Security\\Trusted Locations\\Location%d"),mainVerNum,szAppName,dwSubKeyCount);
			DWORD dwAllowSubfolders=1;
			SHSetValue(HKEY_CURRENT_USER,szRegKey,_T("AllowSubfolders"),REG_DWORD,(LPCVOID)&dwAllowSubfolders,sizeof(dwAllowSubfolders));
			SHSetValue(HKEY_CURRENT_USER,szRegKey,_T("Path"),REG_SZ,(LPCVOID)szLocation,(_tcslen(szLocation)+1)*sizeof(TCHAR));
			TCHAR szDescription[]=_T("腾硕协作平台本地文件路径");
			SHSetValue(HKEY_CURRENT_USER,szRegKey,_T("Description"),REG_SZ,(LPCVOID)szDescription,(_tcslen(szDescription)+1)*sizeof(TCHAR));
		}
	}
	else{
		SHRegCloseUSKey(hKey);
		ErrMsg(GetLastError(),_T("%s"));
		return S_OK;
	}
	SHRegCloseUSKey(hKey);
	* blRet=VARIANT_TRUE;
	return S_OK;
}