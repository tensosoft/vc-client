

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu Apr 10 11:17:26 2014
 */
/* Compiler settings for util.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IUNIDProvider,0xA5C80CDD,0x7DF9,0x4A7C,0xBA,0x06,0x1C,0xBE,0x2D,0xE3,0x8C,0x5B);


MIDL_DEFINE_GUID(IID, IID_IShell,0xA23EBD08,0x4FDF,0x4146,0x9D,0x44,0x9F,0xBC,0xAB,0x55,0x51,0xE0);


MIDL_DEFINE_GUID(IID, IID_ICommonDialogs,0x2BDD966C,0x8795,0x4D50,0x92,0xE4,0x26,0x79,0xC8,0xED,0x7A,0xCA);


MIDL_DEFINE_GUID(IID, IID_ITextFileWriter,0xE2F5D84B,0xCB34,0x47DA,0xA9,0x8F,0x55,0xE6,0x57,0xB5,0x47,0xFB);


MIDL_DEFINE_GUID(IID, IID_ICrypto,0x173CE8E1,0x8174,0x4930,0xB6,0xD4,0xC3,0x23,0x4B,0x0C,0x2C,0xB1);


MIDL_DEFINE_GUID(IID, IID_IMSOfficeUtil,0xC925C9FA,0xEEBA,0x4071,0x8F,0x4B,0x08,0x0F,0xA7,0xA5,0xED,0x09);


MIDL_DEFINE_GUID(IID, IID_IBrowserUtil,0x22BD1883,0xCDE4,0x42BC,0xAD,0x99,0x77,0xA2,0x87,0x76,0x8B,0xB4);


MIDL_DEFINE_GUID(IID, LIBID_utilLib,0x17D9739A,0xE368,0x4B8E,0xA4,0x14,0x07,0x1F,0x6C,0xD5,0xAD,0x81);


MIDL_DEFINE_GUID(CLSID, CLSID_UNIDProvider,0xE9EE6841,0x0520,0x47DE,0xBF,0x19,0x65,0xE1,0xC4,0xFE,0xA3,0xE5);


MIDL_DEFINE_GUID(CLSID, CLSID_Shell,0x44BDB306,0xF161,0x4FFB,0xBD,0x79,0xBD,0xA6,0x1D,0x7F,0x46,0x71);


MIDL_DEFINE_GUID(CLSID, CLSID_CommonDialogs,0x88D28178,0xC2D2,0x4BAC,0xAF,0xA8,0x33,0x23,0xB5,0x63,0x0F,0x39);


MIDL_DEFINE_GUID(CLSID, CLSID_TextFileWriter,0xE55F6B99,0x51E4,0x498B,0xBE,0x3A,0xD4,0x76,0xDD,0x22,0x6C,0x49);


MIDL_DEFINE_GUID(CLSID, CLSID_Crypto,0x5F62E991,0xAAFD,0x437F,0x83,0x12,0x62,0xB1,0x3F,0x67,0x39,0x84);


MIDL_DEFINE_GUID(CLSID, CLSID_MSOfficeUtil,0x8E569191,0x7B69,0x4EEC,0xBF,0x4C,0x5E,0xE7,0xA4,0x64,0x69,0xD1);


MIDL_DEFINE_GUID(CLSID, CLSID_BrowserUtil,0x01A33637,0x7CC8,0x4E1E,0x86,0x89,0x0F,0xB5,0xB6,0x28,0xCE,0xD5);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



