#include "stdafx.h"
#include "shlutil.h"
#include <shlobj.h>

HRESULT shlutil_SelectFolder(BSTR* szFolderResult)
{
	HRESULT hr=S_OK;
	* szFolderResult=NULL;
	BROWSEINFO bi = { 0 };
	bi.lpszTitle = _T("选择目标路径");
	bi.ulFlags=BIF_RETURNONLYFSDIRS |BIF_USENEWUI;
	LPITEMIDLIST pidl =SHBrowseForFolder(&bi);
	if ( pidl != 0 )
	{
		TCHAR path[MAX_PATH]={0};
		// 获取路径名称
		if (SHGetPathFromIDList(pidl,path)) *szFolderResult=T2BSTR(path);
		else *szFolderResult=SysAllocString(L"0");
		// 释放内存
		IMalloc * imalloc = 0;
		if ( SUCCEEDED( SHGetMalloc ( &imalloc )) )
		{
			imalloc->Free(pidl);
			imalloc->Release();
		}
	}
	return hr;
}
HRESULT shlutil_GetFolderPath(TCHAR* szFolderResult, DWORD dwFlags){
	HRESULT hr = SHGetFolderPath(NULL, dwFlags, NULL, 0, szFolderResult);
	if (!SUCCEEDED(hr)) return hr;
	return S_OK;
}