// Shell.cpp : Shell 的实现

#include "stdafx.h"
#include "./Shell.h"
#include "../commonfiles/MD5.h"
#include "../commonfiles/convutil.h"
#include "./eventlog/eventlog.h"
#include "shlutil.h"
#include <atlstr.h>
#include <gdiplus.h>
using namespace Gdiplus;
#pragma comment(lib, "Version.lib")
#pragma comment(lib, "gdiplus.lib")
#pragma comment(lib, "Ws2_32.lib")
// Shell
#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof((arr)[0]))
STDMETHODIMP Shell::GetWindowsVersionString(BSTR* szVer)
{
	*szVer=NULL;
	OSVERSIONINFO osv;
	osv.dwOSVersionInfoSize=sizeof(osv);
	TCHAR szVersion[80]={0};
	if (GetVersionEx(&osv)){
		_stprintf_s(szVersion,_T("%u.%u.%u"),osv.dwMajorVersion,osv.dwMinorVersion,osv.dwBuildNumber);
		*szVer=T2BSTR(szVersion);
	}
	else return E_FAIL;
	return S_OK;
}

STDMETHODIMP Shell::GetFileVersionString(BSTR szFile, BSTR* szVer)
{
	*szVer=NULL;
	DWORD dwHandle;
	TCHAR *szFileName=OLE2T(szFile);
	DWORD cchver = GetFileVersionInfoSize(szFileName,&dwHandle);
	if (cchver == 0) return GetLastError();
	TCHAR* pver = new TCHAR[cchver];
	BOOL bret = GetFileVersionInfo(szFileName,dwHandle,cchver,pver);
	if (!bret) return GetLastError();
	UINT uLen;
	void *pbuf;
	VS_FIXEDFILEINFO pvsf;
	bret = VerQueryValue(pver,_T("\\"),&pbuf,&uLen);
	if (!bret) {SAFE_DEL_PTR(pver);return GetLastError();}
	memcpy(&pvsf,pbuf,sizeof(VS_FIXEDFILEINFO));
	TCHAR szVersion[80]={0};
	_stprintf_s(szVersion,_T("%u.%u.%u.%u"),HIWORD(pvsf.dwProductVersionMS),LOWORD(pvsf.dwProductVersionMS),HIWORD(pvsf.dwProductVersionLS),LOWORD(pvsf.dwProductVersionLS));
	*szVer=T2BSTR(szVersion);
	SAFE_DEL_PTR(pver);
	return S_OK;
}

STDMETHODIMP Shell::GetWindowsUserName(BSTR* szUser)
{
	*szUser=NULL;
	TCHAR szBuffer[80]={0};
	DWORD dwSize=80;
	if (GetUserName(szBuffer,&dwSize)){
		*szUser=T2BSTR(szBuffer);
	}
	else return GetLastError();
	return S_OK;
}

STDMETHODIMP Shell::GetSystemFolder(BSTR* szFolder)
{
	*szFolder=NULL;
	TCHAR szBuffer[MAX_PATH] = { 0 }; 
	if (!GetSystemDirectory(szBuffer, MAX_PATH)) return E_FAIL;
	_tcscat_s(szBuffer,L"\\");
	*szFolder=T2BSTR(szBuffer);
	return S_OK;
}

STDMETHODIMP Shell::GetPersonalFolder(BSTR* szFolder)
{
	*szFolder=NULL;
	TCHAR szBuffer[MAX_PATH]={0};
	HRESULT hResult = shlutil_GetFolderPath(szBuffer, CSIDL_PERSONAL);
	if (!SUCCEEDED(hResult)) return hResult;
	_tcscat_s(szBuffer,_T("\\"));
	*szFolder=T2BSTR(szBuffer);
	return S_OK;
}

STDMETHODIMP Shell::GetWorkFolder(BSTR szSubFolder,BSTR* szFolder)
{
	*szFolder=NULL;
	TCHAR szBuffer[MAX_PATH]={0};
	ZeroMemory(szBuffer,sizeof(TCHAR)*MAX_PATH);
	HRESULT hResult = shlutil_GetFolderPath(szBuffer, CSIDL_PERSONAL | CSIDL_FLAG_CREATE);
	if (!SUCCEEDED(hResult)) return hResult;
	_tcscat_s(szBuffer,WORK_FOLDER);
	if(szSubFolder && _tcslen(szSubFolder)>0){
		_tcscat_s(szBuffer,L"\\");
		_tcscat_s(szBuffer,szSubFolder);
	}
	CreateDirectory(szBuffer,NULL);
	if (*(szBuffer+_tcslen(szBuffer)-1)!=L'\\'){
		_tcscat_s(szBuffer,L"\\");
	}
	*szFolder=T2BSTR(szBuffer);
	return S_OK;
}

STDMETHODIMP Shell::GetWorkFolderTemp(BSTR* szFolder)
{
	*szFolder=NULL;
	BSTR szWorkFolder;
	TCHAR szBuffer[MAX_PATH]={0};
	if (GetWorkFolder(NULL,&szWorkFolder)==S_OK){
		TCHAR *szRetFolder=OLE2T(szWorkFolder);
		_tcscat_s(szBuffer,szRetFolder);
		_tcscat_s(szBuffer,_T("temp"));
		CreateDirectory(szBuffer,NULL);
		_tcscat_s(szBuffer,_T("\\"));
		*szFolder=T2BSTR(szBuffer);
	}
	else return E_FAIL;
	return S_OK;
}

STDMETHODIMP Shell::GetFileLength(BSTR szFilePath, ULONG* ulFileLength)
{
	HANDLE hFile;
	DWORD dwFileSize;
	LPCTSTR filePath =OLE2T(szFilePath);
	*ulFileLength=0;
	if ((hFile = CreateFile(filePath,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,(HANDLE)NULL)) == (HANDLE)(-1))
	{
		ErrMsg(GetLastError(),_T("文件不存在或者无法打开！"));
		return E_FAIL;
	}
	
	dwFileSize = GetFileSize(hFile, NULL);
	if (dwFileSize == 0xFFFFFFFF) {
		CloseHandle(hFile);
		ErrMsg(GetLastError(),_T("获取文件大小时发生错误！"));
		return E_FAIL;
	}
	CloseHandle(hFile);
	*ulFileLength=dwFileSize;
	return S_OK;
}

STDMETHODIMP Shell::GetInstallPath(BSTR* szPath)
{
	*szPath=NULL;
	TCHAR	strPath[MAX_PATH]={0};
	TCHAR sep=_T('\\');
	DWORD dwRet=GetModuleFileName(_AtlBaseModule.m_hInst, strPath, MAX_PATH);
	if (!dwRet) *szPath=SysAllocString(L"");
	else {
		TCHAR* occur=_tcsrchr(strPath,sep);
		*(occur+1)=_T('\0');
		*szPath=T2BSTR(strPath);
	}
	return S_OK;
}

STDMETHODIMP Shell::ClearTempWorkFolder(void)
{
	VARIANT_BOOL ret;
	BSTR files=SysAllocString(_T("*.*"));
	HRESULT hr=DeleteTempFile(files,&ret);
	if (files) SysFreeString(files);
	return hr;
}

STDMETHODIMP Shell::DeleteTempFile(BSTR szFileName, VARIANT_BOOL* bSuccess)
{
	*bSuccess=VARIANT_FALSE;
	if (szFileName==NULL || SysStringLen(szFileName)==0) {
		WarnMsg(_T("没有指定要删除的临时文件！"));
		return S_OK;
	}

	TCHAR *pszFileName= OLE2T(szFileName);

	BSTR bstrWorkFolderTemp=NULL;
	GetWorkFolderTemp(&bstrWorkFolderTemp);
	
	TCHAR *pszWorkFolderTemp= OLE2T(bstrWorkFolderTemp);

	size_t size=_tcslen(pszWorkFolderTemp)+_tcslen(pszFileName)+2;
	
	TCHAR path[MAX_PATH] ={0};
	_stprintf_s(path,_T("%s%s"),pszWorkFolderTemp,pszFileName);

	TCHAR *pszFrom=new TCHAR[size];
	ZeroMemory(pszFrom,sizeof(TCHAR)*size);
	for(size_t i=0;i<(size-2);i++){
		*(pszFrom+i)=path[i];
	}
	*(pszFrom+size-2)=_T('\0');
	*(pszFrom+size-1)=_T('\0');
	
  SHFILEOPSTRUCT fileop;
	memset(&fileop,0,sizeof(fileop));
	fileop.hwnd   = NULL;					// 不显示状态
  fileop.wFunc  = FO_DELETE;		// 删除标记
  fileop.pFrom  = pszFrom;			// 要删除的文件
  fileop.pTo    = NULL;					// 没有操作目标地址
  fileop.fFlags = FOF_FILESONLY|FOF_NOCONFIRMATION|FOF_SILENT|FOF_NOERRORUI|FOF_NORECURSION ;  // 选项标记
  fileop.fAnyOperationsAborted = FALSE;
  fileop.lpszProgressTitle     = NULL;
  fileop.hNameMappings         = NULL;

  int ret = SHFileOperation(&fileop);
	if(ret==0) *bSuccess=VARIANT_TRUE;
	delete[] pszFrom;
	if (bstrWorkFolderTemp) SysFreeString(bstrWorkFolderTemp);
	return S_OK;
}
STDMETHODIMP Shell::FileExists(BSTR szFilePath, VARIANT_BOOL* bSuccess){
	*bSuccess=VARIANT_FALSE;
	if (!szFilePath || SysStringLen(szFilePath)==0){
		WarnMsg(_T("没有指定要检查的文件（包括完整路径和文件名）！"));
		return S_OK;
	}
	if(PathFileExists(szFilePath)==TRUE)*bSuccess=VARIANT_TRUE;
	/*
	HANDLE hFile;
	LPCTSTR filePath =OLE2T(szFilePath);
	if ((hFile = CreateFile(filePath,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,(HANDLE)NULL)) == (HANDLE)(-1))
	{
		return S_OK;
	}
	CloseHandle(hFile);
	*bSuccess=VARIANT_TRUE;
	*/
	return S_OK;
}
STDMETHODIMP Shell::LaunchFile(BSTR szFilePath,BSTR szVerb,VARIANT_BOOL blWait,VARIANT_BOOL* bSuccess)
{
	if(!szFilePath ||_tcslen(szFilePath)==0){
		WarnMsg(L"没有指定文件名！");
		return S_OK;
	}

	*bSuccess=VARIANT_FALSE;
	TCHAR * verb=_T("open");
	verb=T2BSTR(szVerb);
	if (!verb || _tcslen(verb)<=0) verb=_T("open");

	BOOL isSafeFile=TRUE;
	TCHAR name[MAX_PATH]={0};
	_tcscpy_s(name,szFilePath);
	
	BOOL isFolder=((*(name+_tcslen(name)-1))==L'\\');
	if(!isFolder){
		TCHAR* pdest=_tcsrchr(name,'\\');
		TCHAR cpy[MAX_PATH]={0};
		_tcsncpy_s(cpy,(pdest?pdest:name),MAX_PATH);
		_tcslwr_s(cpy);
		TCHAR * ext=_tcsrchr(cpy,'.');
		if (!ext) {
			WarnMsg(_T("对不起，您必须指定文件扩展名！"));
			return S_OK;
		}
		else{
			if (_tcsrchr(ext,' ')){
				WarnMsg(_T("对不起，文件不能包含参数！"));
				return S_OK;
			}
			const int al=13;	//刚好13个邪恶的（不安全的）扩展名:)
			const TCHAR* subs[al]={_T(".exe"),_T(".cpl"),_T(".scr"),_T(".lnk"),_T(".com"),_T(".bat"),_T(".vbs"),_T(".com"),_T(".pif"),_T(".inf"),_T(".vb"),_T(".js"),_T(".hta")};
			for(int i=0;i<al;i++){
				if (_tcsstr(cpy,subs[i])){
					isSafeFile=FALSE;
					break;
				}
			}
		}//else end
	}	//if !isFolder end

	if (!isSafeFile && (_tcsstr(verb,L"open")||_tcsstr(verb,L"edit"))){
		WarnMsg(_T("对不起，您不能打开/编辑此类型的文件！"));
		return S_OK;
	}
	
	SHELLEXECUTEINFO ShExecInfo;
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
  ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS | SEE_MASK_FLAG_DDEWAIT;
	if(_tcsstr(verb,L"explore")==NULL || isFolder){
		ShExecInfo.lpVerb = verb;
		ShExecInfo.lpFile =name;
		ShExecInfo.lpParameters = NULL;
	}else{
		ShExecInfo.lpVerb = L"open";
		ShExecInfo.lpFile =L"explorer.exe";
		TCHAR szParam[MAX_PATH+100]={0};
		_stprintf_s(szParam,L"/select,\"%s\"",name);
		ShExecInfo.lpParameters = szParam;
	}
  ShExecInfo.lpDirectory = NULL;
  ShExecInfo.nShow = SW_SHOWNORMAL;
  ShExecInfo.hInstApp = NULL;
	ShExecInfo.hwnd = NULL;	//GetForegroundWindow();

	if (ShellExecuteEx(&ShExecInfo)==TRUE){
		*bSuccess=VARIANT_TRUE;
		if(blWait && ShExecInfo.hProcess){
			WaitForSingleObject(ShExecInfo.hProcess,INFINITE);
		}
	}
	return S_OK;
}
STDMETHODIMP Shell::FindAssocProgram(BSTR szFilePath,BSTR szVerb,BSTR* szProgram){
	*szProgram=NULL;
	if (!szFilePath || _tcslen(szFilePath)<=0) {
		WarnMsg(_T("没有指定有效的文件名！"));
		return S_OK;
	}

	TCHAR * szExt=PathFindExtension(szFilePath);
	if (!szExt || _tcslen(szExt)<=0) return S_OK;

	HRESULT hr=S_OK;
	TCHAR szExe[MAX_PATH]={0};
	DWORD cchExe = ARRAY_SIZE(szExe);

	TCHAR * verb=T2BSTR(szVerb);
	if (_tcslen(verb)<=0) verb=_T("open");

	if (SUCCEEDED(hr = AssocQueryString(0, ASSOCSTR_EXECUTABLE,szExt, verb, szExe, &cchExe))){
		*szProgram=T2BSTR(szExe);
		return hr;
	}
	return S_OK;
}

BOOL foundFileInWindowText=FALSE;	//是否发现文件在窗口标题中
//窗口枚举函数
BOOL CALLBACK searcher(HWND hWnd, LPARAM lParam)
{
	TCHAR strTitle[MAX_PATH]={0};
	LPCTSTR file=(LPCTSTR)lParam;
	int res=GetWindowText(hWnd,strTitle,MAX_PATH);
	foundFileInWindowText=FALSE;
	if (_tcsstr(strTitle,file)!=NULL){
		if (!IsWindowVisible(hWnd)) ShowWindow(hWnd, SW_SHOW);
		if (IsIconic(hWnd)) ShowWindow(hWnd,SW_RESTORE);
		SetForegroundWindow(hWnd);
		foundFileInWindowText=TRUE;
		return FALSE;	//停止枚举 
	}
	else foundFileInWindowText=FALSE;
  return TRUE;	//继续枚举
}

STDMETHODIMP Shell::CheckFileOpened(BSTR szFilePath,VARIANT_BOOL* bSuccess)
{
	*bSuccess=VARIANT_FALSE;
	foundFileInWindowText=FALSE;
	if (!szFilePath || _tcslen(szFilePath)<=0) return S_OK;

	TCHAR* filePath =OLE2T(szFilePath);
	TCHAR szFN[MAX_PATH]={0};
	TCHAR* strFileName=_tcsrchr(filePath,_T('\\'));
	if (strFileName){
	TCHAR* p=_tcschr(strFileName,_T('.'));
		if (p!=NULL && p>strFileName) {
			int i=1;
			while (true){
				if ((strFileName+i)<p){
					szFN[i-1]=*(strFileName+i);
					i++;
				}
				else break;
			}
			EnumWindows(searcher,(LPARAM)szFN);
			if (!(*bSuccess) && foundFileInWindowText) *bSuccess=VARIANT_TRUE;
		}
	}
	return S_OK;
}

STDMETHODIMP Shell::GetAppDataFolder(BSTR* szFolder)
{
	*szFolder=NULL;
	TCHAR szBuffer[MAX_PATH]={0};
	ZeroMemory(szBuffer,MAX_PATH);
	HRESULT hResult = shlutil_GetFolderPath(szBuffer,CSIDL_APPDATA | CSIDL_FLAG_CREATE);
	if (!SUCCEEDED(hResult)) return hResult;
	_tcscat_s(szBuffer,L"\\");
	*szFolder=T2BSTR(szBuffer);
	return S_OK;
}

STDMETHODIMP Shell::GetLocalAppDataFolder(BSTR* szFolder)
{
	*szFolder=NULL;
	TCHAR szBuffer[MAX_PATH]={0};
	ZeroMemory(szBuffer,MAX_PATH);
	HRESULT hResult = shlutil_GetFolderPath(szBuffer,CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE);
	if (!SUCCEEDED(hResult)) return hResult;
	_tcscat_s(szBuffer,L"\\");
	*szFolder=T2BSTR(szBuffer);
	return S_OK;
}

STDMETHODIMP Shell::GetScreenWidth(INT* res){
	*res= GetSystemMetrics(SM_CXSCREEN);
	return S_OK;
}
STDMETHODIMP Shell::GetScreenHeight(INT* res){
	*res= GetSystemMetrics(SM_CYSCREEN);
	return S_OK;
}
STDMETHODIMP Shell::GetFileIcon(BSTR szFile,BSTR* szIconFilePath)
{
	*szIconFilePath=NULL;

	if(szFile==NULL || _tcslen(szFile)==0){
		WarnMsg(L"没有指定有效文件名！");
		return S_OK;
	}
	TCHAR* szExtName=_tcsrchr(szFile,L'.');
	if(szExtName==NULL) szExtName=szFile;
	else szExtName++;

	//ie保护模式下可写的位置。
	TCHAR workFolder[MAX_PATH]={0};
	::GetTempPath(MAX_PATH,workFolder);
	
	//BSTR workFolder=NULL;
	//if(this->GetWorkFolder(NULL,&workFolder)!=S_OK){
	//	WarnMsg(L"无法获取工作目录！");
	//	return S_OK;
	//}

	TCHAR szFolder[MAX_PATH]={0};
	_stprintf_s(szFolder,L"%s%s",workFolder,L"icons");
	
	CreateDirectory(szFolder,NULL);
	TCHAR szBuffer[MAX_PATH]={0};
	_stprintf_s(szBuffer,L"%s%s\\%s.png",workFolder,L"icons",szExtName);
	VARIANT_BOOL blExists=FALSE;
	BSTR bstrBuffer=SysAllocString(szBuffer);
	if(this->FileExists(bstrBuffer,&blExists)==S_OK && blExists) {
		FREE_SYS_STR(bstrBuffer);
		//图标文件已经存在则直接返回。
		*szIconFilePath=SysAllocString(szBuffer);
		return S_OK;
	}
	FREE_SYS_STR(bstrBuffer);
	
	SHFILEINFO sfi;
	if(SHGetFileInfo(szFile,FILE_ATTRIBUTE_NORMAL,&sfi,sizeof(SHFILEINFO),SHGFI_USEFILEATTRIBUTES | SHGFI_ICON | SHGFI_SMALLICON )){
		Gdiplus::GdiplusStartupInput gdiplusStartupInput;
		ULONG_PTR gdiplusToken;
		Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

		Gdiplus::Bitmap* bmp = Gdiplus::Bitmap::FromHICON(sfi.hIcon);

		CLSID   encoderClsid;
		Gdiplus::Status  stat;

		UINT  num = 0;          // number of image encoders
		UINT  size = 0;         // size of the image encoder array in bytes
		Gdiplus::ImageCodecInfo* pImageCodecInfo = NULL;
		Gdiplus::GetImageEncodersSize(&num, &size);
		if(size >0){
			pImageCodecInfo = (Gdiplus::ImageCodecInfo*)(malloc(size));
			if(pImageCodecInfo != NULL){

				Gdiplus::GetImageEncoders(num, size, pImageCodecInfo);

				for(UINT j = 0; j < num; ++j)
				{
					if(_tcscmp(pImageCodecInfo[j].MimeType, L"image/png") == 0 )
					{
						 encoderClsid = pImageCodecInfo[j].Clsid;
						 break;
					}
				}
			}

			free(pImageCodecInfo);
		}

		stat = bmp->Save(szBuffer, &encoderClsid, NULL);
		if(stat != Ok){
			if(stat==Win32Error){
				ErrMsg(GetLastError(),L"无法保存图标文件：%s！");
			}else{
				TCHAR szErrMsg[STRLEN_NORMAL]={0};
				_stprintf_s(szErrMsg,L"无法保存图标文件，错误代码为“%d”！",stat);
				WarnMsg(szErrMsg);
			}
		}
		
		delete bmp;

		Gdiplus::GdiplusShutdown(gdiplusToken);
	}
	*szIconFilePath=SysAllocString(szBuffer);
	return S_OK;
}

STDMETHODIMP Shell::GetFileMD5(BSTR szFilePath,BSTR* szMD5Hash){
	*szMD5Hash=NULL;
	if(szFilePath==NULL || _tcslen(szFilePath)==0) return S_OK;
	if(!PathFileExists(szFilePath)) return S_OK;
	char szFileDigest[STRLEN_SMALL]={0};
	CMD5 md5;
	md5.DigestFile(szFilePath,szFileDigest);
	char szFileHash[STRLEN_SMALL]={0};
	md5.Reset();
	convutil::Binary2Hex(reinterpret_cast<unsigned char*>(szFileDigest), 16, szFileHash);
	* szMD5Hash=A2BSTR(szFileHash);
	return S_OK;
}

STDMETHODIMP Shell::AddEventLog(BSTR szMsg,BSTR szSource,INT iType){
	WORD wType=EVENTLOG_SUCCESS;
	DWORD dwID=EVENT_INFORMATION;
	switch(iType){
		case 1:
			wType=EVENTLOG_WARNING_TYPE;
			dwID=EVENT_WARNING;
			break;
		case 2:
			wType=EVENTLOG_ERROR_TYPE;
			dwID=EVENT_ERROR;
			break;
		default:
			break;
	}
	eventlog::DoReportEvent(wType,dwID,szMsg,szSource);
	return S_OK;
}
STDMETHODIMP Shell::EnumFilesCount(BSTR szDir, ULONG *count){
	ATLASSERT(szFile);
	*count=0;
	if(!szDir || _tcslen(szDir)==0) return S_OK;

	TCHAR strPath[MAX_PATH]={0};
	_tcscpy_s(strPath,OLE2T(szDir));
	if(*(strPath+_tcslen(strPath)-1)==L'\\') *(strPath+_tcslen(strPath)-1)='\0';

	if(_tcsicmp(szPath,strPath)==0){
		*count=lFilesCount;
	}else{
		wstring path(strPath);
		if (ListFiles(path, L"*", NULL)) {
			*count=lFilesCount;
			_tcscpy_s(szPath,strPath);
		}
	}
	return S_OK;
}
STDMETHODIMP Shell::EnumFilesSize(BSTR szDir,ULONG* size){
	ATLASSERT(szFile);
	*size=0;
	if(!szDir || _tcslen(szDir)==0) return S_OK;

	TCHAR strPath[MAX_PATH]={0};
	_tcscpy_s(strPath,OLE2T(szDir));
	if(*(strPath+_tcslen(strPath)-1)==L'\\') *(strPath+_tcslen(strPath)-1)='\0';

	if(_tcsicmp(szPath,strPath)==0){
		*size=lPathSize;
	}else{
		wstring path(strPath);
		if (ListFiles(path, L"*", NULL)) {
			*size=lPathSize;
			_tcscpy_s(szPath,strPath);
		}
	}
	return S_OK;
}
STDMETHODIMP Shell::EnumFiles(BSTR szDir,BSTR* szFiles){
	ATLASSERT(szFile);
	*szFiles=NULL;
	if(!szDir || _tcslen(szDir)==0) return S_OK;
	TCHAR strPath[MAX_PATH]={0};
	_tcscpy_s(strPath,OLE2T(szDir));
	if(*(strPath+_tcslen(strPath)-1)==L'\\') *(strPath+_tcslen(strPath)-1)='\0';
	CAtlString atlStr;
	vector<wstring> files;
	wstring path(strPath);
	if (ListFiles(path, L"*", &files) && files.size()>0) {
		for (vector<wstring>::iterator it = files.begin(); it != files.end(); ++it) {
			if(atlStr.GetLength()>0) atlStr.AppendChar(L';');
			atlStr.Append(it->c_str());
		}
		*szFiles=atlStr.AllocSysString();
		_tcscpy_s(szPath,strPath);
	}
	return S_OK;
}

BOOL Shell::ListFiles(wstring path, wstring mask, vector<wstring>* files){
	if(path.length()==0) return FALSE;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA ffd;
	wstring spec;
	stack<wstring> directories;
	directories.push(path);
  if(files) files->clear();
	BOOL breakAllFlag=FALSE;
	size_t rootPathSize=path.length();
	wstring lastRelPath(L"");
	while (!directories.empty()) {
		path = directories.top();
		spec = path + L"\\" + mask;
		directories.pop();
		hFind = FindFirstFile(spec.c_str(), &ffd);
		if (hFind == INVALID_HANDLE_VALUE) return FALSE;
		do {
			if (wcscmp(ffd.cFileName, L".") != 0 && wcscmp(ffd.cFileName, L"..") != 0) {
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					directories.push(path + L"\\" + ffd.cFileName);
				}
				else if(ffd.nFileSizeHigh==0 && ffd.nFileSizeLow>0){
					if((MAXDWORD-ffd.nFileSizeLow)<=lPathSize){
						breakAllFlag=true;
						break;
					}
					lPathSize+= ffd.nFileSizeLow;	//((ffd.nFileSizeHigh * (MAXDWORD+1))
					lFilesCount++;
					if(files) {
						wstring relpath=(path.substr(rootPathSize) + L"\\").substr(1);
						if(relpath.compare(lastRelPath)==0) files->push_back(ffd.cFileName);
						else {
							files->push_back(relpath + ffd.cFileName);
							lastRelPath=relpath;
						}
					}
				}
			}
			if(breakAllFlag) break;
		} while (FindNextFile(hFind, &ffd) != 0);

		if (GetLastError() != ERROR_NO_MORE_FILES) {
			FindClose(hFind);
			return FALSE;
		}

		FindClose(hFind);
		hFind = INVALID_HANDLE_VALUE;
	}

	return TRUE;
}
STDMETHODIMP Shell::GetLocalIPAddress(BSTR* ret){
	WORD wVersionRequested;
	WSADATA wsaData;
	char Name[MAX_PATH]={0};
	PHOSTENT HostInfo;
	wVersionRequested = MAKEWORD(2, 2);
	TCHAR szIP[STRLEN_4K]={0};
	if ( WSAStartup( wVersionRequested, &wsaData ) == 0 ){
		if( gethostname ( Name, sizeof(Name)) == 0){
			if((HostInfo = gethostbyname(Name)) != NULL && HostInfo->h_addrtype== AF_INET){
				int nCount = 0;
				while(HostInfo->h_addr_list[nCount]){
					char *ipaddr = inet_ntoa(*(struct in_addr *)HostInfo->h_addr_list[nCount]);
					USES_CONVERSION;
					if(nCount==0) _tcscpy_s(szIP,A2T(ipaddr));
					else {_tcscat_s(szIP,L";");_tcscat_s(szIP,A2T(ipaddr));}
					++nCount;
				}
			}
		}
		WSACleanup();
	}
	*ret=SysAllocString(szIP);
	return S_OK;
}

STDMETHODIMP Shell::ReadText(BSTR szText){
	if(pVoice==NULL) {
		ErrMsg(0,L"无法初始化朗读对象！");
		return S_OK;
	}
	//LPCWSTR lpcstr=OLE2W(szText);
	pVoice->Speak(szText, SPF_ASYNC, NULL);
	return S_OK;
}