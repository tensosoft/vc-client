// Crypto.h : Crypto 的声明

#pragma once
#include "resource.h"       // 主符号

#include "util_i.h"


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif



// Crypto

class ATL_NO_VTABLE Crypto :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<Crypto, &CLSID_Crypto>,
	public IObjectWithSiteImpl<Crypto>,
	public IDispatchImpl<ICrypto, &IID_ICrypto, &LIBID_utilLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IObjectSafetyImpl<Crypto,INTERFACESAFE_FOR_UNTRUSTED_CALLER |INTERFACESAFE_FOR_UNTRUSTED_DATA>
{
public:
	Crypto()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CRYPTO)


BEGIN_COM_MAP(Crypto)
	COM_INTERFACE_ENTRY(ICrypto)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IObjectWithSite)
	COM_INTERFACE_ENTRY(IObjectSafety)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
	STDMETHOD(EncryptText)(BSTR szRaw,BSTR szPwd, BSTR* szEncrypted);
	STDMETHOD(DecryptText)(BSTR szEncrypted,BSTR szPwd, BSTR* szDecrypted);
	STDMETHOD(GetMD5)(BSTR szIn, BSTR* szResult);
};

OBJECT_ENTRY_AUTO(__uuidof(Crypto), Crypto)
