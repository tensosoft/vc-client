#ifndef __SNAP_H__
#define __SNAP_H__

#define	VC_EXTRALEAN
#include <stdlib.h>
#include <windows.h>
#include <mshtml.h>
#include <exdispid.h>
#include <gdiplus.h>
#include <atlbase.h>
#include <atlwin.h>
#include <atlcom.h>
#include <atlhost.h>
#include <atlimage.h>
#undef VC_EXTRALEAN

#define	ID_TIMEOUTTIMER	0x1101
#define	ID_DELAYTIMER		0x1102

class CEventSink;
class CMain;

//////////////////////////////////////////////////////////////////
// CEventSink
//////////////////////////////////////////////////////////////////
class	CEventSink :
	public CComObjectRootEx	<CComSingleThreadModel>,
	public IDispatch
{
public:
	CEventSink() : m_pMain(NULL) {}

	BEGIN_COM_MAP(CEventSink)
			COM_INTERFACE_ENTRY(IDispatch)
			COM_INTERFACE_ENTRY_IID(DIID_DWebBrowserEvents2, IDispatch)
	END_COM_MAP()

	STDMETHOD(GetTypeInfoCount)(UINT*	pctinfo);
	STDMETHOD(GetTypeInfo)(UINT	itinfo,	LCID lcid, ITypeInfo** pptinfo);
	STDMETHOD(GetIDsOfNames)(REFIID	riid,	LPOLESTR*	rgszNames, UINT	cNames,	LCID lcid, DISPID* rgdispid);
	STDMETHOD(Invoke)(DISPID dispid, REFIID	riid,	LCID lcid, WORD	wFlags,	DISPPARAMS*	pdispparams,VARIANT* pvarResult, EXCEPINFO*	pexcepinfo,	UINT*	puArgErr);

public:
	CMain* m_pMain;
};

//////////////////////////////////////////////////////////////////
// CMain
//////////////////////////////////////////////////////////////////
class	CMain	:public CWindowImpl <CMain>
{
	friend CEventSink;
public:
	CMain(LPTSTR uri,	LPTSTR file, UINT	delay, BOOL	silent)	:	m_uDelay(delay),m_dwCookie(0), m_URI(uri), m_fileName(file), m_bSilent(silent) { }

	BEGIN_MSG_MAP(CMainWindow)
		MESSAGE_HANDLER(WM_CREATE,	OnCreate)
		MESSAGE_HANDLER(WM_SIZE,		OnSize)
		MESSAGE_HANDLER(WM_DESTROY,	OnDestroy)
		MESSAGE_HANDLER(WM_TIMER,		OnTimer)
	END_MSG_MAP()

	LRESULT	OnCreate	(UINT	nMsg,	WPARAM wParam, LPARAM	lParam,	BOOL&	bHandled);
	LRESULT	OnSize		(UINT	nMsg,	WPARAM wParam, LPARAM	lParam,	BOOL&	bHandled);
	LRESULT	OnDestroy	(UINT	nMsg,	WPARAM wParam, LPARAM	lParam,	BOOL&	bHandled);
	LRESULT	OnTimer		(UINT	nMsg,	WPARAM wParam, LPARAM	lParam,	BOOL&	bHandled);

	BOOL SaveSnapshot(void);
	BOOL DelayedSnapshot(void);

private:
	LPTSTR m_URI;
	LPTSTR m_fileName;
	BOOL	 m_bSilent;
	UINT	 m_uDelay;
	UINT	 m_nIDEvent;

protected:
	CComPtr<IUnknown>	m_pWebBrowserUnk;
	CComPtr<IWebBrowser2>	m_pWebBrowser;
	CComObject<CEventSink>*	m_pEventSink;
	HWND m_hwndWebBrowser;
	DWORD	m_dwCookie;
};

static const GUID	myGUID = { 0x445c10c2, 0xa6d4, 0x40a9, { 0x9c, 0x3f, 0x4e, 0x90, 0x42, 0x1d, 0x7e, 0x83	}	};
static CComModule	_Main;

#endif // __SNAP_H__
