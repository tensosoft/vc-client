// Crypto.cpp : Crypto 的实现

#include "stdafx.h"
#include "Crypto.h"
#include "..\commonfiles\cryptutil.h"

// Crypto

STDMETHODIMP Crypto::EncryptText(BSTR szRaw, BSTR szPwd,BSTR* szEncrypted)
{
	*szEncrypted=NULL;
	if (!szRaw){
		MessageBox(GetForegroundWindow(),_T("必须提供待加密内容！"),ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONINFORMATION);
		return S_OK;
	}

	size_t rawLen=_tcslen(szRaw);
	if(rawLen==0){
		MessageBox(GetForegroundWindow(),_T("必须提供待加密内容！"),ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONINFORMATION);
		return S_OK;
	}

	BOOL hasPwd=FALSE;
	if(szPwd && _tcslen(szPwd)>0) {
		if (_tcslen(szPwd)<8){
			MessageBox(GetForegroundWindow(),_T("密码必须至少为8个字符！"),ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONINFORMATION);
			return S_OK;
		}
		hasPwd=TRUE;
	}
	size_t encryptedLen=(FromUTF16(szRaw,NULL,0)+8)*2+1;
	TCHAR* pszEncrypted=new TCHAR[encryptedLen];
	ZeroMemory(pszEncrypted,encryptedLen*sizeof(TCHAR));
	if(hasPwd) Encrypt(szRaw,pszEncrypted,&encryptedLen,szPwd);
	else Encrypt(szRaw,pszEncrypted,&encryptedLen);
	*szEncrypted=SysAllocString(pszEncrypted);
	SAFE_DEL_ARR(pszEncrypted);
	return S_OK;
}

STDMETHODIMP Crypto::DecryptText(BSTR szEncrypted, BSTR szPwd,BSTR* szDecrypted)
{
	*szDecrypted=NULL;
	if (!szEncrypted){
		MessageBox(GetForegroundWindow(),_T("必须提供待解密内容！"),ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONINFORMATION);
		return S_OK;
	}
	
	size_t rawLen=_tcslen(szEncrypted);
	if(rawLen==0){
		MessageBox(GetForegroundWindow(),_T("必须提供待解密内容！"),ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONINFORMATION);
		return S_OK;
	}

	BOOL hasPwd=FALSE;
	if(szPwd && _tcslen(szPwd)>0){
		if (_tcslen(szPwd)<8){
			MessageBox(GetForegroundWindow(),_T("密码必须至少为8个字符！"),ERROR_MSGBOX_CAPTION,MB_OK|MB_ICONINFORMATION);
			return S_OK;
		}
		hasPwd=TRUE;
	}

	size_t decryptedLen=rawLen*2+1;
	TCHAR* pszDecrypted=new TCHAR[decryptedLen];
	ZeroMemory(pszDecrypted,decryptedLen*sizeof(TCHAR));
	if(hasPwd) Decrypt(szEncrypted,pszDecrypted,&decryptedLen,szPwd);
	else Decrypt(szEncrypted,pszDecrypted,&decryptedLen);
	*szDecrypted=SysAllocString(pszDecrypted);
	SAFE_DEL_ARR(pszDecrypted);
	return S_OK;
}


STDMETHODIMP Crypto::GetMD5(BSTR szIn, BSTR* szResult){
	*szResult=NULL;
	HRESULT hr=S_OK;
	if (!szIn){
		MessageBox(NULL,_T("没有提供有效原始文本！"),_T("错误"),MB_OK|MB_ICONERROR);
		return hr;
	}

	HCRYPTPROV hProv = 0;
	HCRYPTHASH hHash = 0;
	if (!CryptAcquireContext(&hProv,NULL,NULL,PROV_RSA_FULL,CRYPT_VERIFYCONTEXT)){
		ErrMsg(GetLastError(),NULL);
		return S_OK;
	}

	if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
	{
		ErrMsg(GetLastError(),NULL);
		return S_OK;
	}
	BOOL len=FromUTF16(OLE2W(szIn),NULL,0);
	char* bytes=new char[len];
	FromUTF16(OLE2W(szIn),bytes,len);
	if (!CryptHashData(hHash, (const BYTE*)bytes, len-1, 0))
	{
		CryptReleaseContext(hProv, 0);
		CryptDestroyHash(hHash);
		ErrMsg(GetLastError(),NULL);
		return S_OK;
	}
   
	BYTE rgbHash[16]={0};
	DWORD cbHash = 16;
	if (!CryptGetHashParam(hHash, HP_HASHVAL, rgbHash, &cbHash, 0)){
		CryptReleaseContext(hProv, 0);
		CryptDestroyHash(hHash);
		ErrMsg(GetLastError(),NULL);
		return S_OK;
	}
	size_t sizeMD5=33;
	TCHAR szMD5[33]={0};
	ToHexString(rgbHash,16,szMD5,&sizeMD5);
	*szResult=SysAllocString(szMD5);

	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv, 0);
	return S_OK;
}