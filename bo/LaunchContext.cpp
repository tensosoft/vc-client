// LaunchContext.cpp : LaunchContext 的实现

#include "stdafx.h"
#include "LaunchContext.h"
#include <mshtml.h>

// LaunchContext


STDMETHODIMP LaunchContext::get_TargetUrl(BSTR* pVal)
{
	ATLASSERT(pVal);
	if(_tcslen(this->m_szUrl)==0 && this->m_spWebBrowser2){
		BSTR bstrUrl=NULL;
		this->m_spWebBrowser2->get_LocationURL(&bstrUrl);
		if(bstrUrl && _tcslen(bstrUrl)<STRLEN_4K) {
			_tcscpy_s(this->m_szUrl,bstrUrl);
		}
		FREE_SYS_STR(bstrUrl);
	}
	*pVal=SysAllocString(this->m_szUrl);
	return S_OK;
}

STDMETHODIMP LaunchContext::put_TargetUrl(BSTR newVal)
{
	ATLASSERT(newVal);

	if(_tcslen(newVal)>=STRLEN_4K){
		ErrMsg(0,L"设置的目标Url过长！");
		return S_OK;
	}
	ZeroMemory(this->m_szUrl,sizeof(TCHAR)*STRLEN_4K);
	_tcscpy_s(this->m_szUrl,newVal);
	return S_OK;
}

STDMETHODIMP LaunchContext::get_UNID(BSTR* pVal)
{
	ATLASSERT(pVal);
	*pVal=SysAllocString(this->m_szUNID);
	return S_OK;
}

STDMETHODIMP LaunchContext::put_UNID(BSTR newVal)
{
	ATLASSERT(newVal);
	if(_tcslen(newVal)!=32){
		ErrMsg(0,L"设置的UNID不合法！");
		return S_OK;
	}
	_tcscpy_s(this->m_szUNID,newVal);
	return StoreObject();
}

STDMETHODIMP LaunchContext::get_PUNID(BSTR* pVal)
{
	ATLASSERT(pVal);
	*pVal=SysAllocString(this->m_szPUNID);
	return S_OK;
}

STDMETHODIMP LaunchContext::put_PUNID(BSTR newVal)
{
	ATLASSERT(newVal);
	if(_tcslen(newVal)!=32){
		ErrMsg(0,L"设置的UNID不合法！");
		return S_OK;
	}
	_tcscpy_s(this->m_szPUNID,newVal);
	return S_OK;
}

STDMETHODIMP LaunchContext::get_Cookie(BSTR* pVal)
{
	ATLASSERT(pVal);
	if(_tcslen(this->m_szCookie)==0 && m_spWebBrowser2){
		CComPtr<IHTMLDocument2> spDoc = NULL;
		HRESULT hr=this->m_spWebBrowser2->get_Document((IDispatch**)&spDoc);
		if(SUCCEEDED(hr) && spDoc){
			BSTR bstrCookie=NULL;
			hr=spDoc->get_cookie(&bstrCookie);
			if(SUCCEEDED(hr) && bstrCookie && _tcslen(bstrCookie)<STRLEN_4K) {
				_tcscpy_s(this->m_szCookie,bstrCookie);
			}
			if(bstrCookie) {
				FREE_SYS_STR(bstrCookie);
			}
		}
	}
	*pVal=SysAllocString(this->m_szCookie);
	return S_OK;
}

STDMETHODIMP LaunchContext::put_Cookie(BSTR newVal)
{
	if(_tcslen(newVal)>=STRLEN_4K){
		ErrMsg(0,L"设置的目标Cookie信息过长！");
		return S_OK;
	}
	ZeroMemory(this->m_szCookie,sizeof(TCHAR)*STRLEN_4K);
	_tcscpy_s(this->m_szCookie,newVal);
	return S_OK;
}

STDMETHODIMP LaunchContext::SetParameter(BSTR szParamName, VARIANT varParamValue)
{
	if(!szParamName || _tcslen(szParamName)==0) {
		ErrMsg(0,L"参数名不合法！");
		return E_FAIL;
	}

	CComBSTR key(szParamName);

	if(varParamValue.vt==VT_NULL){
		ParameterItr itr=m_params.find(key);
		if (itr != m_params.end()){
			m_params.erase(itr);
		}
		return S_OK;
	}
	
	m_params[key] = varParamValue;
	return S_OK;
}

STDMETHODIMP LaunchContext::GetParameterValue(BSTR szParamName, VARIANT* varParamValue)
{
	if (varParamValue == NULL) return E_POINTER;
	ParameterItr itr=m_params.find(CComBSTR(szParamName));
	if (itr == m_params.end()){
		(* varParamValue).vt=VT_NULL;
		return S_OK;
	}
	return CComVariant(itr->second).Detach(varParamValue);
}

STDMETHODIMP LaunchContext::GetParameterNames(VARIANT* pvars)
{
	CComSafeArray<VARIANT> sa;
	ParameterItr itr;
	for ( itr = m_params.begin( ) ; itr!= m_params.end( ) ; itr++ ){
		sa.Add(CComVariant(itr->first));
	}
	CComVariant var(sa);
	var.Detach(pvars);
	return S_OK;
}

STDMETHODIMP LaunchContext::GetParameterCount(LONG* lCount)
{
	if (lCount == NULL) return E_POINTER;
	*lCount=(LONG)m_params.size();
	return S_OK;
}

HRESULT LaunchContext::StoreObject(){
	if(this->m_dwROTRegister!=0) return S_OK;

	HRESULT hr=S_OK;
	if(!m_spCtx){
		hr=CreateBindCtx(0,&m_spCtx);
		if(FAILED(hr) || !m_spCtx){
			ErrMsg(0,L"无法初始化对象注册上下文！");
			return S_OK;
		}
	}

	if(m_spCtx && !m_spROT){
		hr=m_spCtx->GetRunningObjectTable(&m_spROT);
		if(FAILED(hr) || !m_spROT){
			ErrMsg(0,L"无法初始化对象注册表！");
			return S_OK;
		}
	}

	//直接通过GetRunningObjectTable获取IRunningObjectTable也可以
	/*HRESULT hr=GetRunningObjectTable(0, &m_spROT);
	if(FAILED(hr) || !m_spROT){
		ErrMsg(0,L"无法获取对象列表！");
		return S_OK;
	}*/

	hr=CreateItemMoniker(L"!",this->m_szUNID,&m_spMon);
	if(FAILED(hr) || !m_spMon){
		ErrMsg(0,L"无法创建对象别名！");
		return S_OK;
	}

	IUnknown* pvUnk=NULL;
	hr=this->QueryInterface(IID_IUnknown,(void**)&pvUnk);
	if(FAILED(hr) || !pvUnk){
		ErrMsg(0,L"无法访问对象！");
		return S_OK;
	}
	this->m_dwROTRegister=0;
	hr=m_spROT->Register(ROTFLAGS_REGISTRATIONKEEPSALIVE,pvUnk,m_spMon,&this->m_dwROTRegister);	//ROTFLAGS_ALLOWANYCLIENT|
	if(FAILED(hr) || !m_spMon){
		ErrMsg(0,L"无法注册对象！"); 
		pvUnk->Release();
		return S_OK;
	}
	pvUnk->Release();
	return S_OK;
}
STDMETHODIMP LaunchContext::RemoveFromRegister(void)
{
	if(this->m_dwROTRegister>0 && this->m_spROT){
		this->m_spROT->Revoke(this->m_dwROTRegister);
		if(this->m_spMon) this->m_spMon.Release();
		this->m_spROT.Release();
		this->m_dwROTRegister=0;
	}

	return S_OK;
}

STDMETHODIMP LaunchContext::get_Browser(IDispatch** pVal)
{
	if(pVal==NULL) return E_POINTER;
	*pVal=NULL;
	if(this->m_spWebBrowser2) {
		m_spWebBrowser2->QueryInterface(IID_IDispatch,(void**)pVal);
	}

	return S_OK;
}

STDMETHODIMP LaunchContext::put_Browser(IDispatch* newVal)
{
	if(newVal){
		newVal->QueryInterface(IID_IWebBrowser2,(void**)&m_spWebBrowser2);
		newVal->Release();
	}
	return S_OK;
}

STDMETHODIMP LaunchContext::get_FilePath(BSTR* pVal)
{
	if(pVal==NULL) return E_POINTER;
	*pVal=SysAllocString(this->m_szFilePath);
	return S_OK;
}

STDMETHODIMP LaunchContext::put_FilePath(BSTR newVal)
{
	size_t len=_tcslen(newVal);
	if(len==0 || len>=MAX_PATH){
		ErrMsg(0,L"文件名不合法！");
		return S_OK;
	}
	_tcscpy_s(m_szFilePath,newVal);
	return S_OK;
}

STDMETHODIMP LaunchContext::get_StyleTemplate(BSTR* pVal){
	if(pVal==NULL) return E_POINTER;
	*pVal=SysAllocString(this->m_szStyleTemplate);
	return S_OK;
}
STDMETHODIMP LaunchContext::put_StyleTemplate(BSTR newVal){
	size_t len=_tcslen(newVal);
	if(len==0 || len>=MAX_PATH){
		ErrMsg(0,L"文件名不合法！");
		return S_OK;
	}
	_tcscpy_s(m_szStyleTemplate,newVal);
	return S_OK;
}

STDMETHODIMP LaunchContext::get_CodeTemplate(BSTR* pVal){
	if(pVal==NULL) return E_POINTER;
	* pVal=NULL;
	*pVal=SysAllocString(this->m_szCodeTemplate);
	return S_OK;
}
STDMETHODIMP LaunchContext::put_CodeTemplate(BSTR newVal){
	size_t len=_tcslen(newVal);
	if(len==0 || len>=MAX_PATH){
		ErrMsg(0,L"文件名不合法！");
		return S_OK;
	}
	_tcscpy_s(m_szCodeTemplate,newVal);
	return S_OK;
}

STDMETHODIMP LaunchContext::get_UserName(BSTR* pVal){
	if(pVal==NULL) return E_POINTER;
	* pVal=NULL;
	*pVal=SysAllocString(this->m_szUserName);
	return S_OK;
}
STDMETHODIMP LaunchContext::put_UserName(BSTR newVal){
	size_t len=_tcslen(newVal);
	_tcscpy_s(m_szUserName,newVal);
	return S_OK;
}

STDMETHODIMP LaunchContext::get_Window(IDispatch** pVal){
	* pVal=NULL;
	const TCHAR* szErrTip=L"无法获取浏览器窗口对象！";
	if(!this->m_spWebBrowser2) {ErrMsg(0,L"浏览器对象为空！");return S_OK;}
	IDispatch* pDisp=NULL;
	IHTMLDocument2* pHtmlDoc2=NULL;
	HRESULT hr=this->m_spWebBrowser2->get_Document(&pDisp);
	if(SUCCEEDED(hr) && pDisp){
		hr=pDisp->QueryInterface(IID_IHTMLDocument2,(void**)&pHtmlDoc2);
		if(SUCCEEDED(hr)){
			CComPtr<IHTMLWindow2> pWin;
			hr=pHtmlDoc2->get_parentWindow(&pWin);
			if(SUCCEEDED(hr)){
				hr=pWin->QueryInterface(IID_IDispatch,(void**)pVal);
				if(FAILED(hr) || *pVal==NULL){ErrMsg(0,szErrTip);}
			}else{
				TCHAR errtip[MAX_PATH]={0};
				_stprintf_s(errtip,L"无法获取浏览器窗口对象：0x%X",hr);
				ErrMsg(0,errtip);
			}
		}else {
			TCHAR errtip[MAX_PATH]={0};
			_stprintf_s(errtip,L"无法获取文档对象：0x%X",hr);
			ErrMsg(0,errtip);
		}
	}else{
		TCHAR errtip[MAX_PATH]={0};
		_stprintf_s(errtip,L"无法获取文档对象：0x%X",hr);
		ErrMsg(0,errtip);
	}
	SAFE_RELEASE(pHtmlDoc2);
	SAFE_RELEASE(pDisp);
	return S_OK;
}

STDMETHODIMP LaunchContext::get_AccessoryAction(INT* pVal){
	*pVal=this->m_accessoryAction;
	return S_OK;
}

STDMETHODIMP LaunchContext::put_AccessoryAction(INT newVal){
	this->m_accessoryAction=newVal;
	return S_OK;
}

STDMETHODIMP LaunchContext::get_IntegrationAction(INT* pVal){
	*pVal=this->m_integrationAction;
	return S_OK;
}

STDMETHODIMP LaunchContext::put_IntegrationAction(INT newVal){
	this->m_integrationAction=newVal;
	return S_OK;
}

STDMETHODIMP LaunchContext::SwitchToBrowser(void){
	if(!this->m_spWebBrowser2) return S_OK;
	HWND hwndBrowser=NULL;
	this->m_spWebBrowser2->get_HWND((SHANDLE_PTR*)&hwndBrowser);
	if(hwndBrowser){
		BringWindowToTop(hwndBrowser);
	}
	return S_OK;
}