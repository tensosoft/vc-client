// LaunchContextProvider.cpp : LaunchContextProvider 的实现

#include "stdafx.h"
#include "LaunchContextProvider.h"


// LaunchContextProvider


STDMETHODIMP LaunchContextProvider::GetLaunchContext(BSTR szUNID,VARIANT_BOOL blCreateNewIfNoExist,IDispatch** pVal)
{
	if (pVal == NULL) return E_POINTER;
	*pVal=NULL;

	VARIANT_BOOL createNewIfNoExist=VARIANT_FALSE;
	if(blCreateNewIfNoExist) createNewIfNoExist=VARIANT_TRUE;

	size_t keyLen=_tcslen(szUNID);
	if(keyLen!=32){
		ErrMsg(0,L"没有指定UNID或UNID不合法！");
		return S_OK;
	}
	HRESULT hr=S_OK;

	if(!m_spCtx){
		hr=CreateBindCtx(0,&m_spCtx);
		if(FAILED(hr) || !m_spCtx){
			ErrMsg(0,L"无法初始化对象注册上下文！");
			return S_OK;
		}
	}

	if(m_spCtx && !m_spROT){
		hr=m_spCtx->GetRunningObjectTable(&m_spROT);
		if(FAILED(hr) || !m_spROT){
			ErrMsg(0,L"无法初始化对象注册表！");
			return S_OK;
		}
	}
	
	CComPtr<IEnumMoniker> spEnumMon=NULL;
	hr=m_spROT->EnumRunning(&spEnumMon);
	if(hr==E_OUTOFMEMORY || FAILED(hr)){
		ErrMsg(0,L"无法获取对象注册表集合！");
		return S_OK;
	}
	
	TCHAR szKeyStart[MAX_PATH]={0};
	_tcscpy_s(szKeyStart,L"!");
	_tcscat_s(szKeyStart,szUNID);
	
	BOOL matched=FALSE;

	IMoniker* spMon=NULL;
	ULONG fetched=0;
	
	while(spEnumMon->Next(1,&spMon,&fetched)==S_OK){		
		LPOLESTR pDisplayName =(LPOLESTR)HeapAlloc(GetProcessHeap(),0,sizeof(TCHAR)*STRLEN_1K);
		if(!pDisplayName){
			ErrMsg(GetLastError(),L"无法分配内存：%s！");
			return S_OK;
		}
		ZeroMemory(pDisplayName,sizeof(TCHAR)*STRLEN_1K);
		hr=spMon->GetDisplayName(m_spCtx,NULL,&pDisplayName);
		if (SUCCEEDED(hr) && _tcsicmp(pDisplayName,szKeyStart)==0){
			IUnknown* pUnk=NULL;
			hr=m_spROT->GetObject(spMon,&pUnk);
			if(FAILED(hr)){
				ErrMsg(GetLastError(),L"无法获取对象！");
				return S_OK;
			}
			hr=pUnk->QueryInterface(IID_IDispatch,(void**)pVal);
			if(FAILED(hr)){
				ErrMsg(GetLastError(),L"无法获取返回对象！");
				pUnk->Release();
				return S_OK;
			}
			pUnk->Release();
			matched=TRUE;
		}
		if(HeapFree(GetProcessHeap(), 0, pDisplayName)==0){
			ErrMsg(GetLastError(),L"无法释放内存：%s！");
			return S_OK;
		}
		if(matched) break;	//找到匹配则退出
	}
	if(matched) return S_OK;

	// 没找到已经注册的，则根据选项决定是否新建一个并返回。
	if(createNewIfNoExist){
		CoInitialize(NULL);
		{
			CComPtr<ILaunchContext> spLC=NULL;
			hr=CoCreateInstance(__uuidof(LaunchContext),NULL, CLSCTX_INPROC_SERVER,__uuidof(ILaunchContext), (void**)&spLC);
			if(FAILED(hr)){
				ErrMsg(GetLastError(),L"无法创建LaunchContext对象！");
				return S_OK;
			}
			hr=spLC->put_UNID(szUNID);
			if(FAILED(hr)){
				ErrMsg(GetLastError(),L"无法设置对象UNID属性！");
				spLC.Release();
				return S_OK;
			}
			if(this->m_spWebBrowser2){
				hr=spLC->put_Browser(m_spWebBrowser2);
				if(FAILED(hr)){
					ErrMsg(GetLastError(),L"无法设置对象Browser属性！");
					spLC.Release();
					return S_OK;
				}
			}
			hr=spLC->QueryInterface(IID_IDispatch,(void**)pVal);
			if(FAILED(hr)){
				ErrMsg(GetLastError(),L"无法获取返回结果！");
				spLC.Release();
				return S_OK;
			}
			spLC.Release();
		}
		CoUninitialize();
	}
	
	return S_OK;
}
