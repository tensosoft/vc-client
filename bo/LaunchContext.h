// LaunchContext.h : LaunchContext 的声明

#pragma once
#include "resource.h"       // 主符号
#include "bo_i.h"
#include <Exdisp.h>
#include <atlsafe.h>
#include <map>
using namespace std;

typedef std::map<CComBSTR,CComVariant> ParameterType;
typedef std::pair<CComBSTR, CComVariant> ParameterPair;
typedef std::map<CComBSTR, CComVariant>::iterator	ParameterItr;

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif

DEFINE_GUID(SID_STopLevelBrowser,       0x4C96BE40L, 0x915C, 0x11CF, 0x99, 0xD3, 0x00, 0xAA, 0x00, 0x4A, 0xE8, 0x37);
#define SID_SWebBrowserApp    IID_IWebBrowserApp

// LaunchContext

class ATL_NO_VTABLE LaunchContext :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<LaunchContext, &CLSID_LaunchContext>,
	public IObjectWithSiteImpl<LaunchContext>,
	public IDispatchImpl<ILaunchContext, &IID_ILaunchContext, &LIBID_boLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IObjectSafetyImpl<LaunchContext,INTERFACESAFE_FOR_UNTRUSTED_CALLER |INTERFACESAFE_FOR_UNTRUSTED_DATA>
{
public:
	LaunchContext():m_spWebBrowser2(NULL),m_spCtx(NULL),m_spROT(NULL),m_spMon(NULL),m_dwROTRegister(0)
	{
		ZeroMemory(m_szUrl,sizeof(TCHAR)*STRLEN_4K);
		ZeroMemory(m_szUNID,sizeof(TCHAR)*STRLEN_SMALL);
		ZeroMemory(m_szPUNID,sizeof(TCHAR)*STRLEN_SMALL);
		ZeroMemory(m_szCookie,sizeof(TCHAR)*STRLEN_4K);
		ZeroMemory(m_szFilePath,sizeof(TCHAR)*MAX_PATH);
		ZeroMemory(m_szStyleTemplate,sizeof(TCHAR)*MAX_PATH);
		ZeroMemory(m_szCodeTemplate,sizeof(TCHAR)*MAX_PATH);
		ZeroMemory(m_szUserName,sizeof(TCHAR)*STRLEN_NORMAL);
		m_accessoryAction=0;
		m_integrationAction=0;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_LAUNCHCONTEXT)


BEGIN_COM_MAP(LaunchContext)
	COM_INTERFACE_ENTRY(ILaunchContext)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IObjectWithSite)
	COM_INTERFACE_ENTRY(IObjectSafety)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
		if(m_spWebBrowser2) {
			m_spWebBrowser2.Release();
			m_spWebBrowser2=NULL;
		}
	}

	//获取浏览器对象的引用
	STDMETHODIMP LaunchContext::SetSite(IUnknown *pUnkSite)
	{
		//if (!pUnkSite)
		//{
		//	m_spWebBrowser2.Release();
		//	m_spWebBrowser2=NULL;
		//	return S_OK;
		//}
		//CComQIPtr<IServiceProvider> spProv(pUnkSite);
		//if (spProv && !this->m_spWebBrowser2)
		//{
		//	spProv->QueryService(IID_IWebBrowserApp, IID_IWebBrowser2,reinterpret_cast<void **>(&m_spWebBrowser2));
		//	/*CComPtr<IServiceProvider> spProv2;
		//	HRESULT hr = spProv->QueryService(SID_STopLevelBrowser, IID_IServiceProvider, reinterpret_cast<void **>(&spProv2));
		//	if (SUCCEEDED(hr) && spProv2){
		//		 hr=spProv2->QueryService(SID_SWebBrowserApp,IID_IWebBrowser2, reinterpret_cast<void **>(&m_spWebBrowser2));
		//	}*/
		//}
		return S_OK;
	}
private:
	CComPtr<IWebBrowser2> m_spWebBrowser2;		//浏览器对象
	TCHAR m_szUrl[STRLEN_4K];
	TCHAR m_szUNID[STRLEN_SMALL];
	TCHAR m_szPUNID[STRLEN_SMALL];
	TCHAR m_szCookie[STRLEN_4K];
	TCHAR m_szFilePath[MAX_PATH];
	TCHAR m_szStyleTemplate[MAX_PATH];
	TCHAR m_szCodeTemplate[MAX_PATH];
	TCHAR m_szUserName[STRLEN_NORMAL];
	INT m_accessoryAction;
	INT m_integrationAction;
	ParameterType m_params;

	DWORD m_dwROTRegister;
	CComPtr<IBindCtx> m_spCtx;
	CComPtr<IMoniker> m_spMon;
	CComPtr<IRunningObjectTable> m_spROT;
	HRESULT StoreObject();
public:
	STDMETHOD(get_TargetUrl)(BSTR* pVal);
	STDMETHOD(put_TargetUrl)(BSTR newVal);
	STDMETHOD(get_UNID)(BSTR* pVal);
	STDMETHOD(put_UNID)(BSTR newVal);
	STDMETHOD(get_PUNID)(BSTR* pVal);
	STDMETHOD(put_PUNID)(BSTR newVal);
	STDMETHOD(get_Cookie)(BSTR* pVal);
	STDMETHOD(put_Cookie)(BSTR newVal);
	STDMETHOD(SetParameter)(BSTR szParamName, VARIANT varParamValue);
	STDMETHOD(GetParameterValue)(BSTR szParamName, VARIANT* varParamValue);
	STDMETHOD(GetParameterNames)(VARIANT* pvars);
	STDMETHOD(GetParameterCount)(LONG* lCount);
	STDMETHOD(RemoveFromRegister)(void);
	STDMETHOD(get_Browser)(IDispatch** pVal);
	STDMETHOD(put_Browser)(IDispatch* newVal);
	STDMETHOD(get_FilePath)(BSTR* pVal);
	STDMETHOD(put_FilePath)(BSTR newVal);
	STDMETHOD(get_StyleTemplate)(BSTR* pVal);
	STDMETHOD(put_StyleTemplate)(BSTR newVal);
	STDMETHOD(get_CodeTemplate)(BSTR* pVal);
	STDMETHOD(put_CodeTemplate)(BSTR newVal);
	STDMETHOD(get_UserName)(BSTR* pVal);
	STDMETHOD(put_UserName)(BSTR newVal);
	STDMETHOD(get_Window)(IDispatch** pVal);
	STDMETHOD(get_AccessoryAction)(INT* pVal);
	STDMETHOD(put_AccessoryAction)(INT newVal);
	STDMETHOD(get_IntegrationAction)(INT* pVal);
	STDMETHOD(put_IntegrationAction)(INT newVal);
	STDMETHOD(SwitchToBrowser)(void);
};

OBJECT_ENTRY_AUTO(__uuidof(LaunchContext), LaunchContext)