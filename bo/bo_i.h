

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu Apr 10 11:16:39 2014
 */
/* Compiler settings for bo.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __bo_i_h__
#define __bo_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ILaunchContext_FWD_DEFINED__
#define __ILaunchContext_FWD_DEFINED__
typedef interface ILaunchContext ILaunchContext;
#endif 	/* __ILaunchContext_FWD_DEFINED__ */


#ifndef __ILaunchContextProvider_FWD_DEFINED__
#define __ILaunchContextProvider_FWD_DEFINED__
typedef interface ILaunchContextProvider ILaunchContextProvider;
#endif 	/* __ILaunchContextProvider_FWD_DEFINED__ */


#ifndef __LaunchContext_FWD_DEFINED__
#define __LaunchContext_FWD_DEFINED__

#ifdef __cplusplus
typedef class LaunchContext LaunchContext;
#else
typedef struct LaunchContext LaunchContext;
#endif /* __cplusplus */

#endif 	/* __LaunchContext_FWD_DEFINED__ */


#ifndef __LaunchContextProvider_FWD_DEFINED__
#define __LaunchContextProvider_FWD_DEFINED__

#ifdef __cplusplus
typedef class LaunchContextProvider LaunchContextProvider;
#else
typedef struct LaunchContextProvider LaunchContextProvider;
#endif /* __cplusplus */

#endif 	/* __LaunchContextProvider_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __ILaunchContext_INTERFACE_DEFINED__
#define __ILaunchContext_INTERFACE_DEFINED__

/* interface ILaunchContext */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ILaunchContext;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C16E9A38-1241-4ADC-B3CA-4AA16E7FD5A8")
    ILaunchContext : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TargetUrl( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TargetUrl( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UNID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UNID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PUNID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PUNID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Cookie( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Cookie( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE SetParameter( 
            /* [in] */ BSTR szParamName,
            /* [in] */ VARIANT varParamValue) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetParameterValue( 
            /* [in] */ BSTR szParamName,
            /* [retval][out] */ VARIANT *varParamValue) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetParameterNames( 
            /* [retval][out] */ VARIANT *pvars) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetParameterCount( 
            /* [retval][out] */ LONG *lCount) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveFromRegister( void) = 0;
        
        virtual /* [helpcontext][helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Browser( 
            /* [retval][out] */ IDispatch **pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Browser( 
            /* [in] */ IDispatch *newVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FilePath( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FilePath( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StyleTemplate( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StyleTemplate( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CodeTemplate( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CodeTemplate( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UserName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UserName( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Window( 
            /* [retval][out] */ IDispatch **pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AccessoryAction( 
            /* [retval][out] */ INT *pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AccessoryAction( 
            /* [in] */ INT newVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IntegrationAction( 
            /* [retval][out] */ INT *pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_IntegrationAction( 
            /* [in] */ INT newVal) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE SwitchToBrowser( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILaunchContextVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILaunchContext * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILaunchContext * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILaunchContext * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILaunchContext * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILaunchContext * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILaunchContext * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILaunchContext * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpcontext][helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TargetUrl )( 
            ILaunchContext * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpcontext][helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TargetUrl )( 
            ILaunchContext * This,
            /* [in] */ BSTR newVal);
        
        /* [helpcontext][helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UNID )( 
            ILaunchContext * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpcontext][helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UNID )( 
            ILaunchContext * This,
            /* [in] */ BSTR newVal);
        
        /* [helpcontext][helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PUNID )( 
            ILaunchContext * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpcontext][helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PUNID )( 
            ILaunchContext * This,
            /* [in] */ BSTR newVal);
        
        /* [helpcontext][helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Cookie )( 
            ILaunchContext * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpcontext][helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Cookie )( 
            ILaunchContext * This,
            /* [in] */ BSTR newVal);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetParameter )( 
            ILaunchContext * This,
            /* [in] */ BSTR szParamName,
            /* [in] */ VARIANT varParamValue);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetParameterValue )( 
            ILaunchContext * This,
            /* [in] */ BSTR szParamName,
            /* [retval][out] */ VARIANT *varParamValue);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetParameterNames )( 
            ILaunchContext * This,
            /* [retval][out] */ VARIANT *pvars);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetParameterCount )( 
            ILaunchContext * This,
            /* [retval][out] */ LONG *lCount);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveFromRegister )( 
            ILaunchContext * This);
        
        /* [helpcontext][helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Browser )( 
            ILaunchContext * This,
            /* [retval][out] */ IDispatch **pVal);
        
        /* [helpcontext][helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Browser )( 
            ILaunchContext * This,
            /* [in] */ IDispatch *newVal);
        
        /* [helpcontext][helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FilePath )( 
            ILaunchContext * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpcontext][helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FilePath )( 
            ILaunchContext * This,
            /* [in] */ BSTR newVal);
        
        /* [helpcontext][helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StyleTemplate )( 
            ILaunchContext * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpcontext][helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StyleTemplate )( 
            ILaunchContext * This,
            /* [in] */ BSTR newVal);
        
        /* [helpcontext][helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CodeTemplate )( 
            ILaunchContext * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpcontext][helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CodeTemplate )( 
            ILaunchContext * This,
            /* [in] */ BSTR newVal);
        
        /* [helpcontext][helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UserName )( 
            ILaunchContext * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpcontext][helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UserName )( 
            ILaunchContext * This,
            /* [in] */ BSTR newVal);
        
        /* [helpcontext][helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Window )( 
            ILaunchContext * This,
            /* [retval][out] */ IDispatch **pVal);
        
        /* [helpcontext][helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AccessoryAction )( 
            ILaunchContext * This,
            /* [retval][out] */ INT *pVal);
        
        /* [helpcontext][helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AccessoryAction )( 
            ILaunchContext * This,
            /* [in] */ INT newVal);
        
        /* [helpcontext][helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IntegrationAction )( 
            ILaunchContext * This,
            /* [retval][out] */ INT *pVal);
        
        /* [helpcontext][helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_IntegrationAction )( 
            ILaunchContext * This,
            /* [in] */ INT newVal);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SwitchToBrowser )( 
            ILaunchContext * This);
        
        END_INTERFACE
    } ILaunchContextVtbl;

    interface ILaunchContext
    {
        CONST_VTBL struct ILaunchContextVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILaunchContext_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILaunchContext_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILaunchContext_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILaunchContext_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILaunchContext_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILaunchContext_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILaunchContext_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILaunchContext_get_TargetUrl(This,pVal)	\
    ( (This)->lpVtbl -> get_TargetUrl(This,pVal) ) 

#define ILaunchContext_put_TargetUrl(This,newVal)	\
    ( (This)->lpVtbl -> put_TargetUrl(This,newVal) ) 

#define ILaunchContext_get_UNID(This,pVal)	\
    ( (This)->lpVtbl -> get_UNID(This,pVal) ) 

#define ILaunchContext_put_UNID(This,newVal)	\
    ( (This)->lpVtbl -> put_UNID(This,newVal) ) 

#define ILaunchContext_get_PUNID(This,pVal)	\
    ( (This)->lpVtbl -> get_PUNID(This,pVal) ) 

#define ILaunchContext_put_PUNID(This,newVal)	\
    ( (This)->lpVtbl -> put_PUNID(This,newVal) ) 

#define ILaunchContext_get_Cookie(This,pVal)	\
    ( (This)->lpVtbl -> get_Cookie(This,pVal) ) 

#define ILaunchContext_put_Cookie(This,newVal)	\
    ( (This)->lpVtbl -> put_Cookie(This,newVal) ) 

#define ILaunchContext_SetParameter(This,szParamName,varParamValue)	\
    ( (This)->lpVtbl -> SetParameter(This,szParamName,varParamValue) ) 

#define ILaunchContext_GetParameterValue(This,szParamName,varParamValue)	\
    ( (This)->lpVtbl -> GetParameterValue(This,szParamName,varParamValue) ) 

#define ILaunchContext_GetParameterNames(This,pvars)	\
    ( (This)->lpVtbl -> GetParameterNames(This,pvars) ) 

#define ILaunchContext_GetParameterCount(This,lCount)	\
    ( (This)->lpVtbl -> GetParameterCount(This,lCount) ) 

#define ILaunchContext_RemoveFromRegister(This)	\
    ( (This)->lpVtbl -> RemoveFromRegister(This) ) 

#define ILaunchContext_get_Browser(This,pVal)	\
    ( (This)->lpVtbl -> get_Browser(This,pVal) ) 

#define ILaunchContext_put_Browser(This,newVal)	\
    ( (This)->lpVtbl -> put_Browser(This,newVal) ) 

#define ILaunchContext_get_FilePath(This,pVal)	\
    ( (This)->lpVtbl -> get_FilePath(This,pVal) ) 

#define ILaunchContext_put_FilePath(This,newVal)	\
    ( (This)->lpVtbl -> put_FilePath(This,newVal) ) 

#define ILaunchContext_get_StyleTemplate(This,pVal)	\
    ( (This)->lpVtbl -> get_StyleTemplate(This,pVal) ) 

#define ILaunchContext_put_StyleTemplate(This,newVal)	\
    ( (This)->lpVtbl -> put_StyleTemplate(This,newVal) ) 

#define ILaunchContext_get_CodeTemplate(This,pVal)	\
    ( (This)->lpVtbl -> get_CodeTemplate(This,pVal) ) 

#define ILaunchContext_put_CodeTemplate(This,newVal)	\
    ( (This)->lpVtbl -> put_CodeTemplate(This,newVal) ) 

#define ILaunchContext_get_UserName(This,pVal)	\
    ( (This)->lpVtbl -> get_UserName(This,pVal) ) 

#define ILaunchContext_put_UserName(This,newVal)	\
    ( (This)->lpVtbl -> put_UserName(This,newVal) ) 

#define ILaunchContext_get_Window(This,pVal)	\
    ( (This)->lpVtbl -> get_Window(This,pVal) ) 

#define ILaunchContext_get_AccessoryAction(This,pVal)	\
    ( (This)->lpVtbl -> get_AccessoryAction(This,pVal) ) 

#define ILaunchContext_put_AccessoryAction(This,newVal)	\
    ( (This)->lpVtbl -> put_AccessoryAction(This,newVal) ) 

#define ILaunchContext_get_IntegrationAction(This,pVal)	\
    ( (This)->lpVtbl -> get_IntegrationAction(This,pVal) ) 

#define ILaunchContext_put_IntegrationAction(This,newVal)	\
    ( (This)->lpVtbl -> put_IntegrationAction(This,newVal) ) 

#define ILaunchContext_SwitchToBrowser(This)	\
    ( (This)->lpVtbl -> SwitchToBrowser(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILaunchContext_INTERFACE_DEFINED__ */


#ifndef __ILaunchContextProvider_INTERFACE_DEFINED__
#define __ILaunchContextProvider_INTERFACE_DEFINED__

/* interface ILaunchContextProvider */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ILaunchContextProvider;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("11FD53CB-3E52-496D-84D3-0873CDF15A84")
    ILaunchContextProvider : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE GetLaunchContext( 
            /* [in] */ BSTR szUNID,
            /* [defaultvalue][in] */ VARIANT_BOOL blCreateNewIfNoExist,
            /* [retval][out] */ IDispatch **pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILaunchContextProviderVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILaunchContextProvider * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILaunchContextProvider * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILaunchContextProvider * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILaunchContextProvider * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILaunchContextProvider * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILaunchContextProvider * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILaunchContextProvider * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetLaunchContext )( 
            ILaunchContextProvider * This,
            /* [in] */ BSTR szUNID,
            /* [defaultvalue][in] */ VARIANT_BOOL blCreateNewIfNoExist,
            /* [retval][out] */ IDispatch **pVal);
        
        END_INTERFACE
    } ILaunchContextProviderVtbl;

    interface ILaunchContextProvider
    {
        CONST_VTBL struct ILaunchContextProviderVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILaunchContextProvider_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILaunchContextProvider_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILaunchContextProvider_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILaunchContextProvider_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILaunchContextProvider_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILaunchContextProvider_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILaunchContextProvider_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILaunchContextProvider_GetLaunchContext(This,szUNID,blCreateNewIfNoExist,pVal)	\
    ( (This)->lpVtbl -> GetLaunchContext(This,szUNID,blCreateNewIfNoExist,pVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILaunchContextProvider_INTERFACE_DEFINED__ */



#ifndef __boLib_LIBRARY_DEFINED__
#define __boLib_LIBRARY_DEFINED__

/* library boLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_boLib;

EXTERN_C const CLSID CLSID_LaunchContext;

#ifdef __cplusplus

class DECLSPEC_UUID("5F05762F-DF21-4689-B7FA-4A65DFB0BB46")
LaunchContext;
#endif

EXTERN_C const CLSID CLSID_LaunchContextProvider;

#ifdef __cplusplus

class DECLSPEC_UUID("52007948-92A7-4766-B325-D3D250A6575A")
LaunchContextProvider;
#endif
#endif /* __boLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


