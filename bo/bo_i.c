

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu Apr 10 11:16:39 2014
 */
/* Compiler settings for bo.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_ILaunchContext,0xC16E9A38,0x1241,0x4ADC,0xB3,0xCA,0x4A,0xA1,0x6E,0x7F,0xD5,0xA8);


MIDL_DEFINE_GUID(IID, IID_ILaunchContextProvider,0x11FD53CB,0x3E52,0x496D,0x84,0xD3,0x08,0x73,0xCD,0xF1,0x5A,0x84);


MIDL_DEFINE_GUID(IID, LIBID_boLib,0xFBAA1A3A,0xBEEB,0x4482,0x85,0x83,0x76,0x7A,0xD3,0xEE,0x8A,0x7D);


MIDL_DEFINE_GUID(CLSID, CLSID_LaunchContext,0x5F05762F,0xDF21,0x4689,0xB7,0xFA,0x4A,0x65,0xDF,0xB0,0xBB,0x46);


MIDL_DEFINE_GUID(CLSID, CLSID_LaunchContextProvider,0x52007948,0x92A7,0x4766,0xB3,0x25,0xD3,0xD2,0x50,0xA6,0x57,0x5A);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



