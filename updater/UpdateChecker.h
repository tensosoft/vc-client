// UpdateChecker.h : UpdateChecker 的声明

#pragma once
#include "resource.h"       // 主符号
#include "updater_i.h"
#include <Exdisp.h>

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif

// UpdateChecker

class ATL_NO_VTABLE UpdateChecker :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<UpdateChecker, &CLSID_UpdateChecker>,
	public IObjectWithSiteImpl<UpdateChecker>,
	public IDispatchImpl<IUpdateChecker, &IID_IUpdateChecker, &LIBID_updaterLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IObjectSafetyImpl<UpdateChecker,INTERFACESAFE_FOR_UNTRUSTED_CALLER |INTERFACESAFE_FOR_UNTRUSTED_DATA>
{
public:
	UpdateChecker()
	{
		ZeroMemory(this->m_updaterFile,sizeof(TCHAR)*MAX_PATH);
	}

DECLARE_REGISTRY_RESOURCEID(IDR_UPDATECHECKER)

BEGIN_COM_MAP(UpdateChecker)
	COM_INTERFACE_ENTRY(IUpdateChecker)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IObjectWithSite)
	COM_INTERFACE_ENTRY(IObjectSafety)
END_COM_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}
	//获取浏览器对象的引用
	STDMETHODIMP UpdateChecker::SetSite(IUnknown *pUnkSite){
		if (!pUnkSite){
			m_spWebBrowser2.Release();
			m_spWebBrowser2=NULL;
			return S_OK;
		}
		CComQIPtr<IServiceProvider> spProv(pUnkSite);
		if (spProv && !this->m_spWebBrowser2){
			spProv->QueryService(IID_IWebBrowserApp, IID_IWebBrowser2,reinterpret_cast<void **>(&m_spWebBrowser2));
			/*CComPtr<IServiceProvider> spProv2=NULL;
			HRESULT hr = spProv->QueryService(SID_STopLevelBrowser, IID_IServiceProvider, reinterpret_cast<void **>(&spProv2));
			if (SUCCEEDED(hr) && spProv2){
				 hr=spProv2->QueryService(SID_SWebBrowserApp,IID_IWebBrowser2, reinterpret_cast<void **>(&m_spWebBrowser2));
			}*/
		}
		return S_OK;
	}
private:
	TCHAR m_updaterFile[MAX_PATH];											//服务器上的更新包文件名。
	CComPtr<IWebBrowser2> m_spWebBrowser2;							//浏览器对象
public:
	STDMETHOD(Check)(BSTR szUrl,VARIANT_BOOL* pVal);
	STDMETHOD(LaunchUpdaterApp)(BSTR szUrl);
	STDMETHOD(CheckAndLaunchFirstrunInstall)(BSTR szUrl,BSTR szExt,BSTR szTitle);
	STDMETHOD(LoginSync)(VARIANT_BOOL* pVal);
};

OBJECT_ENTRY_AUTO(__uuidof(UpdateChecker), UpdateChecker)
