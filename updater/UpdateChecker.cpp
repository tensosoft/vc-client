// UpdateChecker.cpp : UpdateChecker 的实现

#include "stdafx.h"
#include "UpdateChecker.h"
#include "../commonfiles/MD5.h"
#include "../commonfiles/convutil.h"
#include "../commonfiles/cryptutil.h"
#include <urlmon.h>
#include <shlwapi.h>

// UpdateChecker
#define UPDATER_INI_FN	L"updater.ini"
#define UPDATER_EXE_FN	L"updater.exe"
#define UPDATER_FILE_PATH_NAME	L"client"
#define UPDATER_PACKAGE_SECTION	L"updater"
#define UPDATER_PACKAGE_FILENAME_KEY	L"file"
#define UPDATER_INI_DT_KEY	L"dt"

STDMETHODIMP UpdateChecker::Check(BSTR szUrl,VARIANT_BOOL* pVal)
{
	ATLASSERT(pVal);
	*pVal=VARIANT_FALSE;
	size_t urlLen=_tcslen(szUrl);
	if(urlLen<=0 || urlLen>=(STRLEN_1K-MAX_PATH)){
		ErrMsg(0,L"没有指定检查更新的Url基地址或Url基地址不合法！");
		return S_OK;
	}

	TCHAR szDownloadUrl[STRLEN_1K]={0};
	_tcscpy_s(szDownloadUrl,szUrl);
	if(szDownloadUrl[urlLen-1]!=L'/') _tcscat_s(szDownloadUrl,L"/");
	_tcscat_s(szDownloadUrl,UPDATER_FILE_PATH_NAME);
	_tcscat_s(szDownloadUrl,L"/");
	_tcscat_s(szDownloadUrl,UPDATER_INI_FN);

	TCHAR szTmpFile[MAX_PATH]={0};
	TCHAR szTempPath[MAX_PATH]={0};
	DWORD dwPathLen=GetTempPath(MAX_PATH,szTempPath);
	if(dwPathLen>0 && dwPathLen<=(MAX_PATH-14)){
		if(GetTempFileName(szTempPath,L"d2i",0,szTmpFile)==0){
			ErrMsg(GetLastError(),L"无法创建临时文件：%s");
			return S_OK;
		}
	}
	else{
		ErrMsg(GetLastError(),L"路径太长！");
		return S_OK;
	}

	HRESULT hr=URLDownloadToFile(NULL,szDownloadUrl,szTmpFile,0,NULL);
	if(hr==E_OUTOFMEMORY){
		ErrMsg(0,L"内存不足，无法下载更新配置文件，请联系管理员！");
		return S_OK;
	}
	if(hr==INET_E_DOWNLOAD_FAILURE){
		return S_OK;
	}
	if(!PathFileExists(szTmpFile)){
		return S_OK;
	}
	
	TCHAR szFileValue[MAX_PATH]={0};
	GetPrivateProfileString(UPDATER_PACKAGE_SECTION,UPDATER_PACKAGE_FILENAME_KEY,L"",szFileValue,MAX_PATH,szTmpFile);
	if(_tcslen(szFileValue)==0){
		goto end;
	}

	//检查本地已安装更新包配置信息是否已经包含此更新包
	TCHAR	szInstallPath[MAX_PATH]={0};
	dwPathLen=GetModuleFileName(_AtlBaseModule.m_hInst, szInstallPath, MAX_PATH);
	if(dwPathLen<=0 || GetLastError()==ERROR_INSUFFICIENT_BUFFER){
		DeleteFile(szTmpFile);		//删除临时文件
		ErrMsg(0,L"无法获取本机客户端组件安装路径！");
		return S_OK;
	}
	TCHAR* occur=_tcsrchr(szInstallPath,L'\\');
	if(occur)	*(occur+1)=L'\0';
	TCHAR szLocalIniFN[MAX_PATH+20]={0};
	_tcscpy_s(szLocalIniFN,szInstallPath);
	_tcscat_s(szLocalIniFN,UPDATER_INI_FN);
	if(!PathFileExists(szLocalIniFN)){
		*pVal=VARIANT_TRUE;
		goto end;
	}
	TCHAR szDt[STRLEN_NORMAL]={0};
	GetPrivateProfileString(szFileValue,UPDATER_INI_DT_KEY,L"",szDt,STRLEN_NORMAL,szLocalIniFN);
	if(_tcslen(szDt)==0) *pVal=VARIANT_TRUE;
end:
	DeleteFile(szTmpFile);		//删除临时文件

	if(*pVal==VARIANT_TRUE) _tcscpy_s(m_updaterFile,szFileValue);
	return S_OK;
}

STDMETHODIMP UpdateChecker::LaunchUpdaterApp(BSTR szUrl)
{
	if(_tcslen(this->m_updaterFile)==0){
		ErrMsg(0,L"请您先检查更新且确保当前有需要更新的内容！");
		return S_OK;
	}

	size_t urlLen=_tcslen(szUrl);
	if(urlLen<=0 || urlLen>=(STRLEN_4K-MAX_PATH)){
		ErrMsg(0,L"没有指定下载更新包的Url基地址或Url基地址不合法！");
		return S_OK;
	}

	TCHAR szParam[STRLEN_1K]={0};
	TCHAR szDownloadUrl[STRLEN_1K]={0};

	_tcscpy_s(szDownloadUrl,szUrl);
	if(szDownloadUrl[urlLen-1]!=L'/') _tcscat_s(szDownloadUrl,L"/");
	_tcscat_s(szDownloadUrl,UPDATER_FILE_PATH_NAME);
	_tcscat_s(szDownloadUrl,L"/");
	_tcscat_s(szDownloadUrl,m_updaterFile);
	_tcscpy_s(szParam,L"/u ");
	_tcscat_s(szParam,szDownloadUrl);

	TCHAR	szInstallPath[MAX_PATH]={0};
	DWORD dwPathLen=GetModuleFileName(_AtlBaseModule.m_hInst, szInstallPath, MAX_PATH);
	if(dwPathLen<=0 || GetLastError()==ERROR_INSUFFICIENT_BUFFER){
		ErrMsg(0,L"无法获取本机客户端组件安装路径！");
		return S_OK;
	}
	TCHAR* occur=_tcsrchr(szInstallPath,L'\\');
	if(occur)	*(occur+1)=L'\0';
	TCHAR szFN[MAX_PATH+20]={0};
	_tcscpy_s(szFN,szInstallPath);
	_tcscat_s(szFN,UPDATER_EXE_FN);
	if(!PathFileExists(szFN)){
		ErrMsg(0,L"Updater.exe文件不存在！");
		return S_OK;
	}

	SHELLEXECUTEINFO ShExecInfo;
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
  ShExecInfo.fMask =0;
  ShExecInfo.hwnd = GetForegroundWindow();
	ShExecInfo.lpVerb = L"open";
  ShExecInfo.lpFile =szFN;
  ShExecInfo.lpParameters = szParam;
  ShExecInfo.lpDirectory = szInstallPath;
  ShExecInfo.nShow = SW_SHOWNORMAL;
  ShExecInfo.hInstApp = NULL;

	HANDLE hMutex = CreateMutex(NULL,FALSE,L"discoverx2_updater");
	if (ShellExecuteEx(&ShExecInfo)) return S_OK;

	return S_OK;
}

STDMETHODIMP UpdateChecker::CheckAndLaunchFirstrunInstall(BSTR szUrl,BSTR szExt,BSTR szTitle){
	size_t urlLen=_tcslen(szUrl);
	if(urlLen<=0 || urlLen>=(STRLEN_1K-MAX_PATH)){
		ErrMsg(0,L"没有指定有效的目标系统url基础地址或地址不合法！");
		return S_OK;
	}
	TCHAR strKey[STRLEN_1K]={0};
	_tcscpy_s(strKey,szUrl);
	if(strKey[urlLen-1]==L'/') strKey[urlLen-1]=L'\0';
	TCHAR* szTmp=_tcsstr(strKey,L"://");

	TCHAR strUrl[STRLEN_1K]={0};
	_stprintf_s(strUrl,L"SOFTWARE\\Discoverx2\\%s",(szTmp?szTmp+3:strKey));

	TCHAR ext[STRLEN_SMALL]={0};
	_tcscpy_s(ext,szExt);
	TCHAR title[MAX_PATH]={0};
	_tcscpy_s(title,szTitle);

	HKEY hKey=NULL;
	LONG lRes = RegOpenKeyEx(HKEY_CURRENT_USER,strUrl, 0, KEY_ALL_ACCESS , &hKey);
	BOOL bExistsAndSuccess=(lRes == ERROR_SUCCESS);
	if(bExistsAndSuccess && hKey) {
		RegSetValueEx(hKey,L"ext",0,REG_SZ,(const BYTE*)ext,_tcslen(ext)*sizeof(TCHAR));
		RegSetValueEx(hKey,L"title",0,REG_SZ,(const BYTE*)title,_tcslen(title)*sizeof(TCHAR));
		RegSetValueEx(hKey,NULL,0,REG_SZ,(const BYTE*)title,_tcslen(title)*sizeof(TCHAR));
		RegCloseKey(hKey);
		return S_OK;
	}else if(RegCreateKeyEx(HKEY_CURRENT_USER,strUrl,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&hKey,NULL)==ERROR_SUCCESS){
		RegSetValueEx(hKey,L"ext",0,REG_SZ,(const BYTE*)ext,_tcslen(ext)*sizeof(TCHAR));
		RegSetValueEx(hKey,L"title",0,REG_SZ,(const BYTE*)title,_tcslen(title)*sizeof(TCHAR));
		RegSetValueEx(hKey,NULL,0,REG_SZ,(const BYTE*)title,_tcslen(title)*sizeof(TCHAR));
		RegCloseKey(hKey);
	}

	if(MessageBox(GetForegroundWindow(),L"系统检测到您是第一次访问系统，为了保证客户端功能正常运行系统需要执行初始化操作，是否执行初始化操作？",L"提示",MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON1)==IDNO){
		return S_OK;
	}

	TCHAR	szInstallPath[MAX_PATH]={0};
	DWORD dwPathLen=GetModuleFileName(_AtlBaseModule.m_hInst, szInstallPath, MAX_PATH);
	if(dwPathLen<=0 || GetLastError()==ERROR_INSUFFICIENT_BUFFER){
		ErrMsg(0,L"无法获取本机客户端组件安装路径！");
		return S_OK;
	}
	TCHAR* occur=_tcsrchr(szInstallPath,L'\\');
	if(occur)	*(occur+1)=L'\0';
	TCHAR szFN[MAX_PATH+20]={0};
	_tcscpy_s(szFN,szInstallPath);
	_tcscat_s(szFN,UPDATER_EXE_FN);
	if(!PathFileExists(szFN)){
		ErrMsg(0,L"Updater.exe文件不存在！");
		return S_OK;
	}

	TCHAR szParam[STRLEN_1K]={0};
	ZeroMemory(strUrl,sizeof(TCHAR)*STRLEN_1K);
	_tcscpy_s(strUrl,szUrl);
	_tcscpy_s(szParam,L"/i ");
	_tcscat_s(szParam,strUrl);

	SHELLEXECUTEINFO ShExecInfo;
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
  ShExecInfo.fMask =0;
  ShExecInfo.hwnd = GetForegroundWindow();
	ShExecInfo.lpVerb = L"open";
  ShExecInfo.lpFile =szFN;
  ShExecInfo.lpParameters = szParam;
  ShExecInfo.lpDirectory = szInstallPath;
  ShExecInfo.nShow = SW_SHOWNORMAL;
  ShExecInfo.hInstApp = NULL;

	HANDLE hMutex = CreateMutex(NULL,FALSE,L"discoverx2_updater");
	if (ShellExecuteEx(&ShExecInfo)) return S_OK;

	return S_OK;
}

STDMETHODIMP UpdateChecker::LoginSync(VARIANT_BOOL* pVal){
	*pVal=VARIANT_FALSE;
	CComPtr<IHTMLDocument2> doc2=NULL;
	CComPtr<IHTMLDocument3> doc3=NULL;
	CComPtr<IHTMLElement> el=NULL;
	CComPtr<IHTMLInputElement> elx=NULL;
	HRESULT hr=this->m_spWebBrowser2->get_Document((IDispatch**)&doc2);
	if (FAILED(hr)) goto end;
	hr=doc2->QueryInterface(IID_IHTMLDocument3,(void**)&doc3);
	if (FAILED(hr)) goto end;
	
	BSTR bstrUrl=NULL;
	BSTR bstrUID=SysAllocString(L"uid");
	BSTR bstrPwd=SysAllocString(L"password");
	BSTR bstrSystemName=SysAllocString(L"systemName");
	BSTR bstrUIDValue=NULL;
	BSTR bstrPwdValue=NULL;
	BSTR bstrSystemNameValue=NULL;
	TCHAR szExt[STRLEN_SMALL]={0};

	m_spWebBrowser2->get_LocationURL(&bstrUrl);
	TCHAR szUrl[STRLEN_DEFAULT]={0};
	if(bstrUrl && _tcslen(bstrUrl)>0)_tcscpy_s(szUrl,bstrUrl);
	_tcslwr_s(szUrl);

	TCHAR* szStr1=L"/tsim/login.";
	size_t szStr1Len=_tcslen(szStr1);
	TCHAR* szStr2=L"/login.";
	size_t szStr2Len=_tcslen(szStr2);
	BOOL isStr1=TRUE;

	TCHAR* szSub=_tcsstr(szUrl,szStr1);
	if(szSub==NULL) {szSub=_tcsstr(szUrl,szStr2);isStr1=FALSE;}
	if(szSub){
		TCHAR* szExtTmp=(szSub+(isStr1?szStr1Len:szStr2Len)-1);
		if(szExtTmp){
			TCHAR* szParam=_tcsstr(szExtTmp,L"?");
			if(!szParam) szParam=_tcsstr(szExtTmp,L"#");
			if(szParam) szParam[0]=L'\0';
		}
		if(_tcslen(szExtTmp)>0)_tcscpy_s(szExt,szExtTmp);

		szSub[0]=L'\0';
	}
	szSub=_tcsstr(szUrl,L"://");
	TCHAR szKeyName[STRLEN_DEFAULT]={0};
	_tcscpy_s(szKeyName,(szSub?szSub+3:szUrl));

	//uid
	hr=doc3->getElementById(bstrUID,&el);
	if (FAILED(hr) || !el) goto end;
	hr=el->QueryInterface(IID_IHTMLInputElement,(void**)&elx);
	if (FAILED(hr) || !elx) goto end;
	hr=elx->get_value(&bstrUIDValue);
	if (FAILED(hr)) goto end;

	//password
	hr=doc3->getElementById(bstrPwd,&el);
	if (FAILED(hr) || !el) goto end;
	hr=el->QueryInterface(IID_IHTMLInputElement,(void**)&elx);
	if (FAILED(hr) || !elx) goto end;
	hr=elx->get_value(&bstrPwdValue);
	if (FAILED(hr)) goto end;
	
	//systemName
	hr=doc3->getElementById(bstrSystemName,&el);
	if(hr==S_OK && el){
		hr=el->QueryInterface(IID_IHTMLInputElement,(void**)&elx);
		if(hr==S_OK && elx){
			hr=elx->get_value(&bstrSystemNameValue);
		}
	}

	TCHAR szEncrypted[STRLEN_DEFAULT]={0};
	size_t szLen=STRLEN_DEFAULT;
	Encrypt(bstrPwdValue,szEncrypted,&szLen);

	TCHAR szKey[STRLEN_1K]={0};
	_stprintf_s(szKey,L"Software\\Discoverx2\\%s",szKeyName);
	HUSKEY hKey;
	if(SHRegOpenUSKey(szKey,KEY_ALL_ACCESS,NULL,&hKey,FALSE)==ERROR_SUCCESS){
		if(bstrUIDValue && _tcslen(bstrUIDValue)>0 && bstrPwdValue && _tcslen(bstrPwdValue)>0){
			SHRegDeleteUSValue(hKey,L"uid",SHREGDEL_HKCU);
			SHRegDeleteUSValue(hKey,L"pwd",SHREGDEL_HKCU);
			if(SHRegWriteUSValue(hKey,L"uid",REG_SZ,bstrUIDValue,sizeof(wchar_t)*_tcslen(bstrUIDValue),SHREGSET_HKCU)==ERROR_SUCCESS &&
				SHRegWriteUSValue(hKey,L"pwd",REG_SZ,szEncrypted,sizeof(TCHAR)*_tcslen(szEncrypted),SHREGSET_HKCU)==ERROR_SUCCESS){
				*pVal=VARIANT_TRUE;
			}
		}
		if(_tcslen(szExt)>0){
			SHRegDeleteUSValue(hKey,L"ext",SHREGDEL_HKCU);
			if(SHRegWriteUSValue(hKey,L"ext",REG_SZ,szExt,sizeof(TCHAR)*_tcslen(szExt),SHREGSET_HKCU)!=ERROR_SUCCESS) *pVal=VARIANT_FALSE;
		}
		if(bstrSystemNameValue && _tcslen(bstrSystemNameValue)>0){
			TCHAR szTitle[STRLEN_NORMAL]={0};
			DWORD dwSize=STRLEN_NORMAL*sizeof(TCHAR);
			SHRegQueryUSValue(hKey,L"title",NULL,szTitle,&dwSize,FALSE,NULL,0);
			if(_tcsicmp(bstrSystemNameValue,szTitle)!=0){
				SHRegDeleteUSValue(hKey,L"title",SHREGDEL_HKCU);
				SHRegDeleteUSValue(hKey,NULL,SHREGDEL_HKCU);
				if(SHRegWriteUSValue(hKey,L"title",REG_SZ,bstrSystemNameValue,sizeof(wchar_t)*_tcslen(bstrSystemNameValue),SHREGSET_HKCU)!=ERROR_SUCCESS) *pVal=VARIANT_FALSE;
				SHRegWriteUSValue(hKey,NULL,REG_SZ,bstrSystemNameValue,sizeof(wchar_t)*_tcslen(bstrSystemNameValue),SHREGSET_HKCU);
			}
		}
		SHRegCloseUSKey(hKey);
	}
end:
	FREE_SYS_STR(bstrUID);
	FREE_SYS_STR(bstrPwd);
	FREE_SYS_STR(bstrSystemName);
	FREE_SYS_STR(bstrUIDValue);
	FREE_SYS_STR(bstrPwdValue);
	FREE_SYS_STR(bstrSystemNameValue);
	return S_OK;
}