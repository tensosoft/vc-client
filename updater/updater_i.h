

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu Apr 10 11:17:07 2014
 */
/* Compiler settings for updater.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __updater_i_h__
#define __updater_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IUpdateChecker_FWD_DEFINED__
#define __IUpdateChecker_FWD_DEFINED__
typedef interface IUpdateChecker IUpdateChecker;
#endif 	/* __IUpdateChecker_FWD_DEFINED__ */


#ifndef __UpdateChecker_FWD_DEFINED__
#define __UpdateChecker_FWD_DEFINED__

#ifdef __cplusplus
typedef class UpdateChecker UpdateChecker;
#else
typedef struct UpdateChecker UpdateChecker;
#endif /* __cplusplus */

#endif 	/* __UpdateChecker_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IUpdateChecker_INTERFACE_DEFINED__
#define __IUpdateChecker_INTERFACE_DEFINED__

/* interface IUpdateChecker */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IUpdateChecker;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7FF58807-27DD-44E3-AEFE-7ECE44B38965")
    IUpdateChecker : public IDispatch
    {
    public:
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE Check( 
            /* [in] */ BSTR szUrl,
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE LaunchUpdaterApp( 
            /* [in] */ BSTR szUrl) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE LoginSync( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [helpcontext][helpstring][id] */ HRESULT STDMETHODCALLTYPE CheckAndLaunchFirstrunInstall( 
            /* [in] */ BSTR szUrl,
            /* [defaultvalue][in] */ BSTR szExt = L".jsp",
            /* [defaultvalue][in] */ BSTR szTitle = L"") = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IUpdateCheckerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IUpdateChecker * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IUpdateChecker * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IUpdateChecker * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IUpdateChecker * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IUpdateChecker * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IUpdateChecker * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IUpdateChecker * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Check )( 
            IUpdateChecker * This,
            /* [in] */ BSTR szUrl,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *LaunchUpdaterApp )( 
            IUpdateChecker * This,
            /* [in] */ BSTR szUrl);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *LoginSync )( 
            IUpdateChecker * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [helpcontext][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CheckAndLaunchFirstrunInstall )( 
            IUpdateChecker * This,
            /* [in] */ BSTR szUrl,
            /* [defaultvalue][in] */ BSTR szExt,
            /* [defaultvalue][in] */ BSTR szTitle);
        
        END_INTERFACE
    } IUpdateCheckerVtbl;

    interface IUpdateChecker
    {
        CONST_VTBL struct IUpdateCheckerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IUpdateChecker_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IUpdateChecker_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IUpdateChecker_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IUpdateChecker_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IUpdateChecker_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IUpdateChecker_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IUpdateChecker_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IUpdateChecker_Check(This,szUrl,pVal)	\
    ( (This)->lpVtbl -> Check(This,szUrl,pVal) ) 

#define IUpdateChecker_LaunchUpdaterApp(This,szUrl)	\
    ( (This)->lpVtbl -> LaunchUpdaterApp(This,szUrl) ) 

#define IUpdateChecker_LoginSync(This,pVal)	\
    ( (This)->lpVtbl -> LoginSync(This,pVal) ) 

#define IUpdateChecker_CheckAndLaunchFirstrunInstall(This,szUrl,szExt,szTitle)	\
    ( (This)->lpVtbl -> CheckAndLaunchFirstrunInstall(This,szUrl,szExt,szTitle) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IUpdateChecker_INTERFACE_DEFINED__ */



#ifndef __updaterLib_LIBRARY_DEFINED__
#define __updaterLib_LIBRARY_DEFINED__

/* library updaterLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_updaterLib;

EXTERN_C const CLSID CLSID_UpdateChecker;

#ifdef __cplusplus

class DECLSPEC_UUID("15E57698-A5D4-42D1-BD8B-09E38328F8F1")
UpdateChecker;
#endif
#endif /* __updaterLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


